import { readFileSync } from "fs";

const instructions = readFileSync(process.argv[2], "utf8").split("\n");

const diskSize = 70000000;
const neededSize = 30000000;

type Folder = {
  path: string;
  files: Array<{ filename: string; size: number }>;
  sub: Array<Folder>;
  folderSize: number;
  subSize: number;
};

const fileSystem: Array<Folder> = [
  { path: "/", files: [], sub: [], folderSize: 0, subSize: 0 },
];
let path: Array<string> = [];
let ref = fileSystem[0];
const allPaths: Set<string> = new Set();
allPaths.add("");

// Parse instructions into a filesystem
for (let i = 0; i < instructions.length; i++) {
  const instruction = instructions[i];
  if (instruction.includes("$ cd")) {
    const targetPath = instruction.substring(5);
    if (targetPath === "..") {
      path.pop();

      // try to rebuild the path
      const copy = JSON.parse(JSON.stringify(path)) as Array<string>;
      copy.reverse();

      ref = fileSystem[0];
      while (copy.length > 0) {
        const targetPath = copy.pop();
        ref = ref.sub.find((sub) => sub.path === targetPath) as Folder;
      }
    } else if (targetPath === "/") {
      ref = fileSystem[0];
      path = [];
    } else {
      ref = ref.sub.find((sub) => sub.path === targetPath) as Folder;
      path.push(targetPath);
      allPaths.add(path.join("/"));
    }
  } else if (instruction.includes("$ ls")) {
    for (let j = i + 1; j < instructions.length; j++) {
      const ls = instructions[j];
      if (ls.charAt(0) === "$") {
        // End of the line
        j = Infinity;
      } else if (ls.startsWith("dir")) {
        const dir = ls.substring(4);
        ref.sub.push({
          path: dir,
          files: [],
          sub: [],
          folderSize: 0,
          subSize: 0,
        });
      } else {
        const [size, filename] = ls.split(" ");
        ref.files.push({ filename, size: Number(size) });
        ref.folderSize += Number(size);
      }
    }
  }
}

calc_subsize(fileSystem[0]);

function calc_subsize(ref: Folder): number {
  ref.subSize = ref.files
    .map(({ size }) => size)
    .reduce((sum, filesize) => sum + filesize, 0);
  ref.subSize += ref.sub
    .map((sub) => calc_subsize(sub))
    .reduce((sum, subSize) => sum + subSize, 0);
  return ref.subSize;
}

const sizes = Array.from(allPaths).map((fullPath) => {
  let ref = fileSystem[0];
  const path = fullPath.split("/");
  path.reverse();
  ref = fileSystem[0];
  if (fullPath !== "") {
    while (path.length > 0) {
      const targetPath = path.pop();
      ref = ref.sub.find((sub) => sub.path === targetPath) as Folder;
    }
  }

  return { path: ref.path, size: ref.subSize };
});

const available = diskSize - fileSystem[0].subSize;

sizes.sort((a, b) => a.size - b.size);

for (let i = 0; i < sizes.length; i++) {
  if (sizes[i].size + available > neededSize) {
    console.log(sizes[i]);
    i = Infinity;
  }
}