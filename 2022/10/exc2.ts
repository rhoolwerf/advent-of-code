import { readFileSync } from 'fs';

const commands = readFileSync(process.argv[2], 'utf8')
  .split('\n')
  .map((line) => (line === 'noop' ? { cmd: 'noop' } : { cmd: 'addx', add: Number(line.split(' ')[1]) }));

let cycleCount = 1;
let x = 1;
let output: Array<Array<string>> = [];

commands.forEach(({ cmd, add }) => {
  if (cmd === 'noop') {
    validate_cycle(cycleCount++);
  } else if (add !== undefined) {
    validate_cycle(cycleCount++);
    validate_cycle(cycleCount++);
    x += add;
  }
});

console.log(output.map((row) => row.join('')).join('\n'));

function validate_cycle(cycle: number): void {
  const pos = (cycle % 40) - 1;
  if (pos % 40 === 0) {
    output.push([]);
  }

  const touchesX = [x - 1, x, x + 1].includes(pos);
  const char = touchesX ? '#' : '.';
  // console.log('Cycle', cycle, 'at position', pos, 'using X=', x, 'drawing', char);

  output[output.length - 1].push(char);
}
