import { readFileSync } from 'fs';

const commands = readFileSync(process.argv[2], 'utf8')
  .split('\n')
  .map((line) => (line === 'noop' ? { cmd: 'noop' } : { cmd: 'addx', add: Number(line.split(' ')[1]) }));

let cycleCount = 1;
let x = 1;
let output = 0;

commands.forEach(({ cmd, add }) => {
  if (cmd === 'noop') {
    validate_cycle(cycleCount++);
  } else if (add !== undefined) {
    validate_cycle(cycleCount++);
    validate_cycle(cycleCount++);
    x += add;
  }
});

console.log('Output:', output);

function validate_cycle(cycle: number): void {
  if ([20, 60, 100, 140, 180, 220].includes(cycle)) {
    console.log('Cycle', cycle, 'has value X=', x, 'resulting in', x * cycle);
    output += x * cycle;
  }
}
