import { readFileSync } from 'fs';

const EMPTY = ' ';
const WALL = '#';
const PATH = '.';

const [mapPart, instructionPart] = readFileSync(process.argv[2], 'utf8').split('\n\n');
const map = mapPart.split('\n').map((row) => row.split(''));
const instructions = Array.from(instructionPart.matchAll(/\d+\D?/g))
  .map((r) => r[0])
  .map((i) => /(\d+)(\D?)/.exec(i) as RegExpExecArray)
  .map(([_, steps, nextDirection]) => [Number(steps), nextDirection]);

function showMap() {
  console.log(map.map((row) => row.join('')).join('\n'));
}

function getNextDirection(current: string, next: string): string {
  const mapping: { [key: string]: string } = {
    RR: 'D',
    RL: 'U',
    DR: 'L',
    DL: 'R',
    LR: 'U',
    LL: 'D',
    UR: 'R',
    UL: 'L',
  };
  return mapping[[current, next].join('')];
}

function getNextPosition(y: number, x: number, d: string): [number, number] {
  let [offsetY, offsetX] = getOffsets(d);
  let nextY = y + offsetY;
  let nextX = x + offsetX;

  // Check if there's anything in the desired direction
  if (map[nextY] && map[nextY][nextX] && map[nextY][nextX] !== EMPTY) {
    return [nextY, nextX];
  }

  // Nope, we need to reverse our movement and keep going until we've found a border
  offsetY *= -1;
  offsetX *= -1;

  nextY = y;
  nextX = x;
  do {
    nextY += offsetY;
    nextX += offsetX;
  } while (map[nextY + offsetY] && map[nextY + offsetY][nextX + offsetX] && map[nextY + offsetY][nextX + offsetX] !== EMPTY);
  return [nextY, nextX];
}

function getDirectionalCharacter(d: string): string {
  if (d === 'R') return '>';
  else if (d === 'D') return 'v';
  else if (d === 'L') return '<';
  else return '^';
}

function getOffsets(d: string): [number, number] {
  if (d === 'R') return [0, 1];
  else if (d === 'D') return [1, 0];
  else if (d === 'L') return [0, -1];
  else return [-1, 0];
}

// Determine starting position
let posY = 0;
let posX = 0;
let direction = 'R';

for (let x = 0; x < map[0].length; x++) {
  if (map[0][x] !== EMPTY) {
    posX = x;
    break;
  }
}

// Mark our initial position and direction
map[posY][posX] = getDirectionalCharacter(direction);

for (let i = 0; i < instructions.length; i++) {
  // For as many steps as we need, travel in our current direction
  const [steps, nextDirection] = instructions[i];
  // console.log('Instruction', { posY, posX, direction, i, steps, nextDirection });
  for (let i = 0; i < Number(steps); i++) {
    // Get the next desires position
    const [nextY, nextX] = getNextPosition(posY, posX, direction);

    // console.log('Trying to step from', [posY, posX], 'to', [nextY, nextX]);

    // Is the desired position safe?
    if (map[nextY][nextX] !== WALL) {
      // Step over there!
      posY = nextY;
      posX = nextX;

      // Mark where are now
      map[posY][posX] = getDirectionalCharacter(direction);
    // } else {
    //   console.log('Cannot move to', [nextY, nextX], "because it's not a valid path:", map[nextY][nextX]);
    }
  }

  // Turn into the new direction and mark that on the map
  // console.log({ direction, nextDirection });
  if (nextDirection) {
    direction = getNextDirection(direction, String(nextDirection));
    map[posY][posX] = getDirectionalCharacter(direction);
  } else {
    map[posY][posX] = 'F';
  }
  // console.log({ new: direction });
}

showMap();

console.log({ posY, posX, result: (posY + 1) * 1000 + (posX + 1) * 4 });
