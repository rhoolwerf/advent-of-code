import { readFileSync } from 'fs';

const direction = { D: 'DOWN', U: 'UP', L: 'LEFT', R: 'RIGHT' };

// Build list of individual steps based on input
const steps = readFileSync(process.argv[2], 'utf8')
  .split('\n')
  .map((line) => ({
    d: direction[line.split(' ')[0] as keyof typeof direction],
    s: Number(line.split(' ')[1]),
  }))
  .reduce((steps, { d, s }) => steps.concat(Array.from({ length: s }, () => d)), [] as Array<string>);

let minX = 0;
let maxX = 0;
let minY = 0;
let maxY = 0;

let snake: Array<{ y: number; x: number }> = Array.from({ length: 10 }, () => ({ y: 0, x: 0 }));

const tailPositions: Set<string> = new Set();

steps.forEach((step) => {
  // console.log('');

  // Move the Head
  snake[0].y += step === 'DOWN' ? 1 : step === 'UP' ? -1 : 0;
  snake[0].x += step === 'LEFT' ? -1 : step === 'RIGHT' ? 1 : 0;

  // For each chunk, determine the next step
  for (let i = 1; i < snake.length; i++) {
    const tail = snake[i];
    const head = snake[i - 1];
    const [dY, dX] = [head.y - tail.y, head.x - tail.x];
    const withinRange = -1 <= dY && dY <= 1 && -1 <= dX && dX <= 1;

    // Determine the Tail movement
    if (!withinRange) {
      tail.y += Math.sign(dY);
      tail.x += Math.sign(dX);
    }
  }

  // Register Last-tail position
  tailPositions.add([snake[snake.length - 1].y, snake[snake.length - 1].x].join('|'));

  // Determine bounding box
  minY = Math.min(minY, snake[0].y);
  maxY = Math.max(maxY, snake[0].y);
  minX = Math.min(minX, snake[0].x);
  maxX = Math.max(maxX, snake[0].x);
});

output_grid();
console.log('Tail positions:', tailPositions.size);

function output_grid() {
  const grid: Array<Array<string>> = [];

  const headX = snake[0].x;
  const headY = snake[0].y;

  const tailPosition = (findY: number, findX: number): number => snake.indexOf(snake.find(({ y, x }) => y === findY && x === findX) as { y: number; x: number });

  for (let y = minY; y <= maxY; y++) {
    grid.push([]);
    for (let x = minX; x <= maxX; x++) {
      let char = '.';
      if (x === headX && y == headY) {
        char = 'H';
      } else if (tailPosition(y, x) >= 0) {
        char = String(tailPosition(y, x));
      } else if (tailPositions.has([y, x].join('|'))) {
        char = '#';
      } else if (x === 0 && y === 0) {
        char = 's';
      }
      grid[grid.length - 1].push(char);
    }
  }

  console.log(grid.map((row) => row.join('')).join('\n'));
}
