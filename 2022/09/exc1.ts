import { readFileSync } from 'fs';

const direction = { D: 'DOWN', U: 'UP', L: 'LEFT', R: 'RIGHT' };

// Build list of individual steps based on input
const steps = readFileSync(process.argv[2], 'utf8')
  .split('\n')
  .map((line) => ({
    d: direction[line.split(' ')[0] as keyof typeof direction],
    s: Number(line.split(' ')[1]),
  }))
  .reduce((steps, { d, s }) => steps.concat(Array.from({ length: s }, () => d)), [] as Array<string>);

let minX = 0;
let maxX = 0;
let minY = 0;
let maxY = 0;

let headX = 0;
let headY = 0;
let tailX = 0;
let tailY = 0;

const tailPositions: Set<string> = new Set();

steps.forEach((step) => {
  console.log('');

  headY += step === 'DOWN' ? 1 : step === 'UP' ? -1 : 0;
  headX += step === 'LEFT' ? -1 : step === 'RIGHT' ? 1 : 0;

  // Are head-and-tail touching?
  const [dY, dX] = [Math.abs(tailY - headY), Math.abs(tailX - headX)];
  const withinRange = dY <= 1 && dX <= 1;
  //   console.log(`Head: [${headY},${headX}] / Tail: [${tailY},${tailX}] / Distance: [${dX},${dY}] / Within range: ${withinRange}`);

  // Determine the Tail movement
  if (!withinRange) {
    if (dX && !dY) {
      // Horizontal distance, just move the same direction
      tailX += step === 'LEFT' ? -1 : step === 'RIGHT' ? 1 : 0;
    } else if (!dX && dY) {
      // Vertical distance, just move the same direction
      tailY += step === 'UP' ? -1 : step === 'DOWN' ? 1 : 0;
    } else {
      // Diagonal distance, step into the 'old spot'
      if (step === 'UP') {
        tailX = headX;
        tailY = headY + 1;
      } else if (step === 'DOWN') {
        tailX = headX;
        tailY = headY - 1;
      } else if (step === 'LEFT') {
        tailX = headX + 1;
        tailY = headY;
      } else if (step === 'RIGHT') {
        tailX = headX - 1;
        tailY = headY;
      }
    }
  }

  // Register Tail position
  tailPositions.add([tailY, tailX].join('|'));

  // Determine bounding box
  minX = Math.min(minX, headX);
  maxX = Math.max(maxX, headX);
  minY = Math.min(minY, headY);
  maxY = Math.max(maxY, headY);
});

output_grid();
console.log('Tail positions:', tailPositions.size);

function output_grid() {
  const grid: Array<Array<string>> = [];

  for (let y = minY; y <= maxY; y++) {
    grid.push([]);
    for (let x = minX; x <= maxX; x++) {
      let char = '.';
      if (x === headX && y == headY) {
        char = 'H';
      } else if (x === tailX && y === tailY) {
        char = 'T';
      } else if (tailPositions.has([y, x].join('|'))) {
        char = '#';
      } else if (x === 0 && y === 0) {
        char = 's';
      }
      grid[grid.length - 1].push(char);
    }
  }

  console.log(grid.map((row) => row.join('')).join('\n'));
}
