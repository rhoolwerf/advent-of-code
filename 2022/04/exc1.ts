import { readFileSync } from "fs";

const overlaps = readFileSync(process.argv[2], "utf8")
  .split("\n")
  .map((line) =>
    line
      .split(",")
      .map((elf) => elf.split("-").map(Number))
      .map(([from, to]) => createSections(from, to))
  )
  .map(([elf1, elf2]) => checkFullOverlap(elf1, elf2))
  .filter((overlap) => overlap).length;

console.log(overlaps);

function createSections(from: number, to: number): Array<number> {
  return Array.from({ length: to - from + 1 }, (_e, i) => i + from);
}

function checkFullOverlap(s1: Array<number>, s2: Array<number>): boolean {
  const set1 = new Set(s1);
  const set2 = new Set(s2);

  if (!s1.some((section) => !set2.has(section))) {
    return true;
  }

  if (!s2.some((section) => !set1.has(section))) {
    return true;
  }

  return false;
}
