import { readFileSync } from "fs";

const overlaps = readFileSync(process.argv[2], "utf8")
  .split("\n")
  .map((line) =>
    line
      .split(",")
      .map((elf) => elf.split("-").map(Number))
      .map(([from, to]) => createSections(from, to))
  )
  .map(([elf1, elf2]) => getIntersection(new Set(elf1), new Set(elf2)))
  .map((overlap) => overlap.size > 0)
  .filter((hasOverlap) => hasOverlap).length;

console.log(overlaps);

function createSections(from: number, to: number): Array<number> {
  return Array.from({ length: to - from + 1 }, (_e, i) => i + from);
}

function getIntersection<T>(setA: Set<T>, setB: Set<T>): Set<T> {
  const intersection = new Set(
    [...setA].filter((element) => setB.has(element))
  );

  return intersection;
}
