import { readFileSync } from "fs";

let [p1, p2] = readFileSync(process.argv[2], "utf8")
  .split("\n\n")
  .map((block) => block.split("\n"));

// Construct our 'columns' data
const columns: Array<Array<string>> = [];
p1.slice(0, p1.length - 1)
  .map((row) => {
    const chunks = row.split("");
    const result: Array<string> = [];
    while (chunks.length > 0) {
      const [_leftBracket, c, _rightBracket] = chunks.splice(0, 3);
      result.push(c.trim());
      chunks.shift();
    }
    return result;
  })
  .forEach((row) => {
    row.forEach((value, index) => {
      if (value === "") {
        return;
      }
      if (!columns[index]) {
        columns[index] = [];
      }
      columns[index].unshift(value);
    });
  });

// Parse the instructions
p2.forEach((instruction) => {
  const [_, amount, from, to] = /move ([0-9]+) from ([0-9]+) to ([0-9]+)/
    .exec(instruction)
    ?.map(Number) as Array<number>;

  const toMove: Array<string> = [];
  for (let i = 0; i < amount; i++) {
    const val = columns[from - 1].pop();
    if (val) {
      toMove.push(val);
    }
  }

  while (toMove.length > 0) {
    columns[to - 1].push(String(toMove.pop()));
  }
});

console.log("\nResult:");
console.log(columns.map((column) => column.pop()).join(""));
