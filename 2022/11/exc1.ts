import { readFileSync } from "fs";

type Monkey = {
    id: number,
    items: Array<number>,
    operation: string,
    testDivBy: number,
    ifTrue: number;
    ifFalse: number;
    inspectCount: number;
}

const input = readFileSync(process.argv[2], 'utf8').split('\n\n').map((monkey) => monkey.split('\n'));

// Parse monkeys and their initial data
const monkeys: Array<Monkey> = [];
input.forEach((data) => {
    const id = Number(data[0].split(' ')[1].split(':')[0]);
    const items: Array<number> = data[1].split(': ')[1].split(', ').map(Number);
    const operation = data[2].split(' = ')[1];
    const testDivBy = Number(data[3].split('by ')[1]);
    const ifTrue = Number(data[4].split('monkey ')[1]);
    const ifFalse = Number(data[5].split('monkey ')[1]);

    monkeys.push({ id, items, operation, testDivBy, ifTrue, ifFalse, inspectCount: 0 });
});

for (let round = 1; round <= 20; round++) {
    monkeys.forEach((monkey) => {
        monkey.items.forEach((item) => {
            const op = (old: number): number => Number(eval(monkey.operation));
            item = op(item);
            item = Math.floor(item / 3);
            monkey.inspectCount++;
            const target = monkeys.find((m) => m.id === (item % monkey.testDivBy === 0 ? monkey.ifTrue : monkey.ifFalse)) as Monkey;
            target.items.push(item);
        });
        monkey.items = [];
    });
    // console.log('After round', round);
    // console.log(monkeys.map((m) => `Monkey ${m.id}: ${m.items.join(', ')}`).join('\n'));
}

console.log(monkeys.map((m) => `Monkey ${m.id} inspected ${m.inspectCount} items`).join('\n'));
monkeys.sort((a, b) => b.inspectCount - a.inspectCount);
console.log('Result:', monkeys[0].inspectCount * monkeys[1].inspectCount);