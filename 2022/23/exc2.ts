import { readFileSync } from 'fs';

type Position = [number, number];
type CheckDirection = { name: string; direction: Position; checks: Array<Position> };

const input = readFileSync(process.argv[2], 'utf8');

function key(y: number, x: number): string {
  return [y, x].join(',');
}

let map = new Map<string, boolean>();
input.split('\n').forEach((row, rowIndex) => {
  row.split('').forEach((char, colIndex) => {
    map.set(key(rowIndex, colIndex), char === '#');
  });
});

function showMap() {
  // Determine boundaries
  let minY = Infinity,
    maxY = 0,
    minX = Infinity,
    maxX = 0;

  Array.from(map.keys()).forEach((pos) => {
    const [y, x] = pos.split(',').map(Number);
    minY = Math.min(minY, y);
    maxY = Math.max(maxY, y);
    minX = Math.min(minX, x);
    maxX = Math.max(maxX, x);
  });

  // Build map
  const output: Array<Array<string>> = [];
  for (let y = minY - 2; y <= maxY + 2; y++) {
    output.push([]);
    for (let x = minX - 2; x <= maxX + 2; x++) {
      output[output.length - 1].push(map.get(key(y, x)) ? '#' : '.');
    }
  }

  // Strip of any edges that has no Elf in it

  // North
  while (output[0].map((c) => c === '#').filter((b) => b).length === 0) {
    output.shift();
  }

  // West
  while (
    output
      .map((row) => row[0])
      .map((c) => c === '#')
      .filter((b) => b).length === 0
  ) {
    output.forEach((row) => row.shift());
  }

  // East
  while (
    output
      .map((row) => row[row.length - 1])
      .map((c) => c === '#')
      .filter((b) => b).length === 0
  ) {
    output.forEach((row) => row.pop());
  }

  // South
  while (output[output.length - 1].map((c) => c === '#').filter((b) => b).length === 0) {
    output.pop();
  }

  // Output what we have left
  console.log(output.map((row) => row.join('')).join('\n'));
  console.log(
    output
      .map((row) => row.join(''))
      .join('')
      .split('')
      .map((c) => c === '.')
      .filter((b) => b).length
  );
}

const directionConfig: Array<CheckDirection> = [
  {
    name: 'north',
    direction: [-1, 0],
    checks: [
      [-1, -1],
      [-1, 0],
      [-1, 1],
    ],
  },
  {
    name: 'south',
    direction: [1, 0],
    checks: [
      [1, -1],
      [1, 0],
      [1, 1],
    ],
  },
  {
    name: 'west',
    direction: [0, -1],
    checks: [
      [-1, -1],
      [0, -1],
      [1, -1],
    ],
  },
  {
    name: 'east',
    direction: [0, 1],
    checks: [
      [-1, 1],
      [0, 1],
      [1, 1],
    ],
  },
];

let checkDirections = 0;

for (let round = 1; true; round++) {
  // Propose new directions
  const proposals = new Map<string, Array<string>>();
  Array.from(map.keys())
    .filter((pos) => map.get(pos))
    .forEach((pos) => {
      const [elfY, elfX] = pos.split(',').map(Number);
      // console.log('Check for Elf at', [elfY, elfX]);

      // First of all, check if there's any elf in the vicinity
      let foundElf = false;
      for (let y = -1; y <= 1 && !foundElf; y++) {
        for (let x = -1; x <= 1 && !foundElf; x++) {
          if ((y !== 0 || x !== 0) && map.get(key(elfY + y, elfX + x))) {
            foundElf = true;
          }
        }
      }
      // console.log('- Found elf:', foundElf);
      if (!foundElf) {
        return;
      }

      // Check in each direction if it's safe to move
      for (let i = 0; i < 4; i++) {
        const directions = (i + checkDirections) % 4;
        const { name, direction, checks } = directionConfig[directions];
        let foundElfInDirection = false;
        //   console.log('- Checking:', name);
        for (const [offsetY, offsetX] of checks) {
          if (map.get(key(elfY + offsetY, elfX + offsetX))) {
            foundElfInDirection = true;
          }
        }
        //   console.log('- -', 'Other elf found:', foundElfInDirection);
        if (!foundElfInDirection) {
          const newY = elfY + direction[0];
          const newX = elfX + direction[1];
          // console.log('- - -', 'Proposed position:', [newY, newX]);
          if (!proposals.has(key(newY, newX))) {
            proposals.set(key(newY, newX), []);
          }
          proposals.get(key(newY, newX))?.push(key(elfY, elfX));
          break;
        }
      }
    });

  if (proposals.size === 0) {
    console.log('No more proposals in round', round, ', exiting...');
    break;
  }
  proposals.forEach((sources, target) => {
    if (sources.length === 1) {
      map.set(sources[0], false);
      map.set(target, true);
    }
  });

  //   console.log('\nAfter round', round);
  //   showMap();
  checkDirections++;
}
