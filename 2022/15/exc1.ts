import { readFileSync } from 'fs';

const targetY = process.argv[2] === 't1' ? 10 : process.argv[2] === 'input' ? 2000000 : Number(process.argv[3]);
if (!targetY || isNaN(targetY)) {
  console.log('Geef een targetY op (10 voor t1, 2000000 voor input)');
  process.exit(-1);
}

const occupied: Set<string> = new Set();
const lines = readFileSync(process.argv[2], 'utf8')
  .split('\n')
  .map((row) => /Sensor at x=(-{0,1}[0-9]+), y=(-{0,1}[0-9]+): closest beacon is at x=(-{0,1}[0-9]+), y=(-{0,1}[0-9]+)/.exec(row) as RegExpExecArray)
  .map(([_, sx, sy, bx, by]) => [Number(sx), Number(sy), Number(bx), Number(by)])
  .map(([sx, sy, bx, by]) => ({ sx, sy, bx, by, d: manhattan(sy, sx, by, bx) }));
// console.log(lines);

// Boundaries for drawing
let minX = Infinity;
let maxX = Infinity * -1;
let minY = Infinity;
let maxY = Infinity * -1;

type Range = { from: number; to: number };

// Update boundaries and determine all ranges
const ranges: Map<number, Array<Range>> = new Map();

console.time('Parse lines into ranges');
console.log('Parse lines into ranges..');
lines.forEach(({ sx, sy, bx, by, d }) => {
  occupied.add(key(by, bx));
  occupied.add(key(sy, sx));

  minX = Math.min(sx - d, minX);
  maxX = Math.max(sx + d, maxX);
  minY = Math.min(sy - d, minY);
  maxY = Math.max(sy + d, maxY);

  for (let y = Math.max(sy - d, 0); y <= Math.min(sy + d, 4000000); y++) {
    const deltaY = Math.abs(y - sy);
    const available = d - deltaY;
    const x1 = sx - available;
    const x2 = sx + available;
    const newRange = { from: Math.min(x1, x2), to: Math.max(x1, x2) };
    if (!ranges.has(y)) {
      // No existing ranges yet, just add it
      ranges.set(y, [newRange]);
    } else {
      // For each existing range, to if there is overlap
      const lineRanges = ranges.get(y) as Array<Range>;
      // lineRanges.push({ from, to });
      let hasOverlap = false;
      for (let i = 0; !hasOverlap && i < lineRanges.length; i++) {
        const range = lineRanges[i];

        // A              C  B      D
        // Line A->B -> newRange
        // Line C->D -> range

        // Either comparison means no overlap
        // D < A
        // B < C
        if (range.to < newRange.from && newRange.to < range.from) {
          // no overlap
        } else {
          hasOverlap = true;
          range.from = Math.min(newRange.from, range.from);
          range.to = Math.max(newRange.to, range.to);
        }
      }

      if (!hasOverlap) {
        lineRanges.push(newRange);
        console.log('Line', y, 'has more than one range:', lineRanges);
      }
    }
  }
});
console.timeEnd('Parse lines into ranges');

const targetRanges = ranges.get(targetY) as Array<Range>;
let cannotBe = 0;
for (let x = minX; x <= maxX; x++) {
  targetRanges.forEach(({ from, to }) => {
    if (from <= x && x <= to && !occupied.has(key(targetY, x))) {
      cannotBe++;
    }
  });
}
console.log({ cannotBe });

function key(y: number, x: number): string {
  return [y, x].join('|');
}

function manhattan(y1: number, x1: number, y2: number, x2: number): number {
  // Borrowed from https://www.geeksforgeeks.org/maximum-manhattan-distance-between-a-distinct-pair-from-n-coordinates/
  // Math.abs(A[i][0] - A[j][0]) + Math.abs(A[i][1] - A[j][1])
  return Math.abs(x1 - x2) + Math.abs(y1 - y2);
}
