import { readFileSync } from 'fs';

const occupied: Set<string> = new Set();
const lines = readFileSync(process.argv[2], 'utf8')
  .split('\n')
  .map((row) => /Sensor at x=(-{0,1}[0-9]+), y=(-{0,1}[0-9]+): closest beacon is at x=(-{0,1}[0-9]+), y=(-{0,1}[0-9]+)/.exec(row) as RegExpExecArray)
  .map(([_, sx, sy, bx, by]) => [Number(sx), Number(sy), Number(bx), Number(by)])
  .map(([sx, sy, bx, by]) => ({ sx, sy, bx, by, d: manhattan(sy, sx, by, bx) }));

// Boundaries for drawing
let minX = Infinity;
let maxX = Infinity * -1;
let minY = Infinity;
let maxY = Infinity * -1;

type Range = { from: number; to: number };

// Update boundaries and determine all ranges
const ranges: Map<number, Array<Range>> = new Map();

console.time('Parse lines into ranges');
console.log('Parse lines into ranges..');
lines.forEach(({ sx, sy, bx, by, d }) => {
  occupied.add(key(by, bx));
  occupied.add(key(sy, sx));

  minX = Math.min(sx - d, minX);
  maxX = Math.max(sx + d, maxX);
  minY = Math.min(sy - d, minY);
  maxY = Math.max(sy + d, maxY);

  for (let y = Math.max(sy - d, 0); y <= Math.min(sy + d, 4000000); y++) {
    // if (y === 11) {
    const deltaY = Math.abs(y - sy);
    const available = d - deltaY;
    const x1 = sx - available;
    const x2 = sx + available;
    const newRange = { from: Math.min(x1, x2), to: Math.max(x1, x2) };
    // console.log('');
    // console.log(newRange);
    if (!ranges.has(y)) {
      // No existing ranges yet, just add it
      ranges.set(y, [newRange]);
      // console.log('First range, just adding');
    } else {
      // For each existing range, to if there is overlap
      const lineRanges = ranges.get(y) as Array<Range>;

      // Keep finding overlap until we've found nothing
      let hasOverlap = true;
      while (hasOverlap) {
        hasOverlap = false;
        for (let i = 0; !hasOverlap && i < lineRanges.length; i++) {
          const existingRange = lineRanges[i];
          // console.log('Comparing with', existingRange);

          if (overlap(newRange.from, newRange.to, existingRange.from, existingRange.to)) {
            hasOverlap = true;
            newRange.from = Math.min(newRange.from, existingRange.from);
            newRange.to = Math.max(newRange.to, existingRange.to);
            // console.log('- Has overlap, merged range becomes:', newRange);

            // Existing range needs to be removed
            lineRanges.splice(i, i + 1);
          } else {
            // console.log('- No overlap');
          }
        }
      }

      if (!hasOverlap) {
        lineRanges.push(newRange);
        // console.log('No overlap with existing range, adding it');
      }
    }
    // console.log('Result:', ranges.get(y));
    // }
  }
});
console.timeEnd('Parse lines into ranges');

for (let y = 0; y <= 4000000; y++) {
  if (ranges.has(y) && (ranges.get(y) as Array<Range>).length > 1) {
    console.log('More than one range:', y, ranges.get(y));
  }
}

function key(y: number, x: number): string {
  return [y, x].join('|');
}

function overlap(f1: number, t1: number, f2: number, t2: number): boolean {
  if (f1 <= f2 && f2 <= t1) return true; // f2 within f1-t1
  if (f1 <= t2 && t2 <= t2) return true; // t2 within f1-t1
  if (f2 <= f1 && f1 <= t2) return true; // f1 within f2-t2
  if (f2 <= t1 && t1 <= t2) return true; // t1 within f2-t2
  if (f1 <= f2 && t1 >= t2) return true; // 1 envelopes 2
  if (f2 <= f1 && t2 >= t1) return true; // 2 envelopes 1
  return false;
}

function manhattan(y1: number, x1: number, y2: number, x2: number): number {
  // Borrowed from https://www.geeksforgeeks.org/maximum-manhattan-distance-between-a-distinct-pair-from-n-coordinates/
  // Math.abs(A[i][0] - A[j][0]) + Math.abs(A[i][1] - A[j][1])
  return Math.abs(x1 - x2) + Math.abs(y1 - y2);
}
