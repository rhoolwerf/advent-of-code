import { readFileSync } from "fs";

function find_marker_position(input: string): number {
  const stream = input.split("");
  for (let i = 4; i < stream.length; i++) {
    const window = stream.slice(i - 4, i);
    if (new Set(window).size === 4) {
      return i;
    }
  }

  return -1;
}

[
  { m: "mjqjpqmgbljsphdztnvjfqwrcgsmlb", p: 7 },
  { m: "bvwbjplbgvbhsrlpgdmjqwftvncz", p: 5 },
  { m: "nppdvjthqldpwncqszvftbrmjlhg", p: 6 },
  { m: "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", p: 10 },
  { m: "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", p: 11 },
].forEach(({ m, p }) =>
  console.assert(find_marker_position(m) === p, `'${m}' !== ${p}`)
);

console.log("Result", find_marker_position(readFileSync("input", "utf8")));
