import { readFileSync } from "fs";

function find_marker_position(input: string): number {
  const stream = input.split("");
  for (let i = 14; i < stream.length; i++) {
    const window = stream.slice(i - 14, i);
    if (new Set(window).size === 14) {
      return i;
    }
  }

  return -1;
}

[
  { m: "mjqjpqmgbljsphdztnvjfqwrcgsmlb", p: 19 },
  { m: "bvwbjplbgvbhsrlpgdmjqwftvncz", p: 23 },
  { m: "nppdvjthqldpwncqszvftbrmjlhg", p: 23 },
  { m: "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", p: 29 },
  { m: "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", p: 26 },
].forEach(({ m, p }) =>
  console.assert(find_marker_position(m) === p, `'${m}' !== ${p}`)
);

console.log("Result", find_marker_position(readFileSync("input", "utf8")));
