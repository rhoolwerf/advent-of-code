import { readFileSync } from "fs";

const x = readFileSync(process.argv[2], "utf8").split("\n");
const y = x
  .map((game) => {
    let [elf, player] = game.split(" ");
    player = { X: "A", Y: "B", Z: "C" }[player] as string;

    const rules = {
      A: { A: 3, B: 6, C: 0 },
      B: { A: 0, B: 3, C: 6 },
      C: { A: 6, B: 0, C: 3 },
    } as { [key: string]: { [key: string]: number } };

    let score = rules[elf][player];

    score += Number({ A: 1, B: 2, C: 3 }[player]);
    return score;
  })
  .reduce((sum, score) => sum + score, 0);
console.log(y);

//A=X=rock
//B=Y=paper
//C=Z=scissor
