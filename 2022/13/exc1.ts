import { readFileSync } from 'fs';

const LT = -1;
const EQ = 0;
const GT = 1;
type CompareResult = -1 | 0 | 1;

function isArrayOfNumbers(input: any): input is Array<number> {
  return Array.isArray(input);
}

function isNumber(input: any): input is number {
  return !isNaN(Number(input)) && !Array.isArray(input);
}

const pairs = readFileSync(process.argv[2], 'utf8')
  .split('\n\n')
  .map((pair) => pair.split('\n').map((row) => JSON.parse(row)));

// pairs.forEach((pair, index) => {
//   console.log(index + 1, pair[0], pair[1], compare(pair[0], pair[1]) === LT);
// });

console.log(
  pairs
    .map((p, i) => ({ index: i + 1, result: compare(p[0], p[1]) === LT }))
    .filter(({ result }) => result)
    .reduce((sum, { index }) => sum + index, 0)
);

function compare(left: number | Array<number>, right: number | Array<number>): CompareResult {
  // console.log('Comparing', left, 'with', right);
  if (isNumber(left) && isNumber(right)) {
    return left < right ? LT : left > right ? GT : EQ;
  } else if (Array.isArray(left) && Array.isArray(right)) {
    let i = 0;
    for (; i < left.length; i++) {
      // console.log('', 'Item #', i, 'is', left[i], 'and', right[i]);
      if (right[i] === undefined) {
        return GT;
      } else {
        const c = compare(left[i], right[i]);
        if (c === LT) { return LT; }
        else if (c === GT) { return GT; }
      }
    }
    if (right.length > left.length) {
      return LT;
    }
  } else {
    // One side is an array, the other isn't. Let's wrap and retry
    return compare(Array.isArray(left) ? left : [left], Array.isArray(right) ? right : [right]);
  }

  // If we have no answer so far, just return that
  return EQ;
}