import { readFileSync } from 'fs';

const LT = -1;
const EQ = 0;
const GT = 1;
type CompareResult = -1 | 0 | 1;

function isArrayOfNumbers(input: any): input is Array<number> {
  return Array.isArray(input);
}

function isNumber(input: any): input is number {
  return !isNaN(Number(input)) && !Array.isArray(input);
}

const packets = readFileSync(process.argv[2], 'utf8')
  .split('\n\n').join('\n').split('\n').map((row) => JSON.parse(row));

const p1 = [[2]];
const p2 = [[6]];
packets.push(p1);
packets.push(p2);

packets.sort(compare);

console.log(packets.indexOf(p1) + 1, packets.indexOf(p2) + 1, (packets.indexOf(p1) + 1) * (packets.indexOf(p2) + 1));

function compare(left: number | Array<number>, right: number | Array<number>): CompareResult {
  if (isNumber(left) && isNumber(right)) {
    return left < right ? LT : left > right ? GT : EQ;
  } else if (Array.isArray(left) && Array.isArray(right)) {
    let i = 0;
    for (; i < left.length; i++) {
      if (right[i] === undefined) {
        return GT;
      } else {
        const c = compare(left[i], right[i]);
        if (c === LT) { return LT; }
        else if (c === GT) { return GT; }
      }
    }
    if (right.length > left.length) {
      return LT;
    }
  } else {
    // One side is an array, the other isn't. Let's wrap and retry
    return compare(Array.isArray(left) ? left : [left], Array.isArray(right) ? right : [right]);
  }

  // If we have no answer so far, just return that
  return EQ;
}