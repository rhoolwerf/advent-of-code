import { readFileSync } from 'fs';
import { EOL } from 'os';

type Cube = { x: number; y: number; z: number; exposed: number };

const cubes = readFileSync(process.argv[2], 'utf8')
  .split(EOL)
  .map((line) => line.split(',').map(Number))
  .map(([x, y, z]) => ({ x, y, z, exposed: 6 } as Cube));

const exists = (x1: number, y1: number, z1: number) => cubes.find(({ x, y, z }) => x === x1 && y === y1 && z === z1);

for (const cube of cubes) {
  const { x, y, z } = cube;

  if (exists(x - 1, y, z)) cube.exposed--;
  if (exists(x + 1, y, z)) cube.exposed--;
  if (exists(x, y - 1, z)) cube.exposed--;
  if (exists(x, y + 1, z)) cube.exposed--;
  if (exists(x, y, z - 1)) cube.exposed--;
  if (exists(x, y, z + 1)) cube.exposed--;
}

console.log(cubes.reduce((sum, { exposed }) => sum + exposed, 0));
