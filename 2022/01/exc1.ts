import { readFileSync } from "fs";

const elves = readFileSync(process.argv[2], "utf-8")
  .split("\n\n")
  .map((elf) =>
    elf
      .split("\n")
      .map(Number)
      .reduce((sum, calories) => sum + calories, 0)
  );

elves.sort((a, b) => b - a);

console.log(elves[0]);
