import { readFileSync } from 'fs';

type Valve = { id: string; flowRate: number; leadTo: Array<string> };
type Action = { valve: string; action: 'open' | 'walkto'; minute: number };

const valves: Array<Valve> = readFileSync(process.argv[2], 'utf8')
  .split('\n')
  .map((row) => {
    const [_, id, flowRateString, leadToString] = /Valve ([A-Z]+) has flow rate=(\d+)+; tunnels{0,1} leads{0,1} to valves{0,1} (.*)/.exec(row) as RegExpExecArray;
    return { id, flowRate: Number(flowRateString), leadTo: leadToString.split(', ') };
  });
// console.dir({ valves }, { depth: null });

processValve('AA', [], 30);

function processValve(id: string, actions: Array<Action>, remaining: number): void {
  //   console.log('Considering valve', id);
  const valve = valves.find((valve) => valve.id === id) as Valve;

  let minutes = remaining;

  // Check if we can still continue
  if (!checkIfRemaining(minutes, actions)) {
    return;
  }

  // If valve has a flowRate and hasn't already been opened
  if (valve.flowRate > 0 && !actions.find((action) => action.valve === valve.id && action.action === 'open')) {
    // console.log('Opening valve', id);
    actions.push({ valve: valve.id, action: 'open', minute: minutes-- });
  }

  // Now that we've taken an action, check if we can still continue
  if (!checkIfRemaining(minutes - 1, actions)) {
    return;
  }

  // For each path we can walk to, try to walk it
  valve.leadTo.forEach((target) => {
    const newActions: Array<Action> = [...actions].concat({ valve: valve.id, action: 'walkto', minute: minutes - 1 });
    processValve(target, newActions, minutes - 2);
  });
}

function checkIfRemaining(minutes: number, actions: Array<Action>): boolean {
  if (minutes <= 0) {
    const open = actions
      .filter(({ action }) => action === 'open')
      .map(({ valve }) => valve)
      .join(', ');
    // if ('BB, CC, DD, EE, HH, JJ' === open) {
    console.log('End of the line:', open);
    // }
    return false;
  }
  return true;
}
