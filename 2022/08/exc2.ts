import { readFileSync } from 'fs';

const rows = readFileSync(process.argv[2], 'utf8')
  .split('\n')
  .map((row) => row.split('').map(Number));

function calc_score(y: number, x: number): number {
  const check = function (y1: number, x1: number, y2: number, x2: number): { adjust: number; continue: boolean } {
    if (rows[y1][x1] < rows[y2][x2]) {
      return { adjust: 1, continue: true };
    } else if (rows[y1][x1] >= rows[y2][x2]) {
      return { adjust: 1, continue: false };
    }
    return { adjust: 0, continue: false };
  };

  let scoreUp = 0;
  let scoreLeft = 0;
  let scoreRight = 0;
  let scoreDown = 0;

  // Go up
  for (let y1 = y - 1; y1 >= 0; y1--) {
    const { adjust, continue: cont } = check(y1, x, y, x);
    if (adjust) {
      scoreUp++;
    }
    if (!cont) {
      break;
    }
  }

  // Go down
  for (let y1 = y + 1; y1 <= rows.length - 1; y1++) {
    const { adjust, continue: cont } = check(y1, x, y, x);
    if (adjust) {
      scoreDown++;
    }
    if (!cont) {
      break;
    }
  }

  // Go left
  for (let x1 = x - 1; x >= 0; x1--) {
    const { adjust, continue: cont } = check(y, x1, y, x);
    if (adjust) {
      scoreLeft++;
    }
    if (!cont) {
      break;
    }
  }

  // Go right
  for (let x1 = x + 1; x <= rows[y].length - 1; x1++) {
    const { adjust, continue: cont } = check(y, x1, y, x);
    if (adjust) {
      scoreRight++;
    }
    if (!cont) {
      break;
    }
  }

  return scoreUp * scoreLeft * scoreRight * scoreDown;
}

let maxScore = 0;
for (let y = 0; y < rows.length; y++) {
  for (let x = 0; x < rows[y].length; x++) {
    maxScore = Math.max(calc_score(y, x), maxScore);
  }
}
console.log('Max score:', maxScore);

// console.log('1,2:', calc_score(1, 2));
// console.log('3,2:', calc_score(3, 2));
