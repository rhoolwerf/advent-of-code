import { readFileSync } from 'fs';
import { union } from 'set-operations';

const rows = readFileSync(process.argv[2], 'utf8')
  .split('\n')
  .map((row) => row.split('').map(Number));

const visibleFromLeft: Set<string> = new Set();
const visibleFromRight: Set<string> = new Set();
const visibleFromTop: Set<string> = new Set();
const visibleFromBottom: Set<string> = new Set();

const columns: Array<Array<number>> = [];
for (let y = 0; y < rows.length; y++) {
  const row = rows[y];
  for (let x = 0; x < row.length; x++) {
    // Build the transposed array
    if (!columns[x]) {
      columns.push([]);
    }
    columns[x].push(row[x]);

    // As we're talking about an entire row, check from left
    if (row.slice(0, x).filter((cell) => cell >= row[x]).length === 0) {
      visibleFromLeft.add(key(y, x));
    }

    // Also check from right
    if (row.slice(x + 1).filter((cell) => cell >= row[x]).length === 0) {
      visibleFromRight.add(key(y, x));
    }
  }
}

// Now parse columns for top/bottom views
for (let x = 0; x < columns.length; x++) {
  for (let y = 0; y < columns[x].length; y++) {
    if (columns[x].slice(0, y).filter((cell) => cell >= columns[x][y]).length === 0) {
      visibleFromTop.add(key(y, x));
    }

    if (columns[x].slice(y + 1).filter((cell) => cell >= columns[x][y]).length === 0) {
      visibleFromBottom.add(key(y, x));
    }
  }
}

function key(y: number, x: number): string {
  return [y, x].join('-');
}

const visibleFromAllSidesArray = union(Array.from(visibleFromBottom), union(Array.from(visibleFromLeft), union(Array.from(visibleFromRight), Array.from(visibleFromTop)))) as Array<string>;
const visibleFromAllSidesSet = new Set(visibleFromAllSidesArray);

function visualizeVisibleTrees(visibleTrees: Set<string>, text?: string): void {
  if (text) {
    console.log(text);
  }
  console.log(rows.map((row, y) => row.map((column, x) => (visibleTrees.has(key(y, x)) ? `\x1b[33m${column}\x1b[0m` : column)).join('')).join('\n'));
}

visualizeVisibleTrees(visibleFromLeft, 'From left:');
visualizeVisibleTrees(visibleFromRight, 'From right:');
visualizeVisibleTrees(visibleFromTop, 'From top:');
visualizeVisibleTrees(visibleFromBottom, 'From bottom:');
visualizeVisibleTrees(visibleFromAllSidesSet, 'Combined');

console.log('Visible trees:', visibleFromAllSidesSet.size);
