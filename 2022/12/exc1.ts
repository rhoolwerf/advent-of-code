import { readFileSync } from 'fs';
import createGraph from 'ngraph.graph';
import { aStar } from 'ngraph.path';

type NodeData = { y: number; x: number };

function key(y: number, x: number): string {
  return [y, x].join(',');
}

let startX: number = 0;
let startY: number = 0;
let endX: number = 0;
let endY: number = 0;

// Parse input into grid
const grid = readFileSync(process.argv[2], 'utf8')
  .split('\n')
  .map((row) => row.split(''));

// Parse grid into graph (nodes and links)
const graph = createGraph<NodeData, undefined>();

for (let y = 0; y < grid.length; y++) {
  for (let x = 0; x < grid[y].length; x++) {
    if (grid[y][x] === 'S') {
      startY = y;
      startX = x;
    } else if (grid[y][x] === 'E') {
      endY = y;
      endX = x;
    }

    // Register node
    graph.addNode(key(y, x), { y, x });

    // Register the four possible directions as links
    if (grid[y + 1]) {
      graph.addLink(key(y, x), key(y + 1, x));
    }
    if (grid[y][x + 1]) {
      graph.addLink(key(y, x), key(y, x + 1));
    }
  }
}

function charToNum(char: string): number {
  if (char === 'S') {
    return 'a'.charCodeAt(0) - 1;
  } else if (char === 'E') {
    return 'z'.charCodeAt(0) + 1;
  } else {
    return char.charCodeAt(0);
  }
}

// Find a path from S to E
const pathFinder = aStar<NodeData, undefined>(graph, {
  distance: (fromNode, toNode) => {
    const from = charToNum(grid[fromNode.data.y][fromNode.data.x]);
    const to = charToNum(grid[toNode.data.y][toNode.data.x]);

    const diff = to - from;
    const result = diff <= 1 ? 1 : Infinity;
    return result;
  },
});
const path = pathFinder.find(key(endY, endX), key(startY, startX));

console.log('Result:', path.length - 1);
