import { readFileSync } from 'fs';

// Map input to a bunch of individual lines
console.time('Read input');
const lines = readFileSync(process.argv[2], 'utf8')
  .split('\n')
  .map((row) => {
    return row
      .split(' -> ')
      .map((coord) => coord.split(',').map(Number))
      .map(([x, y]) => [y, x]);
  })
  .map((row) =>
    row
      .map((_, index, coords) => {
        return [coords[index], coords[index + 1]];
      })
      .filter(([_, to]) => to !== undefined)
  )
  .flat();
  console.timeEnd('Read input');
  // console.dir(lines, { depth: null });

// Determine boundaries of our grid
console.time('Determine boundaries');
let minY = Infinity;
let maxY = Infinity * -1;
let minX = Infinity;
let maxX = Infinity * -1;
lines.forEach((coords) => {
  coords.forEach((coord) => {
    let [y, x] = coord;
    minY = Math.min(y, minY);
    maxY = Math.max(y, maxY);
    minX = Math.min(x, minX);
    maxX = Math.max(x, maxX);
  });
});

// We need to draw an 'infinite' line at maxY+2
maxY += 3;

// Padding
minY -= 2;
minX = 0; // Just start at 0 to be safe
maxX = 1000; // We need to support full width of the tower

lines.push([
  [maxY - 1, minX],
  [maxY - 1, maxX],
]);
console.timeEnd('Determine boundaries');

// Build the grid
const grid: Array<Array<string>> = [];
for (let y = 0; y <= maxY; y++) {
  grid.push([]);
  for (let x = 0; x <= maxX; x++) {
    grid[y].push('.');
  }
}

// Map each line onto the grid
console.time('Build grid');
lines.forEach(([from, to]) => {
  const fromY = Math.min(from[0], to[0]);
  const toY = Math.max(from[0], to[0]);
  const fromX = Math.min(from[1], to[1]);
  const toX = Math.max(from[1], to[1]);

  for (let y = fromY; y <= toY; y++) {
    for (let x = fromX; x <= toX; x++) {
      grid[y][x] = '#';
    }
  }
});
console.timeEnd('Build grid');

// Start dropping sand!
let sand = 1;
console.time('Dropping sand');
for (; true; sand++) {
  let sandY = 0;
  let sandX = 500;

  // Keep going untill we're stuck
  while (true) {
    // Can it drop straight down?
    for (let y = sandY; y <= maxY; y++) {
      if (grid[y][sandX] !== '.') {
        // We've hit a block
        sandY = y - 1;
        break;
      }
    }

    // Are we at the bottom border?
    if (sandY === maxY) {
      // We've reached the infinite bottom line, next sand-thingie!
      break;
    }

    // Now that we're straight down, can we go to diagonal?
    if (grid[sandY + 1][sandX - 1] === '.') {
      // We can go left
      sandX--;
      sandY++;
    } else if (grid[sandY + 1][sandX + 1] === '.') {
      // We can go right
      sandX++;
      sandY++;
    } else {
      // No where to go, we're done for this one
      grid[sandY][sandX] = 'o';
      break;
    }
  }

  if (sandY === 0 && sandX === 500) {
    // We've reached the ultimate tower!
    break;
  }
}
console.timeEnd('Dropping sand');

// showGrid();
console.log("We've reached stable after", sand, 'sands!');

function showGrid() {
  // Get boundaries
  let minY = Infinity;
  let maxY = 0;
  let minX = Infinity;
  let maxX = 0;

  for (let y = 0; y <= grid.length - 3; y++) {
    for (let x = 0; x <= grid[y].length - 1; x++) {
      if (grid[y][x] !== '.') {
        minY = Math.min(minY, y);
        maxY = Math.max(maxY, y);

        minX = Math.min(minX, x);
        maxX = Math.max(maxX, x);
      }
    }
  }

  // Only show within boundaries of the grid (plus padding)
  const sub: Array<Array<string>> = [];
  for (let y = minY; y <= maxY; y++) {
    sub.push([]);
    for (let x = minX; x <= maxX; x++) {
      sub[sub.length - 1].push(grid[y][x]);
    }
  }

  console.log(sub.map((row) => row.join('')).join('\n'), '\n');
}
