import { readFileSync } from "fs";

const rucksacks = readFileSync(process.argv[2], "utf8")
  .split("\n")
  .map((rucksack) => rucksack.split(""))
  .map((rucksack) => [
    rucksack.slice(0, rucksack.length / 2),
    rucksack.slice(rucksack.length / 2),
  ])
  .map(([left, right]) => [new Set(left), new Set(right)])
  .map(([left, right]) => getIntersection(left, right))
  .map((intersect) => Array.from(intersect)[0])
  .map((char) => getValue(char))
  .reduce((sum, value) => sum + value, 0);

console.log(rucksacks);

// https://bobbyhadz.com/blog/javascript-get-intersection-of-two-sets
function getIntersection<T>(setA: Set<T>, setB: Set<T>): Set<T> {
  const intersection = new Set(
    [...setA].filter((element) => setB.has(element))
  );

  return intersection;
}

function getValue(char: string): number {
  const values = {
    a: 1,
    b: 2,
    c: 3,
    d: 4,
    e: 5,
    f: 6,
    g: 7,
    h: 8,
    i: 9,
    j: 10,
    k: 11,
    l: 12,
    m: 13,
    n: 14,
    o: 15,
    p: 16,
    q: 17,
    r: 18,
    s: 19,
    t: 20,
    u: 21,
    v: 22,
    w: 23,
    x: 24,
    y: 25,
    z: 26,
    A: 27,
    B: 28,
    C: 29,
    D: 30,
    E: 31,
    F: 32,
    G: 33,
    H: 34,
    I: 35,
    J: 36,
    K: 37,
    L: 38,
    M: 39,
    N: 40,
    O: 41,
    P: 42,
    Q: 43,
    R: 44,
    S: 45,
    T: 46,
    U: 47,
    V: 48,
    W: 49,
    X: 50,
    Y: 51,
    Z: 52,
  } as { [key: string]: number };

  return values[char];
}
