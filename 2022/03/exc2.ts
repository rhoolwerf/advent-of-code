import { readFileSync } from "fs";

const rucksacks = readFileSync(process.argv[2], "utf8")
  .split("\n")
  .reduce(
    (groups, rucksack) => {
      if (groups[groups.length - 1].length === 3) {
        groups.push([]);
      }
      groups[groups.length - 1].push(rucksack);
      return groups;
    },
    [[]] as Array<Array<string>>
  )
  .map((group) =>
    group.reduce(
      (intersection, rucksack) =>
        getIntersection(intersection, new Set(rucksack.split(""))),
      new Set(Object.keys(getValueMapping()))
    )
  )
  .map((set) => Array.from(set)[0])
  .map((char) => getValue(char))
  .reduce((sum, value) => sum + value, 0);

console.log(rucksacks);

// https://bobbyhadz.com/blog/javascript-get-intersection-of-two-sets
function getIntersection<T>(setA: Set<T>, setB: Set<T>): Set<T> {
  const intersection = new Set(
    [...setA].filter((element) => setB.has(element))
  );

  return intersection;
}

function getValue(char: string): number {
  return getValueMapping()[char];
}

function getValueMapping(): { [key: string]: number } {
  return {
    a: 1,
    b: 2,
    c: 3,
    d: 4,
    e: 5,
    f: 6,
    g: 7,
    h: 8,
    i: 9,
    j: 10,
    k: 11,
    l: 12,
    m: 13,
    n: 14,
    o: 15,
    p: 16,
    q: 17,
    r: 18,
    s: 19,
    t: 20,
    u: 21,
    v: 22,
    w: 23,
    x: 24,
    y: 25,
    z: 26,
    A: 27,
    B: 28,
    C: 29,
    D: 30,
    E: 31,
    F: 32,
    G: 33,
    H: 34,
    I: 35,
    J: 36,
    K: 37,
    L: 38,
    M: 39,
    N: 40,
    O: 41,
    P: 42,
    Q: 43,
    R: 44,
    S: 45,
    T: 46,
    U: 47,
    V: 48,
    W: 49,
    X: 50,
    Y: 51,
    Z: 52,
  };
}
