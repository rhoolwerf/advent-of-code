function simulate(memory) {
    const configurations = new Set();
    let steps = 0;


    while (!configurations.has(memory.join(','))) {
        // Register configuration
        configurations.add(memory.join(','));

        // Find highest number
        let highest = [...memory].sort((a, b) => b - a)[0];
        let index = memory.indexOf(highest);
        // console.log(memory.map((e, i) => i === index ? `(${e})` : ` ${e} `).join(''), '->', highest, 'at', index);

        // Reset and distribute
        memory[index] = 0;
        while (highest-- > 0) {
            index = (index + 1) % memory.length;
            memory[index]++;
        }

        // Increase shuffle-steps
        // if (steps > 10) { break; }
        steps++;
    }

    return steps;
}


// console.log(simulate([0, 2, 7, 0]));
console.log(simulate([5, 1, 10, 0, 1, 7, 13, 14, 3, 12, 8, 10, 7, 12, 0, 6]));