const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n");

const regex = /([a-z]+) \(([0-9]+)\)( -> ([a-z, ]+))*/;

const relations = new Set();
const programs = [];

aInput.forEach(sInput => {
    const [_input, program, weight, _children, children] = regex.exec(sInput).map(x => x && x.split(', '));
    programs.push(program[0]);
    if (children) {
        children.forEach(child => relations.add(child));
    }
});
console.log(programs.find(p => !relations.has(p)));