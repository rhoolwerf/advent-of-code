const UP = 1;
const LEFT = 2;
const DOWN = 3;
const RIGHT = 4;

function get_manhattan_distance(target) {
    let curX = 0;
    let curY = 0;
    const coords = new Map();
    let currentStep = 1;
    let currentDirection = RIGHT;
    coords.set([curX, curY].join(','), currentStep++);

    for (let i = 0; i < target - 1; i++) {

        // Plot next step
        [curX, curY] = get_next_coords(currentDirection, curX, curY);
        coords.set([curX, curY].join(','), currentStep++);

        // If taking a left turn is not occupied, change direction
        if (!coords.has(get_next_coords(get_next_direction(currentDirection), curX, curY).join(','))) {
            currentDirection = get_next_direction(currentDirection);
        }
    }

    return Math.abs(curX) + Math.abs(curY);
}

console.log(1, get_manhattan_distance(1));
console.log(12, get_manhattan_distance(12));
console.log(23, get_manhattan_distance(23));
console.log(1024, get_manhattan_distance(1024));
console.log(361527, get_manhattan_distance(361527));

function get_next_coords(next_direction, x, y) {
    if (next_direction === RIGHT) {
        return [x + 1, y];
    } else if (next_direction === UP) {
        return [x, y - 1];
    } else if (next_direction === LEFT) {
        return [x - 1, y];
    } else if (next_direction === DOWN) {
        return [x, y + 1];
    }
}

function get_next_direction(currentDirection) {
    if (currentDirection === UP) { return LEFT; }
    else if (currentDirection === LEFT) { return DOWN; }
    else if (currentDirection === DOWN) { return RIGHT; }
    else if (currentDirection === RIGHT) { return UP; }
}