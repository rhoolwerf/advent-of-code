import { readFileSync } from "fs";

const result = readFileSync(process.argv[2], 'utf8').split('\n').reduce((a, c) => {
    const { groups: { cmd, steps } } = /^(?<cmd>[a-z]+) (?<steps>[0-9]+)$/.exec(c);
    if (cmd === 'forward') {
        a.p += Number(steps);
        a.d += Number(steps) * a.a;
    } else {
        a.a += Number(steps) * (cmd === 'up' ? -1 : 1)
    }
    return a;
}, { p: 0, d: 0, a: 0 });

console.log(result, result.d * result.p);