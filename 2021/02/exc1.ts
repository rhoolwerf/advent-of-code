import { readFileSync } from "fs";

const result = readFileSync(process.argv[2], 'utf8').split('\n').reduce((a, c) => {
    const { groups: { cmd, steps } } = /^(?<cmd>[a-z]+) (?<steps>[0-9]+)$/.exec(c);
    if (cmd === 'forward') { a.p += Number(steps); }
    else { a.d += Number(steps) * (cmd === 'up' ? -1 : 1) }
    return a;
}, { p: 0, d: 0 });

console.log(result, result.p * result.d);