interface Person {
    name: string;
}
function say_hello(p: Person) {
    console.log(`Hello ${p.name}`);
}

say_hello({ name: 'Roald' });