import { readFileSync } from "fs";

const data = readFileSync(process.argv[2], 'utf8').split('\n\n').map(x => x.split('\n'));

const start = data[0][0].split('');
const map = data[1].reduce((acc, curr) => acc.set(curr.split(' -> ')[0], curr.split(' -> ')[1]), new Map());

function run(times: number) {
    let input = [...start];
    for (let i = 0; i < times; i++) {
        const result = [];
        input.forEach((char, index) => {
            result.push(char);
            if (index < input.length - 1) {
                result.push(map.get([input[index], input[index + 1]].join('')));
            }
        });
        input = result;
    }
    return input.join('');
}

// console.log(run(1) === 'NCNBCHB');
// console.log(run(2) === 'NBCCNBBBCBHCB');
// console.log(run(3) === 'NBBBCNCCNBBNBNBBCHBHHBCHB');
// console.log(run(4) === 'NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB');

const after10 = Array.from(
    run(10).split('').reduce((acc, curr) => acc.set(curr, (acc.get(curr) || 0) + 1), new Map())
);
after10.sort((a, b) => a[1] - b[1]);
console.log(after10[after10.length - 1][1] - after10[0][1]);