import { readFileSync } from "fs";

const data = readFileSync(process.argv[2], 'utf8').split('\n\n').map(x => x.split('\n'));

const start = data[0][0].split('');
const map = data[1].reduce((acc, curr) => acc.set(curr.split(' -> ')[0], curr.split(' -> ')[1]), new Map());

// This is heavily inspired by explanation by Mark
function run(times: number) {

    // Initialize first state
    let state = new Map();
    start.forEach((c: string, i: number) => {
        if (i < start.length - 1) {
            const key = `${start[i]}${start[i + 1]}`;
            state.set(key, (state.get(key) || 0) + 1);
        }
    });

    for (let i = 1; i <= times; i++) {
        const newState = new Map();
        for (const pair of Array.from(state.keys())) {
            const count = state.get(pair);
            const insert = map.get(pair);
            const firstKey = `${pair.charAt(0)}${insert}`;
            const secondKey = `${insert}${pair.charAt(1)}`;
            newState.set(firstKey, (newState.get(firstKey) || 0) + count);
            newState.set(secondKey, (newState.get(secondKey) || 0) + count);
        }
        state = newState;
    }

    // For some reason, this'll result in a double-count
    const weirdCount = Array.from(state.keys()).reduce((acc, cur) => {
        acc.set(cur.charAt(0), (acc.get(cur.charAt(0)) || 0) + state.get(cur));
        acc.set(cur.charAt(1), (acc.get(cur.charAt(1)) || 0) + state.get(cur));
        return acc;
    }, new Map());

    // Re-map and reduce the count to proper numbers (and do some excessive typing to keep TS happy)
    const weirdArray: [[string, number]] = Array.from(weirdCount.keys()).reduce((acc: [[string, number]], cur: string) => {
        acc.push([cur, Math.round(weirdCount.get(cur) / 2)]);
        return acc;
    }, []) as [[string, number]];

    weirdArray.sort((a, b) => a[1] - b[1]);
    return weirdArray[weirdArray.length - 1][1] - weirdArray[0][1];
}

console.log(run(40));