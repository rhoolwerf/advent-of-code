import { readFileSync } from 'fs';

const brackets = { '[': ']', '{': '}', '<': '>', '(': ')' };
const bracketValues = { ')': 1, ']': 2, '}': 3, '>': 4 };
function score(line: string): number {
    const chunks = [];

    for (const bracket of line.split('')) {
        if (bracket === '(' || bracket === '{' || bracket === '<' || bracket === '[') {
            chunks.push(bracket);
        } else {
            const expected = brackets[chunks.pop()];
            if (bracket !== expected) {
                // console.log(line, `expected '${expected}', but got '${bracket}'`);
                return 0;
            }
        }
    }

    const sum = () => chunks.reduce((sum, bracket) => sum * 5 + bracketValues[brackets[bracket]], 0);

    chunks.reverse();
    console.log(line, 'has remaining', chunks.map((bracket) => brackets[bracket]).join(''), sum());

    return sum();
}

const lines = readFileSync(process.argv[2], 'utf-8').split('\n');
const scores = lines.map((line) => score(line)).filter((score) => score);
scores.sort((a, b) => a - b);
console.log(scores);
console.log(scores[Math.round((scores.length - 1) / 2)]);
