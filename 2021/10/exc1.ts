import { readFileSync } from 'fs';

const brackets = { '[': ']', '{': '}', '<': '>', '(': ')' };
const bracketValues = { ')': 3, ']': 57, '}': 1197, '>': 25137 };
function score(line: string): number {
    const chunks = [];

    for (const bracket of line.split('')) {
        if (bracket === '(' || bracket === '{' || bracket === '<' || bracket === '[') {
            chunks.push(bracket);
        } else {
            const expected = brackets[chunks.pop()];
            if (bracket !== expected) {
                console.log(line, `expected '${expected}', but got '${bracket}'`);
                return bracketValues[bracket];
            }
        }
    }

    return 0;
}

const lines = readFileSync(process.argv[2], 'utf-8').split('\n');
console.log(lines.map((line) => score(line)).reduce((sum, score) => sum + score, 0));
