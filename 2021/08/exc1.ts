import { readFileSync } from "fs";

const input = readFileSync(process.argv[2], 'utf8').split('\n').map(x => x.split('|').map(y => y.trim())[1]).map(x => x.split(' '));

console.log(input.map(x => x.reduce((a, c) => {
    if (c.length === 2 || c.length === 4 || c.length === 3 || c.length === 7) {
        return a + 1;
    }
    return a;
}, 0)).reduce((a, c) => a + c, 0));