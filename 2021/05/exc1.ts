import { readFileSync } from "fs";

const data = readFileSync(process.argv[2], 'utf8').split('\n').map(x => /([0-9]+),([0-9]+) -> ([0-9]+),([0-9]+)/.exec(x)).map(x => [x[1], x[2], x[3], x[4]]).map(x => x.map(Number));

const map = new Map();
for (const line of data) {
    const [x1, y1, x2, y2] = line;
    // console.log(x1, y1, x2, y2, x1 === x2, y1 === y2, x1 === x2 || y1 === y2);
    if (x1 === x2 || y1 === y2) {
        // console.log('- working');
        for (let i = Math.min(y1, y2); i <= Math.max(y1, y2); i++) {
            for (let j = Math.min(x1, x2); j <= Math.max(x1, x2); j++) {
                // console.log('- - ', i, j);
                const key = [j, i].join('-');
                map.set(key, (map.get(key) || 0) + 1);
            }
        }
    }
}
console.log(Array.from(map.values()).filter(x => x >= 2).length);