import { readFileSync } from "fs";

const data = readFileSync(process.argv[2], 'utf8').split('\n').map(x => /([0-9]+),([0-9]+) -> ([0-9]+),([0-9]+)/.exec(x)).map(x => [x[1], x[2], x[3], x[4]]).map(x => x.map(Number));

// let minX = 0;
// let minY = 0;
// let maxX = 0;
// let maxY = 0;

const map = new Map();
for (const line of data) {
    const [x1, y1, x2, y2] = line;

    // minX = Math.min(minX, Math.min(x1, x2));
    // maxX = Math.max(maxX, Math.max(x1, x2));
    // minY = Math.min(minY, Math.min(y1, y2));
    // maxY = Math.max(maxY, Math.max(y1, y2));

    if (x1 === x2 || y1 === y2) {
        for (let i = Math.min(y1, y2); i <= Math.max(y1, y2); i++) {
            for (let j = Math.min(x1, x2); j <= Math.max(x1, x2); j++) {
                set_point(j, i);
            }
        }
    } else if (Math.abs(x2 - x1) === Math.abs(y2 - y1)) {
        let x = x1;
        let xTimes = Math.abs(x2 - x1);
        let y = y1;
        let yTimes = Math.abs(y2 - y1);
        while (yTimes-- >= 0) {
            while (xTimes-- >= 0) {
                set_point(x, y);
                y += (y1 < y2 ? 1 : -1);
                x += (x1 < x2 ? 1 : -1);
            }
        }
    }
}

// for (let y = minY; y <= maxY; y++) {
//     let sOutput = '';
//     for (let x = minX; x <= maxX; x++) {
//         sOutput += map.get(get_key(x, y)) || '.';
//     }
//     console.log(sOutput);
// }

function get_key(x: number, y: number) {
    return [y, x].join('-');
}

function set_point(x: number, y: number) {
    const key = get_key(x, y);
    map.set(key, (map.get(key) || 0) + 1);
}
console.log(Array.from(map.values()).filter(x => x >= 2).length);