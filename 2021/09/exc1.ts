import { readFileSync } from 'fs';

const input = readFileSync(process.argv[2], 'utf8')
    .split('\n')
    .map((x) => x.split(''));

const lowpoints = [];
for (let y = 0; y < input.length; y++) {
    for (let x = 0; x < input[y].length; x++) {
        const value = input[y][x];
        const above = input[y - 1] ? input[y - 1][x] : 99;
        const left = input[y][x - 1] || 99;
        const below = input[y + 1] ? input[y + 1][x] : 99;
        const right = input[y][x + 1] || 99;

        if (value < above && value < left && value < below && value < right) {
            lowpoints.push(value);
        }
    }
}

console.log(
    lowpoints
        .map(Number)
        .map((x) => x + 1)
        .reduce((acc, curr) => acc + curr, 0)
);
