import { readFileSync } from "fs";

const aNumbers = readFileSync(process.argv[2], 'utf8').split('\n').map(x => x.split('').map(Number));

function find(validator: Function) {
    let aInput = [...aNumbers];

    for (let i = 0; i < aInput[0].length; i++) {
        const aCount = [0, 0];

        aInput.forEach(aNumber => {
            aCount[aNumber[i]]++;
        });

        aInput = aInput.filter(aNumber => aNumber[i] === ((validator(aCount[0], aCount[1]) ? 1 : 0)));

        if (aInput.length === 1) {
            return parseInt(aInput[0].join(''), 2);
        }
    }
}
let iOxygen = find((a: number, b: number) => a <= b);
let iCO2 = find((a: number, b: number) => a > b);
console.log(iOxygen, iCO2, iOxygen * iCO2);