import { readFileSync } from "fs";

const aNumbers = readFileSync(process.argv[2], 'utf8').split('\n').map(x => x.split('').map(Number));

const aCount = [];
aNumbers.forEach(aNumber => {
    aNumber.forEach((iNumber, iIndex) => {
        if (!aCount[iIndex]) {
            aCount[iIndex] = [0, 0];
        }

        aCount[iIndex][iNumber]++;
    });
});

console.log(aCount);

const aTotalGamma = [];
const aTotalEpsilon = [];
aCount.forEach(aSubCount => {
    aTotalGamma.push(Number(aSubCount[0] < aSubCount[1]));
    aTotalEpsilon.push(Number(aSubCount[0] >= aSubCount[1]));
});
const iGamma = parseInt(aTotalGamma.join(''), 2);
const iEpsilon = parseInt(aTotalEpsilon.join(''), 2);
console.log(iGamma, iEpsilon, iGamma * iEpsilon);
