import { readFileSync } from "fs";

const numbers = readFileSync(process.argv[2], 'utf8').split(',').map(Number);
const maxNumber = numbers.reduce((a, c) => a > c ? a : c, 0);
let minCost = 0;

numbers.sort((a, b) => a - b);

for (let i = 0; i < maxNumber; i++) {
    const cost = numbers.reduce((a, c) => a + Math.abs(c - i), 0);
    // console.log(i, cost, minCost, cost <= minCost);
    if (!minCost || cost <= minCost) {
        minCost = cost;
    } else {
        // If we're increasing again, we might as well quit
        break;
    }
}
console.log(minCost);