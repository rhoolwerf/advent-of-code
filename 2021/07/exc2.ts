import { readFileSync } from "fs";

const numbers = readFileSync(process.argv[2], 'utf8').split(',').map(Number);
const maxNumber = numbers.reduce((a, c) => a > c ? a : c, 0);
let minCost = 0;

numbers.sort((a, b) => a - b);

for (let i = 0; i < maxNumber; i++) {
    const cost = numbers.reduce((a, c) => a + expand_cost(c - i), 0);

    if (!minCost || cost <= minCost) {
        minCost = cost;
    } else {
        // If we're increasing again, we might as well quit
        break;
    }
}
console.log(minCost);

function expand_cost(cost: number) {
    cost = Math.abs(cost);
    let result = cost;
    while (cost-- >= 0) {
        result += cost;
    }
    return result + 1;
}