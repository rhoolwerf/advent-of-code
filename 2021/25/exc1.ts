import { readFileSync } from 'fs';

type State = Array<Array<string>>;

let state = readFileSync(process.argv[2], 'utf8')
    .split('\n')
    .map((row) => row.split(''));

// console.log('Starting state:');
// print_state(state);
for (let i = 1; i <= 1000; i++) {
    const newState = process_state(state);
    // console.log(`After ${i} step(s):`);
    // print_state(newState);

    if (state.map((row) => row.join('')).join('\n') === newState.map((row) => row.join('')).join('\n')) {
        console.log(`After ${i} steps, no more changes`);
        break;
    }
    state = newState;
}

function process_state(state: State): State {
    // First go right where possible
    let firstStep = JSON.parse(JSON.stringify(state));
    for (let y = 0; y < state.length; y++) {
        for (let x = 0; x < state[y].length; x++) {
            if (state[y][x] === '>') {
                if (state[y][(x + 1) % state[y].length] === '.') {
                    firstStep[y][x] = '.';
                    firstStep[y][(x + 1) % state[y].length] = '>';
                }
            }
        }
    }

    let secondStep = JSON.parse(JSON.stringify(firstStep));
    for (let x = 0; x < state[0].length; x++) {
        for (let y = 0; y < state.length; y++) {
            if (firstStep[y][x] === 'v') {
                if (firstStep[(y + 1) % state.length][x] === '.') {
                    secondStep[y][x] = '.';
                    secondStep[(y + 1) % state.length][x] = 'v';
                }
            }
        }
    }

    return secondStep;
}

function print_state(state: State) {
    console.log(state.map((row) => row.join('')).join('\n'));
}
