import { readFileSync } from "fs";

let fishCount = readFileSync(process.argv[2], 'utf8').split(',').reduce((acc, cur) => { acc[cur]++; return acc; }, {
    0: 0,
    1: 0,
    2: 0,
    3: 0,
    4: 0,
    5: 0,
    6: 0,
    7: 0,
    8: 0
});

for (let day = 1; day <= Number(process.argv[3]); day++) {
    const newState = {
        0: fishCount['1'],
        1: fishCount['2'],
        2: fishCount['3'],
        3: fishCount['4'],
        4: fishCount['5'],
        5: fishCount['6'],
        6: fishCount['7'] + fishCount['0'],
        7: fishCount['8'],
        8: fishCount['0']
    }

    fishCount = newState;
}

console.log(Object.values(fishCount).reduce((acc, cur) => acc + cur, 0));