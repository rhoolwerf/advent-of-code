import { readFileSync } from "fs";

const fishes = readFileSync(process.argv[2], 'utf8').split(',').map(Number);

for (let day = 1; day <= 80; day++) {
    const newFishes = [];
    for (let i = 0; i < fishes.length; i++) {
        if (fishes[i] !== 0) {
            fishes[i]--;
        } else {
            fishes[i] = 6;
            newFishes.push(8);
        }
    }

    fishes.push(...newFishes);
    // console.log('After day', day, ':', fishes.join(','));
}
console.log(fishes.length);