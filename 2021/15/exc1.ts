import { readFileSync } from "fs";

const map = readFileSync(process.argv[2], 'utf8').split('\n').map(line => line.split('').map(Number));

// Build helper data
const nodes = [];
const links = [];
for (let y = 0; y < map.length; y++) {
    for (let x = 0; x < map[y].length; x++) {
        nodes.push({ y, x });

        if (map[y - 1] && map[y - 1][x]) { links.push({ from: { y, x }, to: { y: y - 1, x } }); }
        if (map[y] && map[y][x - 1]) { links.push({ from: { y, x }, to: { y, x: x - 1 } }); }
        if (map[y + 1] && map[y + 1][x]) { links.push({ from: { y, x }, to: { y: y + 1, x } }); }
        if (map[y] && map[y][x + 1]) { links.push({ from: { y, x }, to: { y, x: x + 1 } }); }
    }
}
const finalCoordinates = { y: map.length - 1, x: map[map.length - 1].length - 1 };

// Build the actual graph
const createGraph = require('ngraph.graph');
const graph = createGraph();

nodes.forEach(({ y, x }) => graph.addNode(get_key(y, x), { y, x }));
links.forEach(({ from: { x: fX, y: fY }, to: { x: tX, y: tY } }) => {
    graph.addLink(get_key(fY, fX), get_key(tY, tX));
});


// Find us a path!
const pathLib = require('ngraph.path');
const pathFinder = pathLib.aStar(graph, {
    distance(fromNode, toNode, link) {
        const { y, x } = toNode.data;
        return map[y][x];
    // },
    // heuristic(fromNode, toNode) {
    //     return Math.abs(toNode.data.x - finalCoordinates.x) + Math.abs(toNode.data.y - finalCoordinates.y);
    }
});
const path = pathFinder.find(get_key(0, 0), get_key(finalCoordinates.y, finalCoordinates.x));
// console.log(path);

const stepPoints = path.map(node => node.data).map(({ y, x }) => map[y][x]);
const riskFactor = stepPoints.reduce((a, c) => a + c, 0);
console.log(stepPoints, riskFactor - map[0][0]);

function get_key(y: number, x: number): string {
    return [y, x].join(',');
}