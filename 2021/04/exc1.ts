import { readFileSync } from "fs";

const blocks = readFileSync(process.argv[2], 'utf8').split('\n\n').map(x => x.split('\n'));
const numbers = blocks[0][0].split(',').map(Number);
const boards = blocks.slice(1).map(x => x.map(y => y.split(' ').filter(z => z).map(Number)));

// const number = numbers[0];

for (const number of numbers) {
    process_boards(number);
    const fullBoard = validate_boards();
    if (fullBoard.length > 0) {
        const results = fullBoard.map(row => row.filter(cell => cell >= 0).reduce((a, c) => a + c, 0)).reduce((a, c) => a + c, 0);
        console.log('Full board found after number', number, results, number * results);
        break;
    }
}

function process_boards(number) {
    // replace all matching cells with X
    for (let i = 0; i < boards.length; i++) {
        for (let j = 0; j < boards[i].length; j++) {
            for (let k = 0; k < boards[i][j].length; k++) {
                if (boards[i][j][k] === number) {
                    boards[i][j][k] = -1;
                }
            }
        }
    }
}
function validate_boards() {
    // validate boards
    for (const board of boards) {
        // validate columns
        for (let column = 0; column < board[0].length; column++) {
            if (board.map(row => row[column]).join('') === '-1-1-1-1-1') {
                return board;
            }
        }

        // validate rows
        if (board.map(row => row.join('')).filter(row => row === '-1-1-1-1-1').length > 0) {
            return board;
        }
    }

    // if we get here, no dice!
    return [];
}
