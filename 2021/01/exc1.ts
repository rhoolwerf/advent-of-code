import { readFileSync } from "fs";
console.log(
    readFileSync(process.argv[2], { encoding: 'utf8' })
        .split('\n')
        .map(Number)
        .map((v, i, a) => v < a[i + 1])
        .filter(x => x)
        .length
);