import { readFileSync } from 'fs';
import { Processor } from '../util/opcode_processor';
import { permutator } from '../util/util';

function run_amps(instructions: string, sequence: Array<number>): number {
    let output = 0;

    for (const input of sequence) {
        output = new Processor(instructions, { input: [input, output] }).run_once().output.pop();
    }

    return output;
}

function test(instr: string, seq: string, expected: number) {
    const actual = run_amps(instr, seq.split(',').map(Number));
    console.assert(expected === actual, '', { instr, seq, expected, actual });
}

test('3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0', '4,3,2,1,0', 43210);
test('3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0', '0,1,2,3,4', 54321);
test(
    '3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0',
    '1,0,4,3,2',
    65210
);

const instr = readFileSync('./input', 'utf8');
console.log(Math.max(...permutator([4, 3, 2, 1, 0]).map((permutation) => run_amps(instr, permutation))));
