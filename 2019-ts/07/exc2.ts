import { readFileSync } from 'fs';
import { Processor } from '../util/opcode_processor';
import { permutator } from '../util/util';

function run_amps(instructions: string, sequence: Array<number>): number {
    let nextInput = 0;

    const machines: Array<Processor> = [];
    for (let i = 0, keepRunning = true; keepRunning; i++) {
        if (!machines[i % 5]) {
            const proc = new Processor(instructions, { input: [sequence[i], nextInput] });
            proc.run_once(); // Run once
            nextInput = proc.output.pop();
            machines.push(proc);
        } else {
            const machine = machines[i % 5];
            machine.input.push(nextInput);
            machine.run_once();
            keepRunning = !machine.halted;
            if (keepRunning) {
                nextInput = machine.output.pop();
            }
        }
    }

    return nextInput;
}

function run_amps_max(instr: string): number {
    return Math.max(...permutator([5, 6, 7, 8, 9]).map((p) => run_amps(instr, p)));
}

function test(instr: string, seq: string, expected: number) {
    const actual = run_amps(instr, seq.split(',').map(Number));
    console.assert(expected === actual, '', { instr, seq, expected, actual });
}

function test_max(instr: string, expected: number) {
    const actual = run_amps_max(instr);
    console.assert(expected === actual, '', { instr, expected, actual });
}

test('3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5', '9,8,7,6,5', 139629729);
test(
    '3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10',
    '9,7,8,5,6',
    18216
);

test_max('3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5', 139629729);
test_max(
    '3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10',
    18216
);

console.log(run_amps_max(readFileSync('./input', 'utf8')));
