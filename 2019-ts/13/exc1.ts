import { readFileSync } from 'fs';
import { Processor } from '../util/opcode_processor';

const input = readFileSync('./input', 'utf8');
const proc = new Processor(input);
const output = proc.run_till_halt().output;

const grid = new Map<[number, number], number>();
for (let i = 0; i < output.length; i += 3) {
    const [x, y, tile] = output.slice(i, i + 3);
    grid.set([x, y], tile);
}

console.log([...grid.keys()]);
