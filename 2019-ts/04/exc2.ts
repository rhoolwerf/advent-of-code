export {};
function check_hash(input: string): boolean {
    const digits = input.split('').map(Number);

    // Check if it's a six-digit-number
    if (digits.length !== 6) return false;

    // At least one adjacent same digit
    const count = digits.reduce(
        (carry, digit) => {
            carry[digit] = (carry[digit] || 0) + 1;
            return carry;
        },
        {} as { [key: number]: number }
    );
    if (![...Object.values(count)].includes(2)) return false;

    // Only increasing?
    if (
        !digits
            .map((v, i) => v <= digits[i + 1])
            .slice(0, -1)
            .every(Boolean)
    )
        return false;

    // All checks passed
    return true;
}

function test(input: string, expected: boolean): void {
    const actual = check_hash(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

test('111111', false);
test('111122', true);
test('122345', true);
test('11111', false);
test('123444', false);
test('123450', false);
test('123789', false);

const input = '136760-595730';
const [begin, end] = input.split('-').map(Number);
let answer = 0;
for (let i = begin; i <= end; i++) {
    if (check_hash(String(i))) answer++;
}
console.log({ answer });
