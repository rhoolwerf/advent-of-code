import { readFileSync } from 'fs';
import { Processor } from '../util/opcode_processor';

const proc = new Processor(readFileSync('./input', 'utf8'), { input: '2' });
console.log(proc.run_once().output.pop());
