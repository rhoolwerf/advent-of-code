import { readFileSync } from 'fs';
import { Processor } from '../util/opcode_processor';

// const proc = new Processor('109,19,99');
// proc.relative_base = 2000;
// proc.run_once();
// console.log(proc);

// const program = '109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99';
// const proc = new Processor('109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99');
// while (proc.)

// console.log(String(new Processor('1102,34915192,34915192,7,4,7,99,0').run_till_halt().output[0]).length);

// console.log(new Processor('104,1125899906842624,99').run_once().output);

const proc = new Processor(readFileSync('./input', 'utf8'), { input: '1' });
console.log(proc.run_once().output.pop());
