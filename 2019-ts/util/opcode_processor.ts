// Mode
const POSITION = '0';
const IMMEDIATE = '1';
const RELATIVE = '2';

// Opcode constants
const READ_INPUT = 3;
const WRITE_OUTPUT = 4;
const HALT = 99;

// Opcode mapper
const OPCODE = {
    1: 'add',
    2: 'multiply',
    3: 'read_input',
    4: 'write_output',
    5: 'jump_if_true',
    6: 'jump_if_false',
    7: 'less_than',
    8: 'equals',
    9: 'adjust_relative_base',
};

type Config = {
    input?: string | Array<number>;
    relative_base?: number;
};

export class Processor {
    public instructions: Array<number> = [];
    public position: number = 0;
    public input: Array<number> = [];
    public output: Array<number> = [];
    public halted = false;
    public relative_base = 0;

    constructor(instructions: string | Array<number>, config: Config = { input: [], relative_base: 0 }) {
        if (typeof instructions === 'string') {
            this.instructions = instructions.split(',').map(Number);
        } else {
            this.instructions = JSON.parse(JSON.stringify(instructions));
        }

        if (typeof config.input === 'string') {
            this.input = config.input.split(',').map(Number);
        } else if (Array.isArray(config.input)) {
            this.input = JSON.parse(JSON.stringify(config.input));
        }

        if (typeof config.relative_base === 'number') {
            this.relative_base = config.relative_base;
        }
    }

    public run_once(): Processor {
        while (this.position < this.instructions.length) {
            const base = this.instructions[this.position];
            const instruction = String(base).padStart(5, '0');

            const cmd = Number(instruction.substring(3));
            const m1 = instruction.substring(2, 3);
            const m2 = instruction.substring(1, 2);
            const m3 = instruction.substring(0, 1);

            // Can we call the method dynamically?
            if (cmd in OPCODE && OPCODE[cmd] && OPCODE[cmd] in this && typeof this[OPCODE[cmd]]) {
                // If we want to read from input, but no input is given, 'wait' for it
                // 'wait' means, do nothing but tell the caller it can still continue
                if (cmd === READ_INPUT && this.input.length === 0) return this;

                // Dynamically call the required method
                this[OPCODE[cmd]](m1, m2, m3);

                // If we just wrote to output, halt processing
                if (cmd === WRITE_OUTPUT) return this;
            } else if (cmd === HALT) {
                this.halted = true;
                return this;
            } else {
                console.error('Unknown cmd:', cmd, 'found at position', this.position);
                process.exit(-1);
            }
        }

        console.error('It ran out, without a HALT-code');
        process.exit(-1);
    }

    public run_till_halt(): Processor {
        while (!this.halted) this.run_once();
        return this;
    }

    private add(m1: string, m2: string, m3: string) {
        const v1 = this.read_position(this.position + 1, m1);
        const v2 = this.read_position(this.position + 2, m2);
        this.write_position(this.position + 3, m3, v1 + v2);
        this.position += 4;
    }

    private multiply(m1: string, m2: string, m3: string) {
        const v1 = this.read_position(this.position + 1, m1);
        const v2 = this.read_position(this.position + 2, m2);
        this.write_position(this.position + 3, m3, v1 * v2);
        this.position += 4;
    }

    private read_input(m1: string) {
        this.write_position(this.position + 1, m1, this.input.shift());
        this.position += 2;
    }

    private write_output(m1: string) {
        this.output.push(this.read_position(this.position + 1, m1));
        this.position += 2;
    }

    private jump_if_true(m1: string, m2: string) {
        const v1 = this.read_position(this.position + 1, m1);
        const v2 = this.read_position(this.position + 2, m2);
        if (v1 !== 0) {
            this.position = v2;
        } else {
            this.position += 3;
        }
    }

    private jump_if_false(m1: string, m2: string) {
        const v1 = this.read_position(this.position + 1, m1);
        const v2 = this.read_position(this.position + 2, m2);
        if (v1 === 0) {
            this.position = v2;
        } else {
            this.position += 3;
        }
    }

    private less_than(m1: string, m2: string, m3: string) {
        const v1 = this.read_position(this.position + 1, m1);
        const v2 = this.read_position(this.position + 2, m2);
        this.write_position(this.position + 3, m3, Number(v1 < v2));
        this.position += 4;
    }

    private equals(m1: string, m2: string, m3: string) {
        const v1 = this.read_position(this.position + 1, m1);
        const v2 = this.read_position(this.position + 2, m2);
        this.write_position(this.position + 3, m3, Number(v1 === v2));
        this.position += 4;
    }

    private adjust_relative_base(m1: string) {
        this.relative_base += this.read_position(this.position + 1, m1);
        this.position += 2;
    }

    private read_position(pos: number, mode: string): number {
        if (mode === IMMEDIATE) {
            return this.instructions[pos];
        } else if (mode === POSITION) {
            const target = this.instructions[pos];
            return this.instructions[target];
        } else if (mode === RELATIVE) {
            const target = this.instructions[pos] + this.relative_base;
            return this.instructions[target];
        } else throw new Error(`read_position: Unknown mode: ${mode}`);
    }

    private write_position(pos: number, mode: string, value: number): void {
        if (mode === POSITION) {
            const target = this.instructions[pos];
            this.instructions[target] = value;
        } else if (mode === RELATIVE) {
            const target = this.instructions[pos] + this.relative_base;
            this.instructions[target] = value;
        } else throw new Error(`write_position: Unknown mode: ${mode}`);
    }
}
