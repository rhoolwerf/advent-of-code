import { readFileSync } from 'fs';
import { Processor } from './opcode_processor';
import { permutator } from './util';

/*
 * This file is meant to run all previous puzzles and their tests
 * as to ensure the Opcode Processor is always backwards compatible
 */

///////////////////////////////////////////////////////////////////////
// Day 2 - Part 1
function test_day_02_part_1(input: string, expected: string): void {
    const proc = new Processor(input);
    proc.run_once();

    const actual = proc.instructions.join(',');
    console.assert(expected === actual, '', { input, expected, actual });
}

test_day_02_part_1('1,0,0,0,99', '2,0,0,0,99');
test_day_02_part_1('2,3,0,3,99', '2,3,0,6,99');
test_day_02_part_1('2,4,4,5,99,0', '2,4,4,5,99,9801');
test_day_02_part_1('1,1,1,4,99,5,6,0,99', '30,1,1,4,2,5,6,0,99');

const input_d02p1 = readFileSync('../02/input', 'utf8').split(',').map(Number);
input_d02p1[1] = 12;
input_d02p1[2] = 2;
const proc = new Processor(input_d02p1);
proc.run_once();
console.assert(proc.instructions[0] === 3850704, 'Puzzle day 2 part 1 fails');

///////////////////////////////////////////////////////////////////////
// Day 2 - Part 2
function test_day_02_part_2(input: string, expected: string): void {
    const proc = new Processor(input);
    proc.run_once();

    const actual = proc.instructions.join(',');
    console.assert(expected === actual, '', { input, expected, actual });
}

test_day_02_part_2('1,0,0,0,99', '2,0,0,0,99');
test_day_02_part_2('2,3,0,3,99', '2,3,0,6,99');
test_day_02_part_2('2,4,4,5,99,0', '2,4,4,5,99,9801');
test_day_02_part_2('1,1,1,4,99,5,6,0,99', '30,1,1,4,2,5,6,0,99');

const input_d02p2 = readFileSync('../02/input', 'utf8').split(',').map(Number);

function run(noun: number, verb: number): number {
    input_d02p2[1] = noun;
    input_d02p2[2] = verb;
    const proc = new Processor(input_d02p2);
    proc.run_once();
    return proc.instructions[0];
}

console.assert(run(67, 18) === 19690720, 'puzzle day 02 part 1 failed');

///////////////////////////////////////////////////////////////////////
// Day 5 - Part 1
function test_day_05_part_1(input: string, expected: string): void {
    const actual = new Processor(input).run_once().instructions.join(',');
    console.assert(expected === actual, '', { input, expected, actual });
}

test_day_05_part_1('1002,4,3,4,33', '1002,4,3,4,99');
test_day_05_part_1('1101,100,-1,4,0', '1101,100,-1,4,99');
console.assert(
    new Processor(readFileSync('../05/input', 'utf8'), { input: [1] }).run_till_halt().output.pop() === 7265618,
    'puzzle day 5 part 1 fails'
);

///////////////////////////////////////////////////////////////////////
// Day 5 - Part 2
function test_day_05_part_2(instr: string, input: string, expected: string): void {
    const actual = new Processor(instr, { input }).run_once().output.join(',');
    console.assert(expected === actual, '', { instr, input, expected, actual });
}

// Equals 8 - Position mode
test_day_05_part_2('3,9,8,9,10,9,4,9,99,-1,8', '8', '1');
test_day_05_part_2('3,9,8,9,10,9,4,9,99,-1,8', '7', '0');
test_day_05_part_2('3,9,8,9,10,9,4,9,99,-1,8', '9', '0');

// Less than 8 - Position mode
test_day_05_part_2('3,9,7,9,10,9,4,9,99,-1,8', '8', '0');
test_day_05_part_2('3,9,7,9,10,9,4,9,99,-1,8', '7', '1');

// Equals 8 - Immediate mode
test_day_05_part_2('3,3,1108,-1,8,3,4,3,99', '8', '1');
test_day_05_part_2('3,3,1108,-1,8,3,4,3,99', '7', '0');
test_day_05_part_2('3,3,1108,-1,8,3,4,3,99', '9', '0');

// Less than 8 - Position mode
test_day_05_part_2('3,3,1107,-1,8,3,4,3,99', '8', '0');
test_day_05_part_2('3,3,1107,-1,8,3,4,3,99', '7', '1');

// Jump - Position
test_day_05_part_2('3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9', '0', '0');
test_day_05_part_2('3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9', '1337', '1');

// Jump - Immediate
test_day_05_part_2('3,3,1105,-1,9,1101,0,0,12,4,12,99,1', '0', '0');
test_day_05_part_2('3,3,1105,-1,9,1101,0,0,12,4,12,99,1', '1337', '1');

// More complex
const complex_instructions =
    '3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99';
test_day_05_part_2(complex_instructions, '7', '999');
test_day_05_part_2(complex_instructions, '8', '1000');
test_day_05_part_2(complex_instructions, '9', '1001');

// Actual puzzle
test_day_05_part_2(readFileSync('../05/input', 'utf8'), '5', '7731427');

///////////////////////////////////////////////////////////////////////
// Day 7 - Part 1
function d7p1(instructions: string, sequence: Array<number>): number {
    let output = 0;

    for (const input of sequence) {
        output = new Processor(instructions, { input: [input, output] }).run_once().output.pop();
    }

    return output;
}

function test_day_07_part_1(instr: string, seq: string, expected: number) {
    const actual = d7p1(instr, seq.split(',').map(Number));
    console.assert(expected === actual, '', { instr, seq, expected, actual });
}

test_day_07_part_1('3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0', '4,3,2,1,0', 43210);
test_day_07_part_1('3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0', '0,1,2,3,4', 54321);
test_day_07_part_1(
    '3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0',
    '1,0,4,3,2',
    65210
);

const input_d07p1 = readFileSync('../07/input', 'utf8');
console.assert(
    Math.max(...permutator([4, 3, 2, 1, 0]).map((permutation) => d7p1(input_d07p1, permutation))) === 398674,
    'puzzle day 7 part 1 fails'
);

///////////////////////////////////////////////////////////////////////
// Day 7 - Part 2
function d07p2(instructions: string, sequence: Array<number>): number {
    let nextInput = 0;

    const machines: Array<Processor> = [];
    for (let i = 0, keepRunning = true; keepRunning; i++) {
        if (!machines[i % 5]) {
            const proc = new Processor(instructions, { input: [sequence[i], nextInput] });
            proc.run_once(); // Run once
            nextInput = proc.output.pop();
            machines.push(proc);
        } else {
            const machine = machines[i % 5];
            machine.input.push(nextInput);
            machine.run_once();
            keepRunning = !machine.halted;
            if (keepRunning) {
                nextInput = machine.output.pop();
            }
        }
    }

    return nextInput;
}

function d07p2_max(instr: string): number {
    return Math.max(...permutator([5, 6, 7, 8, 9]).map((p) => d07p2(instr, p)));
}

console.assert(d07p2_max(readFileSync('../07/input', 'utf8')) === 39431233, 'day 7 part 2 failed');

///////////////////////////////////////////////////////////////////////
// Day 9 - Part 1
console.assert(
    new Processor(readFileSync('../09/input', 'utf8'), { input: '1' }).run_once().output.pop() === 3546494377,
    'day 9 p 1 fails'
);

///////////////////////////////////////////////////////////////////////
// Day 9 - Part 2
console.assert(
    new Processor(readFileSync('../09/input', 'utf8'), { input: '2' }).run_once().output.pop() === 47253,
    'day 9 p 2 fails'
);
