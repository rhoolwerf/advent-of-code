import { readFileSync } from 'fs';
import { EOL } from 'os';
import { sum } from '../util/util';

function calc_mass(input: number): number {
    const calc = Math.floor(input / 3) - 2;
    if (calc < 0) return 0;
    return calc + calc_mass(calc);
}

function test(input: number, expected: number): void {
    const actual = calc_mass(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

test(12, 2);
test(1969, 966);
test(100756, 50346);

console.log(sum(readFileSync('./input', 'utf8').split(EOL).map(Number).map(calc_mass)));
