import { readFileSync } from 'fs';
import { EOL } from 'os';

function split_into_layers(input: string, width: number, height: number): Array<Array<string>> {
    const layers: Array<Array<string>> = [];
    for (let start = 0; start < input.length; start += width * height) {
        const img = Array.from({ length: height }, (_, j) => {
            const begin = start + width * j;
            const end = begin + width;
            return input.substring(begin, end);
        });

        layers.push(img);
    }

    return layers;
}

function form_image(input: string, width: number, height: number): Array<Array<string>> {
    const layers = split_into_layers(input, width, height);
    const grid: Array<Array<string>> = Array.from({ length: height }, () => Array.from({ length: width }, () => '2'));

    for (const layer of layers) {
        for (let y = 0; y < height; y++) {
            for (let x = 0; x < width; x++) {
                if (grid[y][x] === '2') {
                    grid[y][x] = layer[y][x];
                }
            }
        }
    }

    return grid;
}

function show_image(input: string, width: number, height: number) {
    const grid = form_image(input, width, height);
    console.log(grid.map((row) => row.map((c) => (c === '1' ? 'X' : ' ')).join('')).join(EOL));
}

// show_image('0222112222120000', 2, 2);
show_image(readFileSync('./input', 'utf8'), 25, 6);
