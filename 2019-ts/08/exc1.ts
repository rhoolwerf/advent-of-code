import { readFileSync } from 'fs';

function split_into_layers(input: string, width: number, height: number): Array<Array<string>> {
    const layers: Array<Array<string>> = [];
    for (let start = 0; start < input.length; start += width * height) {
        const img = Array.from({ length: height }, (_, j) => {
            const begin = start + width * j;
            const end = begin + width;
            return input.substring(begin, end);
        });

        layers.push(img);
    }

    return layers;
}

// split_into_layers('123456789012', 3, 2);
// split_into_layers('123456789123456789', 3, 3);
// split_into_layers('123456789123456789', 3, 2);

const input = readFileSync('./input', 'utf8');
const layers = split_into_layers(input, 25, 6);
const counts = layers.map((layer) => {
    return layer
        .join('')
        .split('')
        .reduce(
            (counter, digit) => {
                counter[digit] = (counter[digit] || 0) + 1;
                return counter;
            },
            {} as { [key: number]: number }
        );
});
counts.sort((a, b) => a['0'] - b['0']);
const [fewest] = counts;

console.log(fewest['1'] * fewest['2']);
