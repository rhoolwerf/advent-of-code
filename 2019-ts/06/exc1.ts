import { readFileSync } from 'fs';
import { EOL } from 'os';
import { sum } from '../util/util';

function calc_orbits(file: string): number {
    const input = readFileSync(file, 'utf8')
        .split(EOL)
        .map((star) =>
            [...star.matchAll(/([A-Z0-9]+)\)([A-Z0-9]+)/g)].map(([_, parent, child]) => [parent, child]).flat()
        );

    const relations: { [key: string]: string } = {};
    input.forEach(([parent, child]) => (relations[child] = parent));

    const calc_steps = (node: string): number => {
        return !relations[node] ? 0 : 1 + calc_steps(relations[node]);
    };

    return sum([...Object.keys(relations)].map(calc_steps));
}

console.assert(calc_orbits('./t1') === 42, "t1 doesn't match 42");
console.log(calc_orbits('./input'));
