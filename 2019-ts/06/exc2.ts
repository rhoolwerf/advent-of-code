import { readFileSync } from 'fs';
import { EOL } from 'os';
import { sum } from '../util/util';

function calc_orbits(file: string): number {
    const input = readFileSync(file, 'utf8')
        .split(EOL)
        .map((star) =>
            [...star.matchAll(/([A-Z0-9]+)\)([A-Z0-9]+)/g)].map(([_, parent, child]) => [parent, child]).flat()
        );

    const relations: { [key: string]: string } = {};
    input.forEach(([parent, child]) => (relations[child] = parent));

    const get_path_to_root = (node: string): string => {
        return !relations[node] ? node : `${get_path_to_root(relations[node])}-${node}`;
    };

    const YOU = get_path_to_root('YOU').split('-');
    const SAN = get_path_to_root('SAN').split('-');

    while (YOU[0] === SAN[0]) {
        YOU.shift();
        SAN.shift();
    }

    return YOU.length + SAN.length - 2;
}

console.assert(calc_orbits('./t2') === 4, 't2 fails, instead: ' + calc_orbits('./t2'));
console.log(calc_orbits('./input'));
