import { readFileSync } from 'fs';
import { Processor } from '../util/opcode_processor';

function test(input: string, expected: string): void {
    const proc = new Processor(input);
    proc.run_once();

    const actual = proc.instructions.join(',');
    console.assert(expected === actual, '', { input, expected, actual });
}

test('1,0,0,0,99', '2,0,0,0,99');
test('2,3,0,3,99', '2,3,0,6,99');
test('2,4,4,5,99,0', '2,4,4,5,99,9801');
test('1,1,1,4,99,5,6,0,99', '30,1,1,4,2,5,6,0,99');

const input = readFileSync('./input', 'utf8').split(',').map(Number);
input[1] = 12;
input[2] = 2;
const proc = new Processor(input);
proc.run_once();
console.log(proc.instructions[0]);
