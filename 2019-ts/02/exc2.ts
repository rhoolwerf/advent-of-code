import { readFileSync } from 'fs';
import { Processor } from '../util/opcode_processor';

function test(input: string, expected: string): void {
    const proc = new Processor(input);
    proc.run_once();

    const actual = proc.instructions.join(',');
    console.assert(expected === actual, '', { input, expected, actual });
}

test('1,0,0,0,99', '2,0,0,0,99');
test('2,3,0,3,99', '2,3,0,6,99');
test('2,4,4,5,99,0', '2,4,4,5,99,9801');
test('1,1,1,4,99,5,6,0,99', '30,1,1,4,2,5,6,0,99');

const input = readFileSync('./input', 'utf8').split(',').map(Number);

function run(noun: number, verb: number): number {
    input[1] = noun;
    input[2] = verb;
    const proc = new Processor(input);
    proc.run_once();
    return proc.instructions[0];
}

for (let noun = 0; noun <= 99; noun++) {
    for (let verb = 0; verb <= 99; verb++) {
        const result = run(noun, verb);
        if (result === 19690720) {
            console.log('Result found:', { noun, verb, answer: 100 * noun + verb });
        }
    }
}
