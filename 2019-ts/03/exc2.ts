import { EOL } from 'os';
import { sum, get_intersections } from '../util/util';
import { readFileSync } from 'fs';

type Coordinate = string;

const DIR = {
    D: { dirY: 1, dirX: 0 },
    U: { dirY: -1, dirX: 0 },
    L: { dirY: 0, dirX: -1 },
    R: { dirY: 0, dirX: 1 },
};

function run(input: string): number {
    const wires = input
        .split(EOL)
        .map((wire) => [...wire.matchAll(/([DRUL])(\d+)/g)].map(([_, dir, steps]) => ({ dir, steps: Number(steps) })));

    const points: [Map<Coordinate, number>, Map<Coordinate, number>] = [new Map(), new Map()];
    for (let i = 0; i < wires.length; i++) {
        const wire = wires[i];
        const pos = { y: 0, x: 0 };
        let totalSteps = 0;

        for (let { dir, steps } of wire) {
            const { dirY, dirX } = DIR[dir];
            while (steps-- > 0) {
                pos.y += dirY;
                pos.x += dirX;
                points[i].set([pos.y, pos.x].join(','), ++totalSteps);
            }
        }
    }

    const coords = points.map((m) => [...m.keys()]);
    const intersections = get_intersections(coords[0], coords[1]);
    const interSteps = intersections
        .map((c) => ({ c, l: points[0].get(c), r: points[1].get(c) }))
        .map(({ c, l, r }) => ({ c, r, l, steps: l + r }));
    return Math.min(...interSteps.map(({ steps }) => steps));
}

function test(wire1: string, wire2: string, expected: number): void {
    const actual = run(`${wire1}${EOL}${wire2}`);
    console.assert(expected === actual, '', { wire1, wire2, expected, actual });
}

test('R8,U5,L5,D3', 'U7,R6,D4,L4', 30);
test('R75,D30,R83,U83,L12,D49,R71,U7,L72', 'U62,R66,U55,R34,D71,R55,D58,R83', 610);
test('R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51', 'U98,R91,D20,R16,D67,R40,U7,R15,U6,R7', 410);

console.log(run(readFileSync('./input', 'utf8')));
