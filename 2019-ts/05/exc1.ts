import { readFileSync } from 'fs';
import { Processor } from '../util/opcode_processor';

function test(input: string, expected: string): void {
    const actual = new Processor(input).run_till_halt().instructions.join(',');
    console.assert(expected === actual, '', { input, expected, actual });
}

test('1002,4,3,4,33', '1002,4,3,4,99');
test('1101,100,-1,4,0', '1101,100,-1,4,99');

console.log(new Processor(readFileSync('./input', 'utf8'), { input: [1] }).run_till_halt().output.pop());
