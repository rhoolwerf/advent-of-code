import { readFileSync } from 'fs';
import { Processor } from '../util/opcode_processor';

function test(instr: string, input: string, expected: string): void {
    const actual = new Processor(instr, { input }).run_once().output.join(',');
    console.assert(expected === actual, '', { instr, input, expected, actual });
}

// Equals 8 - Position mode
test('3,9,8,9,10,9,4,9,99,-1,8', '8', '1');
test('3,9,8,9,10,9,4,9,99,-1,8', '7', '0');
test('3,9,8,9,10,9,4,9,99,-1,8', '9', '0');

// Less than 8 - Position mode
test('3,9,7,9,10,9,4,9,99,-1,8', '8', '0');
test('3,9,7,9,10,9,4,9,99,-1,8', '7', '1');

// Equals 8 - Immediate mode
test('3,3,1108,-1,8,3,4,3,99', '8', '1');
test('3,3,1108,-1,8,3,4,3,99', '7', '0');
test('3,3,1108,-1,8,3,4,3,99', '9', '0');

// Less than 8 - Position mode
test('3,3,1107,-1,8,3,4,3,99', '8', '0');
test('3,3,1107,-1,8,3,4,3,99', '7', '1');

// Jump - Position
test('3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9', '0', '0');
test('3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9', '1337', '1');

// Jump - Immediate
test('3,3,1105,-1,9,1101,0,0,12,4,12,99,1', '0', '0');
test('3,3,1105,-1,9,1101,0,0,12,4,12,99,1', '1337', '1');

// More complex
const complex_instructions =
    '3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99';
test(complex_instructions, '7', '999');
test(complex_instructions, '8', '1000');
test(complex_instructions, '9', '1001');

// Actual puzzle
const proc = new Processor(readFileSync('./input', 'utf8'), { input: '5' }).run_once();
console.log(proc.output);
