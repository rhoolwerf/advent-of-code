import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';

type Config = { aX: number; aY: number; bX: number; bY: number; dX: number; dY: number };
function parse_file(filename: string): Array<Config> {
    return readFileSync(join(__dirname, filename), 'utf8')
        .split(`${EOL}${EOL}`)
        .map((b) => parse_input(b));
}
function parse_input(input: string): Config {
    const [aX, aY, bX, bY, dX, dY] = [...input.matchAll(/(\d+)/g)].map(([match]) => Number(match));
    return { aX, aY, bX, bY, dX, dY };
}

function calc_cost({ aX, aY, bX, bY, dX, dY }: Config): number {
    // Formula 1: a = (dX/aX) - (bX/aX)*b; -> of ook (dX - b * bX) / aX
    // const b = 40;
    // const a = (dX - b * bX) / aX;
    // console.log({ a, b });

    // Lots of help from ChatGPT how to resolve, as my algebra was too rusty
    const b = (aX * dY - aY * dX) / (aX * bY - aY * bX);
    const a = (dX - b * bX) / aX;
    return Math.floor(b) !== b || Math.floor(a) !== a ? 0 : a * 3 + b;
}

// // Poging 1, bruteforce-ish
// function calc_cost({ aX, aY, bX, bY, dX, dY }: Config): number {
//     const possible: Array<{ a: number; b: number; cost: number }> = [];
//     for (let a = 0; a * aX <= dX; a++) {
//         for (let b = 0; a * aX + b * bX <= dX; b++) {
//             const posX = a * aX + b * bX;
//             const posY = a * aY + b * bY;
//             if (posX === dX && posY === dY) {
//                 possible.push({ a, b, cost: a * 3 + b });
//             }
//         }
//     }
//     const [cheapest] = possible.sort(({ cost: a }, { cost: b }) => a - b);
//     console.log({ cheapest });
//     return cheapest?.cost || 0;
// }

function test_calc_cost(filename: string, expected: number) {
    const actual = parse_file(filename).reduce((sum, conf) => sum + calc_cost(conf), 0);
    console.assert(expected === actual, '', { filename, expected, actual });
}

test_calc_cost('t1', 480);
test_calc_cost('t2', 280);
test_calc_cost('t3', 0);
test_calc_cost('t4', 200);
test_calc_cost('t5', 0);
console.log(parse_file('input').reduce((sum, conf) => sum + calc_cost(conf), 0));
console.log('Done');
