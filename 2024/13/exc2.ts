import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';

type Config = { aX: number; aY: number; bX: number; bY: number; dX: number; dY: number };
function parse_file(filename: string): Array<Config> {
    return readFileSync(join(__dirname, filename), 'utf8')
        .split(`${EOL}${EOL}`)
        .map((b) => parse_input(b));
}
function parse_input(input: string): Config {
    const [aX, aY, bX, bY, dX, dY] = [...input.matchAll(/(\d+)/g)].map(([match]) => Number(match));
    return { aX, aY, bX, bY, dX: dX + 10000000000000, dY: dY + 10000000000000 };
}

function calc_cost({ aX, aY, bX, bY, dX, dY }: Config): number {
    // Lots of help from ChatGPT how to resolve, as my algebra was too rusty
    const b = (aX * dY - aY * dX) / (aX * bY - aY * bX);
    const a = (dX - b * bX) / aX;
    return Math.floor(b) !== b || Math.floor(a) !== a ? 0 : a * 3 + b;
}

console.log(parse_file('input').reduce((sum, conf) => sum + calc_cost(conf), 0));
console.log('Done');
