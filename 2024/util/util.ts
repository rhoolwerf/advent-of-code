export function sum(input: Array<number>): number {
    return input.reduce((sum, val) => sum + val, 0);
}

export function mul(input: Array<number>): number {
    return input.reduce((acc, val) => acc * val, 1);
}

export function delay(duration: number = 500) {
    const stop = performance.now() + duration;
    while (performance.now() < stop);
}

export function get_intersections<T>(left: Array<T>, right: Array<T>): Array<T> {
    const setB = new Set<T>(right);
    return [...new Set<T>(left)].filter((v) => setB.has(v));
}

export function permutator<T>(inputArr: Array<T>): Array<Array<T>> {
    let result: Array<Array<T>> = [];

    function permute(arr: Array<T>, m: Array<T> = []) {
        if (arr.length === 0) {
            result.push(m);
        } else {
            for (let i = 0; i < arr.length; i++) {
                let curr = arr.slice();
                let next = curr.splice(i, 1);
                permute(curr.slice(), m.concat(next));
            }
        }
    }

    permute(inputArr);

    return result;
}
