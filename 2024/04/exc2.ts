import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';

function check(grid: Array<Array<string>>, y: number, x: number): boolean {
    const cell = (j: number, i: number): string => (grid[j] && grid[j][i]) || '.';

    if (
        (cell(y - 1, x - 1) === 'M' && cell(y + 1, x + 1) === 'S') ||
        (cell(y - 1, x - 1) === 'S' && cell(y + 1, x + 1) === 'M')
    ) {
    } else {
        return false;
    }

    if (
        (cell(y + 1, x - 1) === 'M' && cell(y - 1, x + 1) === 'S') ||
        (cell(y + 1, x - 1) === 'S' && cell(y - 1, x + 1) === 'M')
    ) {
    } else {
        return false;
    }

    return true;
}

function count(filename: string): number {
    const grid = readFileSync(join(__dirname, filename), 'utf8')
        .split(EOL)
        .map((r) => r.split(''));

    let sum = 0;
    for (let y = 0; y < grid.length; y++) {
        for (let x = 0; x < grid[y].length; x++) {
            if (grid[y][x] === 'A' && check(grid, y, x)) sum++;
        }
    }

    return sum;
}

function test(filename: string, expected: number) {
    const actual = count(filename);
    console.assert(expected === actual, '', { expected, actual });
}

test('t1', 9);
console.log(count('input'));
