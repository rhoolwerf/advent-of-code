import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';

function check(grid: Array<Array<string>>, y: number, x: number, offsetY: number, offsetX: number): boolean {
    return [
        grid[y + 1 * offsetY] && grid[y + 1 * offsetY][x + 1 * offsetX] == 'M',
        grid[y + 2 * offsetY] && grid[y + 2 * offsetY][x + 2 * offsetX] == 'A',
        grid[y + 3 * offsetY] && grid[y + 3 * offsetY][x + 3 * offsetX] == 'S',
    ].every(Boolean);
}

function count(filename: string): number {
    const grid = readFileSync(join(__dirname, filename), 'utf8')
        .split(EOL)
        .map((r) => r.split(''));

    let sum = 0;
    // const y = 9;
    // const x = 1;
    for (let y = 0; y < grid.length; y++) {
        for (let x = 0; x < grid[y].length; x++) {
            if (grid[y][x] === 'X') {
                const directions = [
                    // Up
                    check(grid, y, x, -1, 0),
                    // Up-Right
                    check(grid, y, x, -1, 1),
                    // Right
                    check(grid, y, x, 0, 1),
                    // Down-right
                    check(grid, y, x, 1, 1),
                    // Down
                    check(grid, y, x, 1, 0),
                    // Down-left
                    check(grid, y, x, 1, -1),
                    // Left
                    check(grid, y, x, 0, -1),
                    // Up-left
                    check(grid, y, x, -1, -1),
                ];
                sum += directions.filter(Boolean).length;
            }
        }
    }

    return sum;
}

function test(filename: string, expected: number) {
    const actual = count(filename);
    console.assert(expected === actual, '', { expected, actual });
}

test('t1', 18);
console.log(count('input'));
