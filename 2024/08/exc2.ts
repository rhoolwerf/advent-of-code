import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';

type Position = { y: number; x: number };

type Antenna = {
    name: string;
    positions: Array<Position>;
};

function count_antinodes(filename: string): number {
    // Parse into a grid
    const grid = readFileSync(join(__dirname, filename), 'utf8')
        .split(EOL)
        .map((r) => r.split(''));

    // Find all antennas
    const antennas: Array<Antenna> = [];
    for (let y = 0; y < grid.length; y++) {
        for (let x = 0; x < grid[0].length; x++) {
            if (grid[y][x] !== '.') {
                let antenna = antennas.find(({ name }) => name === grid[y][x]);
                if (!antenna) {
                    antenna = { name: grid[y][x], positions: [] };
                    antennas.push(antenna);
                }
                antenna.positions.push({ y, x });
            }
        }
    }

    // Calculate all antinodes
    const antinodes = new Set<string>();
    for (const { positions } of antennas) {
        for (let i = 0; i < positions.length; i++) {
            for (let j = i + 1; j < positions.length; j++) {
                const p1 = positions[i];
                const p2 = positions[j];

                // Calculate the offset
                const dY = p1.y - p2.y;
                const dX = p1.x - p2.x;

                // Determine all antinodes
                for (let y = p1.y, x = p1.x; grid[y] && grid[y][x]; y += dY, x += dX) {
                    antinodes.add(`${y}|${x}`);
                }

                for (let y = p2.y, x = p2.x; grid[y] && grid[y][x]; y -= dY, x -= dX) {
                    antinodes.add(`${y}|${x}`);
                }
            }
        }
    }

    return antinodes.size;
}

function display(grid: Array<Array<string>>, antinodes: Set<string>) {
    console.log(grid.map((r, y) => r.map((c, x) => (antinodes.has(`${y}|${x}`) ? '#' : c)).join('')).join(EOL));
}

function test(filename: string, expected: number) {
    const actual = count_antinodes(filename);
    console.assert(expected === actual, '', { filename, expected, actual });
}

test('t4', 9);
test('t1', 34);
console.log(count_antinodes('input'));
