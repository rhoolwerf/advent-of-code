import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';

type Position = { y: number; x: number };

type Antenna = {
    name: string;
    positions: Array<Position>;
};

function count_antinodes(filename: string): number {
    // Parse into a grid
    const grid = readFileSync(join(__dirname, filename), 'utf8')
        .split(EOL)
        .map((r) => r.split(''));

    // Find all antennas
    const antennas: Array<Antenna> = [];
    for (let y = 0; y < grid.length; y++) {
        for (let x = 0; x < grid[0].length; x++) {
            if (grid[y][x] !== '.') {
                let antenna = antennas.find(({ name }) => name === grid[y][x]);
                if (!antenna) {
                    antenna = { name: grid[y][x], positions: [] };
                    antennas.push(antenna);
                }
                antenna.positions.push({ y, x });
            }
        }
    }

    // Calculate all antinodes
    const antinodes = new Set<string>();
    for (const { positions } of antennas) {
        for (let i = 0; i < positions.length; i++) {
            for (let j = i + 1; j < positions.length; j++) {
                // Calculate the offset
                const dY = positions[i].y - positions[j].y;
                const dX = positions[i].x - positions[j].x;

                if (grid[positions[i].y + dY] && grid[positions[i].y + dY][positions[i].x + dX]) {
                    antinodes.add(`${positions[i].y + dY}|${positions[i].x + dX}`);
                }

                if (grid[positions[j].y - dY] && grid[positions[j].y - dY][positions[j].x - dX]) {
                    antinodes.add(`${positions[j].y - dY}|${positions[j].x - dX}`);
                }
            }
        }
    }

    return antinodes.size;
}

function display(grid: Array<Array<string>>, antinodes: Set<string>) {
    console.log(grid.map((r, y) => r.map((c, x) => (antinodes.has(`${y}|${x}`) ? '#' : c)).join('')).join(EOL));
}

function test(filename: string, expected: number) {
    const actual = count_antinodes(filename);
    console.assert(expected === actual, '', { filename, expected, actual });
}

test('t1', 14);
test('t2', 2);
test('t3', 4);
console.log(count_antinodes('input'));
