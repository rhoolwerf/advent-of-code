import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';
import { coord2string, Coordinate, Coordinates } from '../graph/pogingen/util';
import kleur = require('kleur');

const directions = [
    [-1, 0],
    [0, 1],
    [1, 0],
    [0, -1],
];

function bfs(grid: Array<Array<string>>, start: Coordinate, end: Coordinate): Coordinates {
    const queue: Array<[Coordinate, Coordinates]> = [[start, []]];
    const visited: Set<string> = new Set();

    while (queue.length) {
        const [current, path] = queue.shift();

        // Are we at the end?
        if (current.y === end.y && current.x === end.x) return path.concat(current);

        // Register where we are
        visited.add(coord2string(current));

        for (const [offsetY, offsetX] of directions) {
            const nY = current.y + offsetY;
            const nX = current.x + offsetX;

            // Have we been here before?
            if (visited.has(coord2string({ y: nY, x: nX }))) continue;

            // Can we go there?
            if (grid[nY][nX] === '#') continue;

            // Add to the queue
            queue.push([{ y: nY, x: nX }, path.concat(current)]);
        }
    }

    return [];
}

function display(grid: Array<Array<string>>, start: Coordinate, end: Coordinate, path: Coordinates) {
    for (let y = 0; y < grid.length; y++) {
        const row: Array<string> = [];
        for (let x = 0; x < grid[0].length; x++) {
            let cell = kleur.gray('.');
            if (y === start.y && x === start.x) cell = kleur.red('S');
            else if (y === end.y && x === end.x) cell = kleur.red('E');
            else if (path.some((p) => p.y === y && p.x === x)) cell = kleur.blue('O');
            row.push(cell);
        }
        console.log(row.join(''));
    }
}

function run(filename: string): Array<number> {
    const grid = readFileSync(join(__dirname, filename), 'utf8')
        .split(EOL)
        .map((r) => r.split(''));
    const [start] = grid
        .map((r, y) => r.map((c, x) => (c === 'S' ? { y, x } : undefined)))
        .flat()
        .filter(Boolean);
    const [end] = grid
        .map((r, y) => r.map((c, x) => (c === 'E' ? { y, x } : undefined)))
        .flat()
        .filter(Boolean);

    const walls = grid
        .map((r, y) => r.map((c, x) => (c === '#' ? { y, x } : undefined)))
        .flat()
        .filter(Boolean);

    // Find any straight-through paths
    const possible: Array<{ wall: Coordinate; paths: Array<Coordinate> }> = [];
    const valid = ['.', 'S', 'E'];
    for (const wall of walls) {
        if (valid.includes(grid[wall.y - 1]?.[wall.x]) && valid.includes(grid[wall.y + 1]?.[wall.x])) {
            possible.push({
                wall,
                paths: [
                    { y: wall.y - 1, x: wall.x },
                    { y: wall.y + 1, x: wall.x },
                ],
            });
        }
        if (valid.includes(grid[wall.y]?.[wall.x - 1]) && valid.includes(grid[wall.y]?.[wall.x + 1])) {
            possible.push({
                wall,
                paths: [
                    { y: wall.y, x: wall.x - 1 },
                    { y: wall.y, x: wall.x + 1 },
                ],
            });
        }
    }

    // Find initial route (is BFS really the best solution here?!)
    const basic = bfs(grid, start, end);
    // display(grid, start, end, basic);

    const saved = possible
        .map(({ wall, paths: [p1, p2] }) => {
            // Find place in path so substitute to
            const p1I = basic.findIndex((b) => b.y === p1.y && b.x === p1.x);
            const p2I = basic.findIndex((b) => b.y === p2.y && b.x === p2.x);

            const i1 = Math.min(p1I, p2I);
            const i2 = Math.max(p1I, p2I);

            const cheated = [...basic.slice(0, i1 + 1), wall, ...basic.slice(i2)];
            return basic.length - cheated.length;
        })
        .sort((a, b) => a - b)
        .filter(Boolean);

    return saved;
}

function time(filename: string): Array<number> {
    console.time(filename);
    const result = run(filename);
    console.timeEnd(filename);
    return result;
}

console.log(
    time('t1').reduce((count, value) => {
        count[value] = (count[value] || 0) + 1;
        return count;
    }, {})
);
console.log(time('input').filter((v) => v >= 100).length);
