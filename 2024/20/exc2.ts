import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';
import { coord2string, Coordinate, Coordinates } from '../graph/pogingen/util';
import kleur = require('kleur');

const directions = [
    [-1, 0],
    [0, 1],
    [1, 0],
    [0, -1],
];

function bfs(grid: Array<Array<string>>, start: Coordinate, end: Coordinate): Coordinates {
    const queue: Array<[Coordinate, Coordinates]> = [[start, []]];
    const visited: Set<string> = new Set();

    while (queue.length) {
        const [current, path] = queue.shift();

        // Are we at the end?
        if (current.y === end.y && current.x === end.x) return path.concat(current);

        // Register where we are
        visited.add(coord2string(current));

        for (const [offsetY, offsetX] of directions) {
            const nY = current.y + offsetY;
            const nX = current.x + offsetX;

            // Have we been here before?
            if (visited.has(coord2string({ y: nY, x: nX }))) continue;

            // Can we go there?
            if (grid[nY][nX] === '#') continue;

            // Add to the queue
            queue.push([{ y: nY, x: nX }, path.concat(current)]);
        }
    }

    return [];
}

function display(grid: Array<Array<string>>, start: Coordinate, end: Coordinate, path: Coordinates) {
    for (let y = 0; y < grid.length; y++) {
        const row: Array<string> = [];
        for (let x = 0; x < grid[0].length; x++) {
            let cell = kleur.gray('.');
            if (y === start.y && x === start.x) cell = kleur.red('S');
            else if (y === end.y && x === end.x) cell = kleur.red('E');
            else if (path.some((p) => p.y === y && p.x === x)) cell = kleur.blue('O');
            row.push(cell);
        }
        console.log(row.join(''));
    }
}

function run(filename: string): Array<number> {
    const grid = readFileSync(join(__dirname, filename), 'utf8')
        .split(EOL)
        .map((r) => r.split(''));
    const [start] = grid
        .map((r, y) => r.map((c, x) => (c === 'S' ? { y, x } : undefined)))
        .flat()
        .filter(Boolean);
    const [end] = grid
        .map((r, y) => r.map((c, x) => (c === 'E' ? { y, x } : undefined)))
        .flat()
        .filter(Boolean);

    // Find the default route
    const basic = bfs(grid, start, end);

    // For each couple of coordinates, can we consider it a valid cheat?
    const saved: Array<number> = [];
    for (let i = 0; i < basic.length; i++) {
        for (let j = i + 1; j < basic.length; j++) {
            // const i = 0;
            // const j = basic.length - 3;
            const p1 = basic[i];
            const p2 = basic[j];
            const manhattan = Math.abs(p1.y - p2.y) + Math.abs(p1.x - p2.x);

            if (manhattan <= 20) {
                const cheatedLength = manhattan + i + (basic.length - j);
                const savedLength = basic.length - cheatedLength;
                saved.push(savedLength);
            }
        }
    }

    return saved.filter(Boolean);
}

function time(filename: string): Array<number> {
    console.time(filename);
    const result = run(filename);
    console.timeEnd(filename);
    return result;
}

console.log(
    time('t1')
        .filter((v) => v >= 50)
        .reduce((count, value) => {
            count[value] = (count[value] || 0) + 1;
            return count;
        }, {})
);
console.log(time('input').filter((v) => v >= 100).length);
