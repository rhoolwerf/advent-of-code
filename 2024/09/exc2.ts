import { readFileSync } from 'fs';
import { join } from 'path';

type Disk = Array<number | undefined>;

// Map input to disk
function map_input_to_disk(input: string): Disk {
    return input
        .split('')
        .map((d, i) => ({ length: Number(d), id: i % 2 === 0 ? i / 2 : undefined }))
        .filter(({ length }) => length > 0)
        .map(({ length, id }) => Array.from({ length }, () => id))
        .flat();
}

function test_map(input: string, expected: string) {
    const actual = map_input_to_disk(input)
        .flat()
        .map((d) => (typeof d === 'number' ? d : '.'))
        .join('');
    console.assert(expected === actual, 'test_map', { input, expected, actual });
}

test_map('12345', '0..111....22222');
test_map('2333133121414131402', '00...111...2...333.44.5555.6666.777.888899');

// Defrag disk
function defrag(disk: Disk): Disk {
    const defragged: Disk = Array.from(disk);

    for (let block_end = defragged.length - 1; block_end >= 0; ) {
        if (typeof defragged[block_end] === 'number') {
            // Get all relevant information of this block
            const block_id = defragged[block_end];
            const block_length = defragged.filter((d) => d === block_id).length;
            const block_start = block_end - block_length + 1;
            block_end -= block_length;

            // Find a suitable empty block
            let empty_start = 0;
            let empty_length = 0;
            let suitable_found = false;

            while (!suitable_found && empty_start < block_start) {
                while (typeof defragged[empty_start] === 'number' && empty_start < block_start) empty_start++;
                let empty_end = empty_start + 1;
                while (typeof defragged[empty_end] !== 'number' && empty_end < defragged.length) empty_end++;

                empty_length = empty_end - empty_start;
                if (empty_length >= block_length && empty_start < block_start) suitable_found = true;
                else empty_start = empty_end;
            }

            if (suitable_found) {
                for (let i = 0; i < block_length; i++) {
                    defragged[empty_start + i] = block_id;
                    defragged[block_start + i] = undefined;
                }
            }
        } else {
            block_end--;
        }
    }

    return defragged;
}

function test_defrag(input: string, expected: string) {
    const actual = defrag(map_input_to_disk(input))
        .map((d) => (typeof d === 'number' ? d : '.'))
        .join('');

    console.assert(expected === actual, 'test_defrag', { input, expected, actual });
}

test_defrag('12345', '0..111....22222');
test_defrag('2333133121414131402', '00992111777.44.333....5555.6666.....8888..');

// Checksum
function checksum(disk: Disk): number {
    return disk
        .map((d, i) => d * i)
        .filter((d) => !isNaN(d))
        .reduce((sum, val) => sum + val, 0);
}

function test_checksum(input: string, expected: number) {
    const actual = checksum(defrag(map_input_to_disk(input)));
    console.assert(expected === actual, 'test_checksum', { input, expected, actual });
}

test_checksum('2333133121414131402', 2858);
console.time('Puzzle input...');
console.log(checksum(defrag(map_input_to_disk(readFileSync(join(__dirname, 'input'), 'utf8')))));
console.timeEnd('Puzzle input...');
