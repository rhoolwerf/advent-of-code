import { readFileSync } from 'fs';
import { join } from 'path';

type Disk = Array<number | undefined>;

// Map input to disk
function map_input_to_disk(input: string): Disk {
    return input
        .split('')
        .map((d, i) => ({ length: Number(d), id: i % 2 === 0 ? i / 2 : undefined }))
        .map(({ length, id }) => Array.from({ length }, () => id))
        .flat();
}

function test_map(input: string, expected: string) {
    const actual = map_input_to_disk(input)
        .map((d) => (typeof d === 'number' ? d : '.'))
        .join('');
    console.assert(expected === actual, '', { input, expected, actual });
}

test_map('12345', '0..111....22222');
test_map('2333133121414131402', '00...111...2...333.44.5555.6666.777.888899');

// Defrag disk
function defrag(disk: Disk): Disk {
    const defragged: Disk = Array.from(disk);

    for (let i = 0; i < defragged.length; i++) {
        if (typeof defragged[i] !== 'number') {
            for (let j = defragged.length - 1; j > i; j--) {
                if (typeof defragged[j] === 'number') {
                    defragged[i] = defragged[j];
                    defragged[j] = undefined;
                    break;
                }
            }
        }
    }

    return defragged;
}

function test_defrag(input: string, expected: string) {
    const actual = defrag(map_input_to_disk(input))
        .map((d) => (typeof d === 'number' ? d : '.'))
        .join('');

    console.assert(expected === actual, '', { input, expected, actual });
}

test_defrag('12345', '022111222......');
test_defrag('2333133121414131402', '0099811188827773336446555566..............');

// Checksum
function checksum(disk: Disk): number {
    return disk
        .map((d, i) => d * i)
        .filter((d) => !isNaN(d))
        .reduce((sum, val) => sum + val, 0);
}

function test_checksum(input: string, expected: number) {
    const actual = checksum(defrag(map_input_to_disk(input)));
    console.assert(expected === actual, '', { input, expected, actual });
}

test_checksum('2333133121414131402', 1928);
console.log(checksum(defrag(map_input_to_disk(readFileSync(join(__dirname, 'input'), 'utf8')))));
