import { readFileSync } from 'fs';
import { EOL } from 'os';

// const left: Array<number> = [];
// const right: Array<number> = [];
// const input = readFileSync(process.argv[2], 'utf8').split(EOL);
// input.forEach((row) => {
//     const [v1, v2] = row.split(/\s+/).map(Number);
//     left.push(v1);
//     right.push(v2);
// });

// let sum = 0;
// for (let i = 0; i < left.length; i++) {
//     const v1 = left[i];
//     let score = 0;

//     for (let j = 0; j < right.length; j++) {
//         const v2 = right[j];
//         if (v1 === v2) {
//             score += v1;
//         }
//     }
//     sum += score;
// }
// console.log({ sum });

const left: Array<number> = [];
const right: Array<number> = [];
readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((row) => row.split(/\s+/).map(Number))
    .forEach(([v1, v2]) => {
        left.push(v1);
        right.push(v2);
    });

const score = left.map((v1) => right.filter((v2) => v1 === v2).length * v1).reduce((sum, value) => sum + value, 0);
console.log({ score });
