import { readFileSync } from 'fs';
import { EOL } from 'os';

const left: Array<number> = [];
const right: Array<number> = [];
const input = readFileSync(process.argv[2], 'utf8').split(EOL);
input.forEach((row) => {
    const [v1, v2] = row.split(/\s+/).map(Number);
    left.push(v1);
    right.push(v2);
});

left.sort((a, b) => a - b);
right.sort((a, b) => a - b);

let sum = 0;
for (let i = 0; i < left.length; i++) {
    const v1 = left[i];
    const v2 = right[i];
    const diff = Math.abs(v2 - v1);
    sum += diff;
}

console.log({ sum });
