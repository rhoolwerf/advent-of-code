import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';

const UP = [-1, 0];
const RIGHT = [0, 1];
const DOWN = [1, 0];
const LEFT = [0, -1];

// Literal string build is a lot quicker than [].join('')
const key = (h: number, i: number, j: number, k: number) => `${h}|${i}|${j}|${k}`;

function count_loops(filename: string): number {
    // Parse input
    const input = readFileSync(join(__dirname, filename), 'utf8');
    const grid = input.split(EOL).map((r) => r.split(''));

    // Get starting position
    const flat = grid.map((r) => r.join('')).join('');
    const start = [Math.floor(flat.indexOf('^') / grid.length), flat.indexOf('^') % grid.length];

    // Parse once and get all possible positions to block
    const { visited } = walk_grid(grid, start);
    const unique = new Set(Array.from(visited).map((c) => c.split('|').slice(0, 2).join('|')));
    const positions = Array.from(unique).map((c) => c.split('|').map(Number));

    // For each possible position, check if blocking it causes a loop
    let count = 0;
    for (const [y, x] of positions) {
        // const copy = JSON.parse(JSON.stringify(grid)) as Array<Array<string>>;
        const copy = grid.map((r) => [...r]); // This method is a lot cheaper and achieves the same
        copy[y][x] = '#';
        
        const result = walk_grid(copy, start);
        if (result.loops) count++;
    }

    return count;
}

function walk_grid(grid: Array<Array<string>>, start: Array<number>): { visited: Set<string>; loops: boolean } {
    const visited = new Set<string>();

    let [y, x] = start;
    let dir = UP;

    // As long as we've not reached outside the grid...
    while (grid[y] && grid[y][x]) {
        const [offsetY, offsetX] = dir;

        // Have we been here before in the same orientation?
        const check = key(y, x, offsetY, offsetX);
        if (visited.has(check)) return { visited, loops: true };

        // Register where we are at now
        visited.add(check);

        // Peek at the next spot, would that be a blocking stone?
        if (grid[y + offsetY] && grid[y + offsetY][x + offsetX] === '#') {
            // Blocking stone, turn right and try again
            if (dir === UP) dir = RIGHT;
            else if (dir === RIGHT) dir = DOWN;
            else if (dir === DOWN) dir = LEFT;
            else if (dir === LEFT) dir = UP;

            // As we could be in a corner, don't automatically step forward but repeat check logic
        } else {
            // No blocking stone, we can safely move forward
            y += offsetY;
            x += offsetX;
        }
    }

    // We've gone outside, so we're not looping
    return { visited, loops: false };
}

function display(grid: Array<Array<string>>, visited: Set<string>) {
    const get_char = (y: number, x: number): string => {
        if (visited.has(key(y, x, UP[0], UP[1]))) return '^';
        if (visited.has(key(y, x, RIGHT[0], RIGHT[1]))) return '>';
        if (visited.has(key(y, x, DOWN[0], DOWN[1]))) return 'v';
        if (visited.has(key(y, x, LEFT[0], LEFT[1]))) return '<';
        return grid[y][x];
    };

    console.log(grid.map((row, y) => row.map((_, x) => get_char(y, x)).join('')).join(EOL));
}

function test(filename: string, expected: number) {
    const actual = count_loops(filename);
    console.assert(expected === actual, '', { filename, expected, actual });
}

test('t1', 6);
console.log(count_loops('input'));
