import { dir } from 'console';
import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';

const UP = [-1, 0];
const RIGHT = [0, 1];
const DOWN = [1, 0];
const LEFT = [0, -1];

function calc_visited(filename: string): number {
    const visited = new Set<string>();
    const key = (j: number, i: number) => [j, i].join('|');

    // Parse input
    const input = readFileSync(join(__dirname, filename), 'utf8');
    const grid = input.split(EOL).map((r) => r.split(''));
    const flat = grid.map((r) => r.join('')).join('');

    // Get starting position
    let y = Math.floor(flat.indexOf('^') / grid.length);
    let x = flat.indexOf('^') % grid.length;
    let dir = UP;

    // As long as we've not reached outside the grid...
    while (grid[y] && grid[y][x]) {
        // Register where we are at now
        visited.add(key(y, x));

        // Peek at the next spot, would that be a blocking stone?
        let [offsetY, offsetX] = dir;
        if (grid[y + offsetY] && grid[y + offsetY][x + offsetX] === '#') {
            // Turn right
            if (dir === UP) dir = RIGHT;
            else if (dir === RIGHT) dir = DOWN;
            else if (dir === DOWN) dir = LEFT;
            else if (dir === LEFT) dir = UP;

            // Register new direction
            [offsetY, offsetX] = dir;
        }

        y += offsetY;
        x += offsetX;
    }

    return visited.size;
}

function display(grid: Array<Array<string>>, visited: Set<string>) {
    console.log(
        grid.map((row, y) => row.map((cell, x) => (visited.has([y, x].join('|')) ? 'X' : cell)).join('')).join(EOL)
    );
}

function test(filename: string, expected: number) {
    const actual = calc_visited(filename);
    console.assert(expected === actual, '', { filename, expected, actual });
}

test('t1', 41);
console.log(calc_visited('input'));
