import { Worker, isMainThread, parentPort, workerData } from 'worker_threads';

type Data = {
    msg: string;
    nr: number;
};

if (isMainThread) {
    const w = new Worker(__filename, {
        workerData: { msg: 'Hello World!', nr: 12 },
        execArgv: ['-r', 'ts-node/register'],
    });
    w.on('message', (msg) => console.log('Main thread received:', { msg }));
} else {
    const data = workerData as Data;
    console.log('Client received:', data.nr);
    parentPort?.postMessage({ test: 'Message!', number: 42 });
}
