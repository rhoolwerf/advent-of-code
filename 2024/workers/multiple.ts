import { Worker, isMainThread, parentPort, workerData } from 'worker_threads';

type Data = {
    msg: string;
    nr: number;
};

if (isMainThread) {
    console.log('Making promises');
    const promises = Array.from(
        { length: 5 },
        () =>
            new Promise((resolve, reject) => {
                const w = new Worker(__filename, { execArgv: ['-r', 'ts-node/register'] });
                w.on('message', (msg) => resolve(msg));
                w.on('error', (error) => reject(error));
            })
    );

    console.log('Awaiting them all...');
    console.time('Duration...');
    Promise.all(promises).then((results) => {
        console.log({ results });
        console.timeEnd('Duration...');
    });
} else {
    const delay = Math.random() * 1000;
    setTimeout(() => {
        parentPort?.postMessage({ delay });
    }, delay);
}
