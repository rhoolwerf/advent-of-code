import { sum } from '../util/util';

type Cell = number | Array<Cell>;

const cache = new Map<string, number>();
function process_array(input: Cell, depth: number): number {
    if (depth < 0) return 1;

    if (Array.isArray(input)) {
        let sum = 0;
        for (let i = 0; i < input.length; i++) {
            sum += process_array(input[i], depth - 1);
        }
        return sum;
    }

    // If it's a number directly, we can safely cache the result as it'll never change
    const key = JSON.stringify({ input, depth });
    if (!cache.has(key)) {
        if (typeof input === 'number') {
            const strinput = String(input);
            if (input === 0) {
                cache.set(key, process_array(1, depth - 1));
            } else if (strinput.length % 2 === 0) {
                const index = strinput.length / 2;
                cache.set(
                    key,
                    process_array([Number(strinput.substring(0, index)), Number(strinput.substring(index))], depth)
                );
            } else {
                cache.set(key, process_array(input * 2024, depth - 1));
            }
        }
    }
    return cache.get(key);
}

function count_stones(input: Cell, depth: number): number {
    return process_array(input, depth);
}

function test_count_stones(input: string, depth: number, expected: number) {
    const actual = count_stones(input.split(' ').map(Number), depth);
    console.assert(expected === actual, '', { input, depth, expected, actual });
}

test_count_stones('125 17', 6, 22);
test_count_stones('125 17', 25, 55312);
test_count_stones('0 44 175060 3442 593 54398 9 8101095', 25, 197157);
console.time('Input');
console.log(count_stones('0 44 175060 3442 593 54398 9 8101095'.split(' ').map(Number), 75));
console.timeEnd('Input');
