type Cell = number | Array<Cell>;

function process_array(input: Cell, depth: number): Cell {
    if (depth < 0) return input;

    if (Array.isArray(input)) {
        for (let i = 0; i < input.length; i++) {
            input[i] = process_array(input[i], depth - 1);
        }
        return input;
    }

    if (typeof input === 'number') {
        if (input === 0) return process_array(1, depth - 1);
        const strinput = String(input);
        if (strinput.length % 2 === 0) {
            const index = strinput.length / 2;
            return process_array([Number(strinput.substring(0, index)), Number(strinput.substring(index))], depth);
        }
        return process_array(input * 2024, depth - 1);
    }

    throw new Error('Unknown branch?');
}

function flatten(input: Cell): Cell {
    if (Array.isArray(input)) {
        for (let i = 0; i < input.length; i++) {
            input[i] = flatten(input[i]);
        }
        return input.flat();
    } else return input;
}

function test_array(input: string, depth: number, expected: string) {
    const actual = (flatten(process_array(input.split(' ').map(Number), depth)) as Array<Cell>).join(' ');
    console.assert(expected === actual, '', { input, depth, expected, actual });
}

function count_stones(input: Cell, depth: number): number {
    return (flatten(process_array(input, depth)) as Array<Cell>).length;
}

function test_count_stones(input: string, depth: number, expected: number) {
    const actual = count_stones(input.split(' ').map(Number), depth);
    console.assert(expected === actual, '', { input, depth, expected, actual });
}

test_array('0 1 10 99 999', 1, '1 2024 1 0 9 9 2021976');
test_array('125 17', 1, '253000 1 7');
test_array('125 17', 2, '253 0 2024 14168');
test_array('125 17', 3, '512072 1 20 24 28676032');
test_array('125 17', 4, '512 72 2024 2 0 2 4 2867 6032');
test_array('125 17', 5, '1036288 7 2 20 24 4048 1 4048 8096 28 67 60 32');
test_array('125 17', 6, '2097446912 14168 4048 2 0 2 4 40 48 2024 40 48 80 96 2 8 6 7 6 0 3 2');
test_count_stones('125 17', 6, 22);
test_count_stones('125 17', 25, 55312);
console.log(count_stones('0 44 175060 3442 593 54398 9 8101095'.split(' ').map(Number), 25));
