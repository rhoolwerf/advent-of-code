import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';
import { sum } from '../util/util';

const directions: { [key: string]: { offsetY: number; offsetX: number } } = {
    '^': { offsetY: -1, offsetX: 0 },
    '>': { offsetY: 0, offsetX: 1 },
    v: { offsetY: 1, offsetX: 0 },
    '<': { offsetY: 0, offsetX: -1 },
};

function run(filename: string): number {
    const input = readFileSync(join(__dirname, filename), 'utf8').split(`${EOL}${EOL}`);
    const base = input[0].split(EOL).map((r) => r.split(''));
    const steps = input[1].split(EOL).join('').split('');

    let { y: rY, x: rX } = base
        .map((r, y) => r.map((cell, x) => (cell === '@' ? { y, x } : undefined)).filter(Boolean))
        .flat()[0];

    const boxes = base.map((r, y) => r.map((cell, x) => (cell === 'O' ? { y, x } : undefined)).filter(Boolean)).flat();
    const walls = base.map((r, y) => r.map((cell, x) => (cell === '#' ? { y, x } : undefined)).filter(Boolean)).flat();

    const hasBox = (y: number, x: number) => boxes.find((b) => b.y === y && b.x === x);
    const hasWall = (y: number, x: number) => walls.find((b) => b.y === y && b.x === x);
    const hasRobot = (y: number, x: number) => rY === y && rX === x;
    const isEmpty = (y: number, x: number) => !hasBox(y, x) && !hasRobot(y, x) && !hasWall(y, x);

    const display = () => {
        const grid = Array.from({ length: base.length }, (_, y) =>
            Array.from({ length: base[0].length }, (_, x) =>
                hasBox(y, x) ? 'O' : hasWall(y, x) ? '#' : hasRobot(y, x) ? '@' : '.'
            )
        );
        console.log(grid.map((r) => r.join('')).join(EOL));
        console.log();
    };

    for (const step of steps) {
        // Which way are we headed?
        const { offsetY, offsetX } = directions[step];

        // Look in this direction, collecting any adjacent boxes, until we've reached a wall of empty space
        const toMove: typeof boxes = [];

        let y = rY;
        let x = rX;
        let canMove = false;
        while (!hasWall(y + offsetY, x + offsetX)) {
            y += offsetY;
            x += offsetX;
            if (hasBox(y, x)) toMove.push(hasBox(y, x));
            if (isEmpty(y, x)) {
                canMove = true;
                break;
            }
        }

        if (canMove) {
            toMove.forEach((b) => {
                b.y += offsetY;
                b.x += offsetX;
            });
            rY += offsetY;
            rX += offsetX;
        }
    }

    return sum(boxes.map(({ y, x }) => 100 * y + x));
}

function test(filename: string, expected: number) {
    console.time(filename);
    const actual = run(filename);
    console.timeEnd(filename);
    console.assert(expected === actual, '', { filename, expected, actual });
}

test('t2', 2028);
test('t1', 10092);
console.time('input');
console.log(run('input'));
console.timeEnd('input');
