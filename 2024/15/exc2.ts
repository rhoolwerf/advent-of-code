import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';
import { sum } from '../util/util';

const directions: { [key: string]: { offsetY: number; offsetX: number } } = {
    '^': { offsetY: -1, offsetX: 0 },
    '>': { offsetY: 0, offsetX: 1 },
    v: { offsetY: 1, offsetX: 0 },
    '<': { offsetY: 0, offsetX: -1 },
};

function run(filename: string): number {
    const input = readFileSync(join(__dirname, filename), 'utf8').split(`${EOL}${EOL}`);
    const base = input[0].split(EOL).map((r) => r.split(''));
    const steps = input[1].split(EOL).join('').split('');

    let { y: rY, x: rX } = base
        .map((r, y) => r.map((cell, x) => (cell === '@' ? { y, x: x * 2 } : undefined)).filter(Boolean))
        .flat()[0];

    const boxes = base
        .map((r, y) => r.map((cell, x) => (cell === 'O' ? { y, xL: x * 2, xR: x * 2 + 1 } : undefined)).filter(Boolean))
        .flat();
    const walls = base
        .map((r, y) => r.map((cell, x) => (cell === '#' ? { y, x: x * 2 } : undefined)).filter(Boolean))
        .flat();

    const hasBox = (y: number, x: number) => boxes.find((b) => b.y === y && (b.xL === x || b.xR === x));
    const isBoxLeft = (y: number, x: number) => boxes.find((b) => b.y === y && (b.xL === x || b.xR === x))?.xL === x;
    const hasWall = (y: number, x: number) => walls.find((b) => b.y === y && (b.x === x || b.x + 1 === x));
    const hasRobot = (y: number, x: number) => rY === y && rX === x;
    const isEmpty = (y: number, x: number) => !hasBox(y, x) && !hasRobot(y, x) && !hasWall(y, x);

    // This is to display the grid, also to validate our has/is functions from above
    const display = () => {
        const grid = Array.from({ length: base.length }, (_, y) =>
            Array.from({ length: base[0].length * 2 }, (_, x) => {
                if (hasBox(y, x)) {
                    return isBoxLeft(y, x) ? '[' : ']';
                } else if (hasWall(y, x)) {
                    return '#';
                } else if (hasRobot(y, x)) {
                    return '@';
                } else if (isEmpty(y, x)) {
                    return '.';
                } else {
                    // If this situation occurs, we have a problem somewhere...
                    return '?';
                }
            })
        );
        console.log(grid.map((r) => r.join('')).join(EOL));
        console.log();
    };

    const canMove = (
        y: number,
        x: number,
        offsetY: number,
        offsetX: number,
        boxesToMove: Set<(typeof boxes)[0]>
    ): boolean => {
        // Whatever do we have at y,x?
        if (isEmpty(y, x)) {
            return true;
        } else if (hasWall(y, x)) {
            return false;
        } else if (hasBox(y, x)) {
            // If we're moving horizontally, just check ahead
            if (offsetX !== 0) {
                if (canMove(y, x + 2 * offsetX, offsetY, offsetX, boxesToMove)) {
                    boxesToMove.add(hasBox(y, x));
                    return true;
                } else {
                    return false;
                }
            } else if (offsetY !== 0) {
                // If we're moving vertically, we need to check either side of the box
                const isLeftSide = isBoxLeft(y, x);
                const adjustX = isLeftSide ? 0 : -1;
                if (
                    canMove(y + offsetY, x + adjustX, offsetY, offsetX, boxesToMove) &&
                    canMove(y + offsetY, x + adjustX + 1, offsetY, offsetX, boxesToMove)
                ) {
                    boxesToMove.add(hasBox(y, x));
                    return true;
                } else {
                    return false;
                }
            }
        }
        // We've reached an unknown state, crash out
        console.error('We got a wrong call?!', { y, x, offsetY, offsetX });
        process.exit(-1);
    };

    // display();
    for (const step of steps) {
        // Which way are we headed?
        const { offsetY, offsetX } = directions[step];

        const boxesToMove: Set<(typeof boxes)[0]> = new Set();
        const can = canMove(rY + offsetY, rX + offsetX, offsetY, offsetX, boxesToMove);
        if (can) {
            rY += offsetY;
            rX += offsetX;
            boxesToMove.forEach((b) => {
                b.y += offsetY;
                b.xL += offsetX;
                b.xR += offsetX;
            });
        }
    }

    return sum(boxes.map(({ y, xL }) => 100 * y + xL));
}

function test(filename: string, expected: number) {
    console.time(filename);
    const actual = run(filename);
    console.timeEnd(filename);
    console.assert(expected === actual, '', { filename, expected, actual });
}

test('t1', 9021);
// test('t2', 2028);
// test('t3', 10092);
console.time('input');
console.log(run('input'));
console.timeEnd('input');
