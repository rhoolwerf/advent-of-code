import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';
import { Graph } from '../../Graph';

type Key = { key: string; y: number; x: number };
type Grid = Array<Array<string>>;
type Node = string;

const directions = [
    [-1, 0],
    [0, 1],
    [1, 0],
    [0, -1],
];

class Runner {
    private grid: Grid;
    private all_keys: Array<Key>;
    private key_paths: Map<string, { doors: Array<string>; distance: number }> = new Map();
    constructor(filename: string) {
        this.grid = readFileSync(join(__dirname, filename), 'utf8')
            .split(EOL)
            .map((r) => r.split(''));

        this.build_key_admin();
    }

    private build_key_admin() {
        // Register all keys
        this.all_keys = this.grid
            .map((r, y) => r.map((key, x) => (key.match(/[a-z]/) ? { key, y, x } : undefined)).filter(Boolean))
            .flat();
        this.all_keys.sort(({ key: a }, { key: b }) => a.localeCompare(b));

        // Build a Graph
        const regex = /[a-zA-Z@.]/;
        const g = new Graph<string>();
        for (let y = 0; y < this.grid.length; y++) {
            for (let x = 0; x < this.grid[y].length; x++) {
                if (!this.check_node(regex, y, x)) continue;
                directions
                    .filter(([offsetY, offsetX]) => this.check_node(regex, y + offsetY, x + offsetX))
                    .map(([offsetY, offsetX]) => this.toNode(y + offsetY, offsetX + x))
                    .forEach((next) => g.edge(this.toNode(y, x), next));
            }
        }

        // For each key, find a path to all other keys
        const start = Object.assign({ key: '@' }, this.start);
        for (const from of this.all_keys.concat(start)) {
            for (const to of this.all_keys.filter(({ key }) => from.key !== key).concat(start)) {
                const shortest = g.shortest(this.toNode(from.y, from.x), this.toNode(to.y, to.x));
                const doors = shortest
                    .map(this.fromNode)
                    .map(({ y, x }) => this.grid[y][x])
                    .filter((d) => /[A-Z]/.test(d));
                this.key_paths.set(this.key_path_key(from.y, from.x, to.y, to.x), {
                    doors,
                    distance: shortest.length,
                });
            }
        }
    }

    private key_path_key(fY: number, fX: number, tY: number, tX: number): string {
        return `${this.toNode(fY, fX)} -> ${this.toNode(tY, tX)}`;
    }

    private get start(): { y: number; x: number } {
        const [start] = this.grid
            .map((r, y) => r.map((cell, x) => (cell === '@' ? { y, x } : undefined)).filter(Boolean))
            .flat();

        return start;
    }

    public run(): number {
        const { y, x } = this.start;
        return this.traverse(this.all_keys, y, x, 0, Infinity);
    }

    public cache: { [key: string]: number } = {};
    private traverse(
        keys_remaining: Array<Key>,
        y: number,
        x: number,
        distance_so_far: number,
        min_so_far: number
    ): number {
        if (keys_remaining.length === 0) return distance_so_far;

        // If we've already reached this point once (and cached it), just return that to improve performance
        const cache_key = `${this.grid[y][x]}:${keys_remaining.map(({ key }) => key).join(',')}`;
        if (this.cache[cache_key]) return this.cache[cache_key] + distance_so_far;

        let min_distance = min_so_far;

        // Keep track of what is already unlocked
        const unlocked = this.all_keys
            .filter(({ key }) => !keys_remaining.some((k) => k.key === key))
            .map(({ key }) => key.toUpperCase());

        // For each key, try to find a route
        for (const k of keys_remaining) {
            const { distance, doors } = this.key_paths.get(this.key_path_key(y, x, k.y, k.x));

            // Is there a locked door in the way?
            const is_locked = doors.some((d) => !unlocked.includes(d));
            if (is_locked) continue;

            // If hitting this key would result in a larger path, never-you-mind
            if (distance_so_far + distance > min_so_far) continue;

            const new_distance = this.traverse(
                keys_remaining.filter(({ key }) => k.key !== key),
                k.y,
                k.x,
                distance_so_far + distance - 1,
                Math.min(min_distance, min_so_far)
            );
            min_distance = Math.min(min_distance, new_distance);
        }

        this.cache[cache_key] = min_distance - distance_so_far;

        return min_distance;
    }

    private check_node(regex: RegExp, y: number, x: number): boolean {
        return this.grid[y] && this.grid[y][x] && Boolean(this.grid[y][x].match(regex));
    }

    private toNode(y: number, x: number): Node {
        return `${y}-${x}`;
    }

    private fromNode(node: Node): { y: number; x: number } {
        const index = node.indexOf('-');
        return { y: Number(node.substring(0, index)), x: Number(node.substring(index + 1)) };
    }
}

function test(filename: string, expected: number) {
    const runner = new Runner(filename);
    console.time(filename);
    const actual = runner.run();
    console.timeEnd(filename);
    console.assert(expected === actual, 'distance', { filename, expected, actual });
    // console.log(runner.cache);
}

// test('t1', 8);
// test('t2', 86);
// test('t3', 132);
// // console.profile();
test('t5', 81);
// // console.profileEnd();

// try {
//     console.time('poging tot t4');
// test('t4', 136);
// } catch (error) {
//     console.log('t4 gaf error', error);
// } finally {
//     console.timeEnd('poging tot t4');
// }

// try {
//     console.time('Actual input');
// console.log(new Runner('input').run());
// } catch (error) {
//     console.log('input gaf error', error);
// } finally {
//     console.timeEnd('Actual input');
// }
