import { Graph } from "../Graph";

const g = new Graph<string>();
g.edge('a', 'b');
g.edge('b', 'c');
g.edge('a', 'c');
g.edge('a', 'd');

// const paths = g.paths();
const paths = g.paths('a', 'c');
console.log(paths);
