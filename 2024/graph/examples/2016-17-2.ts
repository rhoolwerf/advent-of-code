// This is my own DFS-ish-attempt for https://adventofcode.com/2016/day/17

type Direction = { offsetY: -1 | 0 | 1; offsetX: -1 | 0 | 1; dir: 'D' | 'R' | 'U' | 'L' };
const directions: Array<Direction> = [
    { offsetY: -1, offsetX: 0, dir: 'U' },
    { offsetY: 1, offsetX: 0, dir: 'D' },
    { offsetY: 0, offsetX: -1, dir: 'L' },
    { offsetY: 0, offsetX: 1, dir: 'R' },
];

import md5 = require('md5');

function traverse(code: string, path_so_far: Array<string>, y: number, x: number): Array<Array<string>> {
    if (y === 3 && x === 3) return [path_so_far];

    const paths: Array<Array<string>> = [];

    const code_with_path = `${code}${path_so_far.join('')}`;
    const hash = md5(code_with_path);
    const edges = directions.filter(({ offsetY, offsetX }, i) => is_open(y + offsetY, x + offsetX, hash.charAt(i)));
    for (const { offsetY, offsetX, dir } of edges) {
        paths.push(...traverse(code, path_so_far.concat(dir), y + offsetY, x + offsetX));
    }

    return paths;
}

function is_open(y: number, x: number, char: string): boolean {
    return y >= 0 && y <= 3 && x >= 0 && x <= 3 && ['b', 'c', 'd', 'e', 'f'].includes(char);
}

function test(code: string, expected: number, time = true) {
    if (time) console.time(code);
    const paths = traverse(code, [], 0, 0);
    if (time) console.timeEnd(code);
    const actual = paths.length > 0 ? paths.toSorted(({ length: a }, { length: b }) => b - a)[0].length : 0;
    console.assert(actual === expected, '', { code, expected, actual });
}

test('ihgpwlah', 370);
test('kglvqrro', 492);
test('ulqzkmiv', 830);
test('awrkjxxr', 0);
test('awrkjxxr', 526);
