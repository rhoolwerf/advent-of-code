import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';

function get_middles_sum(input: string): number {
    const [rules, updates] = input
        .split(`${EOL}${EOL}`)
        .map((block) => block.split(EOL).map((row) => row.split(/\||,/).map(Number)));

    const valid_middles: Array<number> = [];
    for (const pages of updates) {
        let valid = true;
        for (let i = 0; i < pages.length && valid; i++) {
            // Check if all pages after us are valid
            for (let j = i + 1; j < pages.length && valid; j++) {
                const p1 = pages[i];
                const p2 = pages[j];
                if (rules.find((r) => r[0] === p2 && r[1] === p1)) {
                    valid = false;
                }
            }
        }
        if (valid) {
            valid_middles.push(pages[Math.floor(pages.length / 2)]);
        }
    }

    return valid_middles.reduce((sum, val) => sum + val, 0);
}

function test(input: string, expected: number) {
    const actual = get_middles_sum(input);
    console.assert(expected === actual, '', { expected, actual });
}

test(readFileSync(join(__dirname, 't1'), 'utf8'), 143);
console.log(get_middles_sum(readFileSync(join(__dirname, 'input'), 'utf8')));
