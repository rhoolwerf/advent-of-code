import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';
import { sum } from '../util/util';

const directions = [
    [-1, 0], // Up
    [0, 1], // Right
    [1, 0], // Down
    [0, -1], // Left
];

type Region = { plant: string; spots: Array<string> };
type Border = { icon: '|' | '-'; offsetY: number; offsetX: number };
type Neighbour = string;
type Spot = { y: number; x: number; borders: Array<Border>; neighbours: Array<Neighbour> };
type Spots = Map<string, Spot>;

function toNode(y: number, x: number) {
    return `${y}-${x}`;
}

const fromNodeCache = new Map<string, { y: number; x: number }>();
function fromNode(node: string): { y: number; x: number } {
    if (!fromNodeCache.has(node)) {
        const [y, x] = node.split('-').map(Number);
        fromNodeCache.set(node, { y, x });
    }
    return fromNodeCache.get(node);
}

function grid_2_data(grid: Array<Array<string>>): { spots: Spots; regions: Array<Region> } {
    const getEdges = (y: number, x: number): Array<string> => {
        return directions
            .filter(([offsetY, offsetX]) => grid[y + offsetY] && grid[y + offsetY][x + offsetX] === grid[y][x])
            .map(([offsetY, offsetX]) => toNode(y + offsetY, x + offsetX));
    };

    const getBorders = (y: number, x: number): Array<Border> => {
        return directions
            .filter(([offsetY, offsetX]) => !grid[y + offsetY] || grid[y + offsetY][x + offsetX] !== grid[y][x])
            .map(([offsetY, offsetX]) => ({
                offsetY,
                offsetX,
                icon: offsetY !== 0 ? '-' : ('|' as Border['icon']),
            }));
    };

    const touches = (spot: string, touching: Set<string> = new Set()): Set<string> => {
        touching.add(spot);
        const { y, x } = fromNode(spot);
        const edges = getEdges(y, x);
        for (const edge of edges) {
            // If we already know we're touching, we're good
            if (touching.has(edge)) continue;

            // What else are we touching?
            touches(edge, touching);
        }

        return touching;
    };

    // For each plant, determine borders and which region it could be part of
    const regions: Array<Region> = [];
    const spots: Spots = new Map();
    for (let y = 0; y < grid.length; y++) {
        for (let x = 0; x < grid[y].length; x++) {
            const plant = grid[y][x];
            const node = toNode(y, x);

            spots.set(node, { y, x, neighbours: getEdges(y, x), borders: getBorders(y, x) });

            // Find a region for this spot
            const region = regions.filter((r) => r.plant === plant).find(({ spots }) => spots.includes(node));
            if (!region) {
                regions.push({ plant, spots: Array.from(touches(node)) });
            }
        }
    }

    return { spots, regions };
}

function run(filename: string): number {
    const grid = readFileSync(join(__dirname, filename), 'utf8')
        .split(EOL)
        .map((r) => r.split(''));

    const { regions } = grid_2_data(grid);

    // For each region, count per spot how many corners that spot has
    let sum = 0;
    for (const { spots } of regions) {
        let count_corners = 0;
        for (const spot of spots) {
            const { y, x } = fromNode(spot);
            const corners: Array<'up_left' | 'up_right' | 'down_right' | 'down_left'> = [];

            // Up
            const up = spots.includes(toNode(y - 1, x));
            // Up-right
            const up_right = spots.includes(toNode(y - 1, x + 1));
            // Right
            const right = spots.includes(toNode(y, x + 1));
            // Down-right
            const down_right = spots.includes(toNode(y + 1, x + 1));
            // Down
            const down = spots.includes(toNode(y + 1, x));
            // Down-left
            const down_left = spots.includes(toNode(y + 1, x - 1));
            // Left
            const left = spots.includes(toNode(y, x - 1));
            // Up-Left
            const up_left = spots.includes(toNode(y - 1, x - 1));

            // Upper-left?
            if (!up && !left) corners.push('up_left'); // Outside corner
            if (up && left && !up_left) corners.push('up_left'); // Inside corner

            // Upper-right?
            if (!up && !right) corners.push('up_right'); // Outside corner
            if (up && right && !up_right) corners.push('up_right'); // Inside corner

            // Down-right?
            if (!down && !right) corners.push('down_right'); // Outside corner
            if (down && right && !down_right) corners.push('down_right'); // Inside corner

            // Down-left?
            if (!down && !left) corners.push('down_left'); // Outside corner
            if (down && left && !down_left) corners.push('down_left'); // Inside corner

            // Register it
            count_corners += corners.length;
        }
        sum += spots.length * count_corners;
    }

    return sum;
}

function test(filename: string, expected: number) {
    console.time(filename);
    const actual = run(filename);
    console.timeEnd(filename);
    console.assert(expected === actual, '', { filename, expected, actual });
}

test('t1', 80);
test('t2', 436);
test('t3', 1206);
test('t4', 236);
test('t5', 368);
console.time('Input');
console.log(run('input'));
console.timeEnd('Input');
