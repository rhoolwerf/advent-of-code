import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';
import { sum } from '../util/util';

const directions = [
    [-1, 0], // Up
    [0, 1], // Right
    [1, 0], // Down
    [0, -1], // Left
];

type Region = { plant: string; spots: Array<string> };
type Border = { icon: '|' | '-'; offsetY: number; offsetX: number };
type Neighbour = string;
type Spot = { y: number; x: number; borders: Array<Border>; neighbours: Array<Neighbour> };
type Spots = Map<string, Spot>;

function toNode(y: number, x: number) {
    return `${y}-${x}`;
}

const fromNodeCache = new Map<string, { y: number; x: number }>();
function fromNode(node: string): { y: number; x: number } {
    if (!fromNodeCache.has(node)) {
        const [y, x] = node.split('-').map(Number);
        fromNodeCache.set(node, { y, x });
    }
    return fromNodeCache.get(node);
}

function grid_2_data(grid: Array<Array<string>>): { spots: Spots; regions: Array<Region> } {
    const getEdges = (y: number, x: number): Array<string> => {
        return directions
            .filter(([offsetY, offsetX]) => grid[y + offsetY] && grid[y + offsetY][x + offsetX] === grid[y][x])
            .map(([offsetY, offsetX]) => toNode(y + offsetY, x + offsetX));
    };

    const getBorders = (y: number, x: number): Array<Border> => {
        return directions
            .filter(([offsetY, offsetX]) => !grid[y + offsetY] || grid[y + offsetY][x + offsetX] !== grid[y][x])
            .map(([offsetY, offsetX]) => ({
                offsetY,
                offsetX,
                icon: offsetY !== 0 ? '-' : ('|' as Border['icon']),
            }));
    };

    const touches = (spot: string, touching: Set<string> = new Set()): Set<string> => {
        touching.add(spot);
        const { y, x } = fromNode(spot);
        const edges = getEdges(y, x);
        for (const edge of edges) {
            // If we already know we're touching, we're good
            if (touching.has(edge)) continue;

            // What else are we touching?
            touches(edge, touching);
        }

        return touching;
    };

    // For each plant, determine borders and which region it could be part of
    const regions: Array<Region> = [];
    const spots: Spots = new Map();
    for (let y = 0; y < grid.length; y++) {
        for (let x = 0; x < grid[y].length; x++) {
            const plant = grid[y][x];
            const node = toNode(y, x);

            spots.set(node, { y, x, neighbours: getEdges(y, x), borders: getBorders(y, x) });

            // Find a region for this spot
            const region = regions.filter((r) => r.plant === plant).find(({ spots }) => spots.includes(node));
            if (!region) {
                regions.push({ plant, spots: Array.from(touches(node)) });
            }
        }
    }

    return { spots, regions };
}

function run(filename: string): number {
    const grid = readFileSync(join(__dirname, filename), 'utf8')
        .split(EOL)
        .map((r) => r.split(''));

    const { regions, spots } = grid_2_data(grid);

    let sum = 0;
    for (const r of regions) {
        const minY = Math.min(...r.spots.map((spot) => fromNode(spot).y));
        const minX = Math.min(...r.spots.map((spot) => fromNode(spot).x));
        const maxY = Math.max(...r.spots.map((spot) => fromNode(spot).y));
        const maxX = Math.max(...r.spots.map((spot) => fromNode(spot).x));

        // console.log(r.plant, { minY, maxY, minX, maxX });
        const tmp: Array<Array<string>> = Array.from({ length: (maxY - minY + 1) * 4 + 1 }, (_) =>
            Array.from({ length: (maxX - minX + 1) * 4 + 1 }, () => ' ')
        );
        for (const spot of r.spots) {
            const { y, x } = fromNode(spot);
            const mappedY = (y - minY) * 4 + 1;
            const mappedX = (x - minX) * 4 + 1;
            tmp[mappedY][mappedX] = '.';

            spots.get(spot).borders.forEach(({ offsetY, offsetX, icon }) => {
                tmp[mappedY + offsetY][mappedX + offsetX] = icon;
            });
        }

        const vertical: Array<Array<string>> = [];
        const horizontal: Array<Array<string>> = [];
        for (let y = 0; y < tmp.length; y++) {
            const row: Array<string> = [];
            for (let x = 1; x < tmp[0].length; x += 4) {
                row.push(tmp[y][x]);
            }
            horizontal.push(row);
        }

        for (let y = 1; y < tmp.length; y += 4) {
            const row: Array<string> = [];
            for (let x = 0; x < tmp[0].length; x++) {
                row.push(tmp[y][x]);
            }
            vertical.push(row);
        }
        // console.log(tmp.map((r) => r.join('')).join(EOL));
        // console.log(horizontal.map((r) => r.join('')).join(EOL), grid_2_data(horizontal).regions.filter(({plant}) => plant==='-'));
        // console.log(vertical.map((r) => r.join('')).join(EOL), grid_2_data(vertical).regions.filter(({plant}) => plant==='|'));

        const horizontal_sides = grid_2_data(horizontal).regions.filter(({ plant }) => plant === '-').length;
        const vertical_sides = grid_2_data(vertical).regions.filter(({ plant }) => plant === '|').length;
        const sides = horizontal_sides + vertical_sides;
        const result = sides * r.spots.length;
        sum += result;
    }

    return sum;
}

function test(filename: string, expected: number) {
    console.time(filename);
    const actual = run(filename);
    console.timeEnd(filename);
    console.assert(expected === actual, '', { filename, expected, actual });
}

test('t1', 80);
test('t2', 436);
test('t3', 1206);
test('t4', 236);
test('t5', 368);
console.time('Input');
console.log(run('input'));
console.timeEnd('Input');
