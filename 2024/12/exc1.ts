import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';
import { sum } from '../util/util';

const directions = [
    [-1, 0], // Up
    [0, 1], // Right
    [1, 0], // Down
    [0, -1], // Left
];

type Region = { plant: string; spots: Array<string> };
type Neighbour = string;
type Spot = { y: number; x: number; neighbours: Array<Neighbour> };
type Spots = Map<string, Spot>;

function toNode(y: number, x: number) {
    return `${y}-${x}`;
}

const fromNodeCache = new Map<string, { y: number; x: number }>();
function fromNode(node: string): { y: number; x: number } {
    if (!fromNodeCache.has(node)) {
        const [y, x] = node.split('-').map(Number);
        fromNodeCache.set(node, { y, x });
    }
    return fromNodeCache.get(node);
}

function grid_2_data(grid: Array<Array<string>>): { spots: Spots; regions: Array<Region> } {
    const getEdges = (y: number, x: number): Array<string> => {
        return directions
            .filter(([offsetY, offsetX]) => grid[y + offsetY] && grid[y + offsetY][x + offsetX] === grid[y][x])
            .map(([offsetY, offsetX]) => toNode(y + offsetY, x + offsetX));
    };

    const touches = (spot: string, touching: Set<string> = new Set()): Set<string> => {
        touching.add(spot);
        const { y, x } = fromNode(spot);
        const edges = getEdges(y, x);
        for (const edge of edges) {
            // If we already know we're touching, we're good
            if (touching.has(edge)) continue;

            // What else are we touching?
            touches(edge, touching);
        }

        return touching;
    };

    // For each plant, determine borders and which region it could be part of
    const regions: Array<Region> = [];
    const spots: Spots = new Map();
    for (let y = 0; y < grid.length; y++) {
        for (let x = 0; x < grid[y].length; x++) {
            const plant = grid[y][x];
            const node = toNode(y, x);

            spots.set(node, { y, x, neighbours: getEdges(y, x) });

            // Find a region for this spot
            const region = regions.filter((r) => r.plant === plant).find(({ spots }) => spots.includes(node));
            if (!region) {
                regions.push({ plant, spots: Array.from(touches(node)) });
            }
        }
    }

    return { spots, regions };
}

function run(filename: string): number {
    const grid = readFileSync(join(__dirname, filename), 'utf8')
        .split(EOL)
        .map((r) => r.split(''));

    const { regions, spots } = grid_2_data(grid);

    return sum(regions.map((r) => r.spots.length * sum(r.spots.map((spot) => 4 - spots.get(spot).neighbours.length))));
}

function test(filename: string, expected: number) {
    const actual = run(filename);
    console.assert(expected === actual, '', { filename, expected, actual });
}

test('t2', 772);
test('t3', 1930);
console.time('Input');
console.log(run('input'));
console.timeEnd('Input');
