import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';
import { mul } from '../util/util';

function run(filename: string, steps: number, height: number, width: number): number {
    const robots = readFileSync(join(__dirname, filename), 'utf8')
        .split(EOL)
        .map((r) => r.match(/(-?\d+)/g).map(Number))
        .map(([pX, pY, vX, vY]) => ({ pX, pY, vX, vY }));

    const display = () => {
        const grid = Array.from({ length: height }, (_, y) =>
            Array.from({ length: width }, (_, x) => robots.filter(({ pY, pX }) => y === pY && x === pX).length || '.')
        );
        console.log(grid.map((r) => r.join('')).join(EOL));
        console.log();
    };

    for (let i = 1; i <= steps; i++) {
        robots.forEach((r) => {
            r.pY = (r.pY + r.vY) % height;
            r.pX = (r.pX + r.vX) % width;

            if (r.pY < 0) r.pY = height + r.pY;
            if (r.pX < 0) r.pX = width + r.pX;
        });
    }

    const middle_height = height / 2;
    const middle_width = width / 2;
    const q1 = robots.filter(({ pY, pX }) => pY < Math.floor(middle_height) && pX < Math.floor(middle_width)).length;
    const q2 = robots.filter(({ pY, pX }) => pY < Math.floor(middle_height) && pX >= Math.ceil(middle_width)).length;
    const q3 = robots.filter(({ pY, pX }) => pY >= Math.ceil(middle_height) && pX < Math.floor(middle_width)).length;
    const q4 = robots.filter(({ pY, pX }) => pY >= Math.ceil(middle_height) && pX >= Math.ceil(middle_width)).length;
    return mul([q1, q2, q3, q4]);
}

function test(filename: string, steps: number, height: number, width: number, expected: number) {
    console.time(filename);
    const actual = run(filename, steps, width, height);
    console.timeEnd(filename);
    console.assert(expected === actual, '', { input: { filename, steps, width, height }, expected, actual });
}

test('t1', 100, 11, 7, 12);
console.time('input');
console.log(run('input', 100, 103, 101));
console.timeEnd('input');
