import { createWriteStream, readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';
import { sum } from '../util/util';

function run(filename: string, steps: number, height: number, width: number) {
    const robots = readFileSync(join(__dirname, filename), 'utf8')
        .split(EOL)
        .map((r) => r.match(/(-?\d+)/g).map(Number))
        .map(([pX, pY, vX, vY]) => ({ pX, pY, vX, vY }));

    const largest: { i: number; density: number; formation: string } = {
        i: Infinity,
        density: 0,
        formation: '',
    };
    for (let i = 1; i <= steps; i++) {
        robots.forEach((r) => {
            r.pY = (r.pY + r.vY) % height;
            r.pX = (r.pX + r.vX) % width;

            if (r.pY < 0) r.pY = height + r.pY;
            if (r.pX < 0) r.pX = width + r.pX;
        });

        // Calculate 'pixel density'
        const density = sum(
            robots.map(
                ({ pY, pX }) => robots.filter((r) => Math.abs(pY - r.pY) <= 1 && Math.abs(pX - r.pX) <= 1).length
            )
        );

        if (density > largest.density) {
            console.log('new largest found', { i, density });
            largest.density = density;
            largest.i = i;
            largest.formation = JSON.stringify(robots);
        }

        // // After making 10000 images and scrolling through them, I know this is the answer...
        // if (i === 8149) {
        //     // Make use of 'pureimage' librabry
        //     const img = PImage.make(width, height);
        //     const ctx = img.getContext('2d');
        //     ctx.fillStyle = 'white';
        //     ctx.fillRect(0, 0, width, height);

        //     for (let y = 0; y < height; y++) {
        //         for (let x = 0; x < width; x++) {
        //             if (robots.filter(({ pY, pX }) => y === pY && x === pX).length > 0) {
        //                 ctx.fillPixelWithColor(x, y, 500);
        //             }
        //         }
        //     }

        //     try {
        //         await PImage.encodePNGToStream(img, createWriteStream(`${String(i).padStart(5, '0')}.png`));
        //         console.log('image drawn', i);
        //     } catch (e) {
        //         console.error('Error drawing', e);
        //         process.exit(-1);
        //     }
        // }
    }

    console.assert(largest.i === 8149, 'something has gone seriously wrong?!');
}

console.time('input');
run('input', 10000, 103, 101);
console.timeEnd('input');
