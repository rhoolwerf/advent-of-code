import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';
import * as kleur from 'kleur';

type Node = { y: number; x: number };
type Path = Array<Node>;

function fromNode(n: Node): string {
    return `${n.y},${n.x}`;
}

function run(filename: string): number {
    const input = readFileSync(join(__dirname, filename), 'utf8').split(`${EOL}${EOL}`);
    const [height, width, bytes] = input[0].split('x').map(Number);
    const walls = input[1]
        .split(EOL)
        .slice(0, bytes)
        .map((b) => b.split(',').map(Number))
        .map(([x, y]) => ({ y, x }));

    const isWall = (y: number, x: number): boolean =>
        y < 0 || y > height || x < 0 || x > width || walls.some((w) => w.x === x && w.y === y);
    const isEmpty = (y: number, x: number): boolean => !isWall(y, x);

    const display = (path: Path = []) => {
        const grid = Array.from({ length: height + 1 }, (_, y) =>
            Array.from({ length: width + 1 }, (_, x) =>
                isWall(y, x) ? '#' : path.some((p) => p.y === y && p.x === x) ? kleur.blue('O') : kleur.grey('.')
            )
        );
        console.log(grid.map((r) => r.join('')).join(EOL));
    };

    const bfs = (start: Node, end: Node): Path => {
        const queue: Array<[Node, Path]> = [[start, []]];
        const visited: Set<string> = new Set();

        const isVisited = (n: Node): boolean => {
            const nodeString = fromNode(n);
            return visited.has(nodeString);
        };

        while (queue.length) {
            // // Look at the shortest route taken so far
            // queue.sort((a, b) => a[2] - b[2]);

            // Get ready for the next bit
            const [next, path] = queue.shift();

            // Have we reached the end?
            if (next.y === end.y && next.x === end.x) return path.concat(next);

            // If we've already reached this place with a shorter route, just skip this step
            if (isVisited(next)) continue;
            visited.add(fromNode(next));

            // For each of 4 directions, queue a run
            [
                [-1, 0],
                [0, 1],
                [1, 0],
                [0, -1],
            ].forEach(
                ([offsetY, offsetX]) =>
                    isEmpty(next.y + offsetY, next.x + offsetX) &&
                    !isVisited({ y: next.y + offsetY, x: next.x + offsetX }) &&
                    queue.push([{ y: next.y + offsetY, x: next.x + offsetX }, path.concat(next)])
            );
        }
    };

    // console.log({ height, width, bytes, walls });
    // display();

    // If we ever get here, we've found no route
    const shortest = bfs({ y: 0, x: 0 }, { y: height, x: width });
    // display(shortest);
    return shortest.length - 1;
}

function time(filename: string): number {
    console.time(filename);
    const result = run(filename);
    console.timeEnd(filename);
    return result;
}

function test(filename: string, expected: number) {
    const actual = time(filename);
    console.assert(expected === actual, '', { filename, expected, actual });
}

test('t1', 22);
console.log(time('input'));
