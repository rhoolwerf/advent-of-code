import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';
import { sum } from '../util/util';

function isValid(
    desired: string,
    patterns: Array<string>,
    notMatching: Set<string>,
    cache: Map<string, number>
): number {
    const dfs = (remaining: string): number => {
        let results: number = 0;

        for (const p of patterns) {
            if (remaining.startsWith(p)) {
                const remainder = remaining.substring(p.length);
                if (remainder.length === 0) results++;
                if (notMatching.has(remainder)) continue;
                if (cache.has(remainder)) {
                    results += cache.get(remainder);
                    continue;
                }

                const sub = dfs(remainder);
                if (sub) {
                    results += sub;
                    cache.set(remainder, sub);
                } else notMatching.add(remainder);
            }
        }

        return results;
    };

    return dfs(desired);
}

function run(filename: string): number {
    const [patterns, desired] = readFileSync(join(__dirname, filename), 'utf8')
        .split(`${EOL}${EOL}`)
        .map((b) => b.split(/[\s,]/).filter(Boolean));

    // We need to keep track what sub-patterns don't match, because they'll never match and therefor safe a whole bunch of loops/calls
    const notMatching: Set<string> = new Set();

    // We need to cache what sub-results we've already done
    const cache: Map<string, number> = new Map();

    const results = desired.map((d) => isValid(d, patterns, notMatching, cache));
    return sum(results);
}

function time(filename: string): number {
    console.time(filename);
    const result = run(filename);
    console.timeEnd(filename);
    return result;
}

function test(filename: string, expected: number) {
    const actual = time(filename);
    console.assert(expected === actual, '', { filename, expected, actual });
}

test('t1', 16);
console.log(time('input'));

console.log('Done');
