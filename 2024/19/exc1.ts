import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';

function isValid(desired: string, patterns: Array<string>, notMatching: Set<string>): boolean {
    const dfs = (remaining: string): boolean => {
        for (const p of patterns) {
            if (remaining.startsWith(p)) {
                const remainder = remaining.substring(p.length);
                if (remainder.length === 0) return true;
                if (notMatching.has(remainder)) continue;

                const r = dfs(remainder);
                if (r) return true;
                else notMatching.add(remainder);
            }
        }

        return false;
    };

    return dfs(desired);
}

function run(filename: string): number {
    const [patterns, desired] = readFileSync(join(__dirname, filename), 'utf8')
        .split(`${EOL}${EOL}`)
        .map((b) => b.split(/[\s,]/).filter(Boolean));

    // We need to keep track what sub-patterns don't match, because they'll never match and therefor safe a whole bunch of loops/calls
    const notMatching: Set<string> = new Set();

    return desired.map((d) => isValid(d, patterns, notMatching)).filter(Boolean).length;
}

function time(filename: string): number {
    console.time(filename);
    const result = run(filename);
    console.timeEnd(filename);
    return result;
}

function test(filename: string, expected: number) {
    const actual = time(filename);
    console.assert(expected === actual, '', { filename, expected, actual });
}

test('t1', 6);
console.log(time('input'));

// ['bwwuwbgrrbuwbbuuubuwurwbgwugggwrbrrgwugbwrubgubbgrwwbrwggr', 'urgbuwrurrubbuugwrurrurwgggugwwrgbggbbggr'].forEach(
//     (t) => {
//         console.log(
//             isValid(
//                 t,
//                 readFileSync(join(__dirname, 'input'), 'utf8')
//                     .split(`${EOL}${EOL}`)
//                     .map((b) => b.split(/[\s,]/).filter(Boolean))[0],
//                 new Set()
//             )
//         );
//     }
// );
console.log('Done');
