import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';

const OPERATIONS = ['*', '+'];

function validate_line(result: number, chunks: Array<number>): boolean {
    for (let i = 0; i.toString(2).length < chunks.length; i++) {
        // Grab a combination of operations
        const bin = i.toString(2).padStart(chunks.length - 1, '0');
        const opers = bin.split('').map((d) => OPERATIONS[d]);

        // Calculate
        let val = chunks[0];
        for (let j = 1; j < chunks.length; j++) {
            if (opers[j - 1] === '+') val += chunks[j];
            else val *= chunks[j];
        }

        // Are we satisfied?
        if (val === result) return true;
    }

    return false;
}

function test_line(result: number, chunks: Array<number>, expected: boolean) {
    const actual = validate_line(result, chunks);
    console.assert(expected === actual, '', { result, chunks, expected, actual });
}

test_line(190, [10, 19], true);
test_line(3267, [81, 40, 27], true);
test_line(292, [11, 6, 16, 20], true);
test_line(83, [17, 5], false);
test_line(156, [15, 6], false);
test_line(7290, [6, 8, 6, 15], false);
test_line(161011, [16, 10, 13], false);
test_line(192, [17, 8, 14], false);
test_line(21037, [9, 7, 18, 13], false);

function validate_file(filename: string): number {
    const input = readFileSync(join(__dirname, filename), 'utf8')
        .split(EOL)
        .map((r) => [...r.matchAll(/\d+/g)].map(([nr]) => Number(nr)));

    const results = input.map(([result, ...chunks]) => ({ result, valid: validate_line(result, chunks) }));

    return results.filter(({ valid }) => valid).reduce((sum, { result }) => sum + result, 0);
}

function test_file(filename: string, expected: number) {
    const actual = validate_file(filename);
    console.assert(expected === actual, '', { filename, expected, actual });
}

test_file('t1', 3749);
console.time('Actual calculation time');
console.log(validate_file('input'));
console.timeEnd('Actual calculation time');
