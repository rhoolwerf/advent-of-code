import { readFileSync } from 'fs';
import { EOL } from 'os';

function validate(input: Array<number>): boolean {
    const diffs = input.slice(1).map((v2, i) => v2 - input[i]);
    const ascending = diffs[0] > 0;

    // Check all diffs in the same sign
    const all_signs = diffs.map((v) => (ascending ? v > 0 : v < 0)).filter((v) => !v).length;
    if (all_signs > 0) return false;

    // Check if all diffs are within bounds
    const min_steps = Math.min(...diffs.map(Math.abs));
    const max_steps = Math.max(...diffs.map(Math.abs));
    if (min_steps < 1 || max_steps > 3) return false;

    return true;
}

function test(input: Array<number>, expected: boolean) {
    const actual = validate(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

test([7, 6, 4, 2, 1], true);
test([1, 2, 7, 8, 9], false);
test([9, 7, 6, 2, 1], false);
test([1, 3, 2, 4, 5], false);
test([8, 6, 4, 4, 1], false);
test([1, 3, 6, 7, 9], true);

const input = readFileSync('./input', 'utf8')
    .split(EOL)
    .map((row) => row.split(/\s+/).map(Number));
const valid = input.map(validate);
console.log(valid.filter(Boolean).length);
