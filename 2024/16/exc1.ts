import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';

const directions = [
    [-1, 0],
    [0, 1],
    [1, 0],
    [0, -1],
];

type Node = { y: number; x: number };

class Graph {
    constructor(private grid: Array<Array<string>>) {}

    private getKey(y: number, x: number, dir: number): string {
        return `${y},${x},${dir}`;
    }

    private isValid(y: number, x: number): boolean {
        return this.grid[y]?.[x] !== '#';
    }

    // Had to heavily rely on https://github.com/JustSamuel/AOC/blob/9c7a7b1b55044120407df888480b5b75283b7beb/src/day16/index.ts
    // for inspiration, as I've never done this kind of algorithm before
    cheapest(start: Node, end: Node): number {
        const queue: Array<[number, number, number, number]> = [[start.y, start.x, 1, 0]];
        const visited = new Set<string>();
        while (queue.length) {
            // Prioritize lowest score path first
            queue.sort((a, b) => a[3] - b[3]);

            // Grab the next step to consider
            const [y, x, dir, score] = queue.shift();

            // Have we been here before in the same orientation?
            const key = this.getKey(y, x, dir);
            if (visited.has(key)) continue;
            visited.add(key);

            // Are we at the end?
            if (y === end.y && x === end.x) return score;

            // For each of the possible directions, add to queue
            [
                // Straight on
                { add: 0, cost: 1 },
                // Turning right
                { add: 1, cost: 1001 },
                // Turning left
                { add: 3, cost: 1001 },
            ].forEach(({ add, cost }) => {
                const nextD = (dir + add) % 4;
                const nextY = y + directions[nextD][0];
                const nextX = x + directions[nextD][1];

                if (!visited.has(this.getKey(nextY, nextX, nextD)) && this.isValid(nextY, nextX)) {
                    queue.push([nextY, nextX, nextD, score + cost]);
                }
            });
        }

        // If we get here, we've found no path (which is weird considering the puzzle...)
        return Infinity;
    }
}

function run(filename: string): number {
    const grid = readFileSync(join(__dirname, filename), 'utf8')
        .split(EOL)
        .map((r) => r.split(''));

    const start = grid.map((r, y) => r.map((c, x) => (c === 'S' ? { y, x } : undefined)).filter(Boolean)).flat()[0];
    const end = grid.map((r, y) => r.map((c, x) => (c === 'E' ? { y, x } : undefined)).filter(Boolean)).flat()[0];

    const g = new Graph(grid);
    return g.cheapest(start, end);
}

function test(filename: string, expected: number) {
    console.time(filename);
    const actual = run(filename);
    console.timeEnd(filename);
    console.assert(expected === actual, '', { filename, expected, actual });
}

test('t1', 7036);
test('t2', 11048);
test('t3', 3004);
test('t4', 1003);
console.time('input');
console.log(run('input'));
console.timeEnd('input');
