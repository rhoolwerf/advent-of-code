import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';
import { Graph } from '../graph/Graph';

type Trailhead = { y: number; x: number; score: number };
type Node = string;

function toNode(y: number, x: number): string {
    return `${y}-${x}`;
}

function get_heads_with_scores(filename: string): Array<Trailhead> {
    const heads: Array<Trailhead> = [];
    const ends: Array<Node> = [];

    // Parse grid
    const grid = readFileSync(join(__dirname, filename), 'utf8')
        .split(EOL)
        .map((r) => r.split('').map(Number));

    // Register relevant nodes and build graph
    const g = new Graph<Node>();
    for (let y = 0; y < grid.length; y++) {
        for (let x = 0; x < grid[0].length; x++) {
            const pos = grid[y][x];
            if (pos === 0) {
                heads.push({ y, x, score: 0 });
            }
            if (pos === 9) {
                ends.push(toNode(y, x));
            }

            // For each of the four directions, check if a valid path
            [
                [-1, 0], // Up
                [0, 1], // Right
                [1, 0], // Down
                [0, -1], // Left
            ].forEach(([offsetY, offsetX]) => {
                if (grid[y + offsetY] && grid[y + offsetY][x + offsetX] && grid[y + offsetY][x + offsetX] - pos === 1) {
                    g.edge(toNode(y, x), toNode(y + offsetY, x + offsetX));
                }
            });
        }
    }

    // For each head, try to find as many possible ends to connect to
    heads.forEach((head) => {
        for (const end of ends) {
            head.score += g.paths(toNode(head.y, head.x), end).length;
        }
    });

    return heads;
}

function test(filename: string, expected: number) {
    const actual = get_heads_with_scores(filename);
    // console.log(filename, actual);
    const score = actual.reduce((sum, { score }) => sum + score, 0);
    console.assert(expected === score, '', { filename, expected, score });
}

console.time('All including tests');
test('t6', 3);
test('t7', 13);
test('t8', 227);
test('t9', 81);
console.time('Input...');
console.log(get_heads_with_scores('input').reduce((sum, { score }) => sum + score, 0));
console.timeEnd('Input...');
console.timeEnd('All including tests');
