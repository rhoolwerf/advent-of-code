import { readFileSync } from 'fs';
import { EOL } from 'os';
import { join } from 'path';

type Trailhead = { y: number; x: number; score: number };
type Node = string;

function toNode(y: number, x: number): Node {
    return `${y}-${x}`;
}

function fromNode(node: Node): [number, number] {
    // Substring with a single lookup is *a lot* faster than split.map
    // return node.split('-').map(Number) as [number, number];
    const index = node.indexOf('-');
    return [Number(node.substring(0, index)), Number(node.substring(index + 1))] as [number, number];
}

function get_heads_with_scores(filename: string): Array<Trailhead> {
    const heads: Array<Trailhead> = [];
    const ends: Array<Node> = [];

    // Parse grid
    const grid = readFileSync(join(__dirname, filename), 'utf8')
        .split(EOL)
        .map((r) => r.split('').map(Number));

    // Register relevant nodes and build graph
    for (let y = 0; y < grid.length; y++) {
        for (let x = 0; x < grid[0].length; x++) {
            const pos = grid[y][x];
            if (pos === 0) {
                heads.push({ y, x, score: 0 });
            }
            if (pos === 9) {
                ends.push(toNode(y, x));
            }
        }
    }

    // For each head, try to find as many possible ends to connect to
    for (const head of heads) {
        const start = toNode(head.y, head.x);
        for (const end of ends) {
            head.score += dfs(grid, start, [], end).length;
        }
    }

    return heads;
}

function dfs(grid: Array<Array<number>>, start: Node, path_so_far: Array<Node>, end: Node): Array<Array<Node>> {
    const paths: Array<Array<Node>> = [];
    const current_path = path_so_far.concat(start);

    const edges = get_edges(grid, start);
    for (const next of edges) {
        // Is this the desired final node?
        if (next === end) {
            paths.push(current_path.concat(end));
            continue;
        }
        const append = dfs(grid, next, current_path, end);
        paths.push(...append);
    }

    return paths;
}

function get_edges(grid: Array<Array<number>>, node: Node): Array<Node> {
    const [y, x] = fromNode(node);
    return [
        [-1, 0],
        [0, 1],
        [1, 0],
        [0, -1],
    ]
        .filter(
            ([offsetY, offsetX]) =>
                grid[y + offsetY] && grid[y + offsetY][x + offsetX] && grid[y + offsetY][x + offsetX] - grid[y][x] === 1
        )
        .map(([offsetY, offsetX]) => toNode(y + offsetY, x + offsetX));
}

function test(filename: string, expected: number) {
    const actual = get_heads_with_scores(filename);
    // console.log(filename, actual);
    const score = actual.reduce((sum, { score }) => sum + score, 0);
    console.assert(expected === score, '', { filename, expected, score });
}

console.time('All including tests');
test('t6', 3);
test('t7', 13);
test('t8', 227);
test('t9', 81);

// console.profile();
console.log(get_heads_with_scores('input').reduce((sum, { score }) => sum + score, 0));
// console.profileEnd();
console.timeEnd('All including tests');
