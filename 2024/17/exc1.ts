import { readFileSync } from 'fs';
import { join } from 'path';

type Registers = { a: number; b: number; c: number };
type Instructions = Array<number>;
type Output = Array<number>;

class Runner {
    public output: Output = [];

    constructor(
        public registers: Registers,
        private instructions: Instructions
    ) {}

    getValue(combo: number): number {
        if (combo <= 3) return combo;
        else return this.registers[String.fromCharCode(97 + combo - 4)];
    }

    run() {
        let pointer = 0;

        let steps = 0;
        while (pointer < this.instructions.length) {
            steps++;
            const opcode = this.instructions[pointer];
            const literal = this.instructions[pointer + 1];
            const combo = this.getValue(this.instructions[pointer + 1]);

            switch (opcode) {
                case 0: // adv -> division
                    this.registers.a = Math.floor(this.registers.a / 2 ** combo);
                    pointer += 2;
                    break;

                case 1: // bxl -> bitwise XOR
                    this.registers.b ^= literal;
                    pointer += 2;
                    break;

                case 2: // bst -> combo modulo 8
                    this.registers.b = combo % 8;
                    pointer += 2;
                    break;

                case 3: // jnz -> jump if not zero
                    if (this.registers.a === 0) {
                        pointer += 2;
                    } else {
                        pointer = literal;
                    }
                    break;

                case 4: // bxc -> b XOR c
                    this.registers.b ^= this.registers.c;
                    pointer += 2;
                    break;

                case 5: // out -> outputs combo modulo 8
                    this.output.push(combo % 8);
                    pointer += 2;
                    break;

                case 6: // bdv -> same like adv, but store somewhere else
                    this.registers.b = Math.floor(this.registers.a / 2 ** combo);
                    pointer += 2;
                    break;

                case 7: // cdv -> same like adv, but store somewhere else
                    this.registers.c = Math.floor(this.registers.a / 2 ** combo);
                    pointer += 2;
                    break;

                default:
                    console.error('Unknown opcode', { opcode, literal, combo });
                    process.exit(-1);
            }
        }
    }
}

function run(filename: string): { registers: Registers; output: Output } {
    const input = [...readFileSync(join(__dirname, filename), 'utf8').matchAll(/(\d+)/g)].map(([match]) =>
        Number(match)
    );

    const registers = { a: input[0], b: input[1], c: input[2] };
    const instructions = input.slice(3);

    const r = new Runner(registers, instructions);
    r.run();
    return { registers: r.registers, output: r.output };
}

function test(filename: string, expected: { registers: Registers; output: Output }) {
    const actual = run(filename);
    console.assert(expected.registers.a === actual.registers.a, 'registers.a', { filename, expected, actual });
    console.assert(expected.registers.b === actual.registers.b, 'registers.b', { filename, expected, actual });
    console.assert(expected.registers.c === actual.registers.c, 'registers.c', { filename, expected, actual });
    console.assert(expected.output.join(',') === actual.output.join(','), 'registers.output', {
        filename,
        expected,
        actual,
    });
}

test('t1', { registers: { a: 0, b: 0, c: 0 }, output: [4, 6, 3, 5, 6, 3, 5, 2, 1, 0] });
test('t2', { registers: { a: 0, b: 1, c: 9 }, output: [] });
test('t3', { registers: { a: 10, b: 0, c: 0 }, output: [0, 1, 2] });
test('t4', { registers: { a: 0, b: 0, c: 0 }, output: [4, 2, 5, 6, 7, 7, 7, 7, 3, 1, 0] });
test('t5', { registers: { a: 0, b: 26, c: 0 }, output: [] });
test('t6', { registers: { a: 0, b: 44354, c: 43690 }, output: [] });

const { output } = run('input');
console.log(output.join(','));
