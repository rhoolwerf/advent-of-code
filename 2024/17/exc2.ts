function calc(input: number): Array<BigInt> {
    const output: Array<BigInt> = [];
    let A = BigInt(input);
    let B = BigInt(0);
    let C = BigInt(0);

    do {
        // 2,4, -> bst
        B = A % 8n;
        // 1,6, -> bxl
        B ^= 6n;
        // 7,5, -> cdb
        C = A / 2n ** B;
        // 4,4,
        B ^= C;
        // 1,7,
        B ^= 7n;
        // 0,3,
        A = A / 2n ** 3n;
        // 5,5,
        output.push(B % 8n);
        // 3,0 -> jnz -> if A == 0, jmp+2 (out-of-loop), else jump-to-0 (restart)
    } while (A > 0);

    return output;
}

function test_calc(input: number, expected: string) {
    const actual = calc(input).join(',');
    console.assert(expected === actual, 'test_calc', { input, expected, actual });
}

test_calc(37293246, '1,5,0,1,7,4,1,0,3');

function run(digits: Array<number>): number {
    const valid: Array<number> = [];

    const check = (offset: number, so_far: number) => {
        for (let i = 0; i < 7; i++) {
            const r = calc(i + so_far);
            if (r.join(',') === digits.slice(offset).join(',')) {
                if (offset === 0) {
                    valid.push(i + so_far);
                }
                check(offset - 1, (i + so_far) * 8 + 1);
            }
        }
    };

    check(digits.length - 1, 0);

    return Math.min(...valid);
}

// Not entirely sure why this works, but I randomly tried this and it gave the right answer!
console.log(run([2, 4, 1, 6, 7, 5, 4, 4, 1, 7, 0, 3, 5, 5, 3, 0]));
