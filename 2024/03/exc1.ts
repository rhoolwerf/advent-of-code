import { readFileSync } from 'fs';
import { mul, sum } from '../util/util';
import { join } from 'path';

function get_mul(input: string): Array<Array<number>> {
    return [...input.matchAll(/mul\((\d+),(\d+)\)/g)].map(([_, v1, v2]) => [v1, v2].map(Number));
}

function calc(input: string): number {
    return sum(get_mul(input).map((pair) => mul(pair)));
}

function test(input: string, expected: number) {
    const actual = calc(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

test('xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))', 161);

console.log(calc(readFileSync(join(__dirname, 'input'), 'utf8')));
