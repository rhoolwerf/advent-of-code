import { readFileSync } from 'fs';
import { join } from 'path';

function get_mul(input: string): Array<Array<number>> {
    const enabled = input
        .split('do()')
        .map((part) => part.split("don't()")[0])
        .join('');

    return [...enabled.matchAll(/mul\((\d+),(\d+)\)/g)].map(([_, v1, v2]) => [v1, v2].map(Number));
}

function calc(input: string): number {
    return get_mul(input).reduce((sum, [v1, v2]) => sum + v1 * v2, 0);
}

function test(input: string, expected: number) {
    const actual = calc(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

test('xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))', 161);
test("xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))", 48);

console.log(calc(readFileSync(join(__dirname, './input'), 'utf8')));
