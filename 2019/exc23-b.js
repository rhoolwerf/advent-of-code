// Initial instructions
const fs = require('fs');
const sInput = fs.readFileSync('./exc23.in', 'utf8');
const aInstructions = sInput.split(",").map(x => Number(x));
let OpcodeProcessor = require('./opcode_processor.js');

// Initialise all 50 computers and their inputs
let aComputers = [];
let oMessageQueue = new Map();
for (let i = 0; i < 50; i++) {
    aComputers.push(new OpcodeProcessor(aInstructions, [], false));
    oMessageQueue.set(i, [i, -1]); // Initially, input for each computer is its address and -1 (as in, no_input)
}

// For each computer, process 3 times to get all three outputs
let bKeepRunning = true;
let aCurrentNatPacket = [];
let aLastNatPacketUsed = [];
for (let i = 0; bKeepRunning; i++) {
    let bAllComputersIdle = true;
    aComputers.forEach((oComputer, iAddress) => {
        let aComputerInput = [];
        if (oMessageQueue.get(iAddress).length > 0) {
            aComputerInput = [oMessageQueue.get(iAddress).shift(), oMessageQueue.get(iAddress).shift()];
        } else {
            aComputerInput = [-1];// No input avaialble
        }

        oComputer.set_inputs(aComputerInput);

        // Run once to see if there is output available
        oComputer.run();
        if (oComputer.get_last_output() == undefined) {
            // Computer has no output, stop running
        } else {
            // As there is output, a computer isn't idle
            bAllComputersIdle = false;

            // Computer has output, grab all three
            oComputer.run();
            oComputer.run();
            let iTargetAddress = oComputer.aOutputs[0];
            let iX = oComputer.aOutputs[1];
            let iY = oComputer.aOutputs[2];
            oComputer.aOutputs = [];

            if (iTargetAddress == 255) {
                aCurrentNatPacket = [iX, iY];
                // console.log("NatPacket updated to: " + aCurrentNatPacket);
            } else {
                oMessageQueue.get(iTargetAddress).push(iX);
                oMessageQueue.get(iTargetAddress).push(iY);
            }
        }
    });

    if (bAllComputersIdle) {
        // console.log("Comparing current with previous: " + aCurrentNatPacket + " / " + aLastNatPacketUsed);
        if (aCurrentNatPacket[1] == aLastNatPacketUsed[1]) {
            console.log("Using NatPacket-Y twice: " + aCurrentNatPacket[1]);
            bKeepRunning = false;
        } else {
            aLastNatPacketUsed = Array.from(aCurrentNatPacket);
            oMessageQueue.set(0, aCurrentNatPacket);
            // console.log("Using NatPacket " + aCurrentNatPacket + " as input for computer 0");
        }
    }
}