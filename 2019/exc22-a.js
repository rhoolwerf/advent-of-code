const { access } = require("fs");

let aInstructions = [];
let iStackSize = 0;

// Explanation 1 -> reverse stack -> expect: 9 8 7 6 5 4 3 2 1 0
aInstructions = [
    'deal into new stack'
];
iStackSize = 10;

// Explanation 2 -> cut 3 -> expect: 3 4 5 6 7 8 9 0 1 2
aInstructions = [
    'cut 3'
];
iStackSize = 10;

// Explanation 3 -> cut -4 -> expect: 6 7 8 9 0 1 2 3 4 5
aInstructions = [
    'cut -4'
];
iStackSize = 10;

// Explanation 4 -> deal with increment 3 -> expect: 0 7 4 1 8 5 2 9 6 3
aInstructions = [
    'deal with increment 3'
];
iStackSize = 10;

// Test 1 -> expect 0 3 6 9 2 5 8 1 4 7
aInstructions = [
    'deal with increment 7',
    'deal into new stack',
    'deal into new stack'
];
iStackSize = 10;

// Test 2 -> expect 3 0 7 4 1 8 5 2 9 6
aInstructions = [
    'cut 6',
    'deal with increment 7',
    'deal into new stack'
];
iStackSize = 10;

// Test 3 -> expect 6 3 0 7 4 1 8 5 2 9
aInstructions = [
    'deal with increment 7',
    'deal with increment 9',
    'cut -2'
];
iStackSize = 10;

// Test 4 -> expect 9 2 5 8 1 4 7 0 3 6
aInstructions = [
    'deal into new stack',
    'cut -2',
    'deal with increment 7',
    'cut 8',
    'cut -4',
    'deal with increment 7',
    'cut 3',
    'deal with increment 9',
    'deal with increment 3',
    'cut -1'
];
iStackSize = 10;

// Puzzle input
const fs = require('fs');
const sInput = fs.readFileSync('./exc22.in', 'utf8');
aInstructions = sInput.split("\n");
iStackSize = 10007;

// Generate a stack of cards
let aStack = [];
for (let i = 0; i < iStackSize; i++) {
    aStack.push(i);
};

console.log("Initial stack:");
show_stack();

// Process all instructions
aInstructions.forEach(sInstruction => {
    let aNewStack = [];
    if (sInstruction.substring(0, 4) == "deal" && sInstruction == "deal into new stack") {
        // Just reverse the stack
        while (aStack.length > 0) {
            aNewStack.push(aStack.pop());
        }
    } else if (sInstruction.substring(0, 3) == "cut") {
        let iCut = Number(sInstruction.substring(4));
        if (iCut < 0) {
            iCut = iStackSize + iCut;
        }
        aNewStack = aStack.slice(iCut);
        while (iCut-- > 0) { aNewStack.push(aStack.shift()); }
    } else if (sInstruction.substring(0, 4) == "deal" && sInstruction.substring(0, 19) == "deal with increment") {
        let iIncrement = Number(sInstruction.substring(20));
        let iNewPosition = 0;
        while (aStack.length > 0) {
            aNewStack[(iNewPosition % iStackSize)] = aStack.shift();
            iNewPosition += iIncrement;
        }
    }
    aStack = aNewStack;
    console.log("\nAfter instruction: " + sInstruction);
    show_stack();
});

for (let i = 0; i < iStackSize; i++) {
    if (aStack[i] == 2019) {
        console.log("Card 2019 is at position: " + i);
    }
}


function show_stack() {
    console.log(aStack.join(" "));
}