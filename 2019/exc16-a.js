let sInput = "";
let iMaxPhases = 0;
const aOutputElementsBase = [0, 1, 0, -1]

// Test 1 -> Expect 01029498
sInput = "12345678";
iMaxPhases = 4;

// Test 2 -> Expect 24176176
sInput = "80871224585914546619083218645595";
iMaxPhases = 100;

// Test 3 -> Expect 73745418
sInput = "19617804207202209144916044189917";
iMaxPhases = 100;

// Test 4 -> Expect 52432133
sInput = "69317163492948606335995924319873";
iMaxPhases = 100;

// Puzzle input
sInput = "59719811742386712072322509550573967421647565332667367184388997335292349852954113343804787102604664096288440135472284308373326245877593956199225516071210882728614292871131765110416999817460140955856338830118060988497097324334962543389288979535054141495171461720836525090700092901849537843081841755954360811618153200442803197286399570023355821961989595705705045742262477597293974158696594795118783767300148414702347570064139665680516053143032825288231685962359393267461932384683218413483205671636464298057303588424278653449749781937014234119757220011471950196190313903906218080178644004164122665292870495547666700781057929319060171363468213087408071790";
iMaxPhases = 100;

console.log("Input signal: " + sInput);
for (let iPhase = 0; iPhase < iMaxPhases; iPhase++) {
    let sNewInput = "";
    for (let iDigitPointer = 0; iDigitPointer < sInput.length; iDigitPointer++) {
        let aOutputElements = generate_output_elements((iDigitPointer + 1), sInput.length);
        let iNewDigit = 0;
        for (let iOutputElementPointer = 0; iOutputElementPointer < sInput.length; iOutputElementPointer++) {
            iNewDigit += sInput.charAt(iOutputElementPointer) * aOutputElements[iOutputElementPointer];
        }
        let sNewDigit = String(iNewDigit);
        sNewDigit = sNewDigit.charAt(sNewDigit.length - 1);
        sNewInput += sNewDigit;
    }
    // console.log("After " + pad_number(iPhase + 1) + " " + (iPhase == 0 ? "phase" : "phases") + ": " + sNewInput.substring(0, 8));
    sInput = sNewInput;
}
console.log("After " + iMaxPhases + " phases: " + sInput.substring(0, 8));

function generate_output_elements(iMultiplier, iMaxLength) {
    let aResponse = [];
    while (aResponse.length <= iMaxLength + 1) {
        aOutputElementsBase.forEach(iOutputElement => {
            for (let i = 0; i < iMultiplier; i++) {
                aResponse.push(iOutputElement);
            }
        });
    }
    aResponse.shift(); // The first item should always be deleted
    while (aResponse.length > iMaxLength) { aResponse.pop(); }
    return aResponse;
}

function pad_number(iNumber) {
    let sNumber = String(iNumber);
    while (sNumber.length < 4) {
        sNumber = " " + sNumber;
    }
    return sNumber;
}