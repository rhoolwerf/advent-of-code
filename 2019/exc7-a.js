// Opcode Processor
let OpcodeProcessor = require('./opcode_processor.js');

// Generate all possible sequences
let aSequenceValues = [0, 1, 2, 3, 4];
let aSequences = [];

for (let i = 0; i < aSequenceValues.length; i++) {
    aRemainingValues1 = Array.from(aSequenceValues);
    iValue1 = aRemainingValues1.splice(i, 1);

    for (let j = 0; j < aRemainingValues1.length; j++) {
        aRemainingValues2 = Array.from(aRemainingValues1);
        iValue2 = aRemainingValues2.splice(j, 1);

        for (let k = 0; k < aRemainingValues2.length; k++) {
            aRemainingValues3 = Array.from(aRemainingValues2);
            iValue3 = aRemainingValues3.splice(k, 1);

            for (let m = 0; m < aRemainingValues3.length; m++) {
                aRemainingValues4 = Array.from(aRemainingValues3);
                iValue4 = aRemainingValues4.splice(m, 1);

                // console.log('Sequence: ' + iValue1 + "" + iValue2 + "" + iValue3 + "" + iValue4 + "" + aRemainingValues4[0]);
                aSequences.push([iValue1, iValue2, iValue3, iValue4, aRemainingValues4[0]]);
            }
        }
    }
}

function format_sequence(aSequence) {
    let sOutput = "";
    for (let i = 0; i < aSequence.length; i++) {
        sOutput += String(aSequence[i]);
    }
    return sOutput;
}

let aInstructionSet = [];

// Expected 43210in sequence 4,3,2,1,0
aInstructionSet = [3, 15, 3, 16, 1002, 16, 10, 16, 1, 16, 15, 15, 4, 15, 99, 0, 0];

// Expected 54321 in sequence 0,1,2,3,4
aInstructionSet = [3, 23, 3, 24, 1002, 24, 10, 24, 1002, 23, -1, 23, 101, 5, 23, 23, 1, 24, 23, 23, 4, 23, 99, 0, 0];

// Expected 65210 in sequence 1,0,4,3,2
aInstructionSet = [3, 31, 3, 32, 1002, 32, 10, 32, 1001, 31, -2, 31, 1007, 31, 0, 33, 1002, 33, 7, 33, 1, 33, 31, 31, 1, 32, 31, 31, 4, 31, 99, 0, 0, 0];

// Puzzle input
aInstructionSet = [3, 8, 1001, 8, 10, 8, 105, 1, 0, 0, 21, 38, 63, 80, 105, 118, 199, 280, 361, 442, 99999, 3, 9, 102, 5, 9, 9, 1001, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 99, 3, 9, 1001, 9, 4, 9, 102, 4, 9, 9, 101, 4, 9, 9, 102, 2, 9, 9, 101, 2, 9, 9, 4, 9, 99, 3, 9, 1001, 9, 5, 9, 102, 4, 9, 9, 1001, 9, 4, 9, 4, 9, 99, 3, 9, 101, 3, 9, 9, 1002, 9, 5, 9, 101, 3, 9, 9, 102, 5, 9, 9, 101, 3, 9, 9, 4, 9, 99, 3, 9, 1002, 9, 2, 9, 1001, 9, 4, 9, 4, 9, 99, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 99, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 99, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 99, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 99, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 99];

// For each sequence, start
let aMaxSequence = [];
let iMaxValue = 0;
for (let i = 0; i < aSequences.length; i++) {
    let iInput = 0;
    let aSequence = aSequences[i];
    for (let i = 0; i < aSequence.length; i++) {
        let aInputs = [aSequence[i], iInput];
        let processor = new OpcodeProcessor(aInstructionSet, aInputs, false);
        processor.run();
        iInput = processor.get_last_output();
    }
    if (iInput > iMaxValue) {
        iMaxValue = iInput;
        aMaxSequence = aSequence;
    }
}
console.log(format_sequence(aMaxSequence) + ": " + iMaxValue);
