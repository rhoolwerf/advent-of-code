const {
    performance
} = require('perf_hooks');

// Different modes of reading/writing positions
const MODE_POSITION = 0;
const MODE_IMMEDIATE = 1;
const MODE_RELATIVE = 2;

// Different OPCODES
const OPCODE_EXIT = 99;
const OPCODE_ADD = 1;
const OPCODE_MULTI = 2;
const OPCODE_INPUT = 3;
const OPCODE_OUTPUT = 4;
const OPCODE_JUMP_IF_TRUE = 5;
const OPCODE_JUMP_IF_FALSE = 6;
const OPCODE_LESS_THAN = 7;
const OPCODE_EQUALS = 8;
const OPCODE_ADJUST_RELATIVE_BASE = 9;

class OpcodeProcessor {
    constructor(aInstructions, aInputs, bInfo = false, bDebug = false) {
        this.aInstructions = Array.from(aInstructions);
        this.aInputs = Array.from(aInputs);
        this.bDebug = bDebug;
        this.bInfo = bInfo;
        this.bKeepRunning = true;
        this.aOutputs = [];
        this.iInstructionPointer = 0;
        this.sTitle = undefined;
        this.iRelativeBase = 0;
    }

    run() {
        for (; this.iInstructionPointer < this.aInstructions.length && this.bKeepRunning;) {

            // Determine instruction and pad where needed (as leading zero's are omitted)
            let sInstruction = String(this.aInstructions[this.iInstructionPointer]);
            for (let i = sInstruction.length; i < 5; i++) {
                sInstruction = "0" + sInstruction;
            }

            // Split the instruction into OPCODE and modes
            let sOpcode = String(sInstruction.substring(3, 5));
            let iMode1 = Number(sInstruction.substring(2, 3));
            let iMode2 = Number(sInstruction.substring(1, 2));
            let iMode3 = Number(sInstruction.substring(0, 1));

            // Process the OPCODE
            if (this.iInstructionPointer > 0) { this.debug(''); }
            this.debug("Processing opcode: " + sOpcode);
            switch (Number(sOpcode)) {
                case OPCODE_EXIT:
                    // Exit-code, stop processing
                    this.bKeepRunning = false;
                    this.info("Exit command received");
                    break;
                case OPCODE_OUTPUT:
                    // Read from position and output to screen
                    let iOutput = this.read_position(this.iInstructionPointer + 1, iMode1);
                    this.aOutputs.push(iOutput);
                    this.info("Pointer: " + this.format_pointer(this.iInstructionPointer) + ", Output: produced " + iOutput);
                    this.iInstructionPointer += 2;
                    return true; // On output, stop processing
                case OPCODE_INPUT:
                    // Writes Input to Instruction-set
                    if (this.aInputs.length == 0) { return true; }
                    let iInput = this.aInputs.shift();
                    this.info("Pointer: " + this.format_pointer(this.iInstructionPointer) + ", INPUT: used " + iInput);
                    this.write_position(this.iInstructionPointer + 1, iMode1, iInput);
                    this.iInstructionPointer += 2;
                    continue;
                case OPCODE_MULTI:
                    // Multiply parameters 1 and 2 and store in 3
                    let iMultiValue1 = this.read_position(this.iInstructionPointer + 1, iMode1);
                    let iMultiValue2 = this.read_position(this.iInstructionPointer + 2, iMode2);
                    let iMultiResult = iMultiValue1 * iMultiValue2;
                    this.write_position(this.iInstructionPointer + 3, iMode3, iMultiResult);
                    this.iInstructionPointer += 4;
                    continue;
                case OPCODE_ADD:
                    // Add parameters 1 and 2 and store in 3
                    let iAddValue1 = this.read_position(this.iInstructionPointer + 1, iMode1);
                    let iAddValue2 = this.read_position(this.iInstructionPointer + 2, iMode2);
                    let iAddResult = iAddValue1 + iAddValue2;
                    this.write_position(this.iInstructionPointer + 3, iMode3, iAddResult);
                    this.iInstructionPointer += 4;
                    continue;
                case OPCODE_JUMP_IF_TRUE:
                    // Opcode 5 is jump-if-true: if the first parameter is non-zero, it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing.
                    let iJumpIfTrueValue = Number(this.read_position(this.iInstructionPointer + 1, iMode1));
                    // debug("Jumping if value " + iJumpIfTrueValue + " is anything other than 0");
                    if (iJumpIfTrueValue != 0) {
                        this.iInstructionPointer = this.read_position(this.iInstructionPointer + 2, iMode2);
                    } else {
                        this.iInstructionPointer += 3;
                    }
                    continue;

                case OPCODE_JUMP_IF_FALSE:
                    // Opcode 6 is jump-if-false: if the first parameter is zero, it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing.
                    let iJumpIfFalseValue = Number(this.read_position(this.iInstructionPointer + 1, iMode1));
                    // debug("Jumping if value " + iJumpIfFalseValue + " is anything other than 0");
                    if (iJumpIfFalseValue == 0) {
                        this.iInstructionPointer = this.read_position(this.iInstructionPointer + 2, iMode2);
                    } else {
                        this.iInstructionPointer += 3;
                    }
                    continue;

                case OPCODE_LESS_THAN:
                    // Opcode 7 is less than: if the first parameter is less than the second parameter, it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
                    let iLessThanValue1 = this.read_position(this.iInstructionPointer + 1, iMode1);
                    let iLessThanValue2 = this.read_position(this.iInstructionPointer + 2, iMode2);
                    if (iLessThanValue1 < iLessThanValue2) {
                        this.write_position(this.iInstructionPointer + 3, iMode3, 1);
                    } else {
                        this.write_position(this.iInstructionPointer + 3, iMode3, 0);
                    }
                    this.iInstructionPointer += 4;
                    continue;

                case OPCODE_EQUALS:
                    // Opcode 8 is equals: if the first parameter is equal to the second parameter, it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
                    let iEqualsValue1 = this.read_position(this.iInstructionPointer + 1, iMode1);
                    let iEqualsValue2 = this.read_position(this.iInstructionPointer + 2, iMode2);
                    if (iEqualsValue1 == iEqualsValue2) {
                        this.write_position(this.iInstructionPointer + 3, iMode3, 1);
                    } else {
                        this.write_position(this.iInstructionPointer + 3, iMode3, 0);
                    }
                    this.iInstructionPointer += 4;
                    continue;

                case OPCODE_ADJUST_RELATIVE_BASE:
                    // Opcode 9 is 'Adjust Relative Base': the the parameter adjust the relative base property
                    let iRelativeBaseAdjust = this.read_position(this.iInstructionPointer + 1, iMode1);
                    this.debug("Relative base was " + this.iRelativeBase + " and is adjusted with " + iRelativeBaseAdjust + " resulting in " + (this.iRelativeBase + iRelativeBaseAdjust));
                    this.iRelativeBase += iRelativeBaseAdjust;
                    this.iInstructionPointer += 2;
                    continue;

                default:
                    console.log("Error: Opcode '" + sOpcode + "' in instruction '" + sInstruction + "' at position '" + this.iInstructionPointer + "' is not recognised");
                    this.bKeepRunning = false;
                    return false;
            }
        }
        return this.bKeepRunning;
    }

    set_title(sTitle) {
        this.sTitle = sTitle;
    }

    set_inputs(aInputs) {
        this.aInputs = Array.from(aInputs);
    }

    get_last_output() {
        return this.aOutputs[this.aOutputs.length - 1];
    }

    read_position(iPointer, iMode) {
        let iAddress = 0;

        switch (iMode) {
            case MODE_IMMEDIATE:
                iAddress = iPointer;
                break;
            case MODE_POSITION:
                iAddress = this.aInstructions[iPointer];
                break;
            case MODE_RELATIVE:
                iAddress = this.aInstructions[iPointer] + this.iRelativeBase;
        }

        let iValue = Number(this.aInstructions[iAddress]);

        this.debug("Reading pointer " + iPointer + " in mode " + iMode + " resulting in address " + iAddress + " and value " + iValue);

        return Number(iValue);
    }

    write_position(iPointer, iMode, iValue) {
        let iAddress = 0;

        switch (iMode) {
            case MODE_IMMEDIATE:
                iAddress = iPointer;
                break;
            case MODE_POSITION:
                iAddress = this.aInstructions[iPointer];
                break;
            case MODE_RELATIVE:
                iAddress = this.aInstructions[iPointer] + this.iRelativeBase;
        }

        this.debug("Writing value " + iValue + " at pointer " + iPointer + " in mode " + iMode + " so address " + iAddress);

        this.aInstructions[iAddress] = Number(iValue);
    }

    info(sMessage) {
        if (this.bInfo) {
            if (this.sTitle != undefined) {
                console.log("[ INFO] " + this.sTitle + ": " + sMessage);
            } else {
                console.log("[ INFO] " + sMessage);
            }
        }
    }

    debug(sMessage) {
        if (this.bDebug) {
            if (sMessage == "") {
                console.log("");
            } else {
                if (this.sTitle != undefined) {
                    console.log("[DEBUG] " + this.sTitle + ": " + sMessage);
                } else {
                    console.log("[DEBUG] " + sMessage);
                }
            }
        }
    }

    format_pointer(iPointer) {
        let sPointer = String(iPointer);
        while (sPointer.length < 3) {
            sPointer = " " + sPointer;
        }
        return sPointer;
    }

    set_debug(bDebug) {
        this.bDebug = bDebug;
    }

    delay(iMillis) {
        let iNow = performance.now();
        let iStop = iNow + iMillis;
        while (performance.now() < iStop);
    }

    clean_console() {
        // Clears entire terminal for cleaner look
        process.stdout.write("\u001b[3J\u001b[2J\u001b[1J"); console.clear();
    }

    measure_start() {
        this.iMeasureStart = performance.now();
    }
    measure_stop() {
        return performance.now() - this.iMeasureStart;
    }
}

module.exports = OpcodeProcessor;