// Opcode Processor
let OpcodeProcessor = require('./opcode_processor.js');

// Generate all possible sequences
// let aSequenceValues = [0, 1, 2, 3, 4];
let aSequenceValues = [5, 6, 7, 8, 9];
let aSequences = [];

for (let i = 0; i < aSequenceValues.length; i++) {
    aRemainingValues1 = Array.from(aSequenceValues);
    iValue1 = aRemainingValues1.splice(i, 1);

    for (let j = 0; j < aRemainingValues1.length; j++) {
        aRemainingValues2 = Array.from(aRemainingValues1);
        iValue2 = aRemainingValues2.splice(j, 1);

        for (let k = 0; k < aRemainingValues2.length; k++) {
            aRemainingValues3 = Array.from(aRemainingValues2);
            iValue3 = aRemainingValues3.splice(k, 1);

            for (let m = 0; m < aRemainingValues3.length; m++) {
                aRemainingValues4 = Array.from(aRemainingValues3);
                iValue4 = aRemainingValues4.splice(m, 1);

                // console.log('Sequence: ' + iValue1 + "" + iValue2 + "" + iValue3 + "" + iValue4 + "" + aRemainingValues4[0]);
                aSequences.push([iValue1, iValue2, iValue3, iValue4, aRemainingValues4[0]]);
            }
        }
    }
}

function format_sequence(aSequence) {
    let sOutput = "";
    for (let i = 0; i < aSequence.length; i++) {
        sOutput += String(aSequence[i]);
    }
    return sOutput;
}

let aInstructionSet = [];

// Expected 43210in sequence 4,3,2,1,0
aInstructionSet = [3, 15, 3, 16, 1002, 16, 10, 16, 1, 16, 15, 15, 4, 15, 99, 0, 0];

// Expected 54321 in sequence 0,1,2,3,4
aInstructionSet = [3, 23, 3, 24, 1002, 24, 10, 24, 1002, 23, -1, 23, 101, 5, 23, 23, 1, 24, 23, 23, 4, 23, 99, 0, 0];

// Expected 65210 in sequence 1,0,4,3,2
aInstructionSet = [3, 31, 3, 32, 1002, 32, 10, 32, 1001, 31, -2, 31, 1007, 31, 0, 33, 1002, 33, 7, 33, 1, 33, 31, 31, 1, 32, 31, 31, 4, 31, 99, 0, 0, 0];

// Puzzle input
aInstructionSet = [3, 8, 1001, 8, 10, 8, 105, 1, 0, 0, 21, 38, 63, 80, 105, 118, 199, 280, 361, 442, 99999, 3, 9, 102, 5, 9, 9, 1001, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 99, 3, 9, 1001, 9, 4, 9, 102, 4, 9, 9, 101, 4, 9, 9, 102, 2, 9, 9, 101, 2, 9, 9, 4, 9, 99, 3, 9, 1001, 9, 5, 9, 102, 4, 9, 9, 1001, 9, 4, 9, 4, 9, 99, 3, 9, 101, 3, 9, 9, 1002, 9, 5, 9, 101, 3, 9, 9, 102, 5, 9, 9, 101, 3, 9, 9, 4, 9, 99, 3, 9, 1002, 9, 2, 9, 1001, 9, 4, 9, 4, 9, 99, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 99, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 99, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 99, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 99, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 99];

// // Debug from https://www.reddit.com/r/adventofcode/comments/e7eezs/day_7_part_2_implementation_struggles/
// aInstructionSet = [3, 26, 1001, 26, -4, 26, 3, 27, 1002, 27, 2, 27, 1, 27, 26, 27, 4, 27, 1001, 28, -1, 28, 1005, 28, 6, 99, 0, 0, 5];

// // Expected 139629729 in sequence 9,8,7,6,5
// aInstructionSet = [3, 26, 1001, 26, -4, 26, 3, 27, 1002, 27, 2, 27, 1, 27, 26, 27, 4, 27, 1001, 28, -1, 28, 1005, 28, 6, 99, 0, 0, 5];

// For each sequence, start
let aMaxSequence = [];
let iMaxValue = 0;

for (let i = 0; i < aSequences.length; i++) {
    let iNextInput = 0;
    let aSequence = aSequences[i];
    // let aSequence = [5, 6, 7, 8, 9];

    let aVMs = [];
    let bKeepRunning = true;
    for (let i = 0; bKeepRunning; i++) {
        if (aVMs[i % 5] == undefined) {
            let processor = new OpcodeProcessor(aInstructionSet, [aSequence[i % 5], iNextInput], false);
            processor.set_title("VM-" + String.fromCharCode(65 + i % 5));
            processor.run()
            iNextInput = processor.get_last_output();
            aVMs.push(processor);
        } else {
            aVMs[i % 5].set_inputs([iNextInput]);
            bKeepRunning = aVMs[i % 5].run();
            if (bKeepRunning) {
                iNextInput = aVMs[i % 5].get_last_output();
            }
        }
    }
    if (iNextInput > iMaxValue) {
        iMaxValue = iNextInput;
        aMaxSequence = aSequence;
    }
}

console.log(format_sequence(aMaxSequence) + ": " + iMaxValue);
