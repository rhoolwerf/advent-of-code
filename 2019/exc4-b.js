function validate(sNumber) {
    var bDoubleDigit = false;

    var aDigitCount = [];
    for (var i = 0; i < sNumber.length; i++) {
        // Check if the next number is same or higher
        if (i < sNumber.length - 1 && sNumber.charAt(i + 1) < sNumber.charAt(i)) {
            return false;
        }

        if (aDigitCount[sNumber.charAt(i)] == undefined) {
            aDigitCount[sNumber.charAt(i)] = 1;
        } else {
            aDigitCount[sNumber.charAt(i)]++;
        }
    }

    aDigitCount.forEach(iDigitCount => {
        if (iDigitCount == 2) {
            bDoubleDigit = true;
        }
    });

    return bDoubleDigit;
}

var iCount = 0;
for (var x = 134792; x <= 675810; x++) {
    if (validate(String(x))) { iCount++; }
}
console.log(iCount);

// console.log(validate(String(112233)));
// console.log(validate(String(123444)));
// console.log(validate(String(111122)));
