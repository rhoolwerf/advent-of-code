// Read input
const { ADDRGETNETWORKPARAMS } = require('dns');
const fs = require('fs');
const { exit } = require('process');
const { start } = require('repl');
const sInput = fs.readFileSync('./exc20.in', 'utf8');
let aInputLines = sInput.split("\n");

// Build a graph and register all portals
const astar = require('./astar.js');
var aGraphData = [];
let aMapData = [];
var oPortals = new Map();

for (let iY = 0; iY < aInputLines.length; iY++) {
    aGraphData[iY] = [];
    aMapData[aMapData.length] = aInputLines[iY].split("");
    for (let iX = 0; iX < aInputLines[iY].length; iX++) {
        let sChar = aInputLines[iY][iX];
        if (sChar == '.') {
            // As it's a path, notify the graph
            aGraphData[iY][iX] = 1;

            // It could very well be a portal location
            let sPortalName = "";
            if (aInputLines[iY - 1][iX].charCodeAt(0) >= 65 && aInputLines[iY - 1][iX].charCodeAt(0) <= 90) {
                // Upper edge
                sPortalName = aInputLines[iY - 2][iX] + aInputLines[iY - 1][iX];
            } else if (aInputLines[iY + 1][iX].charCodeAt(0) >= 65 && aInputLines[iY + 1][iX].charCodeAt(0) <= 90) {
                // Lower edge
                sPortalName = aInputLines[iY + 1][iX] + aInputLines[iY + 2][iX];
            } else if (aInputLines[iY][iX - 1].charCodeAt(0) >= 65 && aInputLines[iY][iX - 1].charCodeAt(0) <= 90) {
                // Left edge
                sPortalName = aInputLines[iY][iX - 2] + aInputLines[iY][iX - 1];
            } else if (aInputLines[iY][iX + 1].charCodeAt(0) >= 65 && aInputLines[iY][iX + 1].charCodeAt(0) <= 90) {
                // Right edge
                sPortalName = aInputLines[iY][iX + 1] + aInputLines[iY][iX + 2];
            }
            // Is this location a portal?
            if (sPortalName != "") {
                // console.log("Portal '" + sPortalName + "' found at " + iX + "," + iY + "");
                if (!oPortals.has(sPortalName + "1")) { oPortals.set(sPortalName + "1", { x: iX, y: iY }); }
                else { oPortals.set(sPortalName + "2", { x: iX, y: iY }); }
            }
        } else {
            // Anything not a walkable path is unwalkable as far as the graoh is concerned
            aGraphData[iY][iX] = 0;
        }
    }
}
// console.log(aInputLines.join("\n"));

// For each portal, determine route-length (and possibility) to all other portals
var oPortalRoutes = new Map();
var oPortalCombinations = new Set();
let oGraph = new astar.Graph(aGraphData);

oPortals.forEach((oFirstPortalCoordinates, sFirstPortalName) => {
    oPortals.forEach((oSecondPortalCoordinates, sSecondPortalName) => {
        if (sFirstPortalName != sSecondPortalName) {
            if (oPortalCombinations.has(sFirstPortalName + "-" + sSecondPortalName) || oPortalCombinations.has(sSecondPortalName + "-" + sFirstPortalName)) {
                // Combination already exists, no need for another check
            } else {
                oPortalCombinations.add(sFirstPortalName + "-" + sSecondPortalName);
                let oFirstPoint = oGraph.grid[oFirstPortalCoordinates.y][oFirstPortalCoordinates.x];
                let oSecondPoint = oGraph.grid[oSecondPortalCoordinates.y][oSecondPortalCoordinates.x];
                let aPath = astar.astar.search(oGraph, oFirstPoint, oSecondPoint);
                if (aPath.length > 0) {
                    if (!oPortalRoutes.has(sFirstPortalName)) { oPortalRoutes.set(sFirstPortalName, new Map()); }
                    oPortalRoutes.get(sFirstPortalName).set(sSecondPortalName, { steps: aPath.length, path: aPath });

                    if (!oPortalRoutes.has(sSecondPortalName)) { oPortalRoutes.set(sSecondPortalName, new Map()); }
                    oPortalRoutes.get(sSecondPortalName).set(sFirstPortalName, { steps: aPath.length, path: aPath })
                }
            }
        }
    });
});

// console.log(oPortalRoutes);

// Try to traverse
let iLeastStepsTaken = 0;
let sShortestRoute = "";
let aShortestPortalPath = "";
traverse(new Set().add("AA1"), "AA1", 0, 0);
// visualise();
console.log("\nShortest route takes " + iLeastStepsTaken + " steps using route " + sShortestRoute);

function traverse(oPortalsUsed, sCurrentPortal, iStepsTaken, iDepth) {
    // // Don't go too deep
    // if (iDepth > 1) { return; }

    // Who are we?
    print("I'm at " + sCurrentPortal, iDepth);

    // Grab all available routes from the Portal and traverse the route
    let aAvailableRoutes = oPortalRoutes.get(sCurrentPortal);
    aAvailableRoutes.forEach((oRouteData, sTargetPortal) => {
        if (sTargetPortal == "ZZ1") {
            let iFinalStepsTaken = iStepsTaken + oRouteData.steps;
            let sPortalRoute = format_portal_route(new Set(oPortalsUsed).add("ZZ1"));
            // console.log("ZZ reached in " + iFinalStepsTaken + " steps through route: " + sPortalRoute);
            // print("ZZ reached in " + iFinalStepsTaken + " steps through route: " + sPortalRoute, iDepth + 1);
            if (iLeastStepsTaken == 0 || iFinalStepsTaken < iLeastStepsTaken) {
                iLeastStepsTaken = iFinalStepsTaken;
                sShortestRoute = sPortalRoute;
                aShortestPortalPath = Array.from(new Set(oPortalsUsed).add("ZZ1"));
            }
        } else {
            if (oPortalsUsed.has(sTargetPortal)) {
                print("Skipping portal " + sTargetPortal + " as we've already been there", iDepth);
            } else {
                // Create a new list of portals used
                let oNewPortalsUsed = new Set(oPortalsUsed);
                oNewPortalsUsed.add(sTargetPortal);

                // Which end of the Portal are we? Move next from the other side
                let sOtherSide = sTargetPortal.substring(0, 2) + (sTargetPortal.charAt(2) == 1 ? 2 : 1);
                traverse(oNewPortalsUsed, sOtherSide, iStepsTaken + 1 + oRouteData.steps, iDepth + 1);

            }
        }
    });
    // console.log(aAvailableRoutes);
}

function print(sMessage, iDepth) {
    return;
    let sOutput = sMessage;
    for (let i = 0; i < iDepth; i++) {
        sOutput = "  " + sOutput;
    }
    console.log(sOutput);
}

function format_portal_route(oRoute) {
    return Array.from(oRoute).map(x => x.substring(0, 2)).join("-");
}

function visualise() {
    let OpcodeProcessor = require('./opcode_processor.js');
    let processor = new OpcodeProcessor([], []);

    let sCurrentPortal = aShortestPortalPath.shift();
    let iSteps = 0;
    while (aShortestPortalPath.length > 0) {
        let sNextPortal = aShortestPortalPath.shift();

        // Mark the current portal on the map
        let oCurrentPortal = oPortals.get(sCurrentPortal);
        aMapData[oCurrentPortal.y][oCurrentPortal.x] = "?";
        if (sNextPortal != "ZZ1") { iSteps++; }
        processor.clean_console();
        draw_map(iSteps);
        processor.delay(750);

        // Mark all path-points on the map
        console.log("From " + sCurrentPortal + " to " + sNextPortal);
        let oPath = oPortalRoutes.get(sCurrentPortal).get(sNextPortal);
        while (oPath.path.length > 0) {
            let oGridNode = oPath.path.shift();
            aMapData[oGridNode.x][oGridNode.y] = "?";
            iSteps++;

            processor.clean_console();
            draw_map(iSteps);
            processor.delay(125);
        }
        sCurrentPortal = sNextPortal.substring(0, 2) + (sNextPortal.charAt(2) == 1 ? 2 : 1);
    }

}
function draw_map(iSteps) {
    aMapData.forEach(aMapLine => {
        let sOutput = "";
        aMapLine.forEach(sCell => {
            switch (sCell) {
                case '#': sOutput += '\x1b[36m\x1b[46m' + sCell + '\x1b[0m'; break;
                case '?': sOutput += '\x1b[92m\x1b[102m' + sCell + '\x1b[0m'; break;

                default: sOutput += sCell; break;
            }
        })
        console.log(sOutput);
    });
    console.log("Steps: " + iSteps);
}