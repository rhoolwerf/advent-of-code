function validate(sNumber) {
    var bDoubleDigit = false;

    for (var i = 0; i < sNumber.length - 1; i++) {
        // Check if the next number is same or higher
        if (sNumber.charAt(i+1) < sNumber.charAt(i)) {
            return false;
        }

        // Check if number part of double digit
        if (sNumber.charAt(i) == sNumber.charAt(i+1)) {
            bDoubleDigit = true;
        }
    }

    return bDoubleDigit;
}

var iCount = 0;
for (var x = 134792; x <= 675810; x++) {
    if (validate(String(x))) { iCount++; }
}
console.log(iCount);

// console.log(validate(String(111111)));
// console.log(validate(String(223450)));
// console.log(validate(String(123789)));