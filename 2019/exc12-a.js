let aInput = [];
let iMaxSteps = 0;

// First example, should total to 179
aInput = [
    '<x=-1, y=0, z=2>',
    '<x=2, y=-10, z=-7>',
    '<x=4, y=-8, z=8>',
    '<x=3, y=5, z=-1>'
];
iMaxSteps = 10;

// Second example, should total to 1940
aInput = [
    '<x=-8, y=-10, z=0>',
    '<x=5, y=5, z=10>',
    '<x=2, y=-7, z=3>',
    '<x=9, y=-8, z=-3>'
];
iMaxSteps = 100;

// Puzzle input
aInput = [
    '<x=19, y=-10, z=7>',
    '<x=1, y=2, z=-3>',
    '<x=14, y=-4, z=1>',
    '<x=8, y=7, z=-6>'
];
iMaxSteps = 1000;

// Parse input into moons
let aMoons = []
aInput.forEach((sInput, iIndex) => {
    // <x=-1, y=0, z=2>
    aInputChunks = sInput.replace('>', '').split(', ');
    aMoons.push({
        pos: {
            x: Number(aInputChunks[0].split('=')[1]),
            y: Number(aInputChunks[1].split('=')[1]),
            z: Number(aInputChunks[2].split('=')[1])
        },
        vel: {
            x: 0,
            y: 0,
            z: 0
        }
    })
});

// Generate unique pairs of moons
let aMoonCombinations = new Map();
for (let iFirstMoon = 0; iFirstMoon < aMoons.length - 1; iFirstMoon++) {
    aMoonCombinations.set(iFirstMoon, []);
    for (let iSecondMoon = iFirstMoon + 1; iSecondMoon < aMoons.length; iSecondMoon++) {
        aMoonCombinations.get(iFirstMoon).push(iSecondMoon);
    }
}

for (let iSteps = 0; iSteps <= iMaxSteps; iSteps++) {
    // console.log('After ' + pad_number(iSteps, 5) + ' steps:');
    // output_moons();
    // console.log('');

    if (iSteps < iMaxSteps) {
        // Calculate and apply gravity
        aMoonCombinations.forEach((oSecondMoons, iFirstIndex) => {
            oSecondMoons.forEach(iSecondIndex => {
                // Consider each pair of moons
                let oFirstMoon = aMoons[iFirstIndex];
                let oSecondMoon = aMoons[iSecondIndex];

                if (oFirstMoon.pos.x < oSecondMoon.pos.x) { oFirstMoon.vel.x++; oSecondMoon.vel.x--; }
                else if (oFirstMoon.pos.x > oSecondMoon.pos.x) { oFirstMoon.vel.x--; oSecondMoon.vel.x++; }

                if (oFirstMoon.pos.y < oSecondMoon.pos.y) { oFirstMoon.vel.y++; oSecondMoon.vel.y--; }
                else if (oFirstMoon.pos.y > oSecondMoon.pos.y) { oFirstMoon.vel.y--; oSecondMoon.vel.y++; }

                if (oFirstMoon.pos.z < oSecondMoon.pos.z) { oFirstMoon.vel.z++; oSecondMoon.vel.z--; }
                else if (oFirstMoon.pos.z > oSecondMoon.pos.z) { oFirstMoon.vel.z--; oSecondMoon.vel.z++; }
            });
        });

        // Apply velocity to the positions
        aMoons.forEach(oMoon => {
            oMoon.pos.x += oMoon.vel.x;
            oMoon.pos.y += oMoon.vel.y;
            oMoon.pos.z += oMoon.vel.z;
        });
    } else {
        output_energy(iSteps);
    }
}

function output_energy(iSteps) {
    let sOutput = "Energy after " + iSteps + " steps:";
    let aMoonTotal = [];
    let iTotal = 0;
    aMoons.forEach(oMoon => {
        if (sOutput != "") { sOutput += "\n"; }
        let iMoonPotX = Math.abs(oMoon.pos.x);
        let iMoonPotY = Math.abs(oMoon.pos.y);
        let iMoonPotZ = Math.abs(oMoon.pos.z);
        let iMoonPotTot = iMoonPotX + iMoonPotY + iMoonPotZ;

        let iMoonKinX = Math.abs(oMoon.vel.x);
        let iMoonKinY = Math.abs(oMoon.vel.y);
        let iMoonKinZ = Math.abs(oMoon.vel.z);
        let iMoonKinTot = iMoonKinX + iMoonKinY + iMoonKinZ;

        let iMoonTotal = iMoonPotTot * iMoonKinTot;
        iTotal += iMoonTotal;
        aMoonTotal.push(iMoonTotal);

        sOutput += "pot: " + pad_number(iMoonPotX) + " + " + pad_number(iMoonPotY) + " + " + pad_number(iMoonPotZ) + " = " + pad_number(iMoonPotTot);
        sOutput += "; kin: " + pad_number(iMoonKinX) + " + " + pad_number(iMoonKinY) + " + " + pad_number(iMoonKinZ) + " = " + pad_number(iMoonKinTot);
        sOutput += "; total: " + pad_number(iMoonPotTot) + " * " + pad_number(iMoonKinTot) + " = " + pad_number(iMoonTotal);
    });
    sOutput += "\nSum of total energy: ";
    aMoonTotal.forEach((iMoonTotal, iMoonIndex) => {
        if (iMoonIndex > 0) {
            sOutput += " + ";
        }
        sOutput += pad_number(iMoonTotal);
    });
    sOutput += " = " + pad_number(iTotal);
    console.log(sOutput);
}

function output_moons() {
    aMoons.forEach(oMoon => {
        console.log(
            "pos=<x=" + pad_number(oMoon.pos.x) +
            ", y=" + pad_number(oMoon.pos.y) +
            ", z=" + pad_number(oMoon.pos.z) +
            ">, vel=<x=" + pad_number(oMoon.vel.x) +
            ", y=" + pad_number(oMoon.vel.y) +
            ", z=" + pad_number(oMoon.vel.z) +
            ">"
        );
    });
}
function pad_number(iNumber, iTotalLength = 4) {
    let sNumber = String(iNumber);
    while (sNumber.length < iTotalLength) {
        sNumber = " " + sNumber;
    }
    return sNumber;
}