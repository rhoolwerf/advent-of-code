// Input conversion: echo "" | tr '\n' , | sed -e 's/,/'\',\''/g'
let aMap = [];

// Test 1 -> location 3,4 has 8 possible visions
aMap = ['.#..#', '.....', '#####', '....#', '...##'];

// Test 2 -> Best is 5,8 with 33 other asteroids detected:
aMap = ['......#.#.', '#..#.#....', '..#######.', '.#.#.###..', '.#..#.....', '..#....#.#', '#..#....#.', '.##.#..###', '##...#..#.', '.#....####'];

// Test 3 -> Best is 1,2 with 35 other asteroids detected
aMap = ['#.#...#.#.', '.###....#.', '.#....#...', '##.#.#.#.#', '....#.#.#.', '.##..###.#', '..#...##..', '..##....##', '......#...', '.####.###.'];

// Test 4 -> Best is 6,3 with 41 other asteroids detected:
aMap = ['.#..#..###', '####.###.#', '....###.#.', '..###.##.#', '##.##.#.#.', '....###..#', '..#.#..#.#', '#..#.#.###', '.##...##.#', '.....#.#..'];

// Test 5 -> Best is 11,13 with 210 other asteroids detected:
aMap = ['.#..##.###...#######', '##.############..##.', '.#.######.########.#', '.###.#######.####.#.', '#####.##.#.##.###.##', '..#####..#.#########', '####################', '#.####....###.#.#.##', '##.#################', '#####.##.###..####..', '..######..##.#######', '####.##.####...##..#', '.#####..#.######.###', '##...#.##########...', '#.##########.#######', '.####.#.###.###.#.##', '....##.##.###..#####', '.#.#.###########.###', '#.#.#.#####.####.###', '###.##.####.##.#..##'];

// Puzzle input
aMap = ['#....#.....#...#.#.....#.#..#....#', '#..#..##...#......#.....#..###.#.#', '#......#.#.#.....##....#.#.....#..', '..#.#...#.......#.##..#...........', '.##..#...##......##.#.#...........', '.....#.#..##...#..##.....#...#.##.', '....#.##.##.#....###.#........####', '..#....#..####........##.........#', '..#...#......#.#..#..#.#.##......#', '.............#.#....##.......#...#', '.#.#..##.#.#.#.#.......#.....#....', '.....##.###..#.....#.#..###.....##', '.....#...#.#.#......#.#....##.....', '##.#.....#...#....#...#..#....#.#.', '..#.............###.#.##....#.#...', '..##.#.........#.##.####.........#', '##.#...###....#..#...###..##..#..#', '.........#.#.....#........#.......', '#.......#..#.#.#..##.....#.#.....#', '..#....#....#.#.##......#..#.###..', '......##.##.##...#...##.#...###...', '.#.....#...#........#....#.###....', '.#.#.#..#............#..........#.', '..##.....#....#....##..#.#.......#', '..##.....#.#......................', '.#..#...#....#.#.....#.........#..', '........#.............#.#.........', '#...#.#......#.##....#...#.#.#...#', '.#.....#.#.....#.....#.#.##......#', '..##....#.....#.....#....#.##..#..', '#..###.#.#....#......#...#........', '..#......#..#....##...#.#.#...#..#', '.#.##.#.#.....#..#..#........##...', '....#...##.##.##......#..#..##....'];

// Parse map into stars
let aStars = [];
for (let iRow = 0; iRow < aMap.length; iRow++) {
    for (let iColumn = 0; iColumn < aMap[iRow].length; iColumn++) {
        if (aMap[iRow].charAt(iColumn) == '#') {
            aStars.push([iColumn, iRow]);
        }
    }
}

// Now for each star, calculate angle towards all other stars
let aAngleCounter = [];
let aAngleAndDistance = [];

let oStarWithMostUniqueAngles = [];
oStarWithMostUniqueAngles[0] = 0; // Largest angle count
oStarWithMostUniqueAngles[1] = []; // Star coordinates
oStarWithMostUniqueAngles[2] = []; // List of angles
oStarWithMostUniqueAngles[3] = []; // List of stars per angle and their distances

aStars.forEach(oStar => {
    aAngleCounter[oStar] = new Set();
    aStarsPerAngle = [];
    aStars.forEach(oOtherStar => {

        // Only if it not concerns itself
        if (oOtherStar[1] != oStar[1] || oOtherStar[0] != oStar[0]) {
            let iDiffX = oOtherStar[1] - oStar[1];
            let iDiffY = oOtherStar[0] - oStar[0];

            // Calculate angle, with offset for direction
            // let iAngle = ((Math.atan2(iDiffY, iDiffX) * 180 / Math.PI) + 360) % 360;
            let iAngle = ((Math.atan2(iDiffY, iDiffX) * 180 / Math.PI) + 180);
            let iDistance = Math.abs(iDiffX + iDiffY);

            // Add to set, which removes duplicates already
            aAngleCounter[oStar].add(iAngle);

            // Register the star with distance at this angle
            if (aStarsPerAngle[iAngle] == undefined) {
                aStarsPerAngle[iAngle] = [];
            }
            aStarsPerAngle[iAngle].push({
                distance: iDistance,
                coords: oOtherStar
            });
        }
    });
    if (aAngleCounter[oStar].size > oStarWithMostUniqueAngles[0]) {
        oStarWithMostUniqueAngles[0] = aAngleCounter[oStar].size;
        oStarWithMostUniqueAngles[1] = oStar;
        oStarWithMostUniqueAngles[2] = Array.from(aAngleCounter[oStar]);
        oStarWithMostUniqueAngles[3] = aStarsPerAngle;
    }
});
console.log("Star at " + oStarWithMostUniqueAngles[1][1] + "," + oStarWithMostUniqueAngles[1][0] + " has most visible stars: " + oStarWithMostUniqueAngles[0]);

// Grab a proper reference to list with unique angles
let aUniqueAngles = oStarWithMostUniqueAngles[2];
aUniqueAngles.sort((a, b) => b - a);

let aOtherStars = oStarWithMostUniqueAngles[3];

// Sort the distances within the angles
aUniqueAngles.forEach(iAngle => {
    aOtherStars[iAngle].sort((a, b) => {
        return a.distance - b.distance;
    });
});


let bKeepRunning = true;
let iKillCounter = 1;
while (bKeepRunning) {
    bKeepRunning = false;

    for (let i = 0; i < aUniqueAngles.length; i++) {
        let iAngleToShoot = aUniqueAngles[i];
        if (aOtherStars[iAngleToShoot].length > 0) {
            bKeepRunning = true;

            let oStarToKill = aOtherStars[iAngleToShoot].shift();
            // console.log("The " + iKillCounter + "th star to die is at " + oStarToKill.coords);
            if (iKillCounter == 200) {
                console.log("The " + iKillCounter + "th star to die is at " + oStarToKill.coords);
                console.log(oStarToKill.coords[0] * 100 + oStarToKill.coords[1]);
            }

            iKillCounter++;
        }
    }
}

function pad_number(iNumber) {
    let sNumber = String(iNumber);
    while (sNumber.length < 3) { sNumber += " "; }
    return sNumber;
}