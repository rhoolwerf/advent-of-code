// Different modes of reading/writing positions
const MODE_POSITION = 0;
const MODE_IMMEDIATE = 1;

// Different OPCODES
const OPCODE_EXIT = 99;
const OPCODE_ADD = 1;
const OPCODE_MULTI = 2;
const OPCODE_INPUT = 3;
const OPCODE_OUTPUT = 4;
const OPCODE_JUMP_IF_TRUE = 5;
const OPCODE_JUMP_IF_FALSE = 6;
const OPCODE_LESS_THAN = 7;
const OPCODE_EQUALS = 8;

// Input of the puzzle
let aInstructions = [];
let sInput = 5;
let bKeepRunning = true;

// Debugging is so desired
let bDebug = false;
function debug(sMessage) { if (bDebug) { console.log(sMessage); } }

// Process arguments (to determine if we want to debug)
process.argv.forEach(function (val, index, array) {
    if (val.toLowerCase() == '-d') { bDebug = true; }
});

// The program 3,0,4,0,99 outputs whatever it gets as input, then halts.
aInstructions = [3, 0, 4, 0, 99]; // Should result in reading input 1, writing output 1

// This instruction multiplies its first two parameters. The first parameter, 4 in position mode, works like 
// it did before - its value is the value stored at address 4 (33). The second parameter, 3 in immediate mode, 
// simply has value 3. The result of this operation, 33 * 3 = 99, is written according to the third parameter, 
// 4 in position mode, which also works like it did before - 99 is written to address 4.
aInstructions = [1002, 4, 3, 4, 33];

// Negative integer test
aInstructions = [1101, 100, -1, 4, 0];

// Actual puzzle input
aInstructions = [3, 225, 1, 225, 6, 6, 1100, 1, 238, 225, 104, 0, 1102, 72, 20, 224, 1001, 224, -1440, 224, 4, 224, 102, 8, 223, 223, 1001, 224, 5, 224, 1, 224, 223, 223, 1002, 147, 33, 224, 101, -3036, 224, 224, 4, 224, 102, 8, 223, 223, 1001, 224, 5, 224, 1, 224, 223, 223, 1102, 32, 90, 225, 101, 65, 87, 224, 101, -85, 224, 224, 4, 224, 1002, 223, 8, 223, 101, 4, 224, 224, 1, 223, 224, 223, 1102, 33, 92, 225, 1102, 20, 52, 225, 1101, 76, 89, 225, 1, 117, 122, 224, 101, -78, 224, 224, 4, 224, 102, 8, 223, 223, 101, 1, 224, 224, 1, 223, 224, 223, 1102, 54, 22, 225, 1102, 5, 24, 225, 102, 50, 84, 224, 101, -4600, 224, 224, 4, 224, 1002, 223, 8, 223, 101, 3, 224, 224, 1, 223, 224, 223, 1102, 92, 64, 225, 1101, 42, 83, 224, 101, -125, 224, 224, 4, 224, 102, 8, 223, 223, 101, 5, 224, 224, 1, 224, 223, 223, 2, 58, 195, 224, 1001, 224, -6840, 224, 4, 224, 102, 8, 223, 223, 101, 1, 224, 224, 1, 223, 224, 223, 1101, 76, 48, 225, 1001, 92, 65, 224, 1001, 224, -154, 224, 4, 224, 1002, 223, 8, 223, 101, 5, 224, 224, 1, 223, 224, 223, 4, 223, 99, 0, 0, 0, 677, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1105, 0, 99999, 1105, 227, 247, 1105, 1, 99999, 1005, 227, 99999, 1005, 0, 256, 1105, 1, 99999, 1106, 227, 99999, 1106, 0, 265, 1105, 1, 99999, 1006, 0, 99999, 1006, 227, 274, 1105, 1, 99999, 1105, 1, 280, 1105, 1, 99999, 1, 225, 225, 225, 1101, 294, 0, 0, 105, 1, 0, 1105, 1, 99999, 1106, 0, 300, 1105, 1, 99999, 1, 225, 225, 225, 1101, 314, 0, 0, 106, 0, 0, 1105, 1, 99999, 1107, 677, 226, 224, 1002, 223, 2, 223, 1005, 224, 329, 101, 1, 223, 223, 7, 677, 226, 224, 102, 2, 223, 223, 1005, 224, 344, 1001, 223, 1, 223, 1107, 226, 226, 224, 1002, 223, 2, 223, 1006, 224, 359, 1001, 223, 1, 223, 8, 226, 226, 224, 1002, 223, 2, 223, 1006, 224, 374, 101, 1, 223, 223, 108, 226, 226, 224, 102, 2, 223, 223, 1005, 224, 389, 1001, 223, 1, 223, 1008, 226, 226, 224, 1002, 223, 2, 223, 1005, 224, 404, 101, 1, 223, 223, 1107, 226, 677, 224, 1002, 223, 2, 223, 1006, 224, 419, 101, 1, 223, 223, 1008, 226, 677, 224, 1002, 223, 2, 223, 1006, 224, 434, 101, 1, 223, 223, 108, 677, 677, 224, 1002, 223, 2, 223, 1006, 224, 449, 101, 1, 223, 223, 1108, 677, 226, 224, 102, 2, 223, 223, 1006, 224, 464, 1001, 223, 1, 223, 107, 677, 677, 224, 102, 2, 223, 223, 1005, 224, 479, 101, 1, 223, 223, 7, 226, 677, 224, 1002, 223, 2, 223, 1006, 224, 494, 1001, 223, 1, 223, 7, 677, 677, 224, 102, 2, 223, 223, 1006, 224, 509, 101, 1, 223, 223, 107, 226, 677, 224, 1002, 223, 2, 223, 1006, 224, 524, 1001, 223, 1, 223, 1007, 226, 226, 224, 102, 2, 223, 223, 1006, 224, 539, 1001, 223, 1, 223, 108, 677, 226, 224, 102, 2, 223, 223, 1005, 224, 554, 101, 1, 223, 223, 1007, 677, 677, 224, 102, 2, 223, 223, 1006, 224, 569, 101, 1, 223, 223, 8, 677, 226, 224, 102, 2, 223, 223, 1006, 224, 584, 1001, 223, 1, 223, 1008, 677, 677, 224, 1002, 223, 2, 223, 1006, 224, 599, 1001, 223, 1, 223, 1007, 677, 226, 224, 1002, 223, 2, 223, 1005, 224, 614, 101, 1, 223, 223, 1108, 226, 677, 224, 1002, 223, 2, 223, 1005, 224, 629, 101, 1, 223, 223, 1108, 677, 677, 224, 1002, 223, 2, 223, 1005, 224, 644, 1001, 223, 1, 223, 8, 226, 677, 224, 1002, 223, 2, 223, 1006, 224, 659, 101, 1, 223, 223, 107, 226, 226, 224, 102, 2, 223, 223, 1005, 224, 674, 101, 1, 223, 223, 4, 223, 99, 226];

// Process the instructions
for (var iInstructionPointer = 0; iInstructionPointer < aInstructions.length && bKeepRunning;) {

    // Determine instruction and pad where needed (as leading zero's are omitted)
    var sInstruction = String(aInstructions[iInstructionPointer]);
    for (let i = sInstruction.length; i < 5; i++) {
        sInstruction = "0" + sInstruction;
    }

    // Split the instruction into OPCODE and modes
    let sOpcode = String(sInstruction.substring(3, 5));
    let iMode1 = Number(sInstruction.substring(2, 3));
    let iMode2 = Number(sInstruction.substring(1, 2));
    let iMode3 = Number(sInstruction.substring(0, 1));

    // Process the OPCODE
    if (iInstructionPointer > 0) { debug(''); }
    debug("Processing opcode: " + sOpcode);
    switch (Number(sOpcode)) {
        case OPCODE_EXIT:
            // Exit-code, stop processing
            bKeepRunning = false;
            break;
        case OPCODE_OUTPUT:
            // Read from position and output to screen
            let iValue = read_position(iInstructionPointer + 1, iMode1);
            console.log("Output: " + iValue);
            iInstructionPointer += 2;
            continue;
        case OPCODE_INPUT:
            // Writes Input to Instruction-set
            debug("Reading input: " + sInput);
            write_position(iInstructionPointer + 1, iMode1, sInput);
            iInstructionPointer += 2;
            continue;
        case OPCODE_MULTI:
            // Multiply parameters 1 and 2 and store in 3
            let iMultiValue1 = read_position(iInstructionPointer + 1, iMode1);
            let iMultiValue2 = read_position(iInstructionPointer + 2, iMode2);
            let iMultiResult = iMultiValue1 * iMultiValue2;
            write_position(iInstructionPointer + 3, iMode3, iMultiResult);
            iInstructionPointer += 4;
            continue;
        case OPCODE_ADD:
            // Add parameters 1 and 2 and store in 3
            let iAddValue1 = read_position(iInstructionPointer + 1, iMode1);
            let iAddValue2 = read_position(iInstructionPointer + 2, iMode2);
            let iAddResult = iAddValue1 + iAddValue2;
            write_position(iInstructionPointer + 3, iMode3, iAddResult);
            iInstructionPointer += 4;
            continue;
        case OPCODE_JUMP_IF_TRUE:
            // Opcode 5 is jump-if-true: if the first parameter is non-zero, it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing.
            let iJumpIfTrueValue = Number(read_position(iInstructionPointer + 1, iMode1));
            // debug("Jumping if value " + iJumpIfTrueValue + " is anything other than 0");
            if (iJumpIfTrueValue != 0) {
                iInstructionPointer = read_position(iInstructionPointer + 2, iMode2);
            } else {
                iInstructionPointer += 3;
            }
            continue;

        case OPCODE_JUMP_IF_FALSE:
            // Opcode 6 is jump-if-false: if the first parameter is zero, it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing.
            let iJumpIfFalseValue = Number(read_position(iInstructionPointer + 1, iMode1));
            // debug("Jumping if value " + iJumpIfFalseValue + " is anything other than 0");
            if (iJumpIfFalseValue == 0) {
                iInstructionPointer = read_position(iInstructionPointer + 2, iMode2);
            } else {
                iInstructionPointer += 3;
            }
            continue;

        case OPCODE_LESS_THAN:
            // Opcode 7 is less than: if the first parameter is less than the second parameter, it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
            let iLessThanValue1 = read_position(iInstructionPointer + 1, iMode1);
            let iLessThanValue2 = read_position(iInstructionPointer + 2, iMode2);
            if (iLessThanValue1 < iLessThanValue2) {
                write_position(iInstructionPointer + 3, iMode3, 1);
            } else {
                write_position(iInstructionPointer + 3, iMode3, 0);
            }
            iInstructionPointer += 4;
            continue;

        case OPCODE_EQUALS:
            // Opcode 8 is equals: if the first parameter is equal to the second parameter, it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
            let iEqualsValue1 = read_position(iInstructionPointer + 1, iMode1);
            let iEqualsValue2 = read_position(iInstructionPointer + 2, iMode2);
            if (iEqualsValue1 == iEqualsValue2) {
                write_position(iInstructionPointer + 3, iMode3, 1);
            } else {
                write_position(iInstructionPointer + 3, iMode3, 0);
            }
            iInstructionPointer += 4;
            continue;

        default:
            console.log("Error: Opcode '" + sOpcode + "' in instruction '" + sInstruction + "' at position '" + iInstructionPointer + "' is not recognised");
            bKeepRunning = false;
            break;
    }
}

function read_position(iPointer, iMode) {
    let iAddress = 0;

    switch (iMode) {
        case MODE_IMMEDIATE:
            iAddress = iPointer;
            break;
        case MODE_POSITION:
            iAddress = aInstructions[iPointer];
            break;
    }

    debug("Reading pointer " + iPointer + " in mode " + iMode + " resulting in address " + iAddress + " and value " + Number(aInstructions[iAddress]));

    return Number(aInstructions[iAddress]);
}

function write_position(iPointer, iMode, iValue) {
    let iAddress = 0;

    switch (iMode) {
        case MODE_IMMEDIATE:
            iAddress = iPointer;
            break;
        case MODE_POSITION:
            iAddress = aInstructions[iPointer];
            break;
    }

    debug("Writing value " + iValue + " at pointer " + iPointer + " in mode " + iMode + " so address " + iAddress);

    aInstructions[iAddress] = Number(iValue);
}