const EMPTY = '.';
const BUG = '#';

// Test input
let aState = [
    '....#',
    '#..#.',
    '#..##',
    '..#..',
    '#....'
];

aState = [
    '.##.#',
    '###..',
    '#...#',
    '##.#.',
    '.###.'
];

// console.log('Initial state:');
// console.log(aState.join("\n") + "\n");

let oPreviousStates = new Set();
for (let iMinute = 1; iMinute <= 400; iMinute++) {
    oPreviousStates.add(aState.join("\n"));
    let aNewState = [];

    for (let iY = 0; iY < aState.length; iY++) {
        aNewState[iY] = "";
        for (let iX = 0; iX < aState[0].length; iX++) {
            // Determine amount of adjacent bugs
            let iAdjacentBugs = 0;
            if (get_state_point(iY - 1, iX) == BUG) { iAdjacentBugs++; }
            if (get_state_point(iY + 1, iX) == BUG) { iAdjacentBugs++; }
            if (get_state_point(iY, iX - 1) == BUG) { iAdjacentBugs++; }
            if (get_state_point(iY, iX + 1) == BUG) { iAdjacentBugs++; }

            // If current tile is a bug and there aren't the right amount adjacent, kill it
            switch (get_state_point(iY, iX)) {
                case BUG:
                    if (iAdjacentBugs != 1) { aNewState[iY] += EMPTY; }
                    else { aNewState[iY] += BUG; }
                    break;
                case EMPTY:
                    if (iAdjacentBugs == 1 || iAdjacentBugs == 2) { aNewState[iY] += BUG; }
                    else { aNewState[iY] += EMPTY; }
                    break;
            }
        }
    }

    // console.log("After " + iMinute + " minute(s):");
    // console.log(aNewState.join("\n") + "\n");
    aState = Array.from(aNewState);

    if (oPreviousStates.has(aState.join("\n"))) {
        console.log("After " + iMinute + " minute(s):");
        console.log(aState.join("\n") + "\n");
        console.log("Layout has appeared twice!");
        console.log("Biodiversity rating: " + calc_biodiversity_rating());
        break;
    }

}

function get_state_point(iY, iX) {
    if (iY < 0 || iY >= aState.length) { return EMPTY; }
    else if (iX < 0 || iX > aState[iY].length) { return EMPTY; }
    else { return aState[iY][iX]; }
}
function calc_biodiversity_rating() {
    iBiodiversityRating = 0;
    for (let iY = 0; iY < aState.length; iY++) {
        for (let iX = 0; iX < aState[0].length; iX++) {
            if (get_state_point(iY, iX) == BUG) {
                let iOffset = (iY) * 5 + (iX + 1);
                let iBugValue = Math.pow(2, (iOffset - 1));
                // console.log("Bug at: " + iX + "," + iY + " at offset: " + iOffset + " using value: " + iBugValue);
                iBiodiversityRating += iBugValue;
            }
        }
    }
    return iBiodiversityRating;
}