// Some constants
const UP = 0;
const LEFT = 1;
const DOWN = 2;
const RIGHT = 3;

const BLACK_PANEL = ' ';
const WHITE_PANEL = '#';
const EMPTY_PANEL = '-';
const BLACK_PAINT = 0;
const WHITE_PAINT = 1;

const TURN_LEFT = 0;
const TURN_RIGHT = 1;

// Opcode Processor
let OpcodeProcessor = require('./opcode_processor.js');

// Some genernal variables
let aInstructions = [3, 8, 1005, 8, 305, 1106, 0, 11, 0, 0, 0, 104, 1, 104, 0, 3, 8, 1002, 8, -1, 10, 101, 1, 10, 10, 4, 10, 1008, 8, 0, 10, 4, 10, 1002, 8, 1, 29, 3, 8, 102, -1, 8, 10, 1001, 10, 1, 10, 4, 10, 108, 1, 8, 10, 4, 10, 1002, 8, 1, 50, 1, 104, 20, 10, 1, 1102, 6, 10, 1006, 0, 13, 3, 8, 102, -1, 8, 10, 101, 1, 10, 10, 4, 10, 108, 1, 8, 10, 4, 10, 102, 1, 8, 83, 1, 1102, 0, 10, 1006, 0, 96, 2, 1004, 19, 10, 3, 8, 1002, 8, -1, 10, 101, 1, 10, 10, 4, 10, 108, 0, 8, 10, 4, 10, 101, 0, 8, 116, 3, 8, 1002, 8, -1, 10, 1001, 10, 1, 10, 4, 10, 108, 1, 8, 10, 4, 10, 102, 1, 8, 138, 1006, 0, 60, 1, 1008, 12, 10, 3, 8, 102, -1, 8, 10, 101, 1, 10, 10, 4, 10, 1008, 8, 0, 10, 4, 10, 102, 1, 8, 168, 1006, 0, 14, 1006, 0, 28, 3, 8, 1002, 8, -1, 10, 1001, 10, 1, 10, 4, 10, 108, 0, 8, 10, 4, 10, 101, 0, 8, 195, 2, 1005, 9, 10, 1006, 0, 29, 3, 8, 1002, 8, -1, 10, 101, 1, 10, 10, 4, 10, 108, 1, 8, 10, 4, 10, 1002, 8, 1, 224, 2, 1009, 8, 10, 1, 3, 5, 10, 3, 8, 1002, 8, -1, 10, 101, 1, 10, 10, 4, 10, 108, 1, 8, 10, 4, 10, 102, 1, 8, 254, 3, 8, 102, -1, 8, 10, 1001, 10, 1, 10, 4, 10, 1008, 8, 0, 10, 4, 10, 1002, 8, 1, 277, 1, 1003, 18, 10, 1, 1104, 1, 10, 101, 1, 9, 9, 1007, 9, 957, 10, 1005, 10, 15, 99, 109, 627, 104, 0, 104, 1, 21101, 0, 666681062292, 1, 21102, 322, 1, 0, 1105, 1, 426, 21101, 847073883028, 0, 1, 21102, 333, 1, 0, 1105, 1, 426, 3, 10, 104, 0, 104, 1, 3, 10, 104, 0, 104, 0, 3, 10, 104, 0, 104, 1, 3, 10, 104, 0, 104, 1, 3, 10, 104, 0, 104, 0, 3, 10, 104, 0, 104, 1, 21101, 0, 179356855319, 1, 21102, 1, 380, 0, 1105, 1, 426, 21102, 1, 179356998696, 1, 21102, 1, 391, 0, 1105, 1, 426, 3, 10, 104, 0, 104, 0, 3, 10, 104, 0, 104, 0, 21101, 0, 988669698816, 1, 21101, 0, 414, 0, 1106, 0, 426, 21102, 1, 868494500628, 1, 21102, 425, 1, 0, 1106, 0, 426, 99, 109, 2, 21202, -1, 1, 1, 21102, 1, 40, 2, 21102, 457, 1, 3, 21102, 1, 447, 0, 1105, 1, 490, 109, -2, 2105, 1, 0, 0, 1, 0, 0, 1, 109, 2, 3, 10, 204, -1, 1001, 452, 453, 468, 4, 0, 1001, 452, 1, 452, 108, 4, 452, 10, 1006, 10, 484, 1102, 0, 1, 452, 109, -2, 2105, 1, 0, 0, 109, 4, 1201, -1, 0, 489, 1207, -3, 0, 10, 1006, 10, 507, 21102, 0, 1, -3, 22101, 0, -3, 1, 21202, -2, 1, 2, 21101, 1, 0, 3, 21102, 1, 526, 0, 1106, 0, 531, 109, -4, 2105, 1, 0, 109, 5, 1207, -3, 1, 10, 1006, 10, 554, 2207, -4, -2, 10, 1006, 10, 554, 22101, 0, -4, -4, 1106, 0, 622, 21201, -4, 0, 1, 21201, -3, -1, 2, 21202, -2, 2, 3, 21102, 573, 1, 0, 1106, 0, 531, 21202, 1, 1, -4, 21101, 1, 0, -1, 2207, -4, -2, 10, 1006, 10, 592, 21102, 1, 0, -1, 22202, -2, -1, -2, 2107, 0, -3, 10, 1006, 10, 614, 22101, 0, -1, 1, 21102, 614, 1, 0, 105, 1, 489, 21202, -2, -1, -2, 22201, -4, -2, -4, 109, -5, 2105, 1, 0];
let aInput = [];

// Initiate the processor
let processor = new OpcodeProcessor(aInstructions, aInput);

// Initialize the map, current position and direction
let aMap = new Map();
aMap.set(0, new Map());
aMap.get(0).set(0, BLACK_PANEL); // Initial tile is always black
let iCurrentDirection = UP;
let iX = 0;
let iY = 0;

let bKeepRunning = true;
while (bKeepRunning) {
    // Determine the input for the processor and execute
    if (aMap.has(iY) && aMap.get(iY).get(iX) == WHITE_PANEL) {
        sInput = [WHITE_PAINT];
    } else {
        sInput = [BLACK_PAINT];
    }
    processor.set_inputs(sInput);

    bKeepRunning = processor.run();
    if (!bKeepRunning) { break; }
    let iNextColor = processor.get_last_output();
    bKeepRunning = processor.run();
    let iTurnDirection = processor.get_last_output();

    process_output(iNextColor, iTurnDirection);
}
draw_map();
console.log("Tiles painted at least once: " + count_painted_tiles());

// let iNextColor = WHITE_PAINT;
// let iTurnDirection = TURN_LEFT;

// process_output(WHITE_PAINT, TURN_LEFT);
// draw_map();
// process_output(BLACK_PAINT, TURN_LEFT);
// draw_map();
// process_output(1, 0);
// draw_map();
// process_output(1, 0);
// draw_map();
// process_output(0, 1);
// draw_map();
// process_output(1, 0);
// draw_map();
// process_output(1, 0);
// draw_map();
// console.log("Tiles painted at least once: " + count_painted_tiles());

function process_output(iNextColor, iTurnDirection) {
    // Fix some arrays where needed
    if (!aMap.has(iY)) {
        aMap.set(iY, new Map());
    }

    // Paint the current panel!
    switch (iNextColor) {
        case WHITE_PAINT: aMap.get(iY).set(iX, WHITE_PANEL); break;
        case BLACK_PAINT: aMap.get(iY).set(iX, BLACK_PANEL); break;
        default: console.log('Wrong color detected: ' + iNextColor); return;
    }

    // Turn the robot in the received direction
    switch (iTurnDirection) {
        case TURN_LEFT:
            switch (iCurrentDirection) {
                case UP: iCurrentDirection = LEFT; break;
                case LEFT: iCurrentDirection = DOWN; break;
                case DOWN: iCurrentDirection = RIGHT; break;
                case RIGHT: iCurrentDirection = UP; break;
            }
            break;
        case TURN_RIGHT:
            switch (iCurrentDirection) {
                case UP: iCurrentDirection = RIGHT; break;
                case LEFT: iCurrentDirection = UP; break;
                case DOWN: iCurrentDirection = LEFT; break;
                case RIGHT: iCurrentDirection = DOWN; break;
            }
            break;
    }

    // Take a step forward!
    switch (iCurrentDirection) {
        case UP: iY--; break;
        case LEFT: iX--; break;
        case DOWN: iY++; break;
        case RIGHT: iX++; break;
    }
}

function count_painted_tiles() {
    let iPaintedTilesCount = 0;
    aMap.forEach((aRow, iIndexY) => {
        aRow.forEach((iCell, iIndexX) => {
            iPaintedTilesCount++;
        });
    });
    return iPaintedTilesCount;
}

function draw_map() {
    // Draw map
    let iMinX = 0;
    let iMaxX = 0;
    let iMinY = 0;
    let iMaxY = 0;

    aMap.forEach((aRow, iIndexY) => {
        if (iIndexY < iMinY) { iMinY = iIndexY; }
        if (iIndexY > iMaxY) { iMaxY = iIndexY; }
        aRow.forEach((iCell, iIndexX) => {
            if (iIndexX < iMinX) { iMinX = iIndexX; }
            if (iIndexX > iMaxX) { iMaxX = iIndexX; }
        });
    });
    iMinX--;
    iMinY--;
    iMaxX++;
    iMaxY++;

    // console.log('iMinX: ' + iMinX);
    // console.log('iMaxX: ' + iMaxX);
    // console.log('iMinY: ' + iMinY);
    // console.log('iMaxY: ' + iMaxY);

    let sOutput = "";
    for (let y = iMinY; y <= iMaxY; y++) {
        for (let x = iMinX; x <= iMaxX; x++) {
            if (x == iX && y == iY) {
                switch (iCurrentDirection) {
                    case UP: sOutput += '^'; break;
                    case DOWN: sOutput += 'v'; break;
                    case LEFT: sOutput += '<'; break;
                    case RIGHT: sOutput += '>'; break;
                }
            }
            else if (!aMap.has(y)) { sOutput += EMPTY_PANEL; }
            else if (!aMap.get(y).has(x)) { sOutput += EMPTY_PANEL; }
            else { sOutput += String(aMap.get(y).get(x)) };
        }
        sOutput += "\n";
    }
    console.log(sOutput);
}