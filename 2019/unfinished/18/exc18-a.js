// Read input
const fs = require('fs');
const { isMainThread } = require('worker_threads');
const sInput = fs.readFileSync('./exc18.in', 'utf8')
const aInputLines = sInput.split("\n");

// Util
let OpcodeProcessor = require('./opcode_processor.js');
let processor = new OpcodeProcessor([], []);

// Transform input into graph data
const astar = require('./astar.js');
var aGraphBaseData = [];

// Parse input into a map and valuable information
let oMap = new Map();
let oKeys = new Map();
let oDoors = new Map();
let iEntranceX = 0;
let iEntranceY = 0;

for (let iY = 0; iY < aInputLines.length; iY++) {
    oMap.set(iY, new Map());
    aGraphBaseData[iY] = [];
    for (let iX = 0; iX < aInputLines[iY].length; iX++) {
        let iChar = aInputLines[iY].charAt(iX);

        // Update the map
        oMap.get(iY).set(iX, iChar);

        // Assume the tile is not walkable
        let iGraphId = 0;

        // Is it a floor-tile?
        if (iChar == ".") {
            iGraphId = 1; // Floor is of course walkable
        }

        // Is it a key?
        if (char_is_key(iChar)) {
            oKeys.set(iChar, { x: iX, y: iY });
            iGraphId = 1; // Keys can be considered walkable
        }

        // Is it a door?
        if (char_is_door(iChar)) {
            oDoors.set(iChar, { x: iX, y: iY });
        }

        // Is it the player position?
        if (iChar == "@") {
            iEntranceX = iX;
            iEntranceY = iY;
            iGraphId = 1; // Entrance can be considered walkable
        }

        // Register the GraphId
        aGraphBaseData[iY][iX] = iGraphId;
    }
}

// Sort map (as for some reason, I like my keys alphabetised)
function sort_keys() {
    let aKeys = [];
    let oSortedKeys = new Map();
    oKeys.forEach((oValue,sKey) => { aKeys.push(sKey); });
    aKeys.sort();
    aKeys.forEach(sKey => { oSortedKeys.set(sKey, oKeys.get(sKey));});
    oKeys = oSortedKeys;
}

sort_keys();

// return;

// console.log(oMap);
// console.log(oDoors);
// console.log(oKeys);
// console.log(aGraphBaseData);
// console.log("Entrance at Y,X: " + iEntranceY + "," + iEntranceX);
// console.log("\n");


function char_is_key(iChar) {
    return iChar.charCodeAt(0) >= 97 && iChar.charCodeAt(0) <= 122;
}

function char_is_door(iChar) {
    return iChar.charCodeAt(0) >= 65 && iChar.charCodeAt(0) <= 90;
}

// Keep track of least steps taken
let iMinStepsTaken = 0;
let oShortestRouteKeys = new Set();
let iRoutesDetermined = 0;

function determine_next_keys(oKeysUsed, iStepsTaken, iCurrentPosX, iCurrentPosY, iDepth = 0) {

    // If all keys are used, just print the steps
    if (oKeysUsed.size == oKeys.size) {
        if (iMinStepsTaken == 0 || iStepsTaken < iMinStepsTaken) {
            oShortestRouteKeys = oKeysUsed;
            iMinStepsTaken = iStepsTaken;
        }
        iRoutesDetermined++;
        const iModulo = 100000;
        if (iRoutesDetermined % iModulo == 0) {
            iMillisTaken = Math.round(processor.measure_stop());
            console.log("Routes determined: " + iRoutesDetermined + " (last " + iModulo + " routes took: " +iMillisTaken+ "ms");
            processor.measure_start();
            console.log("Done, steps taken: " + iStepsTaken + ", keys used: " + Array.from(oKeysUsed).join(", "));
        }
        print("--Done, steps taken: " + iStepsTaken + ", keys used: " + Array.from(oKeysUsed).join(", "), iDepth);
        return;
    }

    // Build a deep copy of the graph
    let aNewGraphData = [];
    aGraphBaseData.forEach(aGraphLine => {
        aNewGraphData.push(Array.from(aGraphLine));
    });

    // Using the KeysUsed, open doors (updating the local GraphData)
    print("Currently at Y,X: " + iCurrentPosY + "," + iCurrentPosX, iDepth);
    print("Keys being considered: " + Array.from(oKeysUsed).join(", "), iDepth);
    oKeysUsed.forEach((oKeys, sKeyName) => {
        print("Opening door: " + sKeyName.toUpperCase(), iDepth);
        let oDoor = oDoors.get(sKeyName.toUpperCase());
        if (oDoor) {// Apparently we have keys for non-existing doors
            aNewGraphData[oDoor.y][oDoor.x] = 1; // Opened door can be considered as walkable
        }
    });
    // console.log(aNewGraphData);

    // // Don't go too deep yet
    // if (iDepth > 2) { return 0; }

    // Based on the grap we have so far, determine which keys are available from our current position
    let oGraph = new astar.Graph(aNewGraphData);
    let oStartPoint = oGraph.grid[iCurrentPosY][iCurrentPosX];
    oKeys.forEach((oAttemptKey, sAttemptKeyName) => {
        if (!oKeysUsed.has(sAttemptKeyName)) { // Only if we've not already discovered this key
            let oKeyPoint = oGraph.grid[oAttemptKey.y][oAttemptKey.x];
            let aPath = astar.astar.search(oGraph, oStartPoint, oKeyPoint);
            if (aPath.length > 0) {
                print("Attempting with new key: " + sAttemptKeyName, iDepth);

                // Establish a new list of keys that we can consider as found
                let oNewKeysUsed = new Set(oKeysUsed);
                oNewKeysUsed.add(sAttemptKeyName);

                // Step into the next realm
                determine_next_keys(oNewKeysUsed, iStepsTaken + aPath.length, oKeyPoint.y, oKeyPoint.x, iDepth + 1);
            }
        }
    });
}

function print(sMessage, iDepth) {
    return;
    let sMessageCopy = sMessage;
    for (let i = 0; i < iDepth; i++) {
        sMessageCopy = "  " + sMessageCopy;
    }
    console.log(sMessageCopy);
}

processor.measure_start();
determine_next_keys(new Set(), 0, iEntranceX, iEntranceY, 0);
console.log("Minimum steps taken: " + iMinStepsTaken);
console.log("Path used: " + Array.from(oShortestRouteKeys).join(", "));
return;

// let OpcodeProcessor = require('./opcode_processor.js');
// let processor = new OpcodeProcessor([], []);

// Parse the graph
// const astar = require('./astar.js');
// const PATH_TILE = '|';
// let oKeysReachable = new Map();

// oKeys.forEach((oKey, sKeyIndex) => {
//     let graph = new astar.Graph(aGraphData);
//     let start = graph.grid[iEntranceY][iEntranceX]; // Middle point, 
//     let end = graph.grid[oKey.y][oKey.x]; // Y,X
//     let path = astar.astar.search(graph, start, end);

//     if (path.length == 0) {
//         // console.log("No route from Entrance to Key '" + sKeyIndex + "' (" + oKey.x + "," + oKey.y + ")");
//     } else {
//         oKeysReachable.set(sKeyIndex, oKey);
//         // processor.clean_console();
//         // path.forEach(point => {
//         //     // For some reason, X and Y are reversed here?
//         //     // No clue why that happens as I feel like I've inputted everything properly, but it works so what the hell ^-^
//         //     oMap.get(point.x).set(point.y, PATH_TILE);
//         // });
//         // draw_map();

//         // // Return map to original state
//         // path.forEach(point => {
//         //     // For some reason, X and Y are reversed here?
//         //     // No clue why that happens as I feel like I've inputted everything properly, but it works so what the hell ^-^
//         //     oMap.get(point.x).set(point.y, aInputLines[point.y][point.x]);
//         // });
//         // processor.delay(1000);
//     }
// });
// console.log("Reachable keys:");
// console.log(oKeysReachable);

function draw_graph(aGraphData) {
    let sOutput = "";
    for (let iY = 0; iY < aGraphData.length; iY++) {
        for (let iX = 0; iX < aGraphData[iY].length; iX++) {
            let iTileId = aGraphData[iY][iX];
            switch (iTileId) {
                // // case '0': sOutput += '\x1b[105m\x1b[95m' + '0' + '\x1b[0m'; break;
                case 0: sOutput += '\x1b[105m\x1b[95m' + '#' + '\x1b[0m'; break;
                case 1: sOutput += '1'; break;
                default:
                    sOutput += iTileId;
                    break;
            }
        }
        sOutput += "\n";
    }
    console.log(sOutput);
}

function draw_map() {
    let sOutput = "";
    for (let iY = 0; iY < oMap.size; iY++) {
        for (let iX = 0; iX < oMap.get(iY).size; iX++) {
            let iTileId = oMap.get(iY).get(iX);
            switch (iTileId) {
                case '#': sOutput += '\x1b[105m\x1b[95m' + '#' + '\x1b[0m'; break;
                case PATH_TILE: sOutput += '\x1b[92m\x1b[102m' + PATH_TILE + '\x1b[0m'; break;
                default:
                    sOutput += iTileId;
                    break;
            }
        }
        sOutput += "\n";
    }
    console.log(sOutput);
}