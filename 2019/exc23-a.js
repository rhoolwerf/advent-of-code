// Initial instructions
const fs = require('fs');
const sInput = fs.readFileSync('./exc23.in', 'utf8');
const aInstructions = sInput.split(",").map(x => Number(x));
let OpcodeProcessor = require('./opcode_processor.js');

// Initialise all 50 computers and their inputs
let aComputers = [];
let oMessageQueue = new Map();
for (let i = 0; i < 50; i++) {
    aComputers.push(new OpcodeProcessor(aInstructions, [], false));
    oMessageQueue.set(i, [i, -1]); // Initially, input for each computer is its address and -1 (as in, no_input)
}

// For each computer, process 3 times to get all three outputs
let bKeepRunning = true;
for (let i = 0; bKeepRunning; i++) {
    aComputers.forEach((oComputer, iAddress) => {
        let aComputerInput = [];
        if (oMessageQueue.get(iAddress).length > 0) {
            aComputerInput = [oMessageQueue.get(iAddress).shift(), oMessageQueue.get(iAddress).shift()];
        } else {
            aComputerInput = [-1];// No input avaialble
        }

        oComputer.set_inputs(aComputerInput);

        // Run once to see if there is output available
        oComputer.run();
        if (oComputer.get_last_output() == undefined) {
            // Computer has no output, stop running
        } else {
            // Computer has output, grab all three
            oComputer.run();
            oComputer.run();
            let iTargetAddress = oComputer.aOutputs[0];
            let iX = oComputer.aOutputs[1];
            let iY = oComputer.aOutputs[2];
            oComputer.aOutputs = [];

            if (iTargetAddress == 255) {
                bKeepRunning = false;
                console.log("iTargetAddress: " + iTargetAddress);
                console.log("iX: " + iX);
                console.log("iY: " + iY);
            } else {
                oMessageQueue.get(iTargetAddress).push(iX);
                oMessageQueue.get(iTargetAddress).push(iY);
            }
        }
    });
}