import { readFileSync } from 'fs';
import { EOL } from 'os';

let [programs, instructions] = readFileSync(process.argv[2], 'utf8').split(EOL);
programs = programs.split(',').join('');
const initialLength = programs.length;

function swap(i1: number, i2: number) {
    programs = `${programs.substring(0, i1)}${programs[i2]}${programs.substring(i1 + 1, i2)}${
        programs[i1]
    }${programs.substring(i2 + 1)}`;

    // programs = [
    //     programs.substring(0, i1),
    //     programs[i2],
    //     programs.substring(i1 + 1, i2),
    //     programs[i1],
    //     programs.substring(i2 + 1),
    // ].join('');
}

console.time('strings');
for (const instr of instructions.split(',')) {
    if (instr.startsWith('s')) {
        const times = Number(instr.substring(1));
        const index = programs.length - times;
        programs = programs.substring(index) + programs.substring(0, index);
    } else if (instr.startsWith('x')) {
        // Grab indexes, ensure first is lower than last
        const [i1, i2] = instr
            .substring(1)
            .split('/')
            .map(Number)
            .sort((a, b) => a - b);

        swap(i1, i2);
    } else if (instr.startsWith('p')) {
        const [f1, f2] = instr.substring(1).split('/');
        const i1 = programs.indexOf(f1);
        const i2 = programs.indexOf(f2);
        swap(Math.min(i1, i2), Math.max(i1, i2));
    }

    if (programs.length > initialLength) {
        console.log('Logic broke at', instr);
        break;
    }
}
console.timeEnd('strings');
console.log('after:', programs);
