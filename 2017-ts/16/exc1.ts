import { readFileSync } from 'fs';
import { EOL } from 'os';

const [programs, instructions] = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) => line.split(','));

console.time('arrays');
for (const instr of instructions) {
    if (instr.startsWith('s')) {
        const times = Number(instr.substring(1));
        programs.unshift(...programs.splice(programs.length - times));
    } else if (instr.startsWith('x')) {
        const [i1, i2] = instr.substring(1).split('/').map(Number);
        const t = programs[i1];
        programs[i1] = programs[i2];
        programs[i2] = t;
    } else if (instr.startsWith('p')) {
        const [f1, f2] = instr.substring(1).split('/');
        const i1 = programs.findIndex((p) => p === f1);
        const i2 = programs.findIndex((p) => p === f2);
        const t = programs[i1];
        programs[i1] = programs[i2];
        programs[i2] = t;
    }
}
console.timeEnd('arrays');

console.log('after:', programs.join(''));
