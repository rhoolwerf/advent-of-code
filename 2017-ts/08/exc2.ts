import { readFileSync } from 'fs';
import { EOL } from 'os';

type Registers = { [key: string]: number };

const registers: Registers = {};

const instructions = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) => line.match(/(\w+)\s(inc|dec)\s(-?\d+)\sif\s(\w+)\s(.+)\s(-?\d+)/) as RegExpMatchArray)
    .map(([_, reg, op, amount, checkReg, checkOp, checkVal]) => ({
        reg,
        op,
        amount: Number(amount),
        checkReg,
        checkOp,
        checkVal: Number(checkVal),
    }));

let highestValue = 0;
for (const { reg, op, amount, checkReg, checkOp, checkVal } of instructions) {
    const checkRegVal = checkReg in registers ? registers[checkReg] : 0;
    const checkEval = `${checkRegVal} ${checkOp} ${checkVal}`;
    const result = eval(checkEval);

    if (result) {
        if (!(reg in registers)) {
            registers[reg] = 0;
        }

        if (op === 'inc') registers[reg] += amount;
        else registers[reg] -= amount;
    }

    highestValue = Math.max(...Object.values(registers), highestValue);
}

console.log(highestValue);
