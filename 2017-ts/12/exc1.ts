import { readFileSync } from 'fs';
import { EOL } from 'os';
import createGraph from 'ngraph.graph';
import { aStar } from 'ngraph.path';

const input = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) => ({
        id: Number(line.split('<->')[0]),
        linked: line.split('<->')[1].split(',').map(Number) as Array<number>,
    }));

const programs = new Set<number>();
const g = createGraph();
for (const { id, linked } of input) {
    programs.add(id);
    for (const link of linked) {
        // if (link === id) continue;

        g.addLink(id, link);
        programs.add(link);
    }
}

const finder = aStar(g);
console.log(Array.from(programs).filter((id) => finder.find(0, id).length > 0).length);
