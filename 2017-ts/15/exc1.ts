import { readFileSync } from 'fs';
import { EOL } from 'os';

let [genA, genB] = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) => line.split(' ').slice(-1))
    .flatMap(Number);

// console.log('Starting with', { genA, genB });
let equals = 0;
for (let i = 0; i < 40000000; i++) {
    genA = (genA * 16807) % 2147483647;
    genB = (genB * 48271) % 2147483647;
    // console.log('Calculated to', { genA, genB });

    let binA = genA.toString(2);
    let binB = genB.toString(2);

    binA = binA.substring(binA.length - 16).padStart(16, '0');
    binB = binB.substring(binB.length - 16).padStart(16, '0');

    // console.log(binA);
    // console.log(binB);
    // console.log('');

    if (binA === binB) equals++;
}

console.log({ equals });
