import { readFileSync } from 'fs';
import { EOL } from 'os';
import { performance } from 'perf_hooks';

let [genA, genB] = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) => line.split(' ').slice(-1))
    .flatMap(Number);

function getBinary16(inp: number): string {
    const bin = inp.toString(2);
    return bin.substring(bin.length - 16).padStart(16, '0');
}

let equals = 0;

for (let pairCount = 0; pairCount < 5000000; pairCount++) {
    // Keep running until we have a value to check
    do genA = (genA * 16807) % 2147483647;
    while (genA % 4 !== 0);

    do genB = (genB * 48271) % 2147483647;
    while (genB % 8 !== 0);

    // Alright, we have a pair, validate the binary values
    const binA = getBinary16(genA);
    const binB = getBinary16(genB);
    if (binA === binB) equals++;
}

console.log({ equals });
