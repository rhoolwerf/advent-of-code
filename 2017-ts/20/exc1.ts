import { readFileSync } from 'fs';
import { EOL } from 'os';

const TO_STRING_OFFSET = 2;

class Particle {
    constructor(
        public id: number,
        public posX: number,
        public posY: number,
        public posZ: number,
        public velX: number,
        public velY: number,
        public velZ: number,
        public accX: number,
        public accY: number,
        public accZ: number
    ) {}

    process(): void {
        this.velX += this.accX;
        this.velY += this.accY;
        this.velZ += this.accZ;
        this.posX += this.velX;
        this.posY += this.velY;
        this.posZ += this.velZ;
    }

    get distance(): number {
        return Math.abs(this.posX) + Math.abs(this.posY) + Math.abs(this.posZ);
    }

    get toString(): string {
        const padded = (i: number): string => String(i).padStart(TO_STRING_OFFSET, ' ');

        return [
            String(this.id).padStart(4, ' '),
            `p<${padded(this.posX)},${padded(this.posY)},${padded(this.posZ)}>`,
            `v<${padded(this.velX)},${padded(this.velY)},${padded(this.velZ)}>`,
            `a<${padded(this.accX)},${padded(this.accY)},${padded(this.accZ)}>`,
            this.distance,
        ].join(', ');
    }
}

// Parse input into particles
const particles = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) =>
        line
            .split(', ')
            .map((chunk) => chunk.match(/<(-?\d+),(-?\d+),(-?\d+)>/) as RegExpMatchArray)
            .map(([_, p, v, a]) => [p, v, a].map(Number))
    )
    .map(
        ([[pX, pY, pZ], [vX, vY, vZ], [aX, aY, aZ]], index) => new Particle(index, pX, pY, pZ, vX, vY, vZ, aX, aY, aZ)
    );

const closest: Array<Particle> = [];
const furthest = new Particle(
    Infinity,
    Infinity,
    Infinity,
    Infinity,
    Infinity,
    Infinity,
    Infinity,
    Infinity,
    Infinity,
    Infinity
);
for (let i = 0; i < 4000; i++) {
    particles.forEach((p) => p.process());

    // Register the closest particle
    closest.push(particles.reduce((closest, p) => (p.distance < closest.distance ? p : closest), furthest));
}

const closestCount = closest.reduce(
    (count, p) => {
        if (!(p.id in count)) count[p.id] = 0;
        count[p.id]++;
        return count;
    },
    {} as { [key: number]: number }
);

const closestRanking = Object.keys(closestCount)
    .map((key) => ({ p: key, v: closestCount[Number(key)] }))
    .sort((a, b) => b.v - a.v);

console.log(closestRanking[0]);
