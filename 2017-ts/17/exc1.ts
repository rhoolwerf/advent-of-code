import { readFileSync } from 'fs';

const steps = Number(readFileSync(process.argv[2], 'utf8'));
const buffer: Array<number> = [0];
let index = 0;

for (let i = 1; i <= 2017; i++) {
    index = ((index + steps) % buffer.length) + 1;
    const rest = buffer.splice(index);
    buffer.push(i, ...rest);
}

console.log(buffer[index + 1]);

// console.log(buffer.map((val, i) => (i === index ? `(${val})` : ` ${val} `)).join(''));
