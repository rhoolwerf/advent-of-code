import { readFileSync } from 'fs';
import { EOL } from 'os';

const instructions = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) => line.split(' '))
    .map(([op, reg, val]) => [op, reg, isNaN(Number(val)) ? val : Number(val)]);

const registers: { [key: string]: number } = {};
let sound = -1;
for (let i = 0; i < instructions.length; i++) {
    const [op, reg, valueOrRegister] = instructions[i];
    // console.log({ op, reg, val: valueOrRegister });

    const value = typeof valueOrRegister === 'string' ? registers[valueOrRegister] : valueOrRegister;
    switch (op) {
        case 'set':
            registers[reg] = value;
            break;
        case 'add':
            registers[reg] += value;
            break;
        case 'mul':
            registers[reg] *= value;
            break;
        case 'mod':
            registers[reg] %= value;
            break;
        case 'snd':
            sound = registers[reg];
            break;
        case 'rcv':
            if (registers[reg] > 0) {
                console.log('-- Received sound:', { sound });
                process.exit();
            }
            break;
        case 'jgz':
            if (registers[reg] > 0) i += value - 1;
            break;
        default:
            console.error('Unknown instruction', { op, reg, valueOrRegister });
            process.exit(-1);
    }

    // console.log('after:', { registers, sound, i });
}
