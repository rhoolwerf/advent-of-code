import { readFileSync } from 'fs';
import { EOL } from 'os';

const grid: Array<Array<string>> = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) => line.split(''));

let y = 0;
let x = grid[0].findIndex((c) => c === '|');
const path: Array<{ y: number; x: number; c: string }> = [];

let offsetY = 1;
let offsetX = 0;

let hasReachedDeadend = false;

for (let i = 0; !hasReachedDeadend; i++) {
    // Keep on walking until we find a turn in the road
    // Or the end of the road
    while (grid[y][x] !== '+' && grid[y][x] !== ' ') {
        path.push({ y, x, c: grid[y][x] });
        y += offsetY;
        x += offsetX;
    }

    // Also register the turning point
    path.push({ y, x, c: grid[y][x] });
    // console.log("We're arrived at a turn in the road, path so far:", path);

    // Determine where to get next
    let newDirectionFound = false;
    const attempts = [
        [-1, 0],
        [1, 0],
        [0, -1],
        [0, 1],
    ];
    for (let i = 0; i < attempts.length && !newDirectionFound; i++) {
        const [testY, testX] = attempts[i];

        // console.log('Current direction', { offsetY, offsetX });
        // console.log('Attempting', { testY, testX });

        if (testY === offsetY && testX === offsetX) {
            // console.log('- Same direction as before, no-can-do!');
        } else if (testY * -1 === offsetY && testX * -1 === offsetX) {
            // console.log('- We just came from there, no-can-do!');
        } else {
            // Is the direction we're considering a valid path?
            if (grid[y + testY] && grid[y + testY][x + testX] !== ' ') {
                // console.log('Found', y + testY, x + testX, grid[y + testY][x + testX]);
                offsetY = testY;
                offsetX = testX;
                // console.log('New direction:', { offsetY, offsetX });

                y += offsetY;
                x += offsetX;
                newDirectionFound = true;
            }
        }
    }
    if (!newDirectionFound) {
        // console.log("We're at a dead-end ?!");
        hasReachedDeadend = true;
    }
}

console.log(path.length - 1);
