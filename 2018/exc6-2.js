let input = [
    [1, 1],
    [1, 6],
    [8, 3],
    [3, 4],
    [5, 5],
    [8, 9]
];

// Actual input
input = [[311, 74], [240, 84], [54, 241], [99, 336], [53, 244], [269, 353], [175, 75], [119, 271], [267, 301], [251, 248], [216, 259], [327, 50], [120, 248], [56, 162], [42, 278], [309, 269], [176, 74], [305, 86], [93, 359], [311, 189], [85, 111], [255, 106], [286, 108], [233, 228], [105, 211], [66, 256], [213, 291], [67, 53], [308, 190], [320, 131], [254, 179], [338, 44], [88, 70], [296, 113], [278, 75], [92, 316], [274, 92], [147, 121], [71, 181], [113, 268], [317, 53], [188, 180], [42, 267], [251, 98], [278, 85], [268, 266], [334, 337], [74, 69], [102, 227], [194, 239]];

let matrix = [];
let largestX = 0;
let largestY = 0;
let smallestX = undefined;
let smallestY = undefined;

function coordinate_counter_to_name(counter) {
    return String.fromCharCode(Number(counter) % 26 + 65) + (Math.floor(Number(counter / 26)));
}

// Map coordinates into matrix
let coordinate_names = new Set();
for (coordinate_counter in input) {
    let coordinate = input[coordinate_counter];
    if (matrix[coordinate[1]] == undefined) { matrix[coordinate[1]] = []; }

    let coordinate_name = coordinate_counter_to_name(coordinate_counter);
    matrix[coordinate[1]][coordinate[0]] = coordinate_name;
    coordinate_names.add(coordinate_name);

    if (coordinate[1] > largestX) { largestX = coordinate[1]; }
    if (coordinate[0] > largestY) { largestY = coordinate[0]; }

    if (smallestX == undefined || smallestX > coordinate[1]) { smallestX = coordinate[1]; }
    if (smallestY == undefined || smallestY > coordinate[0]) { smallestY = coordinate[0]; }
}

// let outer_coordinates = new Set();
// for (coordinate_counter in input) {
//     let coordinate = input[coordinate_counter];
//     if (coordinate[1] == largestX || coordinate[0] == largestY || coordinate_counter[1] == smallestX || coordinate[0] == smallestY) {
//         outer_coordinates.add(coordinate_counter_to_name(coordinate_counter));
//     }
// }

// console.log(outer_coordinates);
console.log('Matrix is ' + largestX + ' by ' + largestY);

// Initialise further matrix
for (let x = 1; x <= largestX; x++) {
    if (matrix[x] == undefined) { matrix[x] = []; }
    for (let y = 1; y <= largestY; y++) {
        if (matrix[x][y] == undefined) { matrix[x][y] = '-'; }
    }
}

// Enhance matrix
for (let x = 1; x <= largestX; x++) {
    if (matrix[x] == undefined) { matrix[x] = []; }
    for (let y = 1; y <= largestY; y++) {

        // If already registered as a point (equal distance to multiple coordinates), skip it
        if (matrix[x][y] == '.') { continue; }

        // If it's an actual coordinate, skip as well
        if (coordinate_names.has(matrix[x][y])) { continue; }

        // Determine shortest distance
        let shortest_distance = undefined;
        for (coordinate_counter in input) {
            let coordinate = input[coordinate_counter];
            let distance = Math.abs(coordinate[1] - x) + Math.abs(coordinate[0] - y);

            if (shortest_distance == undefined) {
                shortest_distance = distance;
                matrix[x][y] = coordinate_counter_to_name(coordinate_counter).toLowerCase();
            } else if (shortest_distance == distance) {
                matrix[x][y] = '.';
                continue;
            } else if (distance < shortest_distance) {
                shortest_distance = distance;
                matrix[x][y] = coordinate_counter_to_name(coordinate_counter).toLowerCase();
            }
        }
    }
}

// Re-determine 'outer coordinates' (anything that touches the edge)
let outer_coordinates = new Set();
for (let x = 1; x <= largestX; x++) {
    outer_coordinates.add(matrix[x][smallestY].toUpperCase());
    outer_coordinates.add(matrix[x][largestY].toUpperCase());
}

for (let y = 1; y <= largestY; y++) {
    outer_coordinates.add(matrix[smallestX][y].toUpperCase());
    outer_coordinates.add(matrix[largestX][y].toUpperCase());
}

// console.log(outer_coordinates);

// Calculate the largest area that doesn't hit an 'outer coordinate'
let largest_area = undefined;
let count = 0; // Include the actual coordinate itself as well
const max_distance = 10000;
for (let x = 1; x <= largestX; x++) {
    for (let y = 1; y <= largestY; y++) {
        // Calculate distance to all coordinates
        let distance = 0;
        for (coordinate of input) {
            distance += Math.abs(coordinate[1] - x) + Math.abs(coordinate[0] - y);
        }
        if (distance < max_distance) { count++; }
    }
}
console.log(count);


// // Display matrix
// let output = '';
// for (let x = 1; x <= largestX; x++) {
//     for (let y = 1; y <= largestY; y++) {
//         if (matrix[x][y] == '.') { output += '..'; }
//         else { output += matrix[x][y]; }
//     }
//     output += '\n';
// }
// console.log(output);
