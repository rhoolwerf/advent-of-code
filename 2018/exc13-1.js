// Some constants
const RIGHT = ">";
const UP = "^";
const DOWN = "v";
const LEFT = "<";

// Parse the input
const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aLines = sInput.split("\n");

const iWidth = aLines[0].length;
const iHeight = aLines.length;
let oMap = new Map();
let oCleanMap = new Map();
let aCarts = [];

aLines.forEach((sLine, iY) => {
    oCleanMap.set(iY, new Map());
    oMap.set(iY, new Map());
    sLine.split("").forEach((sCell, iX) => {
        oCleanMap.get(iY).set(iX, sCell);
        oMap.get(iY).set(iX, sCell);

        // See if we have a cart?
        let bAdd = true;
        let nX = 0;
        let nY = 0;
        switch (sCell) {
            case RIGHT:
                oCleanMap.get(iY).set(iX, "-");
                nX++;
                break;
            case UP:
                oCleanMap.get(iY).set(iX, "|");
                nY--;
                break;
            case LEFT:
                oCleanMap.get(iY).set(iX, "-");
                nX--;
                break;
            case DOWN:
                oCleanMap.get(iY).set(iX, "|");
                nY++;
                break;
            default:
                bAdd = false;
        }
        if (bAdd) {
            let oCart = {
                y: iY,
                x: iX,
                d: sCell,
                nY: nY,
                nX: nX,
                p: ['L', 'S', 'R']
            };
            determine_offsets_from_direction(oCart);
            aCarts.push(oCart);
        }
    });
});

// Start ticking
// draw_map();
let bKeepRunning = true;
for (let iTick = 0; bKeepRunning; iTick++) {
    aCarts.forEach((oCart, iCartNr) => {
        // Clean up the map
        oMap.get(oCart.y).set(oCart.x, oCleanMap.get(oCart.y).get(oCart.x));

        // Determine next position
        // console.log(`${iCartNr}:`, oCart);
        oCart.x += oCart.nX;
        oCart.y += oCart.nY;

        // What's at this new position?
        switch (oMap.get(oCart.y).get(oCart.x)) {
            case "/":
            case "\\":
                turn_cart(oCart);
                break;
            case "+":
                crossroad_cart(oCart);
                break;
            case "-":
            case "|":
                // Just normal road, nothing wrong here
                break;
            default:
                // No corner, no crossroad, no normal road? Crash!!
                bKeepRunning = false;
                console.log(`Crash at ${oCart.x},${oCart.y}`);
                break;
        }

        // console.log(`${iCartNr}:`, oCart);
        if (bKeepRunning) {
            oMap.get(oCart.y).set(oCart.x, oCart.d);
        } else {
            oMap.get(oCart.y).set(oCart.x, "X");
        }
        // console.log();
    });
    // draw_map();
}

function crossroad_cart(oCart) {
    // Determine turn and cycle the phase
    const sTurn = oCart.p[0];
    oCart.p.push(oCart.p.shift());

    // When going straight ahead, do nothing
    if (sTurn === "S") { return; }

    // What to do is based on current direction
    switch (oCart.d) {
        case RIGHT:
            if (sTurn === "L") { oCart.d = UP; }
            else if (sTurn === "R") { oCart.d = DOWN; }
            break;

        case UP:
            if (sTurn === "L") { oCart.d = LEFT; }
            else if (sTurn === "R") { oCart.d = RIGHT; }
            break;

        case LEFT:
            if (sTurn === "L") { oCart.d = DOWN; }
            else if (sTurn === "R") { oCart.d = UP; }
            break;
        case DOWN:
            if (sTurn === "L") { oCart.d = RIGHT; }
            else if (sTurn === "R") { oCart.d = LEFT; }
            break;
        default:
            break;
    }

    // Determine new X/Y adjustments
    determine_offsets_from_direction(oCart);
}

function determine_offsets_from_direction(oCart) {
    oCart.nX = 0;
    oCart.nY = 0;

    switch (oCart.d) {
        case RIGHT:
            oCart.nX++;
            break;
        case UP:
            oCart.nY--;
            break;
        case LEFT:
            oCart.nX--;
            break;
        case DOWN:
            oCart.nY++;
            break;
    }
}

function turn_cart(oCart) {
    // What's in the 'current' position of the car (after it's been changed)
    let sCell = oMap.get(oCart.y).get(oCart.x);

    // Current state of the cart?
    switch (oCart.d) {
        case RIGHT:
            if (sCell == "/") { oCart.d = UP; }
            else if (sCell == "\\") { oCart.d = DOWN; }
            break;
        case UP:
            if (sCell == "/") { oCart.d = RIGHT; }
            else if (sCell == "\\") { oCart.d = LEFT; }
            break;
        case LEFT:
            if (sCell == "/") { oCart.d = DOWN; }
            else if (sCell == "\\") { oCart.d = UP; }
            break;
        case DOWN:
            if (sCell == "/") { oCart.d = LEFT; }
            else if (sCell == "\\") { oCart.d = RIGHT; }
            break;
    }
    // Determine new X/Y adjustments
    determine_offsets_from_direction(oCart);
}

function draw_map() {
    console.log("");
    for (let iY = 0; iY < iHeight; iY++) {
        let sOutput = "";
        for (let iX = 0; iX < iWidth; iX++) {
            sOutput += oMap.get(iY).get(iX);
        }
        console.log(sOutput);
    }
}