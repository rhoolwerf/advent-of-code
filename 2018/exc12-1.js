const sInputFile = process.argv[2];
const { assert } = require("console");
const fs = require("fs");
const { compileFunction } = require("vm");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n");

const iPadding = 5;
let oState = new Map();
let iStateMin = 0;
let iStateMax = 0;

let oPatterns = new Map();

aInput.forEach((sLine, iIndex) => {
    if (iIndex === 0) {
        const sState = sLine.split(" ")[2];
        iStateMax = sState.length - 1;
        sState.split("").forEach((sChar, iIndex) => {
            oState.set(iIndex, sChar);
        });
    } else if (iIndex >= 2) {
        [sPattern, sResult] = sLine.split(" => ");
        oPatterns.set(sPattern, sResult);
    }
});

// write_state(0);
for (let iGeneration = 1; iGeneration <= 20; iGeneration++) {
    let oNewState = new Map();
    let iTempMax = iStateMax + 2;
    for (let iPot = iStateMin - 2; iPot <= iTempMax; iPot++) {
        // Determine the section to match against patterns
        let sSection = get_point(iPot - 2) + get_point(iPot - 1) + get_point(iPot) + get_point(iPot + 1) + get_point(iPot + 2);
        // console.log(padnum(iPot) + ": " + sSection);

        // Initiate as nothing
        oNewState.set(iPot, '.');

        // Match all patterns to determine what the actual state of the pot will be
        oPatterns.forEach((sResult, sPattern) => {
            if (sSection === sPattern) {
                // We have a pattern match, save the result
                oNewState.set(iPot, sResult);

                // Also update boundaries if need be
                if (iPot < iStateMin) { iStateMin = iPot; }
                else if (iPot > iStateMax) { iStateMax = iPot; }
            }
        });
    }
    oState = oNewState;
    // write_state(iGeneration);
}

// Calculate the value
let iValue = 0;
for (let iPot = iStateMin; iPot <= iStateMax; iPot++) {
    if (get_point(iPot) === "#") {
        iValue += iPot;
    }
}
console.log(iValue);

function get_point(iPos) {
    if (!oState.has(iPos)) { return '.'; }
    else { return oState.get(iPos); }
}

function write_state(iGeneration) {
    let sOutput = padnum(iGeneration) + ": ";
    for (let i = iStateMin; i <= iStateMax; i++) {
        sOutput += get_point(i); //oState.get(i);
    }
    sOutput += " (" + iStateMin + "," + iStateMax + ")";
    console.log(sOutput);
}

function padnum(iNum) {
    let sNum = String(iNum);
    while (sNum.length < 2) {
        sNum = " " + sNum;
    }
    return sNum;
}