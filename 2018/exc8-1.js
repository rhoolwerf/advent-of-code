const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split(" ");

let iNodeCount = 0;
let iMetadataSum = 0;

process_node(0);

function process_node(iPosition) {
    let sNode = String.fromCharCode(65 + iNodeCount++);
    let iSubNodes = aInput[iPosition];
    let iMetadata = aInput[iPosition + 1];
    // console.log(`Position ${iPosition}: Node ${sNode} has ${iSubNodes} subnodes and ${iMetadata} metadata-entries`);

    let iNewPosition = iPosition + 2;
    while (iSubNodes-- > 0) {
        iNewPosition = process_node(iNewPosition);
    }
    while (iMetadata-- > 0) {
        let iMetadataEntry = aInput[iNewPosition++];
        // console.log(`Node ${sNode} has metadata-entry: ${iMetadataEntry}`);
        iMetadataSum += Number(iMetadataEntry);
    }
    return iNewPosition;
}
console.log(iMetadataSum);

// 2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2
// A----------------------------------
//     B----------- C-----------
//                      D-----
// A, which has 2 child nodes (B, C) and 3 metadata entries (1, 1, 2).
// B, which has 0 child nodes and 3 metadata entries (10, 11, 12).
// C, which has 1 child node (D) and 1 metadata entry (2).
// D, which has 0 child nodes and 1 metadata entry (99).