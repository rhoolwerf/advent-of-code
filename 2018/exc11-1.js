const iPuzzleSerial = 8868;
const iSize = 300;

// console.log(calculate_cell(8, 3, 5));
// console.log(calculate_cell(57, 122, 79));
// console.log(calculate_cell(39, 217, 196));
// console.log(calculate_cell(71, 101, 153));


// console.log(find_largest_value(generate_map(18)));
// console.log(find_largest_value(generate_map(42)));
console.log(find_largest_value(generate_map(iPuzzleSerial)));

function find_largest_value(oMap) {
    let iGridSize = 3;
    let oMax = {
        value: 0,
        x: 0,
        y: 0
    };

    for (let y = 0; y < iSize - 3; y++) {
        for (let x = 0; x < iSize - 3; x++) {
            let iValue = 0;
            for (let iGridY = y; iGridY < y + iGridSize; iGridY++) {
                for (let iGridX = x; iGridX < x + iGridSize; iGridX++) {
                    iValue += oMap.get(iGridY).get(iGridX);
                }
            }
            if (iValue > oMax.value) {
                oMax.value = iValue;
                oMax.x = x;
                oMax.y = y;
            }
        }
    }
    return oMax;
}

function generate_map(iSerial) {
    let oMap = new Map();
    for (let y = 0; y < iSize; y++) {
        oMap.set(y, new Map());
        for (let x = 0; x < iSize; x++) {
            oMap.get(y).set(x, calculate_cell(iSerial, x, y));
        }
    }
    return oMap;
}

function calculate_cell(iSerial, iX, iY) {
    let iRackID = iX + 10;
    // console.log('iRackID: ' + iRackID);
    iPower = iRackID * iY;
    // console.log('iPower: ' + iPower);
    iPower += iSerial;
    // console.log('iPower: ' + iPower);
    iPower *= iRackID;
    // console.log('iPower: ' + iPower);
    sPower = String(iPower);
    if (sPower.length < 3) {
        iPower = 0;
    } else {
        iPower = Number(sPower.charAt(sPower.length - 3));
    }
    // console.log('iPower: ' + iPower);
    return iPower - 5;
}
