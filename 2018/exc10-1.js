const sInputFile = process.argv[2];
const fs = require("fs");
const { isContext } = require("vm");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n");

let aPoints = [];

aInput.forEach(sLine => {
    let oPoint = {
        x: Number(sLine.split(",")[0].split("<")[1]),
        y: Number(sLine.split(",")[1].split(">")[0]),
        vX: Number(sLine.split(",")[1].split("<")[1]),
        vY: Number(sLine.split(",")[2].split(">")[0])
    }
    aPoints.push(oPoint);
});

let iSmallestArea = 0;
let oSmallestMap = {
    aMap: null,
    iMinX: 0,
    iMaxX: 0,
    iMinY: 0,
    iMaxY: 0
};
for (let i = 0; i < 20000; i++) {
    aPoints.forEach(oPoint => {
        oPoint.x += oPoint.vX;
        oPoint.y += oPoint.vY;
    });
    draw_map();
}

let iMinX = oSmallestMap.iMinX;
let iMaxX = oSmallestMap.iMaxX;
let iMinY = oSmallestMap.iMinY;
let iMaxY = oSmallestMap.iMaxY;
let aMap = oSmallestMap.aMap;
for (let y = iMinY - 1; y <= iMaxY + 1; y++) {
    let sOutput = "";
    if (!aMap.has(y)) { aMap.set(y, new Map()); }

    for (let x = iMinX - 1; x <= iMaxX + 1; x++) {
        if (!aMap.get(y).has(x)) { sOutput += "."; }
        else { sOutput += "#"; }
    }
    console.log(sOutput);
}


// Calculate the map smallest area and only show that one?
function draw_map() {
    let aMap = new Map();
    let iMinX = 0;
    let iMaxX = 0;
    let iMinY = 0;
    let iMaxY = 0;

    aPoints.forEach(oPoint => {
        if (!aMap.has(oPoint.y)) {
            aMap.set(oPoint.y, new Map())
        }
        aMap.get(oPoint.y).set(oPoint.x, '#');

        if (!iMinX || oPoint.x < iMinX) { iMinX = oPoint.x; }
        if (!iMaxX || oPoint.x > iMaxX) { iMaxX = oPoint.x; }
        if (!iMinY || oPoint.y < iMinY) { iMinY = oPoint.y; }
        if (!iMaxY || oPoint.y > iMaxY) { iMaxY = oPoint.y; }
    });

    let iWidth = Number(iMaxX) - Number(iMinX);
    let iHeight = Number(iMaxY) - Number(iMinY);
    let iArea = iWidth * iHeight;
    if (!iSmallestArea || iArea < iSmallestArea) {
        iSmallestArea = iArea;
        oSmallestMap = {
            aMap: aMap,
            iMinX: iMinX,
            iMaxX: iMaxX,
            iMinY: iMinY,
            iMaxY: iMaxY
        };
    }
}
