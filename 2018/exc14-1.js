const { get } = require("https");

let aRecipes = [3, 7];
let iPos1 = 0;
let iPos2 = 1;

console.log(get_recipe_score(9)==="5158916779");
console.log(get_recipe_score(5)==="0124515891");
console.log(get_recipe_score(18)==="9251071085");
console.log(get_recipe_score(2018)==="5941429882");
console.log(get_recipe_score(286051));

function get_recipe_score(iTargetRecipe) {
    output_recipes(0);

    const iTargetRecipes = iTargetRecipe + 10;
    for (let iCount = 1; aRecipes.length < iTargetRecipes; iCount++) {
        String(aRecipes[iPos1] + aRecipes[iPos2]).split("").forEach(sDigit => {
            aRecipes.push(Number(sDigit));
        });
        iPos1 = (iPos1 + aRecipes[iPos1] + 1) % aRecipes.length;
        iPos2 = (iPos2 + aRecipes[iPos2] + 1) % aRecipes.length;
        output_recipes(iCount);
    }

    // Calculate the score
    let sScore = "";
    for (let i = iTargetRecipe; i < iTargetRecipes; i++) {
        sScore += aRecipes[i];
    }
    // console.log(sScore);
    return sScore;
}


function output_recipes(iCount) {
    return;
    let sOutput = String(iCount);
    while (sOutput.length < 5) { sOutput = " " + sOutput; }
    sOutput += ": ";

    aRecipes.forEach((iValue, iIndex) => {
        if (iIndex === iPos1) { sOutput += `(${iValue})`; }
        else if (iIndex === iPos2) { sOutput += `[${iValue}]`; }
        else { sOutput += ` ${iValue} ` }
    });
    console.log(sOutput);
}