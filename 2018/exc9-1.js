const sInputFile = process.argv[2];
const { POINT_CONVERSION_COMPRESSED } = require("constants");
const fs = require("fs");
const { on } = require("process");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split(" ");

const iPlayerCount = Number(aInput[0]);
const iMarbleCount = Number(aInput[6]);

let aPlayerScores = [];
for (let i = 1; i <= iPlayerCount; i++) {
    aPlayerScores.push(0);
}

let oCurrent = { value: 0 };
oCurrent.prev = oCurrent;
oCurrent.next = oCurrent;

let oHead = oCurrent;

function addAfter(iValue, oMarble) {
    const toAdd = {
        value: iValue,
        prev: oMarble,
        next: oMarble.next
    };

    oMarble.next.prev = toAdd;
    oMarble.next = toAdd;

    return toAdd;
}

iCurrentPlayer = 1;
for (let iMarble = 1; iMarble <= iMarbleCount; iMarble++) {
    if (iMarble % 23 === 0) {
        aPlayerScores[iCurrentPlayer] += iMarble;
        oCurrent = oCurrent.prev.prev.prev.prev.prev.prev.prev;
        aPlayerScores[iCurrentPlayer] += oCurrent.value;
        oCurrent.prev.next = oCurrent.next;
        oCurrent.next.prev = oCurrent.prev;
        oCurrent = oCurrent.next;
    } else {
        oCurrent = addAfter(iMarble, oCurrent.next);
    }

    // let sOutput = "[" + space_number(iCurrentPlayer) + "]";
    // let oNext = oHead.next;
    // while (oNext.value > 0) {
    //     sOutput += " " + format_number(oNext.value);
    //     oNext = oNext.next;
    // }
    // console.log(sOutput);
    iCurrentPlayer = iCurrentPlayer % iPlayerCount + 1;
}

let iHighscore = 0;
aPlayerScores.forEach(iScore => {
    if (iScore > iHighscore) {
        iHighscore = iScore;
    }
});
console.log(iHighscore);

function space_number(iNumber) {
    let sNumber = String(iNumber);
    while (sNumber.length < 2) {
        sNumber = " " + sNumber;
    }
    return sNumber;
}

function format_number(iNumber) {
    let sNumber = String(iNumber);
    while (sNumber.length < 3) {
        sNumber = " " + sNumber;
    }
    return sNumber;
}