const sInputFile = process.argv[2];
const { captureRejectionSymbol } = require("events");
const fs = require("fs");
const { setMaxListeners } = require("process");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split(" ").map((x) => Number(x));

let iNodeCount = 0;
let oNodes = new Map();

process_node(0);

function process_node(iPosition) {
    let sNode = String.fromCharCode(65 + iNodeCount++);
    let iSubNodes = aInput[iPosition];
    let iMetadata = aInput[iPosition + 1];

    let iNewPosition = iPosition + 2;
    oNodes.set(sNode, { Children: [], NodeValue: 0 });
    while (iSubNodes-- > 0) {
        [iNewPosition, sChild] = process_node(iNewPosition);
        oNodes.get(sNode).Children.push(sChild);
    }
    while (iMetadata-- > 0) {
        let iMetadataEntry = aInput[iNewPosition++];
        if (!oNodes.get(sNode).Children.length) {
            oNodes.get(sNode).NodeValue += iMetadataEntry;
        } else {
            if (iMetadataEntry <= oNodes.get(sNode).Children.length) {
                sChild = oNodes.get(sNode).Children[iMetadataEntry - 1];
                // console.log(`For node ${sNode} checking child ${sChild} with value ${oNodes.get(sChild).NodeValue}`);
                oNodes.get(sNode).NodeValue += oNodes.get(sChild).NodeValue;
            }
        }
    }

    return [iNewPosition, sNode];
}
console.log(oNodes.get("A"));

// 2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2
// A----------------------------------
//     B----------- C-----------
//                      D-----
// A, which has 2 child nodes (B, C) and 3 metadata entries (1, 1, 2).
// B, which has 0 child nodes and 3 metadata entries (10, 11, 12).
// C, which has 1 child node (D) and 1 metadata entry (2).
// D, which has 0 child nodes and 1 metadata entry (99).