const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n").map(x => [x.split(" ")[0], Number(x.split(" ")[1])]);

let iAcc = 0;
let oPointersVisited = new Set();
for (let iPointer = 1; iPointer < aInput.length + 1; iPointer++) {
    if (oPointersVisited.has(iPointer)) {
        console.log(`Visiting point ${iPointer} for the second time, iAcc: ${iAcc}`);
        break;
    } else {
        oPointersVisited.add(iPointer);
    }

    switch (aInput[iPointer][0]) {
        case 'acc':
            iAcc += aInput[iPointer][1];
            break;
        case 'jmp':
            iPointer += (aInput[iPointer][1] - 1);
            break;
        case 'nop':
            break;
    }
}