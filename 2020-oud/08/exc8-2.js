const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInputBase = sInput.split("\n").map(x => [x.split(" ")[0], Number(x.split(" ")[1])]);

// Loop over each instruction and in case of jmp/nop, remap and try the instruction-set
for (let iPointer = 0; iPointer < aInputBase.length; iPointer++) {
    // Ensure it's a deep-copy else we're also updating aInputBase
    let aInputConverted = JSON.parse(JSON.stringify(aInputBase));

    switch (aInputConverted[iPointer][0]) {
        case 'jmp':
            aInputConverted[iPointer][0] = 'nop';
            break;
        case 'nop':
            aInputConverted[iPointer][0] = 'jmp';
            break;
        case 'acc':
            continue; // we do not change acc
    }

    let [bSuccess, iAcc] = calc_acc(aInputConverted);
    if (bSuccess) {
        console.log('After changing pointer', iPointer, 'the iAcc was valid:', iAcc);
        break;
    }
}

function calc_acc(aInput) {
    let iAcc = 0;
    let oPointersVisited = new Set();
    for (let iPointer = 0; iPointer < aInput.length; iPointer++) {
        if (oPointersVisited.has(iPointer)) {
            // If we've already visited this pointer, it means we have an infinite loop so report an error
            return [false, 0];
        } else {
            oPointersVisited.add(iPointer);
        }

        switch (aInput[iPointer][0]) {
            case 'acc':
                iAcc += aInput[iPointer][1];
                break;
            case 'jmp':
                iPointer += (aInput[iPointer][1] - 1);
                break;
            case 'nop':
                break;
        }
    }
    // If we arrive here, we have a proper result!
    return [true, iAcc];
}