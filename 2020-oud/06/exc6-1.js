const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n\n").map(x => x.split("\n").join(""));

let iSum = 0;
aInput.forEach(sGroup => {
    iSum += new Set(sGroup.split("")).size;
});
console.log(iSum);