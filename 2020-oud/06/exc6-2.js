const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n\n").map(x => x.split("\n"));

let iSum = 0;
aInput.forEach(aGroup => {
    let iPersonCount = 0;
    let oMap = new Map();
    aGroup.forEach(sPerson => {
        iPersonCount++;
        sPerson.split("").forEach(sChar => {
            if (!oMap.has(sChar)) { oMap.set(sChar, 1); }
            else { oMap.set(sChar, oMap.get(sChar) + 1); }
        });
    });
    let bValid = true;
    for (sEntry of oMap) {
        if (sEntry[1] === iPersonCount) {
            iSum++;
        }
    }
    // console.log(aGroup, oMap, iPersonCount, oMap.size, bValid);
    // if (bValid) {
    //     iSum += oMap.size;
    // }
});
console.log(iSum);