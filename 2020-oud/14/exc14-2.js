const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n").map(x => x.split(" "));

let sMask = "";
let aMemory = [];

aInput.forEach(aLine => {
    if (aLine[0] === "mask") {
        sMask = aLine[2];
    } else {
        // Parse input
        let iMemoryAddress = Number(aLine[0].split("[").map(x => x.split("]"))[1][0]);
        let iValue = Number(aLine[2]);

        // Parse memory address into a mask
        let sMemoryMask = iMemoryAddress.toString(2)

        // Parse the Mask over the MemoryMask
        let aNewBinary = [];
        for (let i = 0; i < sMask.length; i++) {
            switch (sMask.charAt(sMask.length - 1 - i)) {
                case "1":
                    aNewBinary.unshift(1);
                    break;
                case "0":
                    if (sMemoryMask.length - 1 - i >= 0) { aNewBinary.unshift(sMemoryMask.charAt(sMemoryMask.length - 1 - i)); }
                    else { aNewBinary.unshift(0); }
                    break;
                case "X":
                    aNewBinary.unshift('X');
                    break;
            }
        }

        // MemoryMask needs to be converted into a list of addresses
        // let aAddresses = parse_memory_mask(aNewBinary.join(""));
    }
});