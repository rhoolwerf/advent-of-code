const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n").map(x => x.split(" "));

let sMask = "";
let aMemory = [];

aInput.forEach(aLine => {
    if (aLine[0] === "mask") {
        sMask = aLine[2];
    } else {
        let iMemoryAddress = Number(aLine[0].split("[").map(x => x.split("]"))[1][0]);
        let sBinary = Number(aLine[2]).toString(2);
        let aNewBinary = [];
        for (let i = 0; i < sMask.length; i++) {
            switch (sMask.charAt(sMask.length - 1 - i)) {
                case "1":
                    aNewBinary.unshift(1);
                    break;
                case "0":
                    aNewBinary.unshift(0);
                    break;
                case "X":
                    if (sBinary.length - 1 - i >= 0) { aNewBinary.unshift(sBinary.charAt(sBinary.length - 1 - i)); }
                    else { aNewBinary.unshift(0); }
                    break;
            }
        }
        aMemory[iMemoryAddress] = parseInt(aNewBinary.join(""), 2);
    }
});
console.log(aMemory.reduce((acc, curr) => { return acc + curr }, 0));

// borrowed from https://stackoverflow.com/questions/9939760/how-do-i-convert-an-integer-to-binary-in-javascript
function dec2bin(iNumber) {
    return (iNumber >>> 0).toString(2);
}