const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n");

let iCorrect = 0;
for (const sLine of aInput) {
    const sParts = sLine.split(" ");
    const sPassword = sParts[2];
    const sChar = sParts[1].split(":")[0];
    const [iFirst, iSecond] = sParts[0].split("-");

    let bFirst = false;
    let bSecond = false;

    if (sPassword.charAt(iFirst - 1) === sChar) { bFirst = true; }
    if (sPassword.charAt(iSecond - 1) === sChar) { bSecond = true; }

    if ((bFirst && !bSecond) || (!bFirst && bSecond)) {
        iCorrect++;
    }

}
console.log(iCorrect);