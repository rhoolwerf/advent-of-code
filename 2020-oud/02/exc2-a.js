const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n");

let iCorrect = 0;
for (const sLine of aInput) {
    const sParts = sLine.split(" ");
    const sPassword = sParts[2];
    const sChar = sParts[1].split(":")[0];
    const [iMin, iMax] = sParts[0].split("-");
    let iCount = 0;
    for (let i = 0; i < sPassword.length; i++) {
        if (sPassword.charAt(i) === sChar) {
            iCount++;
        }
    }
    if (iCount >= iMin && iCount <= iMax) {
        iCorrect++;
    }
}
console.log(iCorrect);