const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n\n");

let iCountValid = 0;
aInput.forEach(sPassportRaw => {
    let sPassportLines = sPassportRaw.split("\n");
    let aPassportData = {};
    sPassportLines.forEach(sPassportLine => {
        for (sPart of sPassportLine.split(" ")) {
            aPassportData[sPart.split(":")[0]] = sPart.split(":")[1];
        };
    });

    if (aPassportData.byr && aPassportData.iyr && aPassportData.eyr && aPassportData.hgt && aPassportData.hcl && aPassportData.ecl && aPassportData.pid) {
        iCountValid++;
    }
});
console.log(iCountValid)