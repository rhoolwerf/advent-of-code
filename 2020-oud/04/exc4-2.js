const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n\n");

let re = new RegExp('#[a-z0-9]{6}');

let iCountValid = 0;
aInput.forEach(sPassportRaw => {
    let sPassportLines = sPassportRaw.split("\n");
    let aPassportData = {};
    sPassportLines.forEach(sPassportLine => {
        for (sPart of sPassportLine.split(" ")) {
            aPassportData[sPart.split(":")[0]] = sPart.split(":")[1];
        };
    });

    if (!(aPassportData.byr && aPassportData.iyr && aPassportData.eyr &&
        aPassportData.hgt && aPassportData.hcl && aPassportData.ecl && aPassportData.pid)) {
        return;
    }

    if (!(aPassportData.byr >= 1920 && aPassportData.byr <= 2002)) { return; }

    if (!(aPassportData.iyr >= 2010 && aPassportData.iyr <= 2020)) { return; }

    if (!(aPassportData.eyr >= 2020 && aPassportData.eyr <= 2030)) { return; }

    if (!aPassportData.hgt) { return; }
    if (aPassportData.hgt.indexOf("cm") > 0) {
        let iHeight = aPassportData.hgt.split("cm")[0];
        if (!(iHeight >= 150 && iHeight <= 193)) { return; }
    } else if (aPassportData.hgt.indexOf("in") > 0) {
        let iHeight = aPassportData.hgt.split("in")[0];
        if (!(iHeight >= 59 && iHeight <= 76)) { return; }
    } else {
        return;
    }

    if (re.exec(aPassportData.hcl) === null) { return; }

    switch (aPassportData.ecl) {
        case 'amb':
        case 'blu':
        case 'brn':
        case 'gry':
        case 'grn':
        case 'hzl':
        case 'oth':
            break;
        default:
            return;
            break;
    }

    if (!aPassportData.pid) { return; }
    if (!(aPassportData.pid.length == 9 && !isNaN(Number(aPassportData.pid)))) { return; }

    // console.log(aPassportData);
    iCountValid++;

});
console.log(iCountValid)