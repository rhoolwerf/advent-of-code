const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");

aState = sInput.split("\n").map(x => x.split(""));
// output_state();

let iPrevOccupiedCount = 0;
for (let iGeneration = 0; true; iGeneration++) {
    // Deep-copy the state into a new state
    let aNewState = JSON.parse(JSON.stringify(aState));
    aState.forEach((aRow, iY) => {
        aRow.forEach((sCell, iX) => {
            // Only process cell if it's either an occupied or empty seats
            if (aState[iY][iX] === "#" || aState[iY][iX] === "L") {
                let oAdjacentCount = calc_adjacent_seats(iY, iX);
                if (aState[iY][iX] === "L" && oAdjacentCount.occupied === 0) { aNewState[iY][iX] = "#"; }
                else if (aState[iY][iX] === "#" && oAdjacentCount.occupied >= 5) { aNewState[iY][iX] = "L"; }
            }
        });
    });
    aState = aNewState;

    let iOccupiedCount = 0;
    aState.forEach(aRow => {
        aRow.forEach(sCell => {
            if (sCell === "#") { iOccupiedCount++; }
        });
    });
    if (iPrevOccupiedCount === iOccupiedCount) {
        console.log("Generation:", iGeneration, "Occupied seat count:", iOccupiedCount);
        break;
    } else {
        iPrevOccupiedCount = iOccupiedCount;
    }

    // output_state();
}

function calc_adjacent_seats(iY, iX) {
    let aAdjacentSeats = [];
    aAdjacentSeats.push(get_seat_in_direction(iY, iX, -1, -1)); // North-West
    aAdjacentSeats.push(get_seat_in_direction(iY, iX, -1, 0)); // North
    aAdjacentSeats.push(get_seat_in_direction(iY, iX, -1, 1)); // North-East
    aAdjacentSeats.push(get_seat_in_direction(iY, iX, 0, 1)); // East
    aAdjacentSeats.push(get_seat_in_direction(iY, iX, 1, 1)); // South-East
    aAdjacentSeats.push(get_seat_in_direction(iY, iX, 1, 0)); // South
    aAdjacentSeats.push(get_seat_in_direction(iY, iX, 1, -1)); // South-West
    aAdjacentSeats.push(get_seat_in_direction(iY, iX, 0, -1)); // West

    return aAdjacentSeats.reduce((oCount, sSeat) => {
        let sField = (sSeat === "#" ? "occupied" : "empty");
        oCount[sField]++;
        return oCount;
    }, { occupied: 0, empty: 0 });
}

function get_seat_in_direction(iStartY, iStartX, iOffsetY, iOffsetX) {
    let iY = iStartY;
    let iX = iStartX;
    while (true) {
        iY += iOffsetY;
        iX += iOffsetX;
        switch (get_point(iY, iX)) {
            case ".":
                // Floor tile? Keep looking
                continue;
                break;
            // Left the room? Return nothing
            case "?":
                return "";
                breaj;
            default:
                // We've either found an empty or occupied seat, return it
                // console.log(iStartY, iStartX, iOffsetY, iOffsetX, iY, iX, get_point(iY, iX));
                return get_point(iY, iX);
        }
    }
}

function get_point(iY, iX) {
    if (aState[iY] === undefined) { return "?"; }
    else if (aState[iY][iX] === undefined) { return "?"; }
    else { return aState[iY][iX]; }
}

function output_state() {
    console.log("");
    aState.forEach(aRow => {
        console.log(aRow.join(""));
    });
}