const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");

aState = sInput.split("\n").map(x => x.split(""));
// output_state();

let iPrevOccupiedCount = 0;
for (let iGeneration = 0; true; iGeneration++) {
    // Deep-copy the state into a new state
    let aNewState = JSON.parse(JSON.stringify(aState));
    aState.forEach((aRow, iY) => {
        aRow.forEach((sCell, iX) => {
            // Only process cell if it's either an occupied or empty seats
            if (aState[iY][iX] === "#" || aState[iY][iX] === "L") {
                let oAdjacentCount = calc_adjacent_seats(iY, iX);
                if (aState[iY][iX] === "L" && oAdjacentCount.occupied === 0) { aNewState[iY][iX] = "#"; }
                else if (aState[iY][iX] === "#" && oAdjacentCount.occupied >= 4) { aNewState[iY][iX] = "L"; }
            }
        });
    });
    aState = aNewState;

    let iOccupiedCount = 0;
    aState.forEach(aRow => {
        aRow.forEach(sCell => {
            if (sCell === "#") { iOccupiedCount++; }
        });
    });
    if (iPrevOccupiedCount === iOccupiedCount) {
        console.log(iOccupiedCount);
        break;
    } else {
        iPrevOccupiedCount = iOccupiedCount;
    }

    // output_state();
}

function calc_adjacent_seats(iY, iX) {
    let oReturn = { occupied: 0, empty: 0 };
    for (let y = Math.max(iY - 1, 0); y <= Math.min(iY + 1, aState.length - 1); y++) {
        for (let x = Math.max(iX - 1, 0); x <= Math.min(iX + 1, aState[0].length - 1); x++) {
            if (!(y === iY && x === iX)) { // Do not consider given cell, that isn't relevant for adjacent-count
                if (aState[y][x] === "L") { oReturn.empty++; }
                else if (aState[y][x] === "#") { oReturn.occupied++; }
            }
        }
    }
    return oReturn;
}

function output_state() {
    console.log("");
    aState.forEach(aRow => {
        console.log(aRow.join(""));
    });
}