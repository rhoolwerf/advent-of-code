const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n");

// Some buffers
let aSourceRows = [];
for (let i = 0; i <= 127; i++) { aSourceRows.push(i); }
let aSourceSeats = [];
for (let i = 0; i <= 7; i++) { aSourceSeats.push(i); }

// Keep track of highest seat ID
iHighestSeatID = 0;

// Process the boarding tickets
aInput.forEach(sLine => {
    // Determine row
    let aRows = Array.from(aSourceRows);
    for (let i = 0; i < 7; i++) {
        if (sLine.charAt(i) === "F") { aRows = aRows.slice(0, aRows.length / 2); }
        else { aRows = aRows.slice(aRows.length / 2); }
    }

    // Determine seat
    let aSeats = Array.from(aSourceSeats);
    for (let i = 7; i < 10; i++) {
        if (sLine.charAt(i) === "L") { aSeats = aSeats.slice(0, aSeats.length / 2); }
        else { aSeats = aSeats.slice(aSeats.length / 2); }
    }

    let iSeatID = aRows[0] * 8 + aSeats[0];
    if (iSeatID > iHighestSeatID) {
        iHighestSeatID = iSeatID;
    }

    // console.log(`${sLine}: row ${aRows[0]}, column ${aSeats[0]}, seat ID ${iSeatID}.`);
});
console.log(iHighestSeatID);