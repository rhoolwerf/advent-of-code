const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n");

// Keep track of highest seat ID
iHighestSeatID = 0;


// Found a neat trick through https://gathering.tweakers.net/forum/list_message/65087678#65087678
// and https://github.com/rversteeg/AdventOfCode2020/blob/main/Day05.cs
// This consider the input string as a binary (B/R = 1, F/L = 0)

// Process the boarding tickets
aInput.forEach(sLine => {
    // Build the binary
    sBinary = "";
    for (let i = 0; i < sLine.length; i++) {
        if (sLine.charAt(i) === "B" || sLine.charAt(i) === "R") { sBinary += "1"; }
        else { sBinary += "0"; }
    }

    let iSeatID = parseInt(sBinary, 2);

    if (iSeatID > iHighestSeatID) {
        iHighestSeatID = iSeatID;
    }
});
console.log(iHighestSeatID);