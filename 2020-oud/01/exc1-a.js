const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }

const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n").map(x => Number(x));

for (let i = 0; i < aInput.length; i++) {
    for (let j = i + 1; j < aInput.length; j++) {
        let iSum = aInput[i] + aInput[j];
        if (iSum === 2020) {
            let iProduct = aInput[i] * aInput[j];
            console.log(aInput[i] + "\t* " + aInput[j] + "\t= " + iProduct);
        }
    }
}