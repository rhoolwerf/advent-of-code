const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }

const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n").map(x => Number(x));

for (let i = 0; i < aInput.length - 1; i++) {
    for (let j = i + 1; j < aInput.length - 1; j++) {
        for (let k = j + 1; k < aInput.length - 1; k++) {
            let iSum = aInput[i] + aInput[j] + aInput[k];
            if (iSum === 2020) {
                let iProduct = aInput[i] * aInput[j] * aInput[k];
                console.log(iProduct);
            }
        }
    }
}