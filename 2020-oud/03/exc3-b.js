const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aMap = sInput.split("\n");
const iMapWidth = aMap[0].length;

let aSlopes = [
    [1, 1],
    [1, 3],
    [1, 5],
    [1, 7],
    [2, 1]
];

let iMaxSum = 1;
aSlopes.forEach(aSlope => {
    let aPosition = [0, 0];
    let iTreeCount = 0;

    while (aPosition[0] < aMap.length - 1) {
        // Move pawn
        aPosition[0] += aSlope[0];
        aPosition[1] += aSlope[1];

        if (aPosition[0] > aMap.length) {
            // We're out of the maze!
            break;
        }

        // Is there a tree at this position?
        let sChar = aMap[aPosition[0]].charAt(aPosition[1] % iMapWidth);
        if (aMap[aPosition[0]].charAt(aPosition[1] % iMapWidth) === '#') {
            iTreeCount++;
        }
    }
    if (iTreeCount > 0) {
        iMaxSum *= iTreeCount;
    }
});
console.log(iMaxSum);