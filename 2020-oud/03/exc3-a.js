const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aMap = sInput.split("\n");
const aSlope = [1, 3]; // Right 3, down 1
const iMapWidth = aMap[0].length;

let aPosition = [0, 0];
let iTreeCount = 0;

while (aPosition[0] < aMap.length - 1) {
    // Move pawn
    aPosition[0] += aSlope[0];
    aPosition[1] += aSlope[1];

    // Is there a tree at this position?
    let sChar = aMap[aPosition[0]].charAt(aPosition[1] % iMapWidth);
    console.log(aPosition, sChar);
    if (aMap[aPosition[0]].charAt(aPosition[1] % iMapWidth) === '#') {
        iTreeCount++;
    }
}
console.log(iTreeCount);