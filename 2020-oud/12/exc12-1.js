const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n").map(x => { return { d: x.substring(0, 1), v: Number(x.substring(1)) } });
const aFacing = ["N", "E", "S", "W"];

let iX = 0;
let iY = 0;
let sFacing = "E";

aInput.forEach(oInstruction => {

    // Are we changing facing?
    if (oInstruction.d === "L" || oInstruction.d === "R") {
        let iDegrees = (oInstruction.d === "L" ? 360 - oInstruction.v : oInstruction.v);
        let iStepsRight = iDegrees / 90;
        sFacing = aFacing[(aFacing.indexOf(sFacing) + iStepsRight) % aFacing.length];

        // console.log("Turn", oInstruction.d, "for", oInstruction.v, "degrees, meaning", iDegrees, "degrees right or", iStepsRight, "steps", aFacing.indexOf(sFacing), aFacing.indexOf(sFacing) + iStepsRight, (aFacing.indexOf(sFacing) + iStepsRight) % aFacing.length);
        // console.log("From facing", sFacing, "to facing", aFacing[(aFacing.indexOf(sFacing) + iStepsRight) % aFacing.length], "\n");
    } else {
        // Nope we're actually moving!
        let sCommand = oInstruction.d;

        // If we're just moving forward, command = current facing
        if (sCommand === "F") {
            sCommand = sFacing;
        }

        switch (sCommand) {
            case "E":
                iX += oInstruction.v;
                break;
            case "W":
                iX -= oInstruction.v;
                break;
            case "N":
                iY -= oInstruction.v;
                break;
            case "S":
                iY += oInstruction.v;
                break;
            default:
                break;
        }
    }
});
console.log(Math.abs(iX), Math.abs(iY), (Math.abs(iX) + Math.abs(iY)));