const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n").map(x => { return { d: x.substring(0, 1), v: Number(x.substring(1)) } });

let iShipX = 0;
let iShipY = 0;
let iWaypointX = 10;
let iWaypointY = 1;

output("Initial state:");
aInput.forEach(oInstruction => {
    if (oInstruction.d === "L" || oInstruction.d === "R") {
        // Cartesian to Polar
        let iDistance = Math.sqrt(Math.pow(iWaypointX, 2) + Math.pow(iWaypointY, 2));
        let iRadians = Math.atan(iWaypointY / iWaypointX)
        let iDegrees = iRadians * (180 / Math.PI);

        // Add the direction we want to turn
        iDegrees += (oInstruction.d === "R" ? 360 - oInstruction.v : oInstruction.v);

        // Polar to cartesian
        iRadians = iDegrees * (Math.PI / 180);
        iWaypointX = Math.round(iDistance * Math.cos(iRadians));
        iWaypointY = Math.round(iDistance * Math.sin(iRadians));        
    } else {
        switch (oInstruction.d) {
            case "F":
                iShipX += (iWaypointX * oInstruction.v);
                iShipY += (iWaypointY * oInstruction.v);
                break;
            case "N":
                iWaypointY += oInstruction.v;
                break;
            case "S":
                iWaypointY -= oInstruction.v;
                break;
            case "E":
                iWaypointX += oInstruction.v;
                break;
            case "W":
                iWaypointX -= oInstruction.v;
                break;
        }
    }
    output("After " + oInstruction.d + oInstruction.v);
});
console.log(Math.abs(iShipX), Math.abs(iShipY), Math.abs(iShipX) + Math.abs(iShipY));

function output(sMessage) {
    let sOutput = sMessage + "\tShip is at ";
    sOutput += (iShipX >= 0 ? "east" : "west") + " " + Math.abs(iShipX) + ", ";
    sOutput += (iShipY >= 0 ? "north" : "south") + " " + Math.abs(iShipY);

    sOutput += "\tWaypoint is at ";
    sOutput += (iWaypointX >= 0 ? "east" : "west") + " " + Math.abs(iWaypointX) + ", ";
    sOutput += (iWaypointY >= 0 ? "north" : "south") + " " + Math.abs(iWaypointY);

    console.log(sOutput);
}