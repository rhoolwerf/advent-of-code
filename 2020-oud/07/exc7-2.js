const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n");

let oChildren = new Map();

aInput.forEach(sLine => {
    let [sBag, sContains] = sLine.split("contain");
    let aContains = sContains.split(",").map(x => x.trim().replace(".", "").replace("no other bags", ""));
    if (!(aContains.length === 1 && aContains[0] === "")) {

        sBag = sBag.replace("bags", "").trim();

        aContains.forEach(sInsideLine => {
            let sSubBag = sInsideLine.split(" ").slice(1, sInsideLine.split(" ").length - 1).join(" ");
            if (!oChildren.has(sBag)) { oChildren.set(sBag, new Set()); }
            oChildren.get(sBag).add({
                q: Number(sInsideLine.split(" ")[0]),
                b: sSubBag
            });
        });
    }
});

// console.log(oChildren);

console.log(drill_down("shiny gold", "") - 1);

function drill_down(sBag, sDepth) {
    // console.log(sDepth, "Looking for children of:", sBag);
    let iSum = 1;
    if (oChildren.has(sBag)) {
        for (oChild of oChildren.get(sBag)) {
            // console.log(oChild);
            iSum += oChild.q * drill_down(oChild.b, sDepth + " ");
        }
    } else {
        iSum = 1;
    }
    // console.log(sDepth, "Done for:", sBag, "\n");
    return iSum;
}