const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n");

let aBagToParent = new Map();

aInput.forEach(sLine => {
    let [sBag, sContains] = sLine.split("contain");
    let aContains = sContains.split(",").map(x => x.trim().replace(".", "").replace("no other bags", ""));
    if (!(aContains.length === 1 && aContains[0] === "")) {
        sBag = sBag.replace("bags", "").trim();
        // console.log(sBag);
        aContains.forEach(sInsideLine => {
            // console.log("- " + sInsideLine);

            let sSubBag = sInsideLine.split(" ").slice(1, sInsideLine.split(" ").length - 1).join(" ");

            if (!aBagToParent.has(sSubBag)) {
                aBagToParent.set(sSubBag, new Set());
            }
            aBagToParent.get(sSubBag).add({
                subQuantity: Number(sInsideLine.split(" ")[0]),
                parent: sBag
            });
        });
    }
});

// console.log(aBagToParent)

let oAllParents = new Set();
find_bag_parents("shiny gold", oAllParents);
// console.log(oAllParents, oAllParents.size);
console.log(oAllParents.size);

function find_bag_parents(sBag, oAllParents) {
    let oParents = aBagToParent.get(sBag);
    if (oParents) {
        // console.log("Looking for", sBag, "\n", oParents);
        for (oEntry of oParents) {
            oAllParents.add(oEntry.parent);
            find_bag_parents(oEntry.parent, oAllParents);
        }
    }
}
