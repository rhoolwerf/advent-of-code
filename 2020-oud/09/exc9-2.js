const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n").map(x => Number(x));

const iPreAmbleSize = Number(process.argv[3]);

if (!iPreAmbleSize) { console.log("Please give a pre-amble size"); return; };

aInput.forEach((iNumber, iIndex) => {
    if (iIndex >= iPreAmbleSize) {
        let bSumFound = false;
        for (let iPos1 = iIndex - iPreAmbleSize; iPos1 < aInput.length && !bSumFound; iPos1++) {
            for (let iPos2 = iIndex - iPreAmbleSize; iPos2 < aInput.length && !bSumFound; iPos2++) {
                // Ensure we're not using the same number twice
                if (iPos1 !== iPos2) {
                    if ((aInput[iPos1] + aInput[iPos2]) === iNumber) {
                        bSumFound = true;
                    }
                }
            }
        }
        if (!bSumFound) {
            console.log("Could not find a couple for", iNumber);

            // Find the contigous set of at least two numbers which sum up to this number
            bSumFound = false;
            for (let iPos1 = 0; iPos1 < iIndex && !bSumFound; iPos1++) {
                let iSum = aInput[iPos1];
                let aSumNumbers = [aInput[iPos1]];
                for (let iPos2 = iPos1 + 1; iPos2 < iIndex && iSum < iNumber && !bSumFound; iPos2++) {
                    iSum += aInput[iPos2];
                    aSumNumbers.push(aInput[iPos2]);
                    if (iSum === iNumber) {
                        bSumFound = true;
                        aSumNumbers.sort((a, b) => { return a - b; });
                        console.log(aSumNumbers[0], "+", aSumNumbers[aSumNumbers.length - 1], "=", (aSumNumbers[0] + aSumNumbers[aSumNumbers.length - 1]));
                    }
                }
            }
        }
    }
});