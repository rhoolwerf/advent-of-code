const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n").map(x => Number(x)).sort((a, b) => { return a - b; });

// We need a start and an end
aInput.unshift(0);
aInput.push(aInput[aInput.length - 1] + 3);

// Build tree both ways (bottom up, to immediately check for possible routes to the end)
let oChildren = new Map();
let oNodes = new Map();
for (let iPos = aInput.length - 1; iPos >= 0; iPos--) {
    let iNodeNumber = aInput[iPos];

    // console.log("Possible parents for:", aInput[iPos]);
    for (let iPrevPos = iPos - 1; (aInput[iPrevPos] - aInput[iPos]) >= -3; iPrevPos--) {
        let iParent = aInput[iPrevPos];

        if (!oChildren.has(iParent)) { oChildren.set(iParent, new Set()); }
        oChildren.get(iParent).add(iNodeNumber);
    }

    oNodes.set(iNodeNumber, {
        isEnd: (iPos === aInput.length - 1),
        validPaths: new Set()
        // validPaths: (iPos === aInput.length - 1 ? undefined : get_valid_paths(iNodeNumber))
    });

}

console.log(oNodes);
// console.log(oChildren);

console.log(get_valid_paths(19));

function get_valid_paths(iNodeNumber) {
    console.log("Determining valid paths for", iNodeNumber, oChildren.get(iNodeNumber));

    oChildren.get(iNodeNumber).forEach(iChild => {
        let oChild = oNodes.get(iChild);
        console.log("-", iChild, oChild);
        if (oChild.isEnd) { console.log("end reached"); return []; }
        else { return get_valid_paths(iChild); }
    });
}