const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n").map(x => Number(x)).sort((a, b) => { return a - b; });

let iJolt = 0;
let oOffsetCounters = new Map();

aInput.push(aInput[aInput.length - 1] + 3);

aInput.forEach(iInput => {
    let iOffset = iInput - iJolt;
    iJolt = iInput;

    if (!oOffsetCounters.has(iOffset)) { oOffsetCounters.set(iOffset, 1); }
    else { oOffsetCounters.set(iOffset, oOffsetCounters.get(iOffset) + 1); }
});
// console.log(oOffsetCounters);
console.log(oOffsetCounters.get(3) * oOffsetCounters.get(1));
