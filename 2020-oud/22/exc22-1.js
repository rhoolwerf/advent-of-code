const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n\n").map(x => x.split("\n"));
let oDecks = aInput.reduce((acc, curr) => {
    curr.forEach(val => {
        if (val.indexOf("Player") > -1) {
            acc.set(Number(curr[0].split(" ")[1].split(":")[0]), []);
        } else {
            acc.get(Number(curr[0].split(" ")[1].split(":")[0])).push(Number(val));
        }
    });
    return acc;
}, new Map());

for (let i = 1; true; i++) {
    console.log("-- Round", i, "--");
    console.log("Player 1's deck:", oDecks.get(1).join(", "));
    console.log("Player 2's deck:", oDecks.get(2).join(", "));
    let iP1 = oDecks.get(1).shift();
    let iP2 = oDecks.get(2).shift();
    console.log("Player 1 plays:", iP1);
    console.log("Player 2 plays:", iP2);
    if (iP1 > iP2) {
        console.log("Player 1 wins the round!");
        oDecks.get(1).push(iP1);
        oDecks.get(1).push(iP2);
    } else {
        console.log("Player 2 wins the round!");
        oDecks.get(2).push(iP2);
        oDecks.get(2).push(iP1);
    }
    console.log("");

    if (oDecks.get(1).length === 0 || oDecks.get(2).length === 0) {
        // Someone has reached an empty deck
        break;
    }
}

console.log("== Post-game results ==");
console.log("Player 1's deck:", oDecks.get(1).join(", "));
console.log("Player 2's deck:", oDecks.get(2).join(", "));

let oWinningDeck = oDecks.get(1).length > 0 ? oDecks.get(1) : oDecks.get(2);
let iSum = 0;
for (let i = 1; oWinningDeck.length > 0; i++) {
    iSum += i * oWinningDeck.pop();
}
console.log("Winning player's score:", iSum);

