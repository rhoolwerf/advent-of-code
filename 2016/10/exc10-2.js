const sInputFile = process.argv[2];
const fs = require("fs");
const aInstructions = fs.readFileSync(sInputFile, "utf8").split("\n");

const oBotSkeleton = {
    id: 0,
    name: "",
    lowerTo: null,
    higherTo: null,
    chips: []
}
const oBots = new Map();
const oOutputs = new Map();

// Parse the input into workings bots
aInstructions.forEach(sInstruction => {
    const aChunks = sInstruction.split(" ");
    if (aChunks[0] === "value") {
        register_bot(Number(aChunks[5]));
        oBots.get(Number(aChunks[5])).chips.push(Number(aChunks[1]));
    } else {
        register_bot(Number(aChunks[1]));
        register_transfer(Number(aChunks[1]), aChunks[3], aChunks[5], Number(aChunks[6]));
        register_transfer(Number(aChunks[1]), aChunks[8], aChunks[10], Number(aChunks[11]));
    }
});

let bKeepRunning = true;
while (bKeepRunning) {
    let bAllBotsEmpty = true;
    oBots.forEach(oBot => {
        if (oBot.chips.length >= 2) {
            oBot.chips.sort((a, b) => a - b);
            oBot.higherTo.chips.push(oBot.chips.pop());
            oBot.lowerTo.chips.push(oBot.chips.pop());
            bAllBotsEmpty = false;
        }
    });
    if (bAllBotsEmpty) {
        bKeepRunning = false;
    }
}
console.log(oOutputs.get(0).chips[0] * oOutputs.get(1).chips[0] * oOutputs.get(2).chips[0]);

function register_bot(iBotId) {
    if (!oBots.has(iBotId)) {
        const oBotClone = JSON.parse(JSON.stringify(oBotSkeleton));
        oBotClone.id = iBotId;
        oBotClone.name = "Bot " + iBotId;
        oBots.set(iBotId, oBotClone);
    }
}
function register_output(iOutputId) {
    if (!oOutputs.has(iOutputId)) {
        const oBotClone = JSON.parse(JSON.stringify(oBotSkeleton));
        oBotClone.id = iOutputId;
        oBotClone.name = "Output " + iOutputId;
        oOutputs.set(iOutputId, oBotClone);
    }
}
function register_transfer(iBotId, sHighOrLow, sTransferType, iId) {
    if (sTransferType === "bot") {
        register_bot(iId);
        if (sHighOrLow === "high") {
            oBots.get(iBotId).higherTo = oBots.get(iId);
        } else {
            oBots.get(iBotId).lowerTo = oBots.get(iId);
        }
    } else {
        register_output(iId);
        if (sHighOrLow === "high") {
            oBots.get(iBotId).higherTo = oOutputs.get(iId);
        } else {
            oBots.get(iBotId).lowerTo = oOutputs.get(iId);
        }
    }
}