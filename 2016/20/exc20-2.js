const sInputFile = process.argv[2];
const iRequiredRowCount = Number(process.argv[3]);
const fs = require("fs");
const aBlocks = fs.readFileSync(sInputFile, "utf8").split("\n").map(x => { return { min: Number(x.split("-")[0]), max: Number(x.split("-")[1]), o: x } });

aBlocks.sort((a, b) => {
    if (a.min > b.min) { return 1; }
    else if (a.min < b.min) { return -1; }
    else { return a.max - b.max; }
});

let iCountValid = 0;
for (let iAttempt = 0; iAttempt <= 4294967295; iAttempt++) {
    bBlocked = false;
    for (let i = 0; i < aBlocks.length; i++) {
        if (aBlocks[i].min <= iAttempt && iAttempt <= aBlocks[i].max) {
            // We are within a group, jump immediately to end of group to skip unnecessary checks
            bBlocked = true;
            iAttempt = aBlocks[i].max;
            break;
        }
    }
    if (!bBlocked) {
        iCountValid++;
    }
}
console.log(iCountValid);