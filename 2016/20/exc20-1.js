const sInputFile = process.argv[2];
const iRequiredRowCount = Number(process.argv[3]);
const fs = require("fs");
const aBlocks = fs.readFileSync(sInputFile, "utf8").split("\n").map(x => { return { min: Number(x.split("-")[0]), max: Number(x.split("-")[1]), o: x } });

aBlocks.sort((a, b) => {
    if (a.min > b.min) { return 1; }
    else if (a.min < b.min) { return -1; }
    else { return a.max - b.max; }
});

let iAttempt = 0;
while (!check_valid(iAttempt)) { iAttempt++; }
console.log(iAttempt);

function check_valid(iNum) {
    for (let i = 0; i < aBlocks.length; i++) {
        if (aBlocks[i].min <= iNum && aBlocks[i].max >= iNum) {
            return false;
        }
    }
    return true;
}