const {
    performance
} = require('perf_hooks');

const sInputFile = process.argv[2];
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n");

// Init screen
let aScreen = [];

aInput.forEach(sInstruction => {
    const aChunks = sInstruction.split(" ");
    console.log(aChunks);
    if (aChunks[0] === "screen") {
        aScreen = [];
        [iMaxX, iMaxY] = aChunks[1].split("x");
        for (let iY = 0; iY < iMaxY; iY++) {
            aScreen.push([]);
            for (let iX = 0; iX < iMaxX; iX++) {
                aScreen[iY].push(".");
            }
        }
    } else if (aChunks[0] === "rect") {
        [iMaxX, iMaxY] = aChunks[1].split("x");
        for (let iY = 0; iY < iMaxY; iY++) {
            for (let iX = 0; iX < iMaxX; iX++) {
                aScreen[iY][iX] = "#";
            }
        }
    } else if (aChunks[0] === "rotate" && aChunks[1] === "column") {
        // Determine current state of columns
        let iColumn = Number(aChunks[2].split("=")[1]);
        let iOffset = Number(aChunks[4]);
        let aColumns = [];

        for (let i = 0; i < aScreen.length; i++) {
            aColumns.push(aScreen[i][iColumn]);
        }
        while (iOffset-- > 0) {
            aColumns.unshift(aColumns.pop());
        }
        for (let i = 0; i < aScreen.length; i++) {
            aScreen[i][iColumn] = aColumns[i];
        }
    } else if (aChunks[0] === "rotate" && aChunks[1] === "row") {
        let iRow = Number(aChunks[2].split("=")[1]);
        let iOffset = Number(aChunks[4]);
        while (iOffset-- > 0) {
            aScreen[iRow].unshift(aScreen[iRow].pop());
        }
    } else {
        console.log("Unknown command:", aChunks);
        return;
    }
    clean_console();
    aScreen.forEach(aScreenLine => console.log(aScreenLine.join("")));
    delay(50);
    console.log("");
});

let iCountOn = 0;
aScreen.forEach(aRow => { aRow.forEach(sCell => { if (sCell === "#") iCountOn++; }) })
console.log(iCountOn);





function delay(iMillis) {
    let iNow = performance.now();
    let iStop = iNow + iMillis;
    while (performance.now() < iStop);
}

function clean_console() {
    // Clears entire terminal for cleaner look
    process.stdout.write("\u001b[3J\u001b[2J\u001b[1J"); console.clear();
}
