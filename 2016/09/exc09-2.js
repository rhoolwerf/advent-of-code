const sInputFile = "exc09.in";//process.argv[2];
const fs = require("fs");
const sPuzzleInput = fs.readFileSync(sInputFile, "utf8").split("\n").join("");

// console.log(decompress("(3x3)XYZ"));
// console.log(decompress("X(8x2)(3x3)ABCY", true).length);
// console.log(decompress("(27x12)(20x12)(13x14)(7x10)(1x12)A", true).length);
// console.log(decompress("(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN").length);
console.log(decompress(sPuzzleInput));

function decompress(sInput) {
    let iOutput = 0;
    for (let iIndex = 0; iIndex < sInput.length; iIndex++) {
        if (sInput.charAt(iIndex) === "(") {
            // Find the end of the block
            let iEndIndex = iIndex + 1;
            for (; iEndIndex < sInput.length; iEndIndex++) {
                if (sInput.charAt(iEndIndex) === ")") {
                    break;
                }
            }

            // Determine the string to repeat and how often
            let [iLength, iRepeat] = sInput.substring(iIndex + 1, iEndIndex).split("x").map(Number);
            let sRepeatingString = "";
            for (let iRepeating = iEndIndex + 1; iRepeating < iEndIndex + 1 + iLength; iRepeating++) {
                sRepeatingString += sInput.charAt(iRepeating);
            }

            // Repeat what we found and decompress that again
            for (let iRepeatCount = 0; iRepeatCount < iRepeat; iRepeatCount++) {
                iOutput += decompress(sRepeatingString);
            }

            // Increase the pointer to the end of the repeated character (because the for-loop will step after)
            iIndex = iEndIndex + iLength;
        } else {
            iOutput++;
        }
    }

    return iOutput;
}

