const sInputFile = "exc09.in";//process.argv[2];
const fs = require("fs");
const sPuzzleInput = fs.readFileSync(sInputFile, "utf8").split("\n").join("");

// console.log(decompress("ADVENT"));
// console.log(decompress("A(1x5)BC"));
// console.log(decompress("(3x3)XYZ"));
// console.log(decompress("A(2x2)BCD(2x2)EFG"));
// console.log(decompress("(6x1)(1x3)A"));
// console.log(decompress("X(8x2)(3x3)ABCY"));
console.log(decompress(sPuzzleInput));

function decompress(sInput) {
    sOutput = "";
    for (let iIndex = 0; iIndex < sInput.length; iIndex++) {
        if (sInput.charAt(iIndex) === "(") {
            // Find the end of the block
            let iEndIndex = iIndex + 1;
            for (; iEndIndex < sInput.length; iEndIndex++) {
                if (sInput.charAt(iEndIndex) === ")") {
                    break;
                }
            }

            // Determine the string to repeat and how often
            let [iLength, iRepeat] = sInput.substring(iIndex + 1, iEndIndex).split("x").map(Number);
            let sRepeatingString = "";
            for (let iRepeating = iEndIndex + 1; iRepeating < iEndIndex + 1 + iLength; iRepeating++) {
                sRepeatingString += sInput.charAt(iRepeating);
            }

            // Actually repeat what we found
            for (let iRepeatCount = 0; iRepeatCount < iRepeat; iRepeatCount++) {
                sOutput += sRepeatingString;
            }

            // Increase the pointer to the end of the repeated character (because the for-loop will step after)
            iIndex = iEndIndex + iLength;
        } else {
            sOutput += sInput.charAt(iIndex);
        }
    }

    // console.log(sOutput);
    return sOutput.length;
}

