const SEED = 1358;//10;
const oStartPoint = { x: 1, y: 1 };
const oEndPoint = { x: 31, y: 39 };//{ x: 7, y: 4 };

const astar = require('./astar.js');

let aGraphData = [];
let aMap = [];

// Build the graph
for (let iY = 0; iY < 50; iY++) {
    aGraphData[iY] = [];
    aMap[iY] = [];
    let sRow = "";
    for (let iX = 0; iX < 50; iX++) {
        const sTile = get_tile(iY, iX);
        aGraphData[iY][iX] = sTile === "#" ? 0 : 1;
        aMap[iY][iX] = sTile;
    }
}

// Setup astar
let oGraph = new astar.Graph(aGraphData);
let oGraphStart = oGraph.grid[oStartPoint.y][oStartPoint.x];
let oGraphEnd = oGraph.grid[oEndPoint.y][oEndPoint.x];

let aPath = astar.astar.search(oGraph, oGraphStart, oGraphEnd);
console.log(aPath.length);
// aPath.forEach(aPathTile => {
//     aMap[aPathTile.x][aPathTile.y] = "-";
// });
// aMap.forEach(aRow => console.log(aRow.join("")));

function get_tile(iY, iX) {
    if (iY < 0 || iX < 0) {
        return ".";
    } else {
        const iResult = iX * iX + 3 * iX + 2 * iX * iY + iY + iY * iY + SEED;
        return (iResult.toString(2).split("1").length - 1) % 2 === 1 ? "#" : ".";
    }
}