import { readFileSync } from 'fs';

const REGEX_SWAP_POS_X_Y = /swap position ([0-9]+) with position ([0-9]+)/;
const REGEX_SWAP_X_Y = /swap letter ([a-z]) with letter ([a-z])/;
const REGEX_ROTATE_STEPS = /rotate (left|right) ([0-9]+) step/;
const REGEX_ROTATE_BASED_ON = /rotate based on position of letter ([a-z])/;
const REGEX_REVERSE_X_Y = /reverse positions ([0-9]+) through ([0-9]+)/;
const REGEX_MOVE_X_Y = /move position ([0-9]+) to position ([0-9]+)/;

const [_, instructions] = readFileSync(process.argv[2], 'utf8')
  .split('\n\n')
  .map((block) => block.split('\n'));

// Grab all possible permutations of 'fbgdceah'

permutator('fbgdceah'.split('')).forEach((permutation) => {
  if (scramble(permutation.join('')) === 'fbgdceah') {
    console.log('fbgdceah', '->', permutation.join(''));
  }
});

// 'Borrowed' from https://stackoverflow.com/a/20871714
function permutator(inputArr: Array<string>): Array<Array<string>> {
  let result: Array<Array<string>> = [];

  function permute(arr: Array<string>, m: Array<string> = []) {
    if (arr.length === 0) {
      result.push(m);
    } else {
      for (let i = 0; i < arr.length; i++) {
        let curr = arr.slice();
        let next = curr.splice(i, 1);
        permute(curr.slice(), m.concat(next));
      }
    }
  }

  permute(inputArr);

  return result;
}

function scramble(passwordInput: string): string {
  let password = passwordInput.split('');

  instructions.forEach((inst) => {
    if (REGEX_SWAP_POS_X_Y.test(inst)) {
      const [_, x, y] = process_regex(REGEX_SWAP_POS_X_Y, inst).map(Number);
      swap_pos(password, x, y);
      //   console.log('SWAP_POS_X_Y', x, y, password);
    } else if (REGEX_SWAP_X_Y.test(inst)) {
      const [_, c1, c2] = process_regex(REGEX_SWAP_X_Y, inst);
      const i1 = password.indexOf(c1);
      const i2 = password.indexOf(c2);
      swap_pos(password, i1, i2);
      //   console.log('SWAP_X_Y', c1, c2, i1, i2, password);
    } else if (REGEX_REVERSE_X_Y.test(inst)) {
      const [_, x, y] = process_regex(REGEX_REVERSE_X_Y, inst).map(Number);
      password = [password.slice(0, x), password.slice(x, y + 1).reverse(), password.slice(y + 1)].flat();
      //   console.log('REVERSE_X_Y', x, y, password);
    } else if (REGEX_ROTATE_STEPS.test(inst)) {
      const [_, dir, count] = process_regex(REGEX_ROTATE_STEPS, inst);
      rotate(password, dir, Number(count));
      //   console.log('ROTATE_STEPS', dir, count, password);
    } else if (REGEX_MOVE_X_Y.test(inst)) {
      const [_, x, y] = process_regex(REGEX_MOVE_X_Y, inst).map(Number);

      const [x1, y1] = [Math.min(x, y), Math.max(x, y)];

      const c = password.splice(y1 + 1);
      const b = password.splice(x1);

      if (x < y) {
        b.push(b.shift() as string);
      } else {
        b.unshift(b.pop() as string);
      }
      password = password.concat(b.concat(c));

      //   console.log('MOVE_X_Y', x, y, password);
    } else if (REGEX_ROTATE_BASED_ON.test(inst)) {
      const [_, c1] = process_regex(REGEX_ROTATE_BASED_ON, inst);
      const offset = password.indexOf(c1);
      const rotations = 1 + offset + (offset >= 4 ? 1 : 0);
      rotate(password, 'right', rotations);
      //   console.log('ROTATE_BASED_ON', c1, password);
    } else {
      console.error('Unknown instruction:', inst);
    }
  });
  return password.join('');
}

function process_regex(regexp: RegExp, input: string): Array<string> {
  return regexp.exec(input) as RegExpExecArray;
}

function swap_pos(password: Array<string>, x: number, y: number): void {
  const temp = password[y];
  password[y] = password[x];
  password[x] = temp;
}

function rotate(password: Array<string>, dir: string, count: number): void {
  for (let i = 0; i < count; i++) {
    if (dir === 'right') password.unshift(password.pop() as string);
    else password.push(password.shift() as string);
  }
}
