const sInputFile = process.argv[2];
const fs = require("fs");
const aInputBlocks = fs.readFileSync(sInputFile, "utf8").split('\n\n');
const aInstructions = aInputBlocks[1].split('\n');

const swapPositions = /^swap position ([0-9]) with position ([0-9])$/;
const swapLetters = /^swap letter ([a-z]) with letter ([a-z])$/;
const reversePositions = /^reverse positions ([0-9]) through ([0-9])$/;
const rotate = /^rotate (left|right) ([0-9]+) step/;
const movePosition = /^move position ([0-9]) to position ([0-9])$/;
const rotateBasedOnLetter = /^rotate based on position of letter ([a-z])$/;

// Borrowed from https://stackoverflow.com/questions/1431094/how-do-i-replace-a-character-at-a-particular-index-in-javascript
String.prototype.replaceAt = function (index, replacement) {
    return this.substr(0, index) + replacement + this.substr(index + replacement.length);
}

String.prototype.swapPositions = function (x, y) {
    const buffer = this[x];
    const step1 = this.replaceAt(x, this[y]);
    return step1.replaceAt(y, buffer);
}

let sInput = aInputBlocks[0];
aInstructions.forEach(sInstruction => {
    const sOutput = process_instruction(sInput, sInstruction);
    console.log(
        sInput,
        '->',
        `${sInstruction}                                    `.slice(0, 36),
        '->',
        sOutput,
        (sInput === sOutput) ? '-> Nothing happened?' : ''
    );
    sInput = sOutput;
});

function process_instruction(sInput, sInstruction) {
    if (swapPositions.test(sInstruction)) {
        const [_, x, y] = swapPositions.exec(sInstruction).map(Number);
        return sInput.swapPositions(x, y);
    } else if (swapLetters.test(sInstruction)) {
        const [_, x, y] = swapLetters.exec(sInstruction);
        return sInput.swapPositions(sInput.indexOf(x), sInput.indexOf(y));
    } else if (reversePositions.test(sInstruction)) {
        const [_, x, y] = reversePositions.exec(sInstruction).map(Number);
        // if (x >= y) { console.error('reversePositions kan niet omgaan met X>=Y'); return '';}
        const a = sInput.split('');
        const c = a.splice(y + 1);
        const b = a.splice(x);
        return a.concat(b.reverse().concat(c)).join('');
    } else if (rotate.test(sInstruction)) {
        let [_, direction, x] = rotate.exec(sInstruction).map(x => isNaN(Number(x)) ? x : Number(x));
        const a = sInput.split('');
        while (x-- > 0) {
            if (direction === 'right') {
                a.unshift(a.pop());
            } else {
                a.push(a.shift());
            }
        }
        return a.join('');
    } else if (movePosition.test(sInstruction)) {
        const [_, x, y] = movePosition.exec(sInstruction).map(Number);
        const [x1, y1] = [Math.min(x, y), Math.max(x, y)];

        const a = sInput.split('');
        const c = a.splice(y1 + 1);
        const b = a.splice(x1);

        if (x < y) {
            b.push(b.shift());
        } else {
            b.unshift(b.pop());
        }
        return a.concat(b.concat(c)).join('');
    } else if (rotateBasedOnLetter.test(sInstruction)) {
        const [_, letter] = rotateBasedOnLetter.exec(sInstruction);
        const offset = sInput.indexOf(letter);
        let rotations = 1 + offset + (offset >= 4 ? 1 : 0);

        const a = sInput.split('');
        while (rotations-- > 0) {
            a.unshift(a.pop());
        }
        return a.join('');
    } else {
        console.log(`Instruction '${sInstruction}' cannot be parsed'`);
    }

    return sInput;
}