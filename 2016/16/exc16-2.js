
console.assert(generate_hash("1") === "100");
console.assert(generate_hash("0") === "001");
console.assert(generate_hash("11111") === "11111000000");
console.assert(generate_hash("111100001010") === "1111000010100101011110000");

console.assert(calculate_checksum("110010110100") === "100");

console.assert(puzzle("10000", 20) === "01100");


// Part 2 has a different size
console.log(puzzle("11110010111001001", 35651584));

function puzzle(sInput, iSize) {
    let sHash = sInput;
    while (sHash.length < iSize) {
        sHash = generate_hash(sHash);
    }
    return calculate_checksum(sHash.substring(0, iSize));
}

function generate_hash(a) {
    let b = "";
    for (let i = a.length - 1; i >= 0; i--) {
        if (a.charAt(i) === "1") { b += "0"; }
        else { b += "1"; }
    }
    return a + "0" + b;
}

function calculate_checksum(sInput) {
    let sChecksum = "";
    for (let i = 0; i < sInput.length; i += 2) {
        if (sInput.charAt(i) === sInput.charAt(i + 1)) { sChecksum += "1"; }
        else { sChecksum += "0"; }
    }

    if (sChecksum.length % 2 === 0) {
        return calculate_checksum(sChecksum);
    } else {
        return sChecksum;
    }
}