const sInputFile = process.argv[2];
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n");

let aDiscs = [];

aInput.forEach(sDiscInput => {
    const aChunks = sDiscInput.split(" ");
    aDiscs.push({
        id: Number(aChunks[1].split("#")[1]),
        maxPos: Number(aChunks[3]),
        curPos: Number(aChunks[11].split(".")[0])
    });
});

let iAttempt = 0;
for (; !simulate(iAttempt); iAttempt++);
console.log(iAttempt);

// console.log(simulate(0), "\n");
// console.log(simulate(5), "\n");

function simulate(t) {
    // console.log("Simulating t=", t);
    for (let iDisc = 0; iDisc < aDiscs.length; iDisc++) {
        const iDiscTime = t + iDisc + 1;
        const iDiscPosition = (aDiscs[iDisc].curPos + iDiscTime) % aDiscs[iDisc].maxPos;
        // console.log("Disc", aDiscs[iDisc].id, "is at position", iDiscPosition, "at t=", iDiscTime);
        if (iDiscPosition !== 0) {
            return false;
        }
    }

    return true;
}