const md5 = require('md5');

// process_step('hijkl'.split(''), 0, 0, 0);

console.assert(process_hash('ihgpwlah') === 'DDRRRD', `'ihgpwlah' !== 'DDRRRD'`);
console.assert(process_hash('kglvqrro') === 'DDUDRLRRUDRD', `'kglvqrro' !== 'DDUDRLRRUDRD'`);
console.assert(process_hash('ulqzkmiv') === 'DRURDRUDDLLDLUURRDULRLDUUDDDRR', `'ulqzkmiv' !== 'DRURDRUDDLLDLUURRDULRLDUUDDDRR'`);
console.log(process_hash('awrkjxxr'));

function process_hash(hash: string): string {
  const validPaths: Set<string> = new Set();
  process_step(hash, [], 0, 0, validPaths);
  return Array.from(validPaths.keys()).reduce((carry, path) => (carry.length === 0 ? path : path.length < carry.length ? path : carry), '');
}
function process_step(input: string, path: Array<string>, y: number, x: number, validPaths: Set<string>): void {
  if (y === 3 && x === 3) {
    validPaths.add(path.join(''));
  } else {
    const hash = md5(input + path.join('')).slice(0, 4) as string;

    const directions = [
      { d: 'U', open: isOpen(hash.charAt(0), y - 1, x) },
      { d: 'D', open: isOpen(hash.charAt(1), y + 1, x) },
      { d: 'L', open: isOpen(hash.charAt(2), y, x - 1) },
      { d: 'R', open: isOpen(hash.charAt(3), y, x + 1) },
    ].filter(({ open }) => open);

    directions.forEach(({ d }) => {
      const nextPath = (JSON.parse(JSON.stringify(path)) as Array<string>).concat(d);
      const nextY = d === 'U' ? y - 1 : d === 'D' ? y + 1 : y;
      const nextX = d === 'L' ? x - 1 : d === 'R' ? x + 1 : x;
      process_step(input, nextPath, nextY, nextX, validPaths);
    });
  }
}

function isOpen(char: string, y: number, x: number): boolean {
  return ['b', 'c', 'd', 'e', 'f'].includes(char) && 0 <= y && y < 4 && 0 <= x && x < 4;
}
