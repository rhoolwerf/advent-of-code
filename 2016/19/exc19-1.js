let iElfCount = 3001330; //Puzzle Input
// iElfCount = 5; // Test 

let aElfs = [];
for (let i = 0; i < iElfCount; i++) {
    aElfs.push({ id: i + 1, presents: 1 });
}
// console.log(aElfs);

while (aElfs.length > 1) {
    console.log("Elfs remaining:", aElfs.length);

    // Let all elfs steal
    for (let i = 0; i < aElfs.length; i++) {
        if (aElfs[i].presents > 0) {
            // Find the next elf with at least 1 present
            let iNextElf = (i + 1) % aElfs.length;
            while (i !== iNextElf) {
                if (aElfs[iNextElf].presents > 0) {
                    break;
                } else {
                    iNextElf = (iNextElf + 1) % aElfs.length;
                }
            }
            aElfs[i].presents += aElfs[iNextElf].presents;
            aElfs[iNextElf].presents = 0;
        }
    }
    // Clean up any elfs with no presents (as doing this 'live' is really slow)
    aElfs.sort((a, b) => b.presents - a.presents);
    let iFirstElfWithoutPresents = 0;
    for (; iFirstElfWithoutPresents < aElfs.length; iFirstElfWithoutPresents++) {
        if (aElfs[iFirstElfWithoutPresents].presents === 0) {
            break;
        }
    }
    aElfs = aElfs.slice(0, iFirstElfWithoutPresents);
    aElfs.sort((a, b) => a.id - b.id);
}
console.log(aElfs);
