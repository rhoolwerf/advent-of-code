const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8").split(",").map(x => x.trim());

let iX = iY = 0;
let eFacing = "N";
let aPosition = [];
sInput.forEach(sInstruction => {
    switch (sInstruction.substring(0, 1)) {
        case "R":
            switch (eFacing) {
                case "N":
                    eFacing = "E";
                    register_steps(0, 1, Number(sInstruction.substring(1)))
                    break;
                case "E":
                    eFacing = "S";
                    register_steps(1, 0, Number(sInstruction.substring(1)))
                    break;
                case "S":
                    eFacing = "W";
                    register_steps(0, -1, Number(sInstruction.substring(1)))
                    break;
                case "W":
                    eFacing = "N";
                    register_steps(-1, 0, Number(sInstruction.substring(1)))
                    break;
            }
            break;
        case "L":
            switch (eFacing) {
                case "N":
                    eFacing = "W";
                    register_steps(0, -1, Number(sInstruction.substring(1)))
                    break;
                case "W":
                    eFacing = "S";
                    register_steps(1, 0, Number(sInstruction.substring(1)))
                    break;
                case "S":
                    eFacing = "E";
                    register_steps(0, 1, Number(sInstruction.substring(1)))
                    break;
                case "E":
                    eFacing = "N";
                    register_steps(-1, 0, Number(sInstruction.substring(1)))
                    break;
            }
            break;
    }
});

function register_steps(iOffsetY, iOffsetX, iSteps) {
    for (let i = 0; i < iSteps; i++) {
        iX += iOffsetX;
        iY += iOffsetY;

        if (aPosition[iY] === undefined) { aPosition[iY] = []; }
        if (aPosition[iY][iX] === undefined) { aPosition[iY][iX] = 1; }
        else {
            aPosition[iY][iX]++;
            console.log("Visiting", iY, ",", iX, " again (", aPosition[iY][iX], "), distance is ", (Math.abs(iY) + Math.abs(iX)));
        }
    }
}