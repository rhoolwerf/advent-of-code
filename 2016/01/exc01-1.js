const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8").split(",").map(x => x.trim());

let iX = iY = 0;
let eFacing = "N";
sInput.forEach(sInstruction => {
    switch (sInstruction.substring(0, 1)) {
        case "R":
            switch (eFacing) {
                case "N":
                    eFacing = "E";
                    iX += Number(sInstruction.substring(1));
                    break;
                case "E":
                    eFacing = "S";
                    iY += Number(sInstruction.substring(1));
                    break;
                case "S":
                    eFacing = "W";
                    iX -= Number(sInstruction.substring(1));
                    break;
                case "W":
                    eFacing = "N";
                    iY -= Number(sInstruction.substring(1));
                    break;
            }
            break;
        case "L":
            switch (eFacing) {
                case "N":
                    eFacing = "W";
                    iX -= Number(sInstruction.substring(1));
                    break;
                case "W":
                    eFacing = "S";
                    iY += Number(sInstruction.substring(1));
                    break;
                case "S":
                    eFacing = "E";
                    iX += Number(sInstruction.substring(1));
                    break;
                case "E":
                    eFacing = "N";
                    iY -= Number(sInstruction.substring(1));
                    break;
            }
            break;
    }
});
console.log(iY, iX, Math.abs(iY) + Math.abs(iX));