const sInputFile = process.argv[2];
const iRequiredRowCount = Number(process.argv[3]);
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8").split("\n")[0];

let aRows = [sInput];
const iRowSize = sInput.length;
let iSafeTileCount = sInput.split(".").length - 1;

for (let i = 0; i < iRequiredRowCount - 1; i++) {
    append_row();
}
console.log(aRows[aRows.length - 1]);
console.log(iSafeTileCount);

function append_row() {
    let sNewRow = "";
    let iNewRowNr = aRows.length;

    // Determine state of row above
    for (let i = 0; i < iRowSize; i++) {
        let bLeft = (i === 0 ? false : aRows[iNewRowNr - 1].charAt(i - 1) === "^");
        let bCenter = aRows[iNewRowNr - 1].charAt(i) === "^";
        let bRight = (i === iRowSize - 1 ? false : aRows[iNewRowNr - 1].charAt(i + 1) === "^");

        if (bLeft && bCenter && !bRight) { sNewRow += "^"; }
        else if (!bLeft && bCenter && bRight) { sNewRow += "^"; }
        else if (bLeft && !bCenter && !bRight) { sNewRow += "^"; }
        else if (!bLeft && !bCenter && bRight) { sNewRow += "^"; }
        else { sNewRow += "."; iSafeTileCount++; }
    }
    aRows.push(sNewRow);
}