const sInputFile = process.argv[2];
const fs = require("fs");
const aInstructions = fs.readFileSync(sInputFile, "utf8").split("\n");

const oRegister = {
    a: 0,
    b: 0,
    c: 1,// Now we start with a different c
    d: 0,
}

for (let iIndex = 0; iIndex < aInstructions.length; iIndex++) {
    const aChunks = aInstructions[iIndex].split(" ");
    switch (aChunks[0]) {
        case "inc":
            oRegister[aChunks[1]]++;
            break;
        case "dec":
            oRegister[aChunks[1]]--;
            break;
        case "cpy":
            if (isNaN(aChunks[1])) {
                oRegister[aChunks[2]] = oRegister[aChunks[1]];
            } else {
                oRegister[aChunks[2]] = Number(aChunks[1]);
            }
            break;
        case "jnz":
            if (oRegister[aChunks[1]] !== 0) {
                iIndex += Number(aChunks[2]) - 1;
            }
            break;
        default:
            console.log("Unknown command:", aChunks);
            break;
    }
}
console.log(oRegister);