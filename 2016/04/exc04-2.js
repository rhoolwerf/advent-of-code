const sInputFile = process.argv[2];
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n");

const stringToIdAndChecksum = /(\d+)\[([a-z]+)\]/;

aInput.forEach(sLine => {
    // Split the line into usable bits
    const aChunks = sLine.split("-");
    const [_, sId, sChecksum] = stringToIdAndChecksum.exec(aChunks.pop());
    const sEncryptedName = aChunks.join("");

    if (is_valid(aChunks.join(""), sChecksum)) {
        if (decrypt(aChunks.join("-"), Number(sId)).indexOf("north") >= 0) {
            console.log(sLine, decrypt(aChunks.join("-"), Number(sId)), Number(sId));
        }
    }
});

// Inspired by https://github.com/Twisol/advent-of-code/blob/master/2016/day4/main.js
function is_valid(sName, sChecksum) {

    // Calculate all occurrences of a character in the name
    const oOccurrences = {};
    for (sChar of sName) {
        oOccurrences[sChar] = (oOccurrences[sChar] || 0) + 1;
    }

    // Sort the characters by occurrence descending and alphabet ascending
    const aSorted = Object.keys(oOccurrences).sort((a, b) => {
        if (oOccurrences[a] < oOccurrences[b]) { return 1; }
        else if (oOccurrences[a] > oOccurrences[b]) { return -1; }
        else if (a > b) { return 1; }
        else if (a < b) { return -1; }
        else { return 0; }
    });

    return aSorted.slice(0, 5).join("") === sChecksum;
}

function decrypt(sName, iSectorId) {
    // For each character, get the character-code, do minus 97 (to get to 0 for a and 25 for z), add the SectorId, modulo 26 (for the alphabet length), add the 97 again to convert to character
    return sName.split("").map(x => x === "-" ? " " : String.fromCharCode((x.charCodeAt(0) - 97 + iSectorId) % 26 + 97)).join("");
}