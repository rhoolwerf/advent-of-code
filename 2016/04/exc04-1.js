const sInputFile = process.argv[2];
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n");

const stringToIdAndChecksum = /(\d+)\[([a-z]+)\]/;

let iSumSectorId = 0;
aInput.forEach(sLine => {
    // Split the line into usable bits
    const aChunks = sLine.split("-");
    const [_, sId, sChecksum] = stringToIdAndChecksum.exec(aChunks.pop());
    const sEncryptedName = aChunks.join("");

    if (is_valid(aChunks.join(""), sChecksum)) {
        iSumSectorId += Number(sId);
    }
});
console.log(iSumSectorId);

// Inspired by https://github.com/Twisol/advent-of-code/blob/master/2016/day4/main.js
function is_valid(sName, sChecksum) {

    // Calculate all occurrences of a character in the name
    const oOccurrences = {};
    for (sChar of sName) {
        oOccurrences[sChar] = (oOccurrences[sChar] || 0) + 1;
    }

    // Sort the characters by occurrence descending and alphabet ascending
    const aSorted = Object.keys(oOccurrences).sort((a, b) => {
        if (oOccurrences[a] < oOccurrences[b]) { return 1; }
        else if (oOccurrences[a] > oOccurrences[b]) { return -1; }
        else if (a > b) { return 1; }
        else if (a < b) { return -1; }
        else { return 0; }
    });

    return aSorted.slice(0, 5).join("") === sChecksum;
}