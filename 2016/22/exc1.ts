import { readFileSync } from "fs";

const REGEX = /node-x([0-9]+)-y([0-9]+)\s+([0-9]+)T\s+([0-9]+)T\s+([0-9]+)T\s+([0-9]+)/;

const files = readFileSync('input', 'utf8').split('\n').map((line) => {
    ///dev/grid/node-x0-y0     89T   65T    24T   73%
    if (REGEX.test(line)) {
        const [_, x, y, size, used, avail, perc] = (REGEX.exec(line) as RegExpExecArray).map(Number);
        return { x, y, size, used, avail, perc };
    } else { return undefined; }
}).filter((line) => line) as Array<{ x: number, y: number, used: number, avail: number, perc: number }>;

let viablePairs = 0;
for (let i = 0; i < files.length; i++) {
    // Node A is not empty (its Used is not zero)
    if (files[i].used === 0) { continue; }

    for (let j = 0; j < files.length; j++) {
        // Nodes A and B are not the same node
        if (i === j) { continue; }


        // The data on node A (its Used) would fit on node B (its Avail)
        if (files[i].used <= files[j].avail) {
            viablePairs++;
        }
    }
}

console.log(viablePairs);