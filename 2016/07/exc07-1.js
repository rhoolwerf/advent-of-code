const sInputFile = process.argv[2];
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n");

console.assert(has_tls_support("abba[mnop]qrst"));
console.assert(!has_tls_support("abcd[bddb]xyyx"));
console.assert(!has_tls_support("aaaa[qwer]tyui"));
console.assert(has_tls_support("ioxxoj[asdfgh]zxcvbn"));

let iValid = 0;
aInput.forEach(sLine=>{
    if (has_tls_support(sLine)){
        iValid++;
    }
});
console.log(iValid);

function has_tls_support(sInput) {
    let bOutsideBrackets = true;
    let aString = [];
    let bHasAbbaOutside = false;

    for (let i = 0; i < sInput.length; i++) {
        if (sInput.charAt(i) === "[") {
            bOutsideBrackets = false;
            aString = [];
        } else if (sInput.charAt(i) === "]") {
            bOutsideBrackets = true;
            aString = [];
        } else {
            aString.push(sInput.charAt((i)));
            if (has_abba(aString)) {
                if (bOutsideBrackets) {
                    bHasAbbaOutside = true;
                } else {
                    // Abba inside brackets? Definitely wrong!
                    return false;
                }
            }
        }
    }
    return bHasAbbaOutside;
}
function has_abba(aString) {
    if (aString.length < 4) {
        return false;
    } else {
        let sString = aString.slice(Math.max(aString.length - 4, 0)).join("").trim();
        if (sString[0] === sString[3] && sString[1] === sString[2] && sString[0] !== sString[1]) {
            return true;
        } else {
            return false;
        }
    }
}