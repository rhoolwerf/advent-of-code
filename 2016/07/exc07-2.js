const sInputFile = process.argv[2];
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n");

console.assert(has_ssl_support("aba[bab]xyz"));
console.assert(!has_ssl_support("xyx[xyx]xyx"));
console.assert(has_ssl_support("aaa[kek]eke"));
console.assert(has_ssl_support("zazbz[bzb]cdb"));

// console.log(has_ssl_support("xdsqxnovprgovwzkus[fmadbfsbqwzzrzrgdg]aeqornszgvbizdm"));

let iValid = 0;
aInput.forEach(sLine => {
    if (has_ssl_support(sLine)) {
        iValid++;
    }
});
console.log(iValid);

function has_ssl_support(sInput) {
    let bOutsideBrackets = true;
    let aOutsideABA = new Set();
    let aInsideCodes = new Set();

    let aString = [];
    let bHasABA = false;
    for (let i = 0; i < sInput.length; i++) {
        if (sInput.charAt(i) === "[") {
            bOutsideBrackets = false;
            aString = [];
        } else if (sInput.charAt(i) === "]") {
            bOutsideBrackets = true;
            aInsideCodes.add(aString.join(""));
            aString = [];
        } else {
            aString.push(sInput.charAt(i));
            let sTestString = aString.slice(Math.max(aString.length - 3, 0)).join("").trim();
            if (bOutsideBrackets && is_aba(sTestString)) {
                aOutsideABA.add(sTestString);
            }
        }
    }

    for (const sEntry of Array.from(aOutsideABA)) {
        let sTestString = sEntry[1] + sEntry[0] + sEntry[1];
        for (const sCode of Array.from(aInsideCodes)) {
            if (sCode.includes(sTestString)) {
                return true;
            }
        }
    }

    return false;
}

// function has_tls_support(sInput) {
//     let bOutsideBrackets = true;
//     let aString = [];
//     let bHasAbbaOutside = false;

//     for (let i = 0; i < sInput.length; i++) {
//         if (sInput.charAt(i) === "[") {
//             bOutsideBrackets = false;
//             aString = [];
//         } else if (sInput.charAt(i) === "]") {
//             bOutsideBrackets = true;
//             aString = [];
//         } else {
//             aString.push(sInput.charAt((i)));
//             if (has_abba(aString)) {
//                 if (bOutsideBrackets) {
//                     bHasAbbaOutside = true;
//                 } else {
//                     // Abba inside brackets? Definitely wrong!
//                     return false;
//                 }
//             }
//         }
//     }
//     return bHasAbbaOutside;
// }
function is_aba(sString) {
    if (sString.length < 3) {
        return false;
    } else {
        // let sString = aString.slice(Math.max(aString.length - 3, 0)).join("").trim();
        if (sString[0] === sString[2] && sString[0] !== sString[1]) {
            return true;
        } else {
            return false;
        }
    }
}