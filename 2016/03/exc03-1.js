const sInputFile = process.argv[2];
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n");

let iValid = 0;
aInput.forEach(sTriangle => {
    // For each line, match all numbers, map them to an actual number
    aEdges = sTriangle.match(/(\d+)/ig).map(Number);
    if ((aEdges[0] + aEdges[1]) > aEdges[2] && (aEdges[0] + aEdges[2]) > aEdges[1] && (aEdges[1] + aEdges[2] > aEdges[0])) {
        iValid++;
    }
});
console.log(iValid);