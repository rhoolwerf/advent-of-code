const sInputFile = process.argv[2];
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n");

let aTriangles = [];
for (let iRow = 0; iRow < aInput.length; iRow += 3) {
    aEdges1 = aInput[iRow + 0].match(/(\d+)/ig).map(Number);
    aEdges2 = aInput[iRow + 1].match(/(\d+)/ig).map(Number);
    aEdges3 = aInput[iRow + 2].match(/(\d+)/ig).map(Number);

    aTriangles.push([aEdges1[0], aEdges2[0], aEdges3[0]]);
    aTriangles.push([aEdges1[1], aEdges2[1], aEdges3[1]]);
    aTriangles.push([aEdges1[2], aEdges2[2], aEdges3[2]]);
}
let iValid = 0;
aTriangles.forEach(aEdges => {
    if ((aEdges[0] + aEdges[1]) > aEdges[2] && (aEdges[0] + aEdges[2]) > aEdges[1] && (aEdges[1] + aEdges[2] > aEdges[0])) {
        iValid++;
    }
});
console.log(iValid);