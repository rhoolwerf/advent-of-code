const sInputFile = process.argv[2];
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n");

let aCount = [];
aInput.forEach(sLine => {
    for (let iIndex = 0; iIndex < sLine.length; iIndex++) {
        if (aCount[iIndex] === undefined) { aCount[iIndex] = [] }
        aCount[iIndex][sLine.charAt(iIndex)] = aCount[iIndex][sLine.charAt(iIndex)] + 1 || 1;
    }
});

let aMessage = [];
aCount.forEach(aSubCount => {
    let aSortedCount = [];
    for (const aSubCountItem in aSubCount) {
        aSortedCount.push([aSubCountItem, aSubCount[aSubCountItem]]);
    }
    aSortedCount.sort((a, b) => a[1] - b[1]);
    aMessage.push(aSortedCount[0][0]);
});
console.log(aMessage.join(""));