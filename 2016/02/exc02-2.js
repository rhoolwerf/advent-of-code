const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8").split("\n").map(x => x.trim());

const oKeyPad = {
    0: { 2: "1" },
    1: { 1: "2", 2: "3", 3: "4" },
    2: { 0: "5", 1: "6", 2: "7", 3: "8", 4: "9" },
    3: { 1: "A", 2: "B", 3: "C" },
    4: { 2: "D" }
}

let iX = 0;
let iY = 2;

let aCode = [];
sInput.forEach(sLine => {
    for (let i = 0; i < sLine.length; i++) {
        switch (sLine.charAt(i)) {
            case "U":
                if (valid_move(iY - 1, iX)) {
                    iY--;
                }
                break;
            case "R":
                if (valid_move(iY, iX + 1)) {
                    iX++;
                }
                break;
            case "D":
                if (valid_move(iY + 1, iX)) {
                    iY++;
                }
                break;
            case "L":
                if (valid_move(iY, iX - 1)) {
                    iX--;
                }
                break;
        }
    }
    aCode.push(oKeyPad[iY][iX]);
});
console.log(aCode.join(""));

function valid_move(iTargetY, iTargetX) {
    return (oKeyPad[iTargetY] !== undefined) && (oKeyPad[iTargetY][iTargetX] !== undefined);
}
