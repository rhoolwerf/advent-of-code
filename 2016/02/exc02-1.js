const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8").split("\n").map(x => x.trim());

let iX = iY = 1;// Start in the middle of the grid
let aCode = [];
sInput.forEach(sLine => {
    for (let i = 0; i < sLine.length; i++) {
        switch (sLine.charAt(i)) {
            case "U":
                iY--;
                break;
            case "R":
                iX++;
                break;
            case "D":
                iY++;
                break;
            case "L":
                iX--;
                break;
        }

        if (iX < 0) { iX = 0; }
        else if (iX > 2) { iX = 2; }

        if (iY < 0) { iY = 0; }
        else if (iY > 2) { iY = 2; }
    }
    aCode.push(iY * 3 + iX + 1);
});
console.log(aCode.join(""));