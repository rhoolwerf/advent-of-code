function isValid(input: string): boolean {
    return false;
}

function test(input: string, expected: boolean): void {
    const actual = isValid(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

test('aaaaa-bbb-z-y-x-123[abxyz]', true);
test('a-b-c-d-e-f-g-h-987[abcde]', true);
test('not-a-real-room-404[oarel]', true);
test('totally-real-room-200[decoy]', false);
