import { readFileSync } from 'fs';
import { EOL } from 'os';

const steps = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((row) => row.split(''));

let y = 2;
let x = 0;

const grid: Array<Array<string>> = [
    [' ', ' ', '1', ' ', ' '],
    [' ', '2', '3', '4', ' '],
    ['5', '6', '7', '8', '9'],
    [' ', 'A', 'B', 'C', ' '],
    [' ', ' ', 'D', ' ', ' '],
];

const digits: Array<string> = [];
for (const stepsPerDigit of steps) {
    for (const step of stepsPerDigit) {
        if (step === 'L') {
            if (x > 0 && grid[y][x - 1] !== ' ') x--;
        } else if (step === 'R') {
            if (x < 4 && grid[y][x + 1] !== ' ') x++;
        } else if (step === 'U') {
            if (y > 0 && grid[y - 1][x] !== ' ') y--;
        } else if (step === 'D') {
            if (y < 4 && grid[y + 1][x] !== ' ') y++;
        }
    }

    digits.push(grid[y][x]);
}

console.log(digits.join(''));
