import { readFileSync } from 'fs';
import { EOL } from 'os';

const steps = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((row) => row.split(''));

let y = 1;
let x = 1;

const digits: Array<number> = [];
for (const digit of steps) {
    for (const step of digit) {
        if (step === 'L') x = Math.max(0, x - 1);
        if (step === 'U') y = Math.max(0, y - 1);
        if (step === 'R') x = Math.min(2, x + 1);
        if (step === 'D') y = Math.min(2, y + 1);
    }

    const nr = y * 3 + x + 1;
    console.log({ y, x, nr });
    digits.push(nr);
}

console.log(digits.join(''));
