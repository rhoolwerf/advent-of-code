import { readFileSync } from 'fs';

const NORTH = '^';
const EAST = '>';
const SOUTH = 'v';
const WEST = '<';

function find_distance(input: string): number {
    let facing = NORTH;
    let y = 0;
    let x = 0;

    for (const cmd of input.split(', ')) {
        const turn = cmd[0];
        const steps = Number(cmd.substring(1));

        if (facing === NORTH && turn === 'R') facing = EAST;
        else if (facing === NORTH && turn === 'L') facing = WEST;
        else if (facing === EAST && turn === 'R') facing = SOUTH;
        else if (facing === EAST && turn === 'L') facing = NORTH;
        else if (facing === SOUTH && turn === 'R') facing = WEST;
        else if (facing === SOUTH && turn === 'L') facing = EAST;
        else if (facing === WEST && turn === 'R') facing = NORTH;
        else if (facing === WEST && turn === 'L') facing = SOUTH;

        if (facing === NORTH) y -= steps;
        else if (facing === EAST) x += steps;
        else if (facing === SOUTH) y += steps;
        else if (facing === WEST) x -= steps;
    }

    return Math.abs(x) + Math.abs(y);
}

function test(input: string, expected: number): void {
    const actual = find_distance(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

test('R2, L3', 5);
test('R2, R2, R2', 2);
test('R5, L5, R5, R3', 12);

console.log(find_distance(readFileSync('./input', 'utf8')));
