import { readFileSync } from 'fs';

const NORTH = '^';
const EAST = '>';
const SOUTH = 'v';
const WEST = '<';

function key(y: number, x: number) {
    return [y, x].join(',');
}

function find_distance(input: string): number {
    let facing = NORTH;
    let y = 0;
    let x = 0;
    const visited = new Set<string>();

    for (const cmd of input.split(', ')) {
        const turn = cmd[0];
        const steps = Number(cmd.substring(1));

        if (facing === NORTH && turn === 'R') facing = EAST;
        else if (facing === NORTH && turn === 'L') facing = WEST;
        else if (facing === EAST && turn === 'R') facing = SOUTH;
        else if (facing === EAST && turn === 'L') facing = NORTH;
        else if (facing === SOUTH && turn === 'R') facing = WEST;
        else if (facing === SOUTH && turn === 'L') facing = EAST;
        else if (facing === WEST && turn === 'R') facing = NORTH;
        else if (facing === WEST && turn === 'L') facing = SOUTH;

        for (let i = 0; i < steps; i++) {
            if (facing === NORTH) y--;
            else if (facing === EAST) x++;
            else if (facing === SOUTH) y++;
            else if (facing === WEST) x--;

            if (visited.has(key(y, x))) {
                return Math.abs(x) + Math.abs(y);
            }
            visited.add(key(y, x));
        }
    }

    return Infinity;
}

function test(input: string, expected: number): void {
    const actual = find_distance(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

test('R8, R4, R4, R8', 4);

console.log(find_distance(readFileSync('./input', 'utf8')));
