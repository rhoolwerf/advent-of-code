import { readFileSync } from 'fs';
import { EOL } from 'os';

function isPossible(a: number, b: number, c: number): boolean {
    return a + b > c && a + c > b && b + c > a;
}

console.assert(!isPossible(5, 10, 25));

const input = readFileSync('./input', 'utf8')
    .split(EOL)
    .map((row) => row.split(' ').map(Number).filter(Boolean));

const possible = input.filter(([a, b, c]) => isPossible(a, b, c));
console.log(possible.length);
