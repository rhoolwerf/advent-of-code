import { readFileSync } from 'fs';
import { EOL } from 'os';

function isPossible(a: number, b: number, c: number): boolean {
    return a + b > c && a + c > b && b + c > a;
}

console.assert(!isPossible(5, 10, 25));

const input = readFileSync('./input', 'utf8')
    .split(EOL)
    .map((row) => row.split(' ').map(Number).filter(Boolean));

let possible = 0;
for (let i = 0; i < input.length; i += 3) {
    for (let j = 0; j < 3; j++) {
        const a = input[i][j];
        const b = input[i + 1][j];
        const c = input[i + 2][j];
        if (isPossible(a, b, c)) possible++;
    }
}

console.log(possible);
