import { readFileSync } from 'fs';
import { EOL } from 'os';

const rules = readFileSync(process.argv[2], 'utf8').split(EOL);

type Node = {
    id: string;
    leadsTo: Array<Node>;
    requires: Array<Node>;
};

const nodes: Array<Node> = [];

for (const rule of rules) {
    // Break the rule into usable bits
    const [_, prereq, node] = rule.match(/Step ([A-Z]+) must be finished before step ([A-Z]+) can begin/);

    // Find the 'from' node
    let from = nodes.find(({ id }) => id === prereq);
    if (!from) {
        from = {
            id: prereq,
            leadsTo: [],
            requires: [],
        };
        nodes.push(from);
    }

    // Find the 'to' node
    let to = nodes.find(({ id }) => id === node);
    if (!to) {
        to = {
            id: node,
            leadsTo: [],
            requires: [],
        };
        nodes.push(to);
    }

    // Update administration
    from.leadsTo.push(to);
    to.requires.push(from);
}

const needToProcess: Array<Node> = nodes.filter(({ requires }) => requires.length === 0);

const order: Array<Node> = [];

while (needToProcess.length > 0) {
    // Always alphabetically
    needToProcess.sort((a, b) => a.id.localeCompare(b.id));

    // Resolve the first node
    const node = needToProcess.shift() as Node;

    // Keep track of the order
    order.push(node);

    // Add any 'leadsTo' nodes that haven't been processed yet, but also have all requirements met
    node.leadsTo.forEach((possibleTo) => {
        // Validate if not yet processed before
        if (order.some(({ id }) => possibleTo.id === id)) return;

        // Validate all requirements have been met
        if (!possibleTo.requires.every(({ id }) => order.some((o) => o.id === id))) return;

        // All checks survived, add it
        needToProcess.push(possibleTo);
    });
}

console.log(order.map(({ id }) => id).join(''));
