import { readFileSync } from 'fs';
import { EOL } from 'os';

function run(input: Array<string>): number {
    let freq = 0;
    const history: Array<number> = [freq];

    for (let i = 0; true; i++) {
        freq += Number(input[i % input.length]);

        if (history.find((h) => h == freq) !== undefined) return freq;
        else history.push(freq);
    }

    return Infinity;
}

function test(input: string, expected: number): void {
    const actual = run(input.split(','));
    console.assert(expected === actual, '', { input, expected, actual });
}

test('+1, -1', 0);
test('+3, +3, +4, -2, -4', 10);
test('-6, +3, +8, +5, -6', 5);
test('+7, +7, -2, -7, -4', 14);

console.log(run(readFileSync('./input', 'utf8').split(EOL)));
