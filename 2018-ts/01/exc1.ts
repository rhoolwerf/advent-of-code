import { readFileSync } from 'fs';
import { sum } from '../util/util';
import { EOL } from 'os';

function run(input: Array<string>): number {
    return sum(input.map(Number));
}

function test(input: string, expected: number): void {
    const actual = run(input.split(','));
    console.assert(expected === actual, '', { input, expected, actual });
}

test('+1, +1, +1', 3);
test('+1, +1, -2', 0);
test('-1, -2, -3', -6);

console.log(run(readFileSync('./input', 'utf8').split(EOL)));
