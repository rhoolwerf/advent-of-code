import { readFileSync } from 'fs';
import { EOL } from 'os';

type Guard = { id: number; time_asleep: number; minutes_asleep: { [key: number]: number } };

const input = readFileSync(process.argv[2], 'utf8').split(EOL).sort();

const guards: Array<Guard> = [];

let guard: Guard | null = null;

for (let i = 0; i < input.length; i++) {
    const line = input[i];

    // If a guard starts a shift, get the reference
    if (line.endsWith('begins shift')) {
        const id = Number(line.split(' ')[3].substring(1));
        guard = guards.find((g) => g.id === id);

        // If not found, register a new one
        if (!guard) {
            guard = { id, time_asleep: 0, minutes_asleep: {} };
            guards.push(guard);
        }
    }

    // If he falls asleep, register when
    if (line.endsWith('falls asleep')) {
        const start = Number(line.split(' ')[1].split(':')[1].substring(0, 2));
        const end = Number(input[i + 1].split(' ')[1].split(':')[1].substring(0, 2));
        guard.time_asleep += end - start;

        for (let i = start; i < end; i++) {
            guard.minutes_asleep[i] = (guard.minutes_asleep[i] || 0) + 1;
        }
    }
}

let max = 0;
for (const { id, minutes_asleep } of guards) {
    for (const minute of Object.keys(minutes_asleep)) {
        if (minutes_asleep[minute] > max) {
            console.log('Guard', id, 'at minute', minute, 'times', minutes_asleep[minute], Number(minute) * id);
            max = minutes_asleep[minute];
        }
    }
}
