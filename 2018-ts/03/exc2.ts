import { readFileSync } from 'fs';
import { EOL } from 'os';

const fabric = new Map<string, number>();
const key = (y: number, x: number): string => [y, x].join(',');

const gifts = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) => {
        const [id, startX, startY, width, height] = [...line.matchAll(/\d+/g)].map(Number);
        return { id, startX, startY, width, height };
    });

// Parse gifts unto fabric
gifts.forEach(({ startX, startY, width, height }) => {
    for (let x = startX; x < startX + width; x++) {
        for (let y = startY; y < startY + height; y++) {
            fabric.set(key(y, x), (fabric.get(key(y, x)) || 0) + 1);
        }
    }
});

// For each gift, try to figure out if they're the only one claiming the area
for (const { id, startX, startY, width, height } of gifts) {
    let unique = true;
    for (let x = startX; x < startX + width; x++) {
        for (let y = startY; y < startY + height; y++) {
            if (fabric.get(key(y, x)) > 1) unique = false;
        }
    }
    if (unique) {
        console.log('Gift', { id }, 'has no overlap');
    }
}
