import { readFileSync } from 'fs';
import { EOL } from 'os';

const TREE = '|';
const OPEN = '.';
const LUMBERYARD = '#';

let grid = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((row) => row.split(''));

const states: Array<string> = [grid_to_string()];
let repeatFound = false;
for (let i = 1; i <= 1000000000; i++) {
    process_grid();

    if (!repeatFound) {
        const newState = grid_to_string();
        const indexFound = states.indexOf(newState);
        if (indexFound >= 0) {
            repeatFound = true;
            const loopLength = i - indexFound;
            console.log('Repeat detected:', { i, loopLength });

            while (i < 1000000000) i += loopLength;
            i -= loopLength;
        }
        states.push(newState);
    }
}

const trees = grid.flat().filter((c) => c === TREE).length;
const lumberyards = grid.flat().filter((c) => c === LUMBERYARD).length;
const totalResourceValue = trees * lumberyards;
console.log({ trees, lumberyards, totalResourceValue });

function display_grid() {
    console.log(grid_to_string());
}

function grid_to_string() {
    return grid.map((row) => row.join('')).join(EOL);
}

function process_grid() {
    const newGrid: typeof grid = JSON.parse(JSON.stringify(grid));
    for (let y = 0; y < grid.length; y++) {
        for (let x = 0; x < grid[y].length; x++) {
            // Get current
            const current = get_location(y, x);

            // Count surrounding
            const { trees, lumberyards } = count_surrounding(y, x);

            if (current === OPEN && trees >= 3) newGrid[y][x] = TREE;
            if (current === TREE && lumberyards >= 3) newGrid[y][x] = LUMBERYARD;
            if (current === LUMBERYARD) {
                if (lumberyards >= 1 && trees >= 1) {
                    // Remain the same
                    newGrid[y][x] = LUMBERYARD;
                } else {
                    newGrid[y][x] = OPEN;
                }
            }
        }
    }
    grid = newGrid;
}

function count_surrounding(y: number, x: number): { trees: number; lumberyards: number; open: number } {
    let trees = 0;
    let lumberyards = 0;
    let open = 0;

    for (let j = y - 1; j <= y + 1; j++) {
        for (let i = x - 1; i <= x + 1; i++) {
            if (y === j && x === i) continue;

            const pos = get_location(j, i);
            if (pos === TREE) trees++;
            else if (pos === OPEN) open++;
            else if (pos === LUMBERYARD) lumberyards++;
            else {
                console.error('What the hell is:', pos);
                process.exit(-1);
            }
        }
    }

    if (trees + lumberyards + open !== 8) {
        console.error('How the hell did we get this?', { y, x, trees, open, lumberyards });
        process.exit(-1);
    }

    return { trees, lumberyards, open };
}

function get_location(y: number, x: number): string {
    if (!grid[y]) return OPEN;
    if (!grid[y][x]) return OPEN;
    return grid[y][x];
}
