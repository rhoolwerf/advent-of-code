import { readFileSync } from 'fs';

function run(input: string): number {
    const polymer = input.split('');

    // Keep running until we've stopped reducing
    while (true) {
        const startLength = polymer.length;
        for (let i = 0; i < polymer.length - 1; i++) {
            const p1 = polymer[i];
            const p2 = polymer[i + 1];

            if (p1.toLocaleLowerCase() === p2.toLocaleLowerCase() && p1 !== p2) {
                // Match found
                polymer.splice(i, 2);
                break;
            }
        }

        // If we've reduced nothing, we can exit
        if (startLength === polymer.length) return polymer.length;
    }
}

function test(input: string, expected: number): void {
    const actual = run(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

test('aA', 0);
test('abBA', 0);
test('abAB', 4);
test('aabAAB', 6);
test('dabAcCaCBAcCcaDA', 10);

console.log(run(readFileSync('./input', 'utf8')));
