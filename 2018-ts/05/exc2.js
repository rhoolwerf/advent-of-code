"use strict";
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_1 = require("fs");
function run(input) {
    var polymer = input.split('');
    // Keep running until we've stopped reducing
    while (true) {
        var startLength = polymer.length;
        for (var i = 0; i < polymer.length - 1; i++) {
            var p1 = polymer[i];
            var p2 = polymer[i + 1];
            if (p1.toLocaleLowerCase() === p2.toLocaleLowerCase() && p1 !== p2) {
                // Match found
                polymer.splice(i, 2);
                break;
            }
        }
        // If we've reduced nothing, we can exit
        if (startLength === polymer.length)
            return polymer.length;
    }
}
var start = (0, fs_1.readFileSync)(process.argv[2], 'utf8');
// Find all possible units
var units = new Set();
for (var i = 0; i < start.length; i++) {
    var char = start[i];
    if (char === char.toLocaleLowerCase()) {
        units.add(char);
    }
}
// For each units, remove all lower and capital cases, then reduce
var mapped = __spreadArray([], units.values(), true);
var result = [];
var _loop_1 = function (i) {
    var unit = mapped[i];
    var perc = (Number(i) / mapped.length).toFixed(2);
    console.log(i.padStart(String(mapped.length).length), unit, perc);
    // Filter out what we don't want to have
    var filtered = start
        .split('')
        .filter(function (c) { return c.toLocaleLowerCase() !== unit; })
        .join('');
    // Start reducing into a clean polymer
    console.time('Took');
    var reduced = run(filtered);
    console.timeEnd('Took');
    result.push(reduced);
};
for (var i in mapped) {
    _loop_1(i);
}
console.log(Math.min.apply(Math, result));
