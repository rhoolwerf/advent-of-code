import { readFileSync } from 'fs';

function run(input: string): number {
    const polymer = input.split('');

    // Keep running until we've stopped reducing
    while (true) {
        const startLength = polymer.length;
        for (let i = 0; i < polymer.length - 1; i++) {
            const p1 = polymer[i];
            const p2 = polymer[i + 1];

            if (p1.toLocaleLowerCase() === p2.toLocaleLowerCase() && p1 !== p2) {
                // Match found
                polymer.splice(i, 2);
                break;
            }
        }

        // If we've reduced nothing, we can exit
        if (startLength === polymer.length) return polymer.length;
    }
}

const start = readFileSync(process.argv[2], 'utf8');

// Find all possible units
const units = new Set<string>();
for (let i = 0; i < start.length; i++) {
    const char = start[i];
    if (char === char.toLocaleLowerCase()) {
        units.add(char);
    }
}

// For each units, remove all lower and capital cases, then reduce
const mapped = [...units.values()];
const result: Array<number> = [];
for (const i in mapped) {
    const unit = mapped[i];
    const perc = (Number(i) / mapped.length).toFixed(2);
    console.log(i.padStart(String(mapped.length).length), unit, perc);

    // Filter out what we don't want to have
    const filtered = start
        .split('')
        .filter((c) => c.toLocaleLowerCase() !== unit)
        .join('');

    // Start reducing into a clean polymer
    console.time('Took');
    const reduced = run(filtered);
    console.timeEnd('Took');

    result.push(reduced);
}

console.log(Math.min(...result));
