function calc_cell_power_level(y: number, x: number, serial: number): number {
    // Find the fuel cell's rack ID, which is its X coordinate plus 10.
    const rackID = x + 10;

    // Begin with a power level of the rack ID times the Y coordinate.
    let powerLevel = rackID * y;

    // Increase the power level by the value of the grid serial number.
    powerLevel += serial;

    // Set the power level to itself multiplied by the rack ID.
    powerLevel *= rackID;

    // Keep only the hundreds digit of the power level
    // (so 12345 becomes 3; numbers with no hundreds digit become 0).
    powerLevel = Number(String(powerLevel).charAt(String(powerLevel).length - 3)) || 0;

    // Subtract 5 from the power level.
    return powerLevel - 5;
}

const serial = 9435;
const grid = Array.from({ length: 300 }, (_, y) =>
    Array.from({ length: 300 }, (_, x) => calc_cell_power_level(y, x, serial))
);

const largest = { y: Infinity, x: Infinity, size: Infinity, powerLeveL: 0 };
for (let size = 1; size < 300; size++) {
    if (size % 30 === 0) {
        console.log('Progress:', size, `(${(size / 300) * 100}%)`);
    }
    for (let y = 0; y < 300 - size; y++) {
        for (let x = 0; x < 300 - size; x++) {
            // Calc window
            let powerLevel = 0;

            for (let j = y; j < y + size; j++) {
                for (let i = x; i < x + size; i++) {
                    powerLevel += grid[j][i];
                }
            }

            if (powerLevel > largest.powerLeveL) {
                largest.powerLeveL = powerLevel;
                largest.y = y;
                largest.x = x;
                largest.size = size;
            }
        }
    }
}
console.log({ largest }, [largest.x, largest.y, largest.size].join(','));
