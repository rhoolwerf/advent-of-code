const GRID_SIZE = 300;
const WINDIW_SIZE = 3;

function calc_cell_power_level(y: number, x: number, serial: number): number {
    // Find the fuel cell's rack ID, which is its X coordinate plus 10.
    const rackID = x + 10;

    // Begin with a power level of the rack ID times the Y coordinate.
    let powerLevel = rackID * y;

    // Increase the power level by the value of the grid serial number.
    powerLevel += serial;

    // Set the power level to itself multiplied by the rack ID.
    powerLevel *= rackID;

    // Keep only the hundreds digit of the power level
    // (so 12345 becomes 3; numbers with no hundreds digit become 0).
    powerLevel = Number(String(powerLevel).charAt(String(powerLevel).length - 3)) || 0;

    // Subtract 5 from the power level.
    return powerLevel - 5;
}

function test_calc_cell_power_level(y: number, x: number, serial: number, expected: number): void {
    const actual = calc_cell_power_level(y, x, serial);
    console.assert(expected === actual, '', { y, x, serial, expected, actual });
}

test_calc_cell_power_level(5, 3, 8, 4);
test_calc_cell_power_level(79, 122, 57, -5);
test_calc_cell_power_level(196, 217, 39, 0);
test_calc_cell_power_level(153, 101, 71, 4);

function calc_area_power_level(y: number, x: number, serial: number): number {
    let sum = 0;
    for (let j = y; j < y + WINDIW_SIZE; j++) {
        for (let i = x; i < x + WINDIW_SIZE; i++) {
            sum += calc_cell_power_level(j, i, serial);
        }
    }
    return sum;
}

function test_area_cell_power_level(y: number, x: number, serial: number, expected: number): void {
    const actual = calc_area_power_level(y, x, serial);
    console.assert(expected === actual, '', { y, x, serial, expected, actual });
}

test_area_cell_power_level(45, 33, 18, 29);
test_area_cell_power_level(61, 21, 42, 30);

function find_highest_power_level(serial: number): { y: number; x: number; powerLevel: number } {
    const largest = { y: Infinity, x: Infinity, powerLevel: 0 };
    for (let y = 0; y < GRID_SIZE - WINDIW_SIZE; y++) {
        for (let x = 0; x < GRID_SIZE - WINDIW_SIZE; x++) {
            const powerLevel = calc_area_power_level(y, x, serial);
            if (powerLevel > largest.powerLevel) {
                largest.powerLevel = powerLevel;
                largest.y = y;
                largest.x = x;
            }
        }
    }

    return largest;
}

function test_find_highest_power_level(serial: number, expected: { y: number; x: number; powerLevel: number }) {
    const actual = find_highest_power_level(serial);
    console.assert(expected.powerLevel === actual.powerLevel, '', { serial, expected, actual });
    console.assert(expected.y === actual.y, '', { serial, expected, actual });
    console.assert(expected.x === actual.x, '', { serial, expected, actual });
}

test_find_highest_power_level(18, { y: 45, x: 33, powerLevel: 29 });
test_find_highest_power_level(42, { y: 61, x: 21, powerLevel: 30 });

const answer = find_highest_power_level(9435);
console.log({ answer, output: `${answer.x},${answer.y}` });
