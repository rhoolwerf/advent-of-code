import { readFileSync } from 'fs';
import { EOL } from 'os';

const boxes = readFileSync(process.argv[2], 'utf8').split(EOL);

for (let i = 0; i < boxes.length; i++) {
    for (let j = i + 1; j < boxes.length; j++) {
        const equal: Array<string> = [];
        for (let x = 0; x < boxes[i].length; x++) {
            if (boxes[i][x] === boxes[j][x]) {
                equal.push(boxes[i][x]);
            }
        }

        if (equal.length === boxes[i].length - 1) {
            console.log('Found:', { left: boxes[i], right: boxes[j], remainder: equal.join('') });
        }
    }
}
