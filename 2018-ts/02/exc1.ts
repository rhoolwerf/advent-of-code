import { readFileSync } from 'fs';
import { EOL } from 'os';

type Box = {
    data: string;
    counts: { [key: string]: number };
};

const boxes: Array<Box> = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((data) => {
        return {
            data,
            counts: data.split('').reduce(
                (carry, char) => {
                    carry[char] = (carry[char] || 0) + 1;
                    return carry;
                },
                {} as { [key: string]: number }
            ),
        };
    });

const countThree = boxes.filter(({ counts }) => [...Object.values(counts)].some((c) => c === 3)).length;
const countTwo = boxes.filter(({ counts }) => [...Object.values(counts)].some((c) => c === 2)).length;

console.log({ countThree, countTwo, mul: countThree * countTwo });
