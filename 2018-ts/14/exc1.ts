const recipes = [3, 7];
let elf1 = 0;
let elf2 = 1;

function get_recipe_score(target: number): string {
    // Calculate recipes as far as needed
    for (let i = 1; recipes.length < target + 10; i++) {
        // Add the two current recipe scores together, make it a string representation,
        // add each individual digit as a new recipe score
        recipes.push(
            ...String(recipes[elf1] + recipes[elf2])
                .split('')
                .map(Number)
        );

        elf1 = (elf1 + recipes[elf1] + 1) % recipes.length;
        elf2 = (elf2 + recipes[elf2] + 1) % recipes.length;
    }

    return recipes.slice(target, target + 10).join('');
}

function test(input: number, expected: string) {
    const actual = get_recipe_score(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

function time(input: number, expected: string) {
    const msg = `Timing ${JSON.stringify({ input, expected })}`;
    console.time(msg);
    test(input, expected);
    console.timeEnd(msg);
}

time(9, '5158916779');
time(5, '0124515891');
time(18, '9251071085');
time(2018, '5941429882');
console.time('Puzzle input:');
console.log(get_recipe_score(846601));
console.timeEnd('Puzzle input:');
