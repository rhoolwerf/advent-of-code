export function sum(input: Array<number>): number {
    return input.reduce((acc, val) => acc + val, 0);
}

export async function delay(duration: number = 500) {
    return await new Promise((resolve) => setTimeout(() => resolve(void 0), duration));
}
