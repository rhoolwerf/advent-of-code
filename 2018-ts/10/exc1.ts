import { readFileSync } from 'fs';
import { EOL } from 'os';

type Grid = Array<Array<string>>;

type Point = { posY: number; posX: number; velY: number; velX: number };
type Points = Array<Point>;

const data = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((point) => [...point.matchAll(/-?\d+/g)].map(([match]) => match).map(Number))
    .map(([posX, posY, velX, velY]) => ({ posX, posY, velX, velY }));

function display(points: Points) {
    // Get bounding boxes
    const minY = Math.min(...points.map(({ posY }) => posY));
    const maxY = Math.max(...points.map(({ posY }) => posY));
    const minX = Math.min(...points.map(({ posX }) => posX));
    const maxX = Math.max(...points.map(({ posX }) => posX));

    // For each coordinate, render either space or point
    for (let y = minY; y <= maxY; y++) {
        const row: Array<string> = [];
        for (let x = minX; x <= maxX; x++) {
            const hasPoint = points.some(({ posY, posX }) => posY === y && posX === x);
            row.push(hasPoint ? '#' : '.');
        }
        console.log(row.join(''));
    }
}

function render(): { area: number; points: Points } {
    // Get bounding boxes
    const minY = Math.min(...data.map(({ posY }) => posY));
    const maxY = Math.max(...data.map(({ posY }) => posY));
    const minX = Math.min(...data.map(({ posX }) => posX));
    const maxX = Math.max(...data.map(({ posX }) => posX));

    const area = (maxY - minY) * (maxX - minX);

    return { area, points: JSON.parse(JSON.stringify(data)) };
}

console.time('Rendering...');
const smallest: { area: number; points: Points; second: number } = { area: Infinity, points: [], second: -Infinity };
for (let i = 0; i < 20000; i++) {
    data.forEach((p) => {
        p.posY += p.velY;
        p.posX += p.velX;
    });

    const { area, points } = render();
    if (area < smallest.area) {
        smallest.area = area;
        smallest.points = points;
        smallest.second = i;
    }
}
console.timeEnd('Rendering...');

display(smallest.points);
