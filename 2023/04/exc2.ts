import { readFileSync } from 'fs';
import { EOL } from 'os';

const cards = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) =>
        line
            .split(/:|\|/)
            .map((chunk) => Array.from(chunk.matchAll(new RegExp(/((?:\d+)+),?/, 'g'))).map(([nr]) => Number(nr)))
    )
    .map(([[id], winning, attempt]) => ({ id, winning, attempt }))
    .map((card) => ({
        ...card,
        overlap: card.attempt.filter((nr) => card.winning.includes(nr)),
    }))
    .map((card) => ({
        ...card,
        score: card.overlap.length,
        count: 1,
    }));

for (const i in cards) {
    const card = cards[i];
    for (let j = 1; j <= card.score; j++) {
        let subsequent = cards.find(({ id }) => card.id + j === id);
        if (subsequent) {
            subsequent.count += card.count;
        }
    }
}

console.log(cards.reduce((sum, { count }) => sum + count, 0));
