import { readFileSync } from 'fs';
import { EOL } from 'os';

const cards = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) =>
        line
            .split(/:|\|/)
            .map((chunk) => Array.from(chunk.matchAll(new RegExp(/((?:\d+)+),?/, 'g'))).map(([nr]) => Number(nr)))
    )
    .map(([[id], winning, attempt]) => ({ id, winning, attempt }))
    .map((card) => ({
        ...card,
        overlap: card.attempt.filter((nr) => card.winning.includes(nr)),
    }))
    .map((card) => ({
        ...card,
        score: card.overlap.length === 0 ? 0 : Math.pow(2, card.overlap.length - 1),
    }));

console.log(cards.reduce((sum, { score }) => sum + score, 0));
