import { readFileSync } from "fs";
import { EOL } from "os";

const input = readFileSync(process.argv[2], 'utf8').split(EOL).map((row) => row.split(' ').map(Number));

console.log(input.map((row) => processRow(interpretRow(row))).map(([first]) => first).reduce((sum, val) => sum + val, 0))

function interpretRow(row: Array<number>): Array<number> {
    const rows = [row];

    while (new Set(rows[rows.length - 1]).size > 1) {
        rows.push(rows[rows.length - 1].reduce((newRow, val, index, array) => {
            if (index < rows[rows.length - 1].length - 1) {
                newRow.push(array[index + 1] - val);
            }
            return newRow;
        }, [] as Array<number>))
    }

    return rows.map((row) => row.pop() as number);
}

function processRow(data: Array<number>): Array<number> {
    const row = Array.from(data);
    for (let i = row.length - 1; i > 0; i--) {
        row[i - 1] += row[i];
    }

    return row;
}