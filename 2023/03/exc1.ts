import { readFileSync } from 'fs';
import { EOL } from 'os';

const grid: Array<Array<string>> = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) => line.split(''));

// console.log(grid);

let sum = 0;
for (let y = 0; y < grid.length; y++) {
    for (let x = 0; x < grid[y].length; x++) {
        // Are we a digit?
        if (!isNaN(Number(grid[y][x]))) {
            let symbolFound = false;

            // For current and each following digit, test if we're touching anything
            const digits: Array<number> = [];
            for (; !isNaN(Number(grid[y][x])); x++) {
                digits.push(Number(grid[y][x]));
                // console.log({ y, x, c: grid[y][x] });
                for (let y1 = y - 1; y1 <= y + 1 && !symbolFound; y1++) {
                    for (let x1 = x - 1; x1 <= x + 1 && !symbolFound; x1++) {
                        if (grid[y1] && grid[y1][x1] && grid[y1][x1] !== '.' && isNaN(Number(grid[y1][x1]))) {
                            // console.log('- Found', grid[y1][x1], 'at', y1, x1);
                            symbolFound = true;
                        }
                    }
                }
            }

            if (symbolFound) sum += Number(digits.join(''));
        }
    }
}

console.log(sum);
