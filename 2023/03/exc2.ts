import { readFileSync } from 'fs';
import { EOL } from 'os';

const grid: Array<Array<string>> = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) => line.split(''));

// console.log(grid);

const symbolsTouchingDigits = new Map<string, Array<number>>();

for (let y = 0; y < grid.length; y++) {
    for (let x = 0; x < grid[y].length; x++) {
        // Are we a digit?
        if (!isNaN(Number(grid[y][x]))) {
            let symbolFound: string = '';

            // For current and each following digit, test if we're touching anything
            const digits: Array<number> = [];
            for (; !isNaN(Number(grid[y][x])); x++) {
                digits.push(Number(grid[y][x]));

                // console.log({ y, x, c: grid[y][x] });
                for (let y1 = y - 1; y1 <= y + 1 && !symbolFound; y1++) {
                    for (let x1 = x - 1; x1 <= x + 1 && !symbolFound; x1++) {
                        if (grid[y1] && grid[y1][x1] && grid[y1][x1] !== '.' && isNaN(Number(grid[y1][x1]))) {
                            symbolFound = [y1, x1].join(',');
                        }
                    }
                }
            }

            if (symbolFound) {
                const nr = Number(digits.join(''));
                if (!symbolsTouchingDigits.has(symbolFound)) {
                    symbolsTouchingDigits.set(symbolFound, [nr]);
                } else {
                    symbolsTouchingDigits.get(symbolFound)?.push(nr);
                }
            }
        }
    }
}

console.log(
    Array.from(symbolsTouchingDigits.keys())
        .filter((k) => (symbolsTouchingDigits.get(k) || []).length > 1)
        .map((k) => symbolsTouchingDigits.get(k))
        .map((gear) => (gear || []).reduce((sum, val) => sum * val, 1))
        .reduce((sum, val) => sum + val)
);
