import { readFileSync } from 'fs';
import { EOL } from 'os';

const [time, distance] = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) => line.split(' ').filter(Number).join(''))
    .map(Number);

let wins = 0;
for (let index = 0; index < time; index++) {
    if (index * (time - index) > distance) wins++;
}

console.log({ time, distance, wins });
