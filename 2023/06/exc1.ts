import { readFileSync } from 'fs';
import { EOL } from 'os';

type Race = { time: number; distance: number };

const races = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) =>
        Array.from(line.matchAll(new RegExp(/(?:(\d+)+)/, 'g')))
            .map(([match]) => match)
            .map(Number)
    )
    .reduce(
        (obj, line, index, array) =>
            index === 0 ? line.map((time, offset) => ({ time, distance: array[1][index + offset] })) : obj,
        [] as Array<Race>
    );

    const results = races.map(
    ({ time, distance }) =>
        Array.from({ length: time }, (_, index) => ({
            time,
            distance,
            distanceTravelled: index * (time - index),
        })).filter(({ distanceTravelled, distance }) => distanceTravelled > distance).length
);

const score = results.reduce((multiplied, val) => val * multiplied, 1);

console.log(score);
