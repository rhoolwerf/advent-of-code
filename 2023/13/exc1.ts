import { readFileSync } from 'fs';
import { EOL } from 'os';

const input = readFileSync(process.argv[2], 'utf8')
    .split(`${EOL}${EOL}`)
    .map((row) => row.split(EOL));

const sum = input.reduce((sum, grid) => {
    // Try to find a column where it splits
    for (let x = 0; x < grid[0].length - 1; x++) {
        let mirror = true;

        for (let x1 = x, x2 = x + 1; x1 >= 0 && x2 < grid[0].length && mirror; x1--, x2++) {
            const left = grid
                .reduce((values, row) => {
                    values.push(row[x1]);
                    return values;
                }, [] as Array<string>)
                .join('');
            const right = grid
                .reduce((values, row) => {
                    values.push(row[x2]);
                    return values;
                }, [] as Array<string>)
                .join('');

            if (left !== right) {
                mirror = false;
            }
        }
        if (mirror) {
            console.log('Mirror found at column', { x: x + 1 });
            return sum + x + 1;
        }
    }

    // If we reach here, we need to search by rows
    for (let y = 0; y < grid.length - 1; y++) {
        let mirror = true;

        for (let y1 = y, y2 = y + 1; y1 >= 0 && y2 < grid.length && mirror; y1--, y2++) {
            const left = grid[y1];
            const right = grid[y2];

            if (left !== right) {
                mirror = false;
            }
        }

        if (mirror) {
            console.log('Mirror found at row', { y: y + 1 });
            return sum + (y + 1) * 100;
        }
    }

    return Infinity;
}, 0);

console.log({ sum });
