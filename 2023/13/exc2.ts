import { readFileSync } from 'fs';
import { EOL } from 'os';

const input = readFileSync(process.argv[2], 'utf8')
    .split(`${EOL}${EOL}`)
    .map((row) => row.split(EOL));

const sum = input.reduce((sum, grid) => {
    const { y: originalY, x: originalX } = getMirrorPosition(grid);

    for (let y = 0; y < grid.length; y++) {
        for (let x = 0; x < grid[y].length; x++) {
            const copy = JSON.parse(JSON.stringify(grid));
            copy[y] = [copy[y].substring(0, x), copy[y][x] === '#' ? '.' : '#', copy[y].substring(x + 1)].join('');

            const { y: mirrorY, x: mirrorX } = getMirrorPosition(copy, originalY, originalX);
            if (mirrorY !== undefined || mirrorX !== undefined) {
                // console.log('Mirror found!', { mirrorY, mirrorX });
                return sum + (mirrorX === undefined ? (Number(mirrorY) + 1) * 100 : Number(mirrorX) + 1);
            }
        }
    }

    return Infinity;
}, 0);

console.log({ sum });

function getMirrorPosition(grid: Array<string>, originalY?: number, originalX?: number): { y?: number; x?: number } {
    // Try to find a column where it splits
    for (let x = 0; x < grid[0].length - 1; x++) {
        let mirror = true;

        for (let x1 = x, x2 = x + 1; x1 >= 0 && x2 < grid[0].length && mirror; x1--, x2++) {
            const left = grid
                .reduce((values, row) => {
                    values.push(row[x1]);
                    return values;
                }, [] as Array<string>)
                .join('');
            const right = grid
                .reduce((values, row) => {
                    values.push(row[x2]);
                    return values;
                }, [] as Array<string>)
                .join('');

            if (left !== right) {
                mirror = false;
            }
        }
        if (mirror) {
            // We found a mirror, but is it a new one?
            if (x === originalX) {
                // Already knew this one, so skip it
            } else {
                // Woop woop new one, return!
                return { x };
            }
        }
    }

    // If we reach here, we need to search by rows
    for (let y = 0; y < grid.length - 1; y++) {
        let mirror = true;

        for (let y1 = y, y2 = y + 1; y1 >= 0 && y2 < grid.length && mirror; y1--, y2++) {
            const left = grid[y1];
            const right = grid[y2];

            if (left !== right) {
                mirror = false;
            }
        }

        if (mirror) {
            // We found a mirror, but is it a new one?
            if (y === originalY) {
                // Duplicate, never mind...
            } else {
                return { y };
            }
        }
    }

    return {};
}
