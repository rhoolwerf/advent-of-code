import { readFileSync } from 'fs';
import { EOL } from 'os';

const games = readFileSync(process.argv[2], 'utf8').split(EOL);

const max = {
    red: 12,
    green: 13,
    blue: 14,
};

let sum = 0;
for (const game of games) {
    const id = Number(game.split(':')[0].split(' ')[1]);

    const grabs = game
        .split(': ')[1]
        .split('; ')
        .map((grab) => grab.split(', '));

    let valid = true;
    for (const grab of grabs) {
        for (const hand of grab) {
            const [number, color] = hand.split(' ');
            if (Number(number) > max[color as keyof typeof max]) {
                // console.log(number, color, 'exceeding max of', max[color as keyof typeof max]);
                valid = false;
            }
        }
    }

    if (valid) sum += id;
    // console.log({ id, valid });
}

console.log(sum);
