import { readFileSync } from 'fs';
import { EOL } from 'os';

const games = readFileSync(process.argv[2], 'utf8').split(EOL);

let sum = 0;
for (const game of games) {
    const id = Number(game.split(':')[0].split(' ')[1]);

    const grabs = game
        .split(': ')[1]
        .split('; ')
        .map((grab) => grab.split(', '));

    let minRequired = {
        red: 0,
        green: 0,
        blue: 0,
    };
    for (const grab of grabs) {
        for (const hand of grab) {
            const nr = Number(hand.split(' ')[0]);
            const color = hand.split(' ')[1] as keyof typeof minRequired;
            minRequired[color] = Math.max(minRequired[color], nr);
        }
    }
    const power = minRequired.red * minRequired.green * minRequired.blue;
    console.log({ id, minRequired, power });

    sum += power;
}

console.log(sum);
