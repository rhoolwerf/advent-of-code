import { Worker } from 'worker_threads';

console.log('Main thread: startup');
const worker = new Worker('./worker.js', {
    workerData: {
        value: 15,
        path: './worker.ts',
    },
});

worker.on('message', (result) => {
    console.log('Main thread', 'received', result);
});
