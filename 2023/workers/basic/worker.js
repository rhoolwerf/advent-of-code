const path = require('path');
const { workerData } = require('worker_threads');

console.log('worker.js received:', workerData);
require('ts-node').register();
require(path.resolve(__dirname, workerData.path));
