import { Worker, isMainThread, parentPort, workerData } from 'worker_threads';

type Data = {
    msg: string;
    nr: number;
};

if (isMainThread) {
    const w = new Worker('../wrapper.js', { workerData: { path: __filename, msg: 'Hello World!', nr: 12 } });
    w.on('message', (msg) => console.log('Main thread received:', { msg }));
} else {
    const data = workerData as Data;
    console.log('Client received:', data.nr);
    parentPort?.postMessage({ test: 'Message!', number: 42 });
}
