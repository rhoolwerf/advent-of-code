import { readFileSync } from 'fs';
import { EOL } from 'os';

const card2value = {
    A: 14,
    K: 13,
    Q: 12,
    T: 10,
    '9': 9,
    '8': 8,
    '7': 7,
    '6': 6,
    '5': 5,
    '4': 4,
    '3': 3,
    '2': 2,
    J: 1, // Joker is always considered the weakest card
};

enum HandType {
    FiveOfAKind = 'FiveOfAKind',
    FourOfAKind = 'FourOfAKind',
    FullHouse = 'FullHouse',
    ThreeOfAKind = 'ThreeOfAKind',
    TwoPair = 'TwoPair',
    OnePair = 'OnePair',
    HighCard = 'HighCard',
}

const handtype2value = {
    [HandType.FiveOfAKind]: 7,
    [HandType.FourOfAKind]: 6,
    [HandType.FullHouse]: 5,
    [HandType.ThreeOfAKind]: 4,
    [HandType.TwoPair]: 3,
    [HandType.OnePair]: 2,
    [HandType.HighCard]: 1,
};

type CardCount = Array<{ label: keyof typeof card2value; count: number }>;

class Hand {
    static getBestPossibleHandType(cards: string): HandType {
        // Determine all possible permutations
        const split = cards.split('');
        const c0 = split[0] === 'J' ? Object.keys(card2value) : [split[0]];
        const c1 = split[1] === 'J' ? Object.keys(card2value) : [split[1]];
        const c2 = split[2] === 'J' ? Object.keys(card2value) : [split[2]];
        const c3 = split[3] === 'J' ? Object.keys(card2value) : [split[3]];
        const c4 = split[4] === 'J' ? Object.keys(card2value) : [split[4]];

        const permutations = c0.flatMap((c0c) =>
            c1.flatMap((c1c) =>
                c2.flatMap((c2c) => c3.flatMap((c3c) => c4.flatMap((c4c) => c0c + c1c + c2c + c3c + c4c)))
            )
        );

        const handTypes = permutations
            .map((p) => Hand.calcHandType(p))
            .sort((a, b) => handtype2value[b] - handtype2value[a]);

        return handTypes[0];
    }

    static calcHandType(cards: string): HandType {
        const count = Hand.getCardCount(cards);
        if (count.length === 1 && count[0].count === 5) return HandType.FiveOfAKind;
        else if (count.length === 2 && count[0].count === 4) return HandType.FourOfAKind;
        else if (count.length === 2 && count[0].count === 3 && count[1].count === 2) return HandType.FullHouse;
        else if (count.length === 3 && count[0].count === 3) return HandType.ThreeOfAKind;
        else if (count.length === 3 && count[0].count === 2 && count[1].count === 2) return HandType.TwoPair;
        else if (count[0].count === 2) return HandType.OnePair;
        return HandType.HighCard;
    }

    static getCardCount(cards: string): CardCount {
        return cards
            .split('')
            .reduce((count, label) => {
                const c = count.find((cc) => cc.label === label);
                if (!c) count.push({ label: label as keyof typeof card2value, count: 1 });
                else c.count++;
                return count;
            }, [] as CardCount)
            .sort((a, b) => b.count - a.count);
    }

    public handType: HandType;
    public score: number;
    constructor(
        public cards: string,
        public bid: number
    ) {
        this.handType = Hand.getBestPossibleHandType(cards);

        this.score =
            handtype2value[this.getHandType()] * 10000000000 +
            card2value[this.cards[0] as keyof typeof card2value] * 100000000 +
            card2value[this.cards[1] as keyof typeof card2value] * 1000000 +
            card2value[this.cards[2] as keyof typeof card2value] * 10000 +
            card2value[this.cards[3] as keyof typeof card2value] * 100 +
            card2value[this.cards[4] as keyof typeof card2value];
    }

    getHandType(): HandType {
        return this.handType;
    }

    getValue(): number {
        return this.score;
    }
}

const input = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) => {
        const [cards, bid] = line.split(' ');
        return new Hand(cards, Number(bid));
    })
    .sort((a, b) => a.getValue() - b.getValue());

// input.forEach((h) => {
//     console.log({
//         cards: h.cards,
//         type: h.getHandType(),
//         score: h.getValue(),
//     });
// });

console.log(
    input.map(({ bid }, index) => ({ rank: index + 1, bid })).reduce((sum, { rank, bid }) => sum + rank * bid, 0)
);
