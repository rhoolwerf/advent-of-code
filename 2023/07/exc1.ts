import { readFileSync } from 'fs';
import { EOL } from 'os';

const card2value = {
    A: 14,
    K: 13,
    Q: 12,
    J: 11,
    T: 10,
    '9': 9,
    '8': 8,
    '7': 7,
    '6': 6,
    '5': 5,
    '4': 4,
    '3': 3,
    '2': 2,
};

enum HandType {
    FiveOfAKind = 'FiveOfAKind',
    FourOfAKind = 'FourOfAKind',
    FullHouse = 'FullHouse',
    ThreeOfAKind = 'ThreeOfAKind',
    TwoPair = 'TwoPair',
    OnePair = 'OnePair',
    HighCard = 'HighCard',
}

const handtype2value = {
    [HandType.FiveOfAKind]: 7,
    [HandType.FourOfAKind]: 6,
    [HandType.FullHouse]: 5,
    [HandType.ThreeOfAKind]: 4,
    [HandType.TwoPair]: 3,
    [HandType.OnePair]: 2,
    [HandType.HighCard]: 1,
};

type CardCount = Array<{ label: keyof typeof card2value; count: number }>;

class Card {
    constructor(public label: keyof typeof card2value) {}

    get value(): number {
        return card2value[this.label];
    }
}

class Hand {
    constructor(
        public cards: Array<Card>,
        public bid: number
    ) {}

    getCardCount(): CardCount {
        return this.cards
            .reduce((count, { label }) => {
                const c = count.find((cc) => cc.label === label);
                if (!c) count.push({ label, count: 1 });
                else c.count++;
                return count;
            }, [] as CardCount)
            .sort((a, b) => b.count - a.count);
    }

    getHandType(): HandType {
        const count = this.getCardCount();
        if (count.length === 1 && count[0].count === 5) return HandType.FiveOfAKind;
        else if (count.length === 2 && count[0].count === 4) return HandType.FourOfAKind;
        else if (count.length === 2 && count[0].count === 3 && count[1].count === 2) return HandType.FullHouse;
        else if (count.length === 3 && count[0].count === 3) return HandType.ThreeOfAKind;
        else if (count.length === 3 && count[0].count === 2 && count[1].count === 2) return HandType.TwoPair;
        else if (count[0].count === 2) return HandType.OnePair;
        return HandType.HighCard;
    }

    getValue(): number {
        return (
            handtype2value[this.getHandType()] * 10000000000 +
            this.cards[0].value * 100000000 +
            this.cards[1].value * 1000000 +
            this.cards[2].value * 10000 +
            this.cards[3].value * 100 +
            this.cards[4].value
        );
    }
}

const input = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) => {
        const [cards, bid] = line.split(' ');
        return new Hand(
            cards.split('').map((c) => new Card(c as keyof typeof card2value)),
            Number(bid)
        );
    })
    .sort((a, b) => a.getValue() - b.getValue());

// input.forEach((h) => {
//     console.log({
//         cards: h.cards.map(({ label }) => label).join(''),
//         type: h.getHandType(),
//         score: h.getValue(),
//     });
// });

console.log(
    input.map(({ bid }, index) => ({ rank: index + 1, bid })).reduce((sum, { rank, bid }) => sum + rank * bid, 0)
);
