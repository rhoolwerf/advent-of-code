import { readFileSync } from 'fs';
import { EOL } from 'os';

const gridInput = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) => line.split(''));

const gridExpandedRows: Array<Array<string>> = [];
const gridExpandedBoth: Array<Array<string>> = [];

// Copy and expand rows
for (let y = 0; y < gridInput.length; y++) {
    gridExpandedRows.push(gridInput[y]);

    // If row only contains single value, expand row
    if (gridInput[y].reduce((set, pos) => set.add(pos), new Set()).size === 1) {
        gridExpandedRows.push(gridInput[y]);
    }
}

// Expand columns where needed
for (let y = 0; y < gridExpandedRows.length; y++) {
    gridExpandedBoth.push([]);
}
for (let x = 0; x < gridExpandedRows[0].length; x++) {
    // Determine if we need expanding
    let needsExpanding = true;
    for (let y = 0; y < gridExpandedRows.length; y++) {
        if (gridExpandedRows[y][x] === '#') needsExpanding = false;
    }

    // Expand
    for (let y = 0; y < gridExpandedRows.length; y++) {
        gridExpandedBoth[y].push(gridExpandedRows[y][x]);
        if (needsExpanding) gridExpandedBoth[y].push(gridExpandedRows[y][x]);
    }
}

// We now have our grid!
// Find all galaxies with their coordinates
const galaxies: Array<[number, number]> = [];
for (let y = 0; y < gridExpandedBoth.length; y++) {
    for (let x = 0; x < gridExpandedBoth[y].length; x++) {
        if (gridExpandedBoth[y][x] === '#') {
            galaxies.push([y, x]);
        }
    }
}

let sum = 0;
for (let i = 0; i < galaxies.length; i++) {
    for (let j = i + 1; j < galaxies.length; j++) {
        const distance = manhattan(galaxies[i][0], galaxies[i][1], galaxies[j][0], galaxies[j][1]);
        sum += distance;
    }
}
console.log({ sum });

function manhattan(y1: number, x1: number, y2: number, x2: number): number {
    // Borrowed from https://www.geeksforgeeks.org/maximum-manhattan-distance-between-a-distinct-pair-from-n-coordinates/
    // Math.abs(A[i][0] - A[j][0]) + Math.abs(A[i][1] - A[j][1])
    return Math.abs(x1 - x2) + Math.abs(y1 - y2);
}
