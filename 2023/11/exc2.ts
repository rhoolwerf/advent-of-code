import { readFileSync } from 'fs';
import { EOL } from 'os';

const EXPAND_TIMES = 1000000 - 1;

const grid = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) => line.split(''));

const expandedRows: Array<boolean> = [];
const expandedColumns: Array<boolean> = [];
const galaxies: Array<[number, number]> = [];

// Traverse the grid and look for rows to expand as well as galaxies
for (let y = 0; y < grid.length; y++) {
    // Traverse looking for galaxies
    for (let x = 0; x < grid[y].length; x++) {
        // Is this a galaxy?
        if (grid[y][x] === '#') {
            galaxies.push([y, x]);
        }
    }

    // Does this row need to be expanded?
    expandedRows.push(grid[y].reduce((set, pos) => set.add(pos), new Set()).size === 1);
}

// Look for expanded columns
for (let x = 0; x < grid[0].length; x++) {
    expandedColumns.push(grid.map((row) => row[x]).reduce((set, pos) => set.add(pos), new Set()).size === 1);
}

// console.log(grid.map((row) => row.join('')).join(EOL));
// console.log({ expandedRows });
// console.log({ expandedColumns });
// console.log('Before adjusting:', { galaxies });
for (const galaxy of galaxies) {
    // Adjust row
    let offsetY = 0;
    for (let y = 0; y < galaxy[0]; y++) {
        if (expandedRows[y]) {
            offsetY++;
        }
    }

    // Adjust column
    let offsetX = 0;
    for (let x = 0; x < galaxy[1]; x++) {
        if (expandedColumns[x]) {
            offsetX++;
        }
    }

    // console.log({ galaxy, offsetX, offsetY });
    galaxy[0] += offsetY * EXPAND_TIMES;
    galaxy[1] += offsetX * EXPAND_TIMES;
}
// console.log('After adjusting:', { galaxies });

let sum = 0;
for (let i = 0; i < galaxies.length; i++) {
    for (let j = i + 1; j < galaxies.length; j++) {
        const distance = manhattan(galaxies[i][0], galaxies[i][1], galaxies[j][0], galaxies[j][1]);
        sum += distance;
    }
}
console.log({ sum });

function manhattan(y1: number, x1: number, y2: number, x2: number): number {
    // Borrowed from https://www.geeksforgeeks.org/maximum-manhattan-distance-between-a-distinct-pair-from-n-coordinates/
    // Math.abs(A[i][0] - A[j][0]) + Math.abs(A[i][1] - A[j][1])
    return Math.abs(x1 - x2) + Math.abs(y1 - y2);
}
