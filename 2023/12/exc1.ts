import { readFileSync } from 'fs';
import { EOL } from 'os';

type Configuration = {
    hash: string;
    possibilities: Array<Array<string>>;
    checks: Array<number>;
    valid: Array<Array<string>>;
};

const input = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((row) => {
        const [hash, check] = row.split(' ');

        const possibilities = cartesian(...hash.split('').map((c) => (c === '?' ? ['#', '.'] : [c])));
        const checks = check.split(',').map(Number);
        const valid = possibilities.filter((p) => validate(p, checks));

        return {
            hash,
            possibilities,
            checks,
            valid,
        } as Configuration;
    });

console.log(input.map(({ valid }) => valid.length).reduce((sum, val) => sum + val));

function validate(input: Array<string>, checks: Array<number>): boolean {
    // Lets count blocks of springs
    const blocks: Array<number> = [0];
    for (let i = 0; i < input.length; i++) {
        const lastBlock = blocks.length - 1;
        if (input[i] === '#') {
            blocks[lastBlock]++;
        } else {
            if (blocks[lastBlock] > 0) {
                blocks.push(0);
            }
        }
    }

    const clean = blocks.filter(Boolean);
    if (clean.length === checks.length) {
        for (let i = 0; i < checks.length; i++) {
            if (checks[i] !== clean[i]) {
                return false;
            }
        }
        return true;
    }

    // If nothing succeeds as plan, no dice
    return false;
}

// Borrowed from https://stackoverflow.com/a/15310051
function cartesian(...args: Array<Array<string>>) {
    var r: Array<Array<string>> = [],
        max = args.length - 1;
    function helper(arr: Array<string>, i: number) {
        for (var j = 0, l = args[i].length; j < l; j++) {
            var a = arr.slice(0); // clone arr
            a.push(args[i][j]);
            if (i == max) r.push(a);
            else helper(a, i + 1);
        }
    }
    helper([], 0);
    return r;
}
