import { readFileSync } from 'fs';
import { EOL } from 'os';
import { Worker, isMainThread, parentPort, workerData } from 'worker_threads';

type Range = {
    from: number;
    to: number;
};

type Mapper = {
    source: Range;
    destination: Range;
};

type Mapped = {
    seed: number;
    soil: number;
    fertilizer: number;
    water: number;
    light: number;
    temperature: number;
    humidity: number;
    location: number;
};

function mapToRanges(chunk: string): Array<Mapper> {
    return chunk
        .split(EOL)
        .slice(1)
        .map((line) => line.split(' ').map(Number))
        .map(([destinationStart, sourceStart, length]) => ({
            source: { from: sourceStart, to: sourceStart + length - 1 },
            destination: { from: destinationStart, to: destinationStart + length - 1 },
        }));
}

function source2dest(mapper: Array<Mapper>, source: number): number {
    for (const range of mapper) {
        if (source >= range.source.from && source <= range.source.to) {
            const offset = source - range.source.from;
            const destination = range.destination.from + offset;
            return destination;
        }
    }
    return source;
}

function seed2location(seed: number): number {
    const soil = source2dest(seed2soil, seed);
    const fert = source2dest(soil2fert, soil);
    const watr = source2dest(fert2watr, fert);
    const ligt = source2dest(watr2ligt, watr);
    const temp = source2dest(ligt2temp, ligt);
    const humi = source2dest(temp2humi, temp);
    const loca = source2dest(humi2loca, humi);

    return loca;
}

function createWorkerPromise(from: number, to: number): Promise<number> {
    return new Promise((resolve, reject) => {
        const msg = `Running ${from} to ${to}`;
        console.time(msg);
        const w = new Worker('../workers/wrapper.js', {
            workerData: {
                path: __filename,
                from: from,
                to,
                inputFile,
            },
        });
        w.on('message', (data) => {
            console.timeEnd(msg);
            resolve(data);
        });
        w.on('error', reject);
    });
}

const inputFile = isMainThread ? process.argv[2] : workerData.inputFile;
const chunks = readFileSync(inputFile, 'utf8').split(`${EOL}${EOL}`);

const seed2soil = mapToRanges(chunks[1]);
const soil2fert = mapToRanges(chunks[2]);
const fert2watr = mapToRanges(chunks[3]);
const watr2ligt = mapToRanges(chunks[4]);
const ligt2temp = mapToRanges(chunks[5]);
const temp2humi = mapToRanges(chunks[6]);
const humi2loca = mapToRanges(chunks[7]);

if (isMainThread) {
    // Process the input
    console.time('Totale berekening...');
    const promises = chunks[0]
        .split(' ')
        .slice(1)
        .map(Number)
        .reduce(
            (promises, from, index, numbers) => {
                if (index % 2 === 0) {
                    const offset = numbers[index + 1];
                    const to = from + offset;
                    promises.push(createWorkerPromise(from, to));
                }
                return promises;
            },
            [] as Array<Promise<number>>
        );

    Promise.all(promises)
        .then((responses) => {
            console.timeEnd('Totale berekening...');
            console.log('Na alle promises:', { responses });
            console.log('Lowest', Math.min(...responses));
        })
} else {
    const { from, to } = workerData as { from: number; to: number };

    let lowest = Infinity;
    for (let i = from; i <= to; i++) {
        lowest = Math.min(seed2location(i), lowest);
    }
    parentPort?.postMessage(lowest);
}
