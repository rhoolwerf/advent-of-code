import { readFileSync } from 'fs';
import { EOL } from 'os';

type Range = {
    from: number;
    to: number;
};

type Mapper = {
    source: Range;
    destination: Range;
};

type Mapped = {
    seed: number;
    soil: number;
    fertilizer: number;
    water: number;
    light: number;
    temperature: number;
    humidity: number;
    location: number;
};

function mapToRanges(chunk: string): Array<Mapper> {
    return chunk
        .split(EOL)
        .slice(1)
        .map((line) => line.split(' ').map(Number))
        .map(([destinationStart, sourceStart, length]) => ({
            source: { from: sourceStart, to: sourceStart + length - 1 },
            destination: { from: destinationStart, to: destinationStart + length - 1 },
        }));
}

function source2dest(mapper: Array<Mapper>, source: number): number {
    for (const range of mapper) {
        if (source >= range.source.from && source <= range.source.to) {
            const offset = source - range.source.from;
            const destination = range.destination.from + offset;
            return destination;
        }
    }
    return source;
}

function seed2all(seed: number): Mapped {
    const soil = source2dest(seed2soil, seed);
    const fert = source2dest(soil2fert, soil);
    const watr = source2dest(fert2watr, fert);
    const ligt = source2dest(watr2ligt, watr);
    const temp = source2dest(ligt2temp, ligt);
    const humi = source2dest(temp2humi, temp);
    const loca = source2dest(humi2loca, humi);

    return {
        seed,
        soil,
        fertilizer: fert,
        water: watr,
        light: ligt,
        temperature: temp,
        humidity: humi,
        location: loca,
    };
}

// Process the input
const chunks = readFileSync(process.argv[2], 'utf8').split(`${EOL}${EOL}`);

const seed2soil = mapToRanges(chunks[1]);
const soil2fert = mapToRanges(chunks[2]);
const fert2watr = mapToRanges(chunks[3]);
const watr2ligt = mapToRanges(chunks[4]);
const ligt2temp = mapToRanges(chunks[5]);
const temp2humi = mapToRanges(chunks[6]);
const humi2loca = mapToRanges(chunks[7]);

console.time('Totale berekening...');
const lowest = chunks[0]
    .split(' ')
    .slice(1)
    .map(Number)
    .reduce((lowest, seed, index, numbers) => {
        if (index % 2 === 0) {
            const offset = numbers[index + 1];
            const lala = { seed, offset, target: seed + offset };
            console.time(JSON.stringify(lala));
            for (let i = 0; i < numbers[index + 1]; i++) {
                lowest = Math.min(seed2all(seed + i).location, lowest);
            }
            console.timeEnd(JSON.stringify(lala));
        }
        return lowest;
    }, Infinity);

console.log({ lowest });
console.timeEnd('Totale berekening...');
