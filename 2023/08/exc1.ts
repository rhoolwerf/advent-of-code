import { readFileSync } from 'fs';
import { EOL } from 'os';

const input = readFileSync(process.argv[2], 'utf8').split(`${EOL}${EOL}`);
const instructions = input[0];
const nodes = input[1]
    .split(EOL)
    .map((line) => Array.from(line.matchAll(new RegExp(/(\w+)/, 'g'))).map(([match]) => match));

let currentNodeID = 'AAA';
let steps = 0;
while (currentNodeID !== 'ZZZ') {
    const node = nodes.find(([nodeId]) => currentNodeID === nodeId) as Array<string>;
    const targetDirection = instructions[steps % instructions.length];
    const targetIndex = targetDirection === 'L' ? 1 : 2;
    const target = node[targetIndex];
    // console.log({ currentNode: currentNodeID, node, targetDirection, targetIndex, target });
    currentNodeID = target;
    steps++;
}
console.log({ steps });
