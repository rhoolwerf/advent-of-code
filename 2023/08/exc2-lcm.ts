import { readFileSync } from 'fs';
import { EOL } from 'os';

const input = readFileSync(process.argv[2], 'utf8').split(`${EOL}${EOL}`);
const instructions = input[0];
const nodes = input[1]
    .split(EOL)
    .map((line) => Array.from(line.matchAll(new RegExp(/(\w+)/, 'g'))).map(([match]) => match))
    .reduce((map, [start, left, right]) => {
        map.set(start, [left, right]);
        return map;
    }, new Map<string, [string, string]>());

// Find at starting nodes
const startingNodes = Array.from(nodes.keys()).filter((n) => n.endsWith('A'));
const stepsPerNode: Array<number> = [];

for (let i = 0; i < startingNodes.length; i++) {
    let currentNodeID = startingNodes[i];

    let steps = 0;
    console.time(`Timing ghost ${i}...`);
    while (!currentNodeID.endsWith('Z')) {
        const node = nodes.get(currentNodeID) as [string, string];
        const targetDirection = instructions[steps % instructions.length];
        const targetIndex = targetDirection === 'L' ? 0 : 1;
        const target = node[targetIndex];
        currentNodeID = target;
        steps++;
    }

    console.log(startingNodes[i], 'took', steps, 'steps');
    stepsPerNode.push(steps);
}

const stepsToReachAllEnds = stepsPerNode.reduce((acc, cur) => lcm(acc, cur), 1);
console.log({ stepsToReachAllEnds });

// Helper functions borrowed from
// https://github.com/sanishchirayath1/advent-of-code/blob/master/2023/day8/index.js
function gcd(a: number, b: number): number {
    if (b === 0) return a;
    return gcd(b, a % b);
}

function lcm(a: number, b: number): number {
    return (a * b) / gcd(a, b);
}
