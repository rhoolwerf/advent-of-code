import { readFileSync } from 'fs';
import { EOL } from 'os';

const [input] = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((r) =>
        r
            .split(',')
            .map((hash) => ({ hash, value: hash2sum(hash) }))
            .reduce((sum, { value }) => sum + value, 0)
    );

console.log({ input });

function hash2sum(hash: string): number {
    let sum = 0;
    for (let i = 0; i < hash.length; i++) {
        sum += hash.charCodeAt(i);
        sum *= 17;
        sum %= 256;
    }

    return sum;
}
