import { readFileSync } from 'fs';
import { EOL } from 'os';

type Lens = {
    label: string;
    focalLength: number;
};

const input = readFileSync(process.argv[2], 'utf8').split(EOL)[0].split(',');
const boxes: Array<Array<Lens>> = Array.from({ length: 256 }, () => []);

for (const record of input) {
    if (record.indexOf('=') >= 0) {
        const [label, focalLength] = record.split('=');
        const box = boxes[hash2sum(label)];
        const lens = box.find((b) => b.label === label);
        if (lens) {
            lens.focalLength = Number(focalLength);
        } else {
            box.push({ label, focalLength: Number(focalLength) });
        }
    } else if (record.indexOf('-') >= 0) {
        const [label] = record.split('-');
        boxes[hash2sum(label)] = boxes[hash2sum(label)].filter((b) => b.label !== label);
    } else {
        console.error('What am I to do with', { record });
        process.exit(-1);
    }
}
displayBoxes();

const focusPowerPerBox = boxes.map((box, boxIndex) =>
    box.map((lens, lensIndex) => (1 + boxIndex) * (1 + lensIndex) * lens.focalLength)
);
const totalFocusingPower = focusPowerPerBox.flat().reduce((sum, val) => sum + val, 0);

console.log({ totalFocusingPower });

function displayBoxes() {
    console.log(
        boxes
            .map(
                (b, i) =>
                    `Box ${String(i).padStart(3, ' ')}: ${b.map((l) => `[${l.label} ${l.focalLength}]`).join(' ')}`
            )
            .filter((b) => b.length > 9)
            .join(EOL)
    );
}

function hash2sum(hash: string): number {
    let sum = 0;
    for (let i = 0; i < hash.length; i++) {
        sum += hash.charCodeAt(i);
        sum *= 17;
        sum %= 256;
    }

    return sum;
}
