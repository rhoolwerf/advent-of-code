import { readFileSync } from 'fs';
import { EOL } from 'os';

const x = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) => line.split('').filter((c) => !isNaN(Number(c))))
    .map((numbers) => [numbers[0], numbers[numbers.length - 1]])
    .map((numbers) => Number(numbers.join('')))
    .reduce((sum, val) => sum + val, 0);

console.log(x);
