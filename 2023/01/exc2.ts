import { readFileSync } from 'fs';
import { EOL } from 'os';

const mapper = [
    { key: 'one', val: 1 },
    { key: 'two', val: 2 },
    { key: 'three', val: 3 },
    { key: 'four', val: 4 },
    { key: 'five', val: 5 },
    { key: 'six', val: 6 },
    { key: 'seven', val: 7 },
    { key: 'eight', val: 8 },
    { key: 'nine', val: 9 },
];

const x = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) => {
        const numbers: Array<number> = [];
        for (let i = 0; i < line.length; i++) {
            // Is it directly a number?
            if (!isNaN(Number(line[i]))) {
                numbers.push(Number(line[i]));
                continue;
            }

            // Could it be a 'number'?
            for (const m of mapper) {
                if (line.substring(i, i + m.key.length) === m.key) {
                    numbers.push(m.val);
                    break;
                }
            }
        }
        return [line, numbers];
    })
    .map(([line, numbers]) => {
        console.log(line, numbers);
        return [line, numbers];
    })

    // .map((line) => line.split('').filter((c) => !isNaN(Number(c))))
    .map(([_, numbers]) => [numbers[0], numbers[numbers.length - 1]])
    .map((numbers) => Number(numbers.join('')))
    .reduce((sum, val) => sum + val, 0);

console.log(x);
