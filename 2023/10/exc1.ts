import { readFileSync } from 'fs';
import { EOL } from 'os';

const grid = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((row) => row.split(''));

const stepsNeeded: Array<Array<number>> = [];

// Where do we need to start?
let startX = Infinity;
let startY = Infinity;
for (let y = 0; y < grid.length; y++) {
    stepsNeeded.push([]);
    for (let x = 0; x < grid[y].length; x++) {
        stepsNeeded[y].push(Infinity);

        if (grid[y][x] === 'S') {
            startY = y;
            startX = x;
        }
    }
}

// Now that we know where to start from, we need to know where we can go
const startingDirections = whereCanIGo(startY, startX);
traverse(startY, startX, startingDirections[0][0], startingDirections[0][1]);
traverse(startY, startX, startingDirections[1][0], startingDirections[1][1]);

// console.log(stepsNeeded.map((row) => row.map((c) => (c === Infinity ? '.' : String(c))).join('')).join(EOL));
console.log(
    stepsNeeded
        .map((row) => row.reduce((max, steps) => (steps === Infinity ? max : Math.max(max, steps)), 0))
        .reduce((max, steps) => Math.max(max, steps), 0)
);

function traverse(inStartY: number, inStartX: number, inNextY: number, inNextX: number) {
    let curY = inNextY;
    let curX = inNextX;
    let prevY = inStartY;
    let prevX = inStartX;

    // console.log('Coming from', { prevY, prevX });
    // console.log('Currently at', { curY, curX, char: grid[curY][curX] });
    let steps = 0;
    while (grid[curY][curX] !== 'S') {
        // Keep track of how many steps we needed to get here
        stepsNeeded[curY][curX] = Math.min(stepsNeeded[curY][curX], ++steps);

        // Determine next possible positions
        let [next] = whereCanIGo(curY, curX).filter(([y, x]) => !(y === prevY && x === prevX));
        // console.log({ next });

        // Register the next position
        prevY = curY;
        prevX = curX;
        [curY, curX] = next;
    }
}

function whereCanIGo(y: number, x: number): Array<[number, number]> {
    const from = grid[y][x];

    // console.log('Going from', { y, x, from });
    const directions: Array<[number, number]> = [];

    // Can I go down?
    if (validPos(y + 1, x) && ['S', '|', 'F', '7'].includes(from) && ['|', 'L', 'J', 'S'].includes(grid[y + 1][x])) {
        // console.log('- Can go down');
        directions.push([y + 1, x]);
    }

    // Can I go right?
    if (validPos(y, x + 1) && ['S', 'F', '-', 'L'].includes(from) && ['-', 'J', '7', 'S'].includes(grid[y][x + 1])) {
        // console.log('- Can go right');
        directions.push([y, x + 1]);
    }

    // Can I go up?
    if (validPos(y - 1, x) && ['S', '|', 'J', 'L'].includes(from) && ['|', 'F', '7', 'S'].includes(grid[y - 1][x])) {
        // console.log('- Can go up');
        directions.push([y - 1, x]);
    }

    // Can I go left?
    if (validPos(y, x - 1) && ['S', 'J', '7', '-'].includes(from) && ['-', 'F', 'L', 'S'].includes(grid[y][x - 1])) {
        // console.log('- Can go left');
        directions.push([y, x - 1]);
    }

    return directions;
}

function validPos(y: number, x: number): boolean {
    return Boolean(grid[y]) && Boolean(grid[y][x]);
}
