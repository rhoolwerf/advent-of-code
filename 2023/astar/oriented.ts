import createGraph from 'ngraph.graph';
import { aStar } from 'ngraph.path';

type Node = string;

const g = createGraph<Node>();

g.addLink('a', 'b');
g.addLink('b', 'c');
// g.forEachNode((n) => {
//   console.log('Node', n.id);
//   g.forEachLinkedNode(
//     n.id,
//     (linked) => {
//       console.log('- Linked', linked.id);
//     },
//     !false // oriented
//   );
// });

const pathFinderBidirectional = aStar(g);
console.log('Bidirectional', 'a->c', pathFinderBidirectional.find('a', 'c').reverse().map(({id}) => id));
console.log('Bidirectional', 'c->a', pathFinderBidirectional.find('c', 'a').reverse().map(({id}) => id));

const pathFinderOriented = aStar(g, { oriented: true });
console.log('Oriented', 'a->c', pathFinderOriented.find('a', 'c').reverse().map(({id}) => id));
console.log('Oriented', 'c->a', pathFinderOriented.find('c', 'a').reverse().map(({id}) => id));

g.addLink('b', 'a');
g.addLink('c', 'b');
console.log('Oriented after expanding graph', 'a->c', pathFinderOriented.find('a', 'c').reverse().map(({id}) => id));
console.log('Oriented after expanding graph', 'c->a', pathFinderOriented.find('c', 'a').reverse().map(({id}) => id));
