import createGraph from 'ngraph.graph';
import { aStar } from 'ngraph.path';

const g = createGraph();
g.addLink('a', 'b', { weight: 10 });
g.addLink('a', 'c', { weight: 10 });
g.addLink('b', 'd', { weight: 10 });
g.addLink('c', 'd', { weight: 5 });

const pathFinder = aStar(g, {
    distance: (_from, _to, link) => {
        return link.data.weight;
    },
});
console.log(
    pathFinder
        .find('a', 'd')
        .reverse()
        .map(({ id }) => id)
);
