import createGraph from 'ngraph.graph';
import { aStar } from 'ngraph.path';

type Node = string;
type Data = { weight: number };

const g = createGraph<Node, Data>();

g.addLink('a', 'b', { weight: 1 });
g.addLink('b', 'c', { weight: 1 });
g.addLink('c', 'd', { weight: 1 });
g.addLink('b', 'a', { weight: 1 });
g.addLink('c', 'b', { weight: 1 });
g.addLink('d', 'c', { weight: 1 });

g.addLink('a', 'd', { weight: 5 });
g.addLink('d', 'a', { weight: 1 });

const pathFinder = aStar(g, { oriented: true, distance: (_from, _to, { data: { weight } }) => weight });
console.log('a -> d', pathFinder.find('a','d').reverse().map(({id}) => id));
console.log('d -> a', pathFinder.find('d','a').reverse().map(({id}) => id));
