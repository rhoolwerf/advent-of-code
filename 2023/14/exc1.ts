import { readFileSync } from 'fs';
import { EOL } from 'os';

const grid = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((row) => row.split(''));

for (let y = 0; y < grid.length; y++) {
    for (let x = 0; x < grid[y].length; x++) {
        // If our current cell isn't an empty space, do nothing
        if (grid[y][x] !== '.') continue;

        // Look for the first available movable rock, unless we get block by a non-movable rock
        for (let y1 = y + 1; y1 < grid.length; y1++) {
            // Non-movable rock? Exit!
            if (grid[y1][x] === '#') break;
            else if (grid[y1][x] === 'O') {
                // Movable rock, swap and we're done
                grid[y][x] = 'O';
                grid[y1][x] = '.';
                break;
            }
        }
    }
}

console.log(
    grid
        .map((row, index) => row.map((c) => (c === 'O' ? grid.length - index : 0)))
        .flat()
        .reduce((sum, val) => sum + val, 0)
);