import { readFileSync } from 'fs';
import { EOL } from 'os';

const CYCLES = 1000000000;

const grid = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((row) => row.split(''));

// We need to detect cycles
// Learned that from https://www.reddit.com/r/adventofcode/comments/18i25w2/comment/kdah55o/
const cache: Array<string> = [grid2hash()];
let cycleFound = false;
for (let cycle = 1; cycle <= CYCLES; cycle++) {
    north();
    west();
    south();
    east();

    const hash = grid2hash();
    const foundAt = cache.indexOf(hash);
    if (foundAt >= 0 && !cycleFound) {
        cycleFound = true;
        const offset = cycle - foundAt;
        console.log('Cycle', { cycle }, 'is a repeat of', { foundAt }, 'with offset', { offset });
        while (cycle < CYCLES - offset) cycle += offset;
        console.log('Adjusted cycle to', { cycle });
    }

    cache.push(hash);
}

const load = grid
    .map((row, index) => row.map((c) => (c === 'O' ? grid.length - index : 0)))
    .flat()
    .reduce((sum, val) => sum + val, 0);

console.log({ load });

function grid2hash(): string {
    return grid.map((row) => row.join('')).join('');
}

function north() {
    for (let y = 0; y < grid.length; y++) {
        for (let x = 0; x < grid[y].length; x++) {
            // If our current cell isn't an empty space, do nothing
            if (grid[y][x] !== '.') continue;

            // Look for the first available movable rock, unless we get block by a non-movable rock
            for (let y1 = y + 1; y1 < grid.length; y1++) {
                // Non-movable rock? Exit!
                if (grid[y1][x] === '#') break;
                else if (grid[y1][x] === 'O') {
                    // Movable rock, swap and we're done
                    grid[y][x] = 'O';
                    grid[y1][x] = '.';
                    break;
                }
            }
        }
    }
}

function west() {
    for (let x = 0; x < grid[0].length; x++) {
        for (let y = 0; y < grid.length; y++) {
            if (grid[y][x] !== '.') continue;

            for (let x1 = x + 1; x1 < grid[0].length; x1++) {
                if (grid[y][x1] === '#') break;
                else if (grid[y][x1] === 'O') {
                    grid[y][x] = 'O';
                    grid[y][x1] = '.';
                    break;
                }
            }
        }
    }
}

function south() {
    for (let y = grid.length - 1; y >= 0; y--) {
        for (let x = 0; x < grid[0].length; x++) {
            if (grid[y][x] !== '.') continue;

            for (let y1 = y - 1; y1 >= 0; y1--) {
                if (grid[y1][x] === '#') break;
                else if (grid[y1][x] === 'O') {
                    grid[y][x] = 'O';
                    grid[y1][x] = '.';
                    break;
                }
            }
        }
    }
}

function east() {
    for (let x = grid[0].length - 1; x >= 0; x--) {
        for (let y = 0; y < grid.length; y++) {
            if (grid[y][x] !== '.') continue;

            for (let x1 = x - 1; x1 >= 0; x1--) {
                if (grid[y][x1] === '#') break;
                else if (grid[y][x1] === 'O') {
                    grid[y][x] = 'O';
                    grid[y][x1] = '.';
                    break;
                }
            }
        }
    }
}
