import { readFileSync } from 'fs';
import { EOL } from 'os';

const depths = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .reduce(
        (obj, row) => {
            const [depth, range] = row.split(': ').map(Number);
            obj[depth] = range;
            return obj;
        },
        {} as { [key: number]: number }
    );

const scanners = Array.from(Object.keys(depths))
    .map(Number)
    .reduce(
        (obj, layer) => {
            obj[layer] = 0;
            return obj;
        },
        {} as { [key: number]: number }
    );

const offsets = Array.from(Object.keys(depths))
    .map(Number)
    .reduce(
        (obj, layer) => {
            obj[layer] = 1;
            return obj;
        },
        {} as { [key: number]: number }
    );

const maxLayer = Math.max(...Array.from(Object.keys(depths)).map(Number));
const maxDepth = Math.max(...Array.from(Object.values(depths)).map(Number));

let picosecond = 0;
let calcSomething = 0;
for (; picosecond <= maxLayer; picosecond++) {
    console.log('Picosecond', picosecond);
    // output();
    Array.from(Object.keys(depths))
        .map(Number)
        .forEach((layer) => {
            if (scanners[layer] === 0 && layer === picosecond) {
                console.log('Detected at', layer, depths[layer]);
                calcSomething += layer * depths[layer];
            }

            scanners[layer] += offsets[layer];
            if (scanners[layer] === 0) offsets[layer] = 1;
            else if (scanners[layer] === depths[layer] - 1) offsets[layer] = -1;
        });

    console.log('');
    // output();
    console.log('');
}
console.log({ calcSomething });

function output() {
    console.log(Array.from({ length: maxLayer + 1 }, (_, i) => ` ${i} `).join(' '));
    for (let depth = 0; depth < maxDepth; depth++) {
        const blocks: Array<string> = [];
        for (let layer = 0; layer <= maxLayer; layer++) {
            const block = '   '.split('');

            // Does the depth for this layer exist?
            if (depth < depths[layer]) {
                block[0] = '[';
                block[2] = ']';
            }

            // Where is the scanner?
            if (scanners[layer] === depth) {
                block[1] = 'S';
            }

            // Where is the 'player' ?
            if (layer === picosecond && depth === 0) {
                block[0] = '(';
                block[2] = ')';
            }

            blocks.push(block.join(''));
        }
        console.log(blocks.join(' '));
    }
}
