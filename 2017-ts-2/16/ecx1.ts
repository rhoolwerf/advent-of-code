import { readFileSync } from 'fs';
import { EOL } from 'os';

const input = readFileSync(process.argv[2], 'utf8').split(EOL);

const programs = input[0].split('');

console.log('Start:', programs.join(''));
for (const instr of input[1].split(',')) {
    if (instr[0] === 's') {
        for (let i = 0; i < Number(instr.substring(1)); i++) {
            programs.unshift(programs.pop() as string);
        }
    } else if (instr[0] === 'x') {
        const [a, b] = instr.substring(1).split('/').map(Number);
        const tmp = programs[a];
        programs[a] = programs[b];
        programs[b] = tmp;
    } else if (instr[0] === 'p') {
        const [i, j] = instr.substring(1).split('/');
        const a = programs.findIndex((p) => p === i);
        const b = programs.findIndex((p) => p === j);
        const tmp = programs[a];
        programs[a] = programs[b];
        programs[b] = tmp;
    } else {
        console.error('Unknown instruction:', instr);
        process.exit(-1);
    }
}
console.log('After:', programs.join(''));
