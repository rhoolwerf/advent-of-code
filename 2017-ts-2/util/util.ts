export function sum(input: Array<number>): number {
    return input.reduce((acc, val) => acc + val, 0);
}
