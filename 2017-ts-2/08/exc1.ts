import { readFileSync } from 'fs';
import { EOL } from 'os';

const registers: { [key: string]: number } = {};
const instructions = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((row) => row.split(' '));

// Init registers
for (const [register] of instructions) {
    registers[register as keyof typeof registers] = 0;
}

for (const parts of instructions) {
    const register = parts[0] as keyof typeof registers;
    const op = parts[1];
    const offset = Number(parts[2]);
    const checkRegister = parts[4] as keyof typeof registers;
    const condition = parts[5];
    const checkValue = Number(parts[6]);

    if (eval(`registers['${checkRegister}'] ${condition} ${checkValue}`)) {
        registers[register] += offset * (op === 'inc' ? 1 : -1);
    }
}

console.log(Math.max(...Object.values(registers)));
