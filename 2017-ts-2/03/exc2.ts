import { sum } from '../util/util';

function key(y: number, x: number): string {
    return [y, x].join(',');
}

function get_distance(input: number): number {
    let y = 0;
    let x = 0;

    let offsetX = 1;
    let offsetY = 0;

    const visited = new Map<string, number>();
    visited.set(key(y, x), 1);
    for (let i = 2; i <= input; i++) {
        y += offsetY;
        x += offsetX;
        const val = get_sum(visited, y, x);
        visited.set(key(y, x), val);

        // Very wonky, but it works
        if (val > 347991) {
            console.log({ val });
            process.exit(0);
        }

        // If going 'right' and 'up' is empty, go 'up' next
        if (offsetY === 0 && offsetX === 1 && !visited.has(key(y - 1, x))) {
            offsetY = -1;
            offsetX = 0;
            // If going 'up' and 'left' is empty, go 'left' next
        } else if (offsetY === -1 && offsetX === 0 && !visited.has(key(y, x - 1))) {
            offsetY = 0;
            offsetX = -1;
            // If going 'left' and 'down' is empty, go 'down' next
        } else if (offsetY === 0 && offsetX === -1 && !visited.has(key(y + 1, x))) {
            offsetY = 1;
            offsetX = 0;
            // If going 'down' and 'right' is empty, go 'right' next
        } else if (offsetY === 1 && offsetX === 0 && !visited.has(key(y, x + 1))) {
            offsetY = 0;
            offsetX = 1;
        }
    }

    return visited.get(key(y, x)) || Infinity;
}

function get_sum(visited: Map<string, number>, y: number, x: number): number {
    return sum(
        [
            [y - 1, x - 1],
            [y - 1, x],
            [y - 1, x + 1],

            [y, x - 1],
            [y, x + 1],

            [y + 1, x - 1],
            [y + 1, x],
            [y + 1, x + 1],
        ].map(([y, x]) => visited.get(key(y, x)) || 0)
    );
}

function test(input: number, expected: number): void {
    const actual = get_distance(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

test(1, 1);
test(2, 1);
test(3, 2);
test(4, 4);
test(5, 5);

console.log(get_distance(1e6));
