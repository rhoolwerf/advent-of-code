function key(y: number, x: number): string {
    return [y, x].join(',');
}

function get_distance(input: number): number {
    let y = 0;
    let x = 0;

    let offsetX = 1;
    let offsetY = 0;

    const visited = new Map<string, number>();
    visited.set(key(y, x), 1);
    for (let i = 2; i <= input; i++) {
        y += offsetY;
        x += offsetX;
        visited.set(key(y, x), i);

        // If going 'right' and 'up' is empty, go 'up' next
        if (offsetY === 0 && offsetX === 1 && !visited.has(key(y - 1, x))) {
            offsetY = -1;
            offsetX = 0;
            // If going 'up' and 'left' is empty, go 'left' next
        } else if (offsetY === -1 && offsetX === 0 && !visited.has(key(y, x - 1))) {
            offsetY = 0;
            offsetX = -1;
            // If going 'left' and 'down' is empty, go 'down' next
        } else if (offsetY === 0 && offsetX === -1 && !visited.has(key(y + 1, x))) {
            offsetY = 1;
            offsetX = 0;
            // If going 'down' and 'right' is empty, go 'right' next
        } else if (offsetY === 1 && offsetX === 0 && !visited.has(key(y, x + 1))) {
            offsetY = 0;
            offsetX = 1;
        }
    }

    return Math.abs(x) + Math.abs(y);
}

function test(input: number, expected: number): void {
    const actual = get_distance(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

test(1, 0);
test(12, 3);
test(23, 2);
test(1024, 31);

console.log(get_distance(347991));
