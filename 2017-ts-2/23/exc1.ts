import { readFileSync } from 'fs';
import { EOL } from 'os';

const registers: { [key: string]: number } = {};

const instructions = readFileSync(process.argv[2], 'utf8').split(EOL);
let mulCount = 0;

for (let pointer = 0; pointer < instructions.length; ) {
    const instruction = instructions[pointer];
    const [cmd, p1, p2] = instruction.split(' ');
    const x = isNaN(Number(p1)) ? registers[p1] || 0 : Number(p1);
    const y = isNaN(Number(p2)) ? registers[p2] || 0 : Number(p2);
    // console.log({ cmd, p1, p2, x, y });

    switch (cmd) {
        case 'set':
            registers[p1] = y;
            pointer++;
            break;

        case 'sub':
            registers[p1] -= y;
            pointer++;
            break;

        case 'mul':
            registers[p1] *= y;
            mulCount++;
            pointer++;
            break;

        case 'jnz':
            if (x !== 0) pointer += y;
            else pointer++;
            break;

        default:
            console.error('Unknown instruction:', instruction);
            process.exit(-1);
            break;
    }

    // console.log({ registers, pointer }, '\n');
}

console.log({ mulCount });
