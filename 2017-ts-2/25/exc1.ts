import { readFileSync } from 'fs';
import { EOL } from 'os';
import { sum } from '../util/util';

type StateNext = {
    write: 0 | 1;
    movePosition: 1 | -1;
    nextState: string;
};

type State = {
    id: string;
    if0: StateNext;
    if1: StateNext;
};

console.time('Parsing input...');
const blocks = readFileSync(process.argv[2], 'utf8').split(`${EOL}${EOL}`);
const registers = new Map<number, 0 | 1>();
let position = 0;

// Get some starting info
let stateToRun = blocks[0].split(EOL)[0].split(' ')[3].charAt(0);
const steps = Number(blocks[0].split(' ')[8]);

// Parse blocks into programs
const states: Array<State> = blocks.slice(1).map((b) => {
    const lines = b.split(EOL);
    const id = lines[0].split(':')[0].split(' ')[2];

    const if0 = {
        write: Number(lines[2].split('.')[0].split(' ')[8]),
        movePosition: lines[3].split('.')[0].split(' ')[10] === 'right' ? 1 : -1,
        nextState: lines[4].split('.')[0].split(' ')[8],
    } as State['if0'];
    const if1 = {
        write: Number(lines[6].split('.')[0].split(' ')[8]),
        movePosition: lines[7].split('.')[0].split(' ')[10] === 'right' ? 1 : -1,
        nextState: lines[8].split('.')[0].split(' ')[8],
    } as State['if1'];

    return { id, if0, if1 };
});
console.timeEnd('Parsing input...');

console.time(`Running ${steps} steps...`);
for (let step = 0; step < steps; step++) {
    const cur = registers.get(position) || 0;
    const state = states.find(({ id }) => id === stateToRun) as State;

    const next = cur === 0 ? state.if0 : state.if1;
    registers.set(position, next.write);
    position += next.movePosition;
    stateToRun = next.nextState;
}
console.timeEnd(`Running ${steps} steps...`);

const checksum = sum([...registers.values()]);
console.log({ checksum });
