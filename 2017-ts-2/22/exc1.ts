import { readFileSync } from 'fs';
import { EOL } from 'os';

const CLEAN = '.';
const INFECTED = '#';
type Char = typeof INFECTED | typeof CLEAN;

// Build grid based on input
const grid = new Map<string, Char>();

readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .forEach((line, y) => {
        line.split('').forEach((char, x) => {
            grid.set(key(y, x), char as Char);
        });
    });

// Determine starting position and direction
let { posY, posX } = get_middle_position();
let [dirY, dirX] = [-1, 0];
let bursts = 0;

// console.log('Starting out with:');
// display_grid();

for (let i = 0; i < 1e4; i++) {
    run();
    // console.log('\n');
    // display_grid();
}

console.log({ bursts });

// All kinds of helper functions

function run() {
    // Consider current position
    const k = key(posY, posX);
    const cur = grid.get(k) || CLEAN;

    // Which direction to turn to?
    if (cur === INFECTED) [dirY, dirX] = turn_right();
    else [dirY, dirX] = turn_left();

    // Do we burst or not?
    if (cur === INFECTED) grid.set(k, CLEAN);
    else {
        grid.set(k, INFECTED);
        bursts++;
    }

    // Move forward
    posY += dirY;
    posX += dirX;
}

function turn_left(): [number, number] {
    if (dirY === -1 && dirX === 0) return [0, -1];
    if (dirY === 0 && dirX === -1) return [1, 0];
    if (dirY === 1 && dirX === 0) return [0, 1];
    if (dirY === 0 && dirX === 1) return [-1, 0];

    return [Infinity, Infinity];
}

function turn_right(): [number, number] {
    if (dirY === -1 && dirX === 0) return [0, 1];
    if (dirY === 0 && dirX === 1) return [1, 0];
    if (dirY === 1 && dirX === 0) return [0, -1];
    if (dirY === 0 && dirX === -1) return [-1, 0];

    return [Infinity, Infinity];
}

function key(y: number, x: number): string {
    return [y, x].join(',');
}

function display_grid() {
    const { minY, maxY, minX, maxX } = get_bounding_box();
    for (let y = minY - 1; y <= maxY + 1; y++) {
        const row: Array<string> = [];
        for (let x = minX - 1; x <= maxX + 1; x++) {
            const char = [
                posY === y && posX === x ? '[' : ' ',
                grid.get(key(y, x)) || CLEAN,
                posY === y && posX === x ? ']' : ' ',
            ];

            row.push(char.join(''));
        }
        console.log(row.join(''));
    }
}

function get_bounding_box(): { minY: number; maxY: number; minX: number; maxX: number } {
    const keys = [...grid.keys()];
    const minY = Math.min(...keys.map((k) => Number(k.split(',')[0])));
    const maxY = Math.max(...keys.map((k) => Number(k.split(',')[0])));
    const minX = Math.min(...keys.map((k) => Number(k.split(',')[1])));
    const maxX = Math.max(...keys.map((k) => Number(k.split(',')[1])));
    return { minY, maxY, minX, maxX };
}

function get_middle_position(): { posY: number; posX: number } {
    const { minY, maxY, minX, maxX } = get_bounding_box();
    return { posY: (maxY - minY) / 2, posX: (maxX - minX) / 2 };
}
