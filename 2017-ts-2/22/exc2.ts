import { readFileSync } from 'fs';
import { EOL } from 'os';

const CLEAN = '.';
const INFECTED = '#';
const WEAKENED = 'W';
const FLAGGED = 'F';
type State = typeof INFECTED | typeof CLEAN | typeof WEAKENED | typeof FLAGGED;

// Build grid based on input
const grid = new Map<string, State>();

readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .forEach((line, y) => {
        line.split('').forEach((char, x) => {
            grid.set(key(y, x), char as State);
        });
    });

// Determine starting position and direction
let { posY, posX } = get_middle_position();
let [dirY, dirX] = [-1, 0];
let infections = 0;

// console.log('Starting out with:');
// display_grid();

console.time('Processing...');
for (let i = 0; i < 10000000; i++) {
    run();
    // console.log('\n');
    // display_grid();
}
console.timeEnd('Processing...');

console.log({ infections });

// All kinds of helper functions

function run() {
    // Consider current position
    const k = key(posY, posX);
    const cur = grid.get(k) || CLEAN;

    // Which direction to turn to?
    if (cur === CLEAN) {
        [dirY, dirX] = turn_left();
    } else if (cur === WEAKENED) {
        // Do not change direction
    } else if (cur === INFECTED) {
        [dirY, dirX] = turn_right();
    } else if (cur === FLAGGED) {
        [dirY, dirX] = reverse();
    }

    // Modify our state
    if (cur === CLEAN) {
        grid.set(k, WEAKENED);
    } else if (cur === WEAKENED) {
        grid.set(k, INFECTED);
        infections++;
    } else if (cur === INFECTED) {
        grid.set(k, FLAGGED);
    } else if (cur === FLAGGED) {
        grid.set(k, CLEAN);
    }

    // Move forward
    posY += dirY;
    posX += dirX;
}

function turn_left(): [number, number] {
    if (dirY === -1 && dirX === 0) return [0, -1];
    if (dirY === 0 && dirX === -1) return [1, 0];
    if (dirY === 1 && dirX === 0) return [0, 1];
    if (dirY === 0 && dirX === 1) return [-1, 0];

    return [Infinity, Infinity];
}

function turn_right(): [number, number] {
    if (dirY === -1 && dirX === 0) return [0, 1];
    if (dirY === 0 && dirX === 1) return [1, 0];
    if (dirY === 1 && dirX === 0) return [0, -1];
    if (dirY === 0 && dirX === -1) return [-1, 0];

    return [Infinity, Infinity];
}

function reverse(): [number, number] {
    if (dirY === -1 && dirX === 0) return [1, 0];
    if (dirY === 1 && dirX === 0) return [-1, 0];
    if (dirY === 0 && dirX === 1) return [0, -1];
    if (dirY === 0 && dirX === -1) return [0, 1];

    return [Infinity, Infinity];
}

function key(y: number, x: number): string {
    return [y, x].join(',');
}

function display_grid() {
    const { minY, maxY, minX, maxX } = get_bounding_box();
    for (let y = minY - 1; y <= maxY + 1; y++) {
        const row: Array<string> = [];
        for (let x = minX - 1; x <= maxX + 1; x++) {
            const char = [
                posY === y && posX === x ? '[' : ' ',
                grid.get(key(y, x)) || CLEAN,
                posY === y && posX === x ? ']' : ' ',
            ];

            row.push(char.join(''));
        }
        console.log(row.join(''));
    }
}

function get_bounding_box(): { minY: number; maxY: number; minX: number; maxX: number } {
    const keys = [...grid.keys()];
    const minY = Math.min(...keys.map((k) => Number(k.split(',')[0])));
    const maxY = Math.max(...keys.map((k) => Number(k.split(',')[0])));
    const minX = Math.min(...keys.map((k) => Number(k.split(',')[1])));
    const maxX = Math.max(...keys.map((k) => Number(k.split(',')[1])));
    return { minY, maxY, minX, maxX };
}

function get_middle_position(): { posY: number; posX: number } {
    const { minY, maxY, minX, maxX } = get_bounding_box();
    return { posY: (maxY - minY) / 2, posX: (maxX - minX) / 2 };
}
