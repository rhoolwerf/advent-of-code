import { readFileSync } from 'fs';
import { sum } from '../util/util';
import { EOL } from 'os';

const TO_STRING_OFFSET = 2;

class Particle {
    constructor(
        public id: number,
        private posX: number,
        private posY: number,
        private posZ: number,
        private velX: number,
        private velY: number,
        private velZ: number,
        private accX: number,
        private accY: number,
        private accZ: number,
        public collided = false
    ) {}

    public process(): void {
        // Increase velocity based on acceleration
        this.velX += this.accX;
        this.velY += this.accY;
        this.velZ += this.accZ;

        // Change position based on velocity
        this.posX += this.velX;
        this.posY += this.velY;
        this.posZ += this.velZ;
    }

    get distance(): number {
        return sum([Math.abs(this.posX), Math.abs(this.posY), Math.abs(this.posZ)]);
    }

    get toString(): string {
        const padded = (i: number): string => String(i).padStart(TO_STRING_OFFSET, ' ');

        return [
            String(this.id).padStart(4, ' '),
            `p<${padded(this.posX)},${padded(this.posY)},${padded(this.posZ)}>`,
            `v<${padded(this.velX)},${padded(this.velY)},${padded(this.velZ)}>`,
            `a<${padded(this.accX)},${padded(this.accY)},${padded(this.accZ)}>`,
            this.distance,
        ].join(', ');
    }

    get positionHash(): string {
        return [this.posX, this.posY, this.posZ].join(',');
    }
}

// Parse input into Particle-instances
let particles = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) =>
        line
            .split(', ')
            .map((chunk) => chunk.match(/<(-?\d+),(-?\d+),(-?\d+)>/) as RegExpMatchArray)
            .map(([_, p, v, a]) => [p, v, a].map(Number))
    )
    .map(
        ([[pX, pY, pZ], [vX, vY, vZ], [aX, aY, aZ]], index) => new Particle(index, pX, pY, pZ, vX, vY, vZ, aX, aY, aZ)
    );

// Arbitrary amount of steps to simulate "in the long run"
for (let i = 0; i < 1e2; i++) {
    // Process each particle
    particles.forEach((p) => p.process());

    // Find all collisions
    for (let x = 0; x < particles.length; x++) {
        for (let y = x + 1; y < particles.length; y++) {
            if (particles[x].positionHash === particles[y].positionHash) {
                particles[x].collided = true;
                particles[y].collided = true;
            }
        }
    }

    // Remove all collided particles
    particles = particles.filter(({ collided }) => !collided);
}

console.log('Particles left:', particles.length);
