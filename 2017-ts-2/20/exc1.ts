import { readFileSync } from 'fs';
import { sum } from '../util/util';
import { EOL } from 'os';

const TO_STRING_OFFSET = 2;

class Particle {
    constructor(
        public id: number,
        private posX: number,
        private posY: number,
        private posZ: number,
        private velX: number,
        private velY: number,
        private velZ: number,
        private accX: number,
        private accY: number,
        private accZ: number
    ) {}

    public process(): void {
        // Increase velocity based on acceleration
        this.velX += this.accX;
        this.velY += this.accY;
        this.velZ += this.accZ;

        // Change position based on velocity
        this.posX += this.velX;
        this.posY += this.velY;
        this.posZ += this.velZ;
    }

    get distance(): number {
        return sum([Math.abs(this.posX), Math.abs(this.posY), Math.abs(this.posZ)]);
    }

    get toString(): string {
        const padded = (i: number): string => String(i).padStart(TO_STRING_OFFSET, ' ');

        return [
            String(this.id).padStart(4, ' '),
            `p<${padded(this.posX)},${padded(this.posY)},${padded(this.posZ)}>`,
            `v<${padded(this.velX)},${padded(this.velY)},${padded(this.velZ)}>`,
            `a<${padded(this.accX)},${padded(this.accY)},${padded(this.accZ)}>`,
            this.distance,
        ].join(', ');
    }
}

// Parse input into Particle-instances
const particles = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) =>
        line
            .split(', ')
            .map((chunk) => chunk.match(/<(-?\d+),(-?\d+),(-?\d+)>/) as RegExpMatchArray)
            .map(([_, p, v, a]) => [p, v, a].map(Number))
    )
    .map(
        ([[pX, pY, pZ], [vX, vY, vZ], [aX, aY, aZ]], index) => new Particle(index, pX, pY, pZ, vX, vY, vZ, aX, aY, aZ)
    );
// Keep score of who is the closest for each step
const closest: Array<Particle> = [];

// Arbitrary amount of steps to simulate "in the long run"
for (let i = 0; i < 1e4; i++) {
    // Process each particle
    particles.forEach((p) => p.process());

    // Keep track of which is currently closest
    closest.push(
        particles.reduce(
            (closest, p) => (p.distance < closest.distance ? p : closest),
            // As a reference, start infinitely far away
            new Particle(-1, Infinity, Infinity, Infinity, Infinity, Infinity, Infinity, Infinity, Infinity, Infinity)
        )
    );
}

const closestCount = closest.reduce(
    (count, p) => {
        if (!(p.id in count)) count[p.id] = 0;
        count[p.id]++;
        return count;
    },
    {} as { [key: number]: number }
);

const closestRanking = Object.keys(closestCount)
    .map((key) => ({ p: key, v: closestCount[Number(key)] }))
    .sort((a, b) => b.v - a.v);

console.log(closestRanking);
