import { readFileSync } from 'fs';
import { EOL } from 'os';

const registers: { [key: string]: number } = {};
let audio = 0;

const input = readFileSync(process.argv[2], 'utf8').split(EOL);

for (let i = 0; i < input.length; i++) {
    const instr = input[i];
    // console.log({ i, instr });
    const [cmd, r, r_or_v] = instr.split(' ');
    const v = isNaN(Number(r_or_v)) ? registers[r_or_v] : Number(r_or_v);

    switch (cmd) {
        case 'snd':
            audio = registers[r];
            break;

        case 'set':
            registers[r] = v;
            break;

        case 'add':
            registers[r] += v;
            break;

        case 'mul':
            registers[r] *= v;
            break;

        case 'mod':
            registers[r] %= v;
            break;

        case 'rcv':
            if (registers[r] && registers[r] > 0) {
                console.log('Recovering audio with frequency', audio);
                process.exit();
            }
            break;

        case 'jgz':
            if (registers[r] > 0) i += v - 1;
            break;

        default:
            console.error('Unknown instruction:', instr);
            process.exit(-1);
    }

    // console.log({ registers, audio, i });
}
