import { readFileSync } from 'fs';
import { EOL } from 'os';
import { sum } from '../util/util';

const numbers = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((row) =>
        row
            .split(/\t| /)
            .map(Number)
            .sort((a, b) => b - a)
    )
    .map((row) => {
        for (let i = 0; i < row.length; i++) {
            for (let j = i + 1; j < row.length; j++) {
                const p1 = row[i];
                const p2 = row[j];
                const div = p1 / p2;
                if (div === Math.floor(div)) {
                    return { p1, p2, div };
                }
            }
        }
        return { p1: Infinity, p2: Infinity, div: Infinity };
    });

console.log(sum(numbers.map(({ div }) => div)));
