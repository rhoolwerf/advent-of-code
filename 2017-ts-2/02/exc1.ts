import { readFileSync } from 'fs';
import { EOL } from 'os';
import { sum } from '../util/util';

const numbers = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((row) => row.split(/\t| /).map(Number))
    .map((row) => {
        const max = Math.max(...row);
        const min = Math.min(...row);
        const diff = max - min;
        return { min, max, diff };
    });

console.log(sum(numbers.map(({ diff }) => diff)));
