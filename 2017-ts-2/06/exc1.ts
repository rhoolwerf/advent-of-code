import { readFileSync } from 'fs';

function find_repeat(input: string): number {
    const banks = input.split(/\s/).map(Number);
    const distributions = new Set<string>();

    let step = 0;
    for (; !distributions.has(banks.join(',')); step++) {
        distributions.add(banks.join(','));

        let value = Math.max(...banks);
        let index = banks.indexOf(value);
        banks[index] = 0;
        while (value-- > 0) {
            index = (index + 1) % banks.length;
            banks[index]++;
        }
    }

    return step;
}

function test(input: string, expected: number): void {
    const actual = find_repeat(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

test('0 2 7 0', 5);

console.log(find_repeat(readFileSync('./input', 'utf8')));
