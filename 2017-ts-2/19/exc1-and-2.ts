import { readFileSync } from 'fs';
import { EOL } from 'os';

const grid = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) => line.split(''));

const characters: Array<string> = [];

// Find starting position
let posY = 1;
let posX = grid[posY].indexOf('|');
let dirY = 1;
let dirX = 0;

let steps = 0;
for (let i = 0; true; i++) {
    // Keep doing in the same direction untill we find a turn (signalled by '+')
    while (grid[posY][posX] !== '+') {
        // Where are we now, is it any special?
        if (grid[posY][posX] !== '+' && grid[posY][posX] !== '-' && grid[posY][posX] !== '|') {
            console.log('Found character at', { posY, posX, character: grid[posY][posX] });
            characters.push(grid[posY][posX]);
        }

        // Make the step
        posY += dirY;
        posX += dirX;
        steps++;

        // If we've just stepped into space, we're at the end of our rope
        if (grid[posY][posX] === ' ') {
            console.log("We've reached the end, which took us", { steps });
            console.log(characters.join(''));
            process.exit(-1);
        }
    }

    const test = (newDirY: number, newDirX: number): boolean => {
        // Test if attempted position is a space or not?
        if (grid[posY + newDirY][posX + newDirX] === ' ') return false;

        // Test if attempted direction is the same as current direction?
        if (newDirY === dirY && newDirX === dirX) return false;

        // Test if the attempted direction is where we came from?
        if (newDirY === dirY * -1 && newDirX === dirX * -1) return false;

        // All checks succeeded, we are happy!
        return true;
    };

    // We've encountered a turn, where do we want to go next?
    console.log('Turn at', { posY, posX });
    [dirY, dirX] = [
        // Down
        test(-1, 0) ? [-1, 0] : null,

        // Right
        test(0, 1) ? [0, 1] : null,

        // Up
        test(1, 0) ? [1, 0] : null,

        // Left
        test(0, -1) ? [0, -1] : null,
    ].filter(Boolean)[0] as [number, number];
    console.log('New direction:', { dirY, dirX }, 'forcing the first step');
    posY += dirY;
    posX += dirX;
    steps++;

    console.log('\n');
}
