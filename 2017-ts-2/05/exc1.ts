import { readFileSync } from 'fs';
import { EOL } from 'os';

const instructions = readFileSync(process.argv[2], 'utf8').split(EOL).map(Number);
let index = 0;

let steps = 0;
for (; index >= 0 && index < instructions.length; steps++) {
    const offset = instructions[index];
    instructions[index] += 1;
    index += offset;
}
console.log({ steps });
