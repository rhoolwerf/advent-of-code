import { readFileSync } from 'fs';
import createGraph from 'ngraph.graph';
import { aStar } from 'ngraph.path';
import { EOL } from 'os';

const g = createGraph();
const nodes = new Set<string>();

readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .forEach((line) => {
        const [source, targets] = line.split(' <-> ');
        nodes.add(source);
        targets.split(', ').forEach((target) => g.addLink(source, target));
    });

const pathFinder = aStar(g);

console.log(Array.from(nodes).filter((node) => pathFinder.find('0', node).length > 0).length);
