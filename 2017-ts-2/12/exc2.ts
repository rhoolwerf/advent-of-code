import { readFileSync } from 'fs';
import createGraph from 'ngraph.graph';
import { aStar } from 'ngraph.path';
import { EOL } from 'os';

const g = createGraph();
const nodes = new Set<string>();

readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .forEach((line) => {
        const [source, targets] = line.split(' <-> ');
        nodes.add(source);
        targets.split(', ').forEach((target) => g.addLink(source, target));
    });

const pathFinder = aStar(g);

const groups = new Set<string>('0');

Array.from(nodes).forEach((node) => {
    let inGroup = Array.from(groups).some((group) => pathFinder.find(node, group).length > 0);
    if (!inGroup) {
        groups.add(node);
    }
});

console.log(groups.size);
