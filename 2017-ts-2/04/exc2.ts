import { readFileSync } from 'fs';
import { EOL } from 'os';

function is_valid(input: string): boolean {
    const parts = input.split(' ').map((part) => part.split('').sort().join(''));
    const unique = new Set(parts);
    return parts.length === unique.size;
}

function test(input: string, expected: boolean): void {
    const actual = is_valid(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

test('abcde fghij', true);
test('abcde xyz ecdab', false);
test('a ab abc abd abf abj', true);
test('iiii oiii ooii oooi oooo', true);
test('oiii ioii iioi iiio', false);

console.log(readFileSync('./input', 'utf8').split(EOL).map(is_valid).filter(Boolean).length);
