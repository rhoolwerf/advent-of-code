import { readFileSync } from 'fs';
import { EOL } from 'os';

function is_valid(input: string): boolean {
    const parts = input.split(' ');
    const unique = new Set(parts);
    return parts.length === unique.size;
}

function test(input: string, expected: boolean): void {
    const actual = is_valid(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

test('aa bb cc dd ee', true);
test('aa bb cc dd aa', false);
test('aa bb cc dd aaa', true);

console.log(readFileSync('./input', 'utf8').split(EOL).map(is_valid).filter(Boolean).length);
