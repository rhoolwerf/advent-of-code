const input = Number(process.argv[2]) || 371;

const memory = [0];

let pos = 0;
for (let i = 1; i <= 2017; i++) {
    for (let j = 0; j < input; j++) {
        pos = (pos + 1) % memory.length;
    }

    memory.splice(pos + 1, memory.length, i, ...memory.slice(pos + 1));
    pos++;
}

// console.log(
//     memory
//         .map((m) => String(m).padStart(String(Math.max(...memory)).length, ' '))
//         .map((m, i) => (i === pos ? `(${m})` : ` ${m} `))
//         .join(' ')
// );

console.log('Value after 2017 cycles', memory[pos + 1]);
