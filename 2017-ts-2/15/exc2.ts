import { readFileSync } from 'fs';
import { EOL } from 'os';

let [genA, genB] = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((row) => Number(row.split(' ').slice(-1)[0]));

let pairs = 0;
for (let i = 0; i < 5 * 1e6; i++) {
    do {
        genA = (genA * 16807) % 2147483647;
    } while (genA % 4 !== 0);
    do {
        genB = (genB * 48271) % 2147483647;
    } while (genB % 8 !== 0);

    const binA = genA.toString(2).padStart(16, '0').slice(-16);
    const binB = genB.toString(2).padStart(16, '0').slice(-16);

    if (binA === binB) pairs++;
}
console.log({ pairs });
