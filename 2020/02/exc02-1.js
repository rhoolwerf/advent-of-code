const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n").map(x => {
    return {
        min: Number(x.split(" ")[0].split("-")[0]),
        max: Number(x.split(" ")[0].split("-")[1]),
        find: x.split(" ")[1].split(":")[0],
        string: x.split(" ")[2]
    }
});

let iCountValid = 0;

aInput.forEach(oEntry => {
    let iCount = 0;
    for (let i = 0; i < oEntry.string.length; i++) {
        if (oEntry.string.charAt(i) === oEntry.find) {
            iCount++;
        }
    }
    if (oEntry.min <= iCount && iCount <= oEntry.max) {
        iCountValid++;
    }
});
console.log(iCountValid);