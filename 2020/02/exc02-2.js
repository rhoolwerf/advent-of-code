const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n").map(x => {
    return {
        min: Number(x.split(" ")[0].split("-")[0]),
        max: Number(x.split(" ")[0].split("-")[1]),
        find: x.split(" ")[1].split(":")[0],
        string: x.split(" ")[2]
    }
});

let iCountValid = 0;

aInput.forEach(oEntry => {
    if (oEntry.string.charAt(oEntry.min - 1) === oEntry.find || oEntry.string.charAt(oEntry.max - 1) === oEntry.find) {
        if (oEntry.string.charAt(oEntry.min - 1) !== oEntry.string.charAt(oEntry.max - 1)) {
            iCountValid++;
        }
    }
});
console.log(iCountValid);