const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n");

let aState = [];
aInput.forEach(sInput => {
    aState.push(sInput.split(""));
});
// output_state();
let iOccupiedSeatCount = count_occupied_seats();

for (let iGeneration = 0; true; iGeneration++) {
    let aNewState = [];
    for (let iY = 0; iY < aState.length; iY++) {
        aNewState.push([]);
        for (let iX = 0; iX < aState[iY].length; iX++) {
            let oAdjacentSeats = count_adjacent_seats(iY, iX);
            if (aState[iY][iX] === "L" && oAdjacentSeats.occupied === 0) { aNewState[iY][iX] = "#"; }
            else if (aState[iY][iX] === "#" && oAdjacentSeats.occupied >= 5) { aNewState[iY][iX] = "L"; }
            else { aNewState[iY][iX] = aState[iY][iX]; }
        }
    }
    aState = aNewState;
    // output_state();
    let iNewOccupiedSeatCount = count_occupied_seats();
    if (iNewOccupiedSeatCount === iOccupiedSeatCount) {
        console.log(iOccupiedSeatCount);
        break;
    } else {
        iOccupiedSeatCount = iNewOccupiedSeatCount;
    }
}

function count_adjacent_seats(iY, iX) {
    let aAdjacentSeats = [];
    aAdjacentSeats.push(get_seat_in_direction(iY, iX, -1, -1)); // North-West
    aAdjacentSeats.push(get_seat_in_direction(iY, iX, -1, 0)); // North
    aAdjacentSeats.push(get_seat_in_direction(iY, iX, -1, 1)); // North-East
    aAdjacentSeats.push(get_seat_in_direction(iY, iX, 0, 1)); // East
    aAdjacentSeats.push(get_seat_in_direction(iY, iX, 1, 1)); // South-East
    aAdjacentSeats.push(get_seat_in_direction(iY, iX, 1, 0)); // South
    aAdjacentSeats.push(get_seat_in_direction(iY, iX, 1, -1)); // South-West
    aAdjacentSeats.push(get_seat_in_direction(iY, iX, 0, -1)); // West

    return aAdjacentSeats.reduce((oCount, sSeat) => {
        let sField = (sSeat === "#" ? "occupied" : "empty");
        oCount[sField]++;
        return oCount;
    }, { occupied: 0, empty: 0 });
}

function get_seat_in_direction(iStartY, iStartX, iOffsetY, iOffsetX) {
    let iY = iStartY;
    let iX = iStartX;
    while (true) {
        iY += iOffsetY;
        iX += iOffsetX;
        switch (get_point(iY, iX)) {
            case ".":
                // Floor tile? Keep looking
                continue;
                break;
            // Left the room? Return nothing
            case "?":
                return "";
                breaj;
            default:
                // We've either found an empty or occupied seat, return it
                // console.log(iStartY, iStartX, iOffsetY, iOffsetX, iY, iX, get_point(iY, iX));
                return get_point(iY, iX);
        }
    }
}

function get_point(iY, iX) {
    if (aState[iY] === undefined) { return "?"; }
    else if (aState[iY][iX] === undefined) { return "?"; }
    else { return aState[iY][iX]; }
}

function output_state() {
    console.log(aState.map(x => x.join("")).join("\n"), "\n");
}

function count_occupied_seats() {
    let iCount = 0;
    aState.forEach(aRow => {
        aRow.forEach(sSeat => {
            if (sSeat === "#") {
                iCount++;
            }
        });
    });
    return iCount;
}