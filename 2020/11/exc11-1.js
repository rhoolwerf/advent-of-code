const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n");

let aState = [];
aInput.forEach(sInput => {
    aState.push(sInput.split(""));
});
// output_state();
let iOccupiedSeatCount = count_occupied_seats();

for (let iGeneration = 0; true; iGeneration++) {
    let aNewState = [];
    for (let iY = 0; iY < aState.length; iY++) {
        aNewState.push([]);
        for (let iX = 0; iX < aState[iY].length; iX++) {
            let oAdjacentSeats = count_adjacent_seats(iY, iX);
            if (aState[iY][iX] === "L" && oAdjacentSeats.occupied === 0) { aNewState[iY][iX] = "#"; }
            else if (aState[iY][iX] === "#" && oAdjacentSeats.occupied >= 4) { aNewState[iY][iX] = "L"; }
            else { aNewState[iY][iX] = aState[iY][iX]; }
        }
    }
    aState = aNewState;
    // output_state();
    let iNewOccupiedSeatCount = count_occupied_seats();
    if (iNewOccupiedSeatCount === iOccupiedSeatCount) {
        console.log(iOccupiedSeatCount);
        break;
    } else {
        iOccupiedSeatCount = iNewOccupiedSeatCount;
    }
}

function count_adjacent_seats(iPosY, iPosX) {
    let oAdjacentSeats = {
        occupied: 0,
        empty: 0
    }

    for (let iY = Math.max(0, iPosY - 1); iY <= Math.min(aState.length - 1, iPosY + 1); iY++) {
        for (let iX = Math.max(0, iPosX - 1); iX <= Math.min(aState[iY].length - 1, iPosX + 1); iX++) {
            if (iPosY !== iY || iPosX !== iX) {
                if (aState[iY][iX] === "L") { oAdjacentSeats.empty++; }
                else if (aState[iY][iX] === "#") { oAdjacentSeats.occupied++; }
            }
        }
    }

    return oAdjacentSeats;
}


function output_state() {
    console.log(aState.map(x => x.join("")).join("\n"), "\n");
}

function count_occupied_seats() {
    let iCount = 0;
    aState.forEach(aRow => {
        aRow.forEach(sSeat => {
            if (sSeat === "#") {
                iCount++;
            }
        });
    });
    return iCount;
}