const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n").map(Number);

const iPreamble = Number(process.argv[3]);
if (iPreamble === 0 || isNaN(iPreamble)) { console.log("Give a proper preamble"); return; }

for (let i = iPreamble; i < aInput.length; i++) {
    let bFound = false;
    for (let j = i - iPreamble; j < i; j++) {
        for (let k = j + 1; k < i; k++) {
            if (aInput[j] + aInput[k] === aInput[i]) {
                bFound = true;
            }
        }
    }

    if (!bFound) {
        console.log("No sum found for", aInput[i]);
    }
}