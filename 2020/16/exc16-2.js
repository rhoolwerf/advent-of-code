const sInputFile = process.argv[2];
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n\n");

const aNearbyTickets = aInput[2].split(":")[1].trim().split("\n").map(x => x.split(",").map(Number));
const aValidation = aInput[0].split("\n").map(x => { return { p: x.split(":")[0], c: x.split(":")[1].trim().split(" or ").map(y => { return { min: Number(y.split("-")[0]), max: Number(y.split("-")[1]) } }) } });
const aTicket = aInput[1].split("\n")[1].split(",").map(Number);

Number.prototype.between = function (a, b) {
    var min = Math.min.apply(Math, [a, b]),
        max = Math.max.apply(Math, [a, b]);
    return this >= min && this <= max;
};

function find_corrupt_tickets() {
    // Find any tickets that have no validation at all, we need to remove these tickets
    let oCorruptTickets = new Set();
    let aInvalidValue = [];
    aNearbyTickets.forEach((aNearbyTicket, iIndex) => {
        for (let iTicketValueIndex = 0; iTicketValueIndex < aNearbyTicket.length; iTicketValueIndex++) {
            let iTicketValue = aNearbyTicket[iTicketValueIndex];
            let bValueValid = false;
            for (let iValidationIndex = 0; iValidationIndex < aValidation.length; iValidationIndex++) {
                let oValidation = aValidation[iValidationIndex];
                if (iTicketValue.between(oValidation.c[0].min, oValidation.c[0].max) || iTicketValue.between(oValidation.c[1].min, oValidation.c[1].max)) {
                    bValueValid = true;
                }
            }
            if (!bValueValid) {
                aInvalidValue.push(iTicketValue);
                oCorruptTickets.add(iIndex);
            }
        }
    });
    // console.log("Invalid ticket count:", oCorruptTickets);//.length, 
    // console.log("ticket scanning error rate:", aInvalidValue.reduce((acc, curr) => acc += curr, 0));
    return oCorruptTickets;
}


// Find a remove corrupt tickets
let oCorruptTickets = find_corrupt_tickets();
let aCorruptTickets = Array.from(oCorruptTickets);
aCorruptTickets.sort((a, b) => b - a); // Descending
aCorruptTickets.forEach(iCorruptTicketIndex => {
    aNearbyTickets.splice(iCorruptTicketIndex, 1);
});

// Now that we only have decent tickets remaining, try to build fieldmapping

let aTicketIndex = [];
// For a validation, find which columns it is valid for
aValidation.forEach(oValidation => {
    for (let iTicketIndex = 0; iTicketIndex < aNearbyTickets[0].length; iTicketIndex++) {
        let bAllValid = true;
        for (let iTicket = 0; iTicket < aNearbyTickets.length && bAllValid; iTicket++) {
            let iNumber = Number(aNearbyTickets[iTicket][iTicketIndex]);
            if (iNumber.between(oValidation.c[0].min, oValidation.c[0].max) || iNumber.between(oValidation.c[1].min, oValidation.c[1].max)) {
                // Valid
            } else {
                // Invalid
                bAllValid = false;
            }
        }
        if (bAllValid) {
            if (aTicketIndex[iTicketIndex] === undefined) { aTicketIndex[iTicketIndex] = []; }
            aTicketIndex[iTicketIndex].push(oValidation.p);
        }
    }
});

// Now we need to thin the list
let oCleanedUp = new Set();
while (aTicketIndex.flat().length !== aValidation.length) {
    for (let i = 0; i < aTicketIndex.length; i++) {
        if (aTicketIndex[i].length === 1) {
            let sValueToRemove = aTicketIndex[i][0];
            if (!oCleanedUp.has(sValueToRemove)) {
                oCleanedUp.add(sValueToRemove);
                for (let j = 0; j < aTicketIndex.length; j++) {
                    if (j !== i) {
                        aTicketIndex[j] = aTicketIndex[j].filter((v) => { return v !== sValueToRemove });
                    }
                }
            }
        }
    }
}

// Finally, calculate the answer
let aAnswerValues = [];
for (let i = 0; i < aTicketIndex.length; i++) {
    if (aTicketIndex[i][0].includes("departure")) {
        aAnswerValues.push(aTicket[i]);
    }
}
console.log("Puzzle input:", aAnswerValues.reduce((acc, curr) => { return acc * curr; }, 1));