const sInputFile = process.argv[2];
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n\n");

const aNearbyTickets = aInput[2].split(":")[1].trim().split("\n").join(",").split(",").map(Number);
const aValidation = aInput[0].split("\n").map(x => x.split(":")[1].trim().split(" or ").map(y => { return { min: Number(y.split("-")[0]), max: Number(y.split("-")[1]) } })).flat();

let iInvalidSum = 0;
aNearbyTickets.forEach(iTicket => {
    let bValid = false;
    aValidation.forEach(oValidation => {
        if (oValidation.min <= iTicket && iTicket <= oValidation.max) {
            bValid = true;
        }
    });
    if (!bValid) {
        iInvalidSum += iTicket;
    }
});
console.log(iInvalidSum);