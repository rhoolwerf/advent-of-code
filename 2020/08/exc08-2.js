const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n");

for (let i = 0; i < aInput.length; i++) {
    let aInputCopy = Array.from(aInput);
    if (aInput[i].split(" ")[0] === "nop") {
        aInputCopy[i] = "jmp " + aInput[i].split(" ")[1];
    } else if (aInput[i].split(" ")[0] === "jmp") {
        aInputCopy[i] = "nop " + aInput[i].split(" ")[1];
    } else {
        continue;
    }

    let [bResult, iAcc] = calc(aInputCopy);
    if (bResult) {
        console.log(iAcc);
        return;
    }
}

function calc(aInput) {
    let oSet = new Set();
    let iAcc = 0;

    for (let i = 0; i < aInput.length;) {
        if (oSet.has(i)) {
            return [false, 0];
        } else {
            oSet.add(i);
        }

        let aChunks = aInput[i].split(" ");
        switch (aChunks[0]) {
            case "nop":
                i++;
                break;
            case "acc":
                iAcc += Number(aChunks[1]);
                i++;
                break;
            case "jmp":
                i += Number(aChunks[1]);
                break;
            default:
                console.log("Unknown operation:", aInput[i]);
                return;
                break;
        }
    }

    return [true, iAcc];
}