const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n");

calc(aInput);

function calc(aInput) {
    let oSet = new Set();
    let iAcc = 0;

    for (let i = 0; i < aInput.length;) {
        if (oSet.has(i)) {
            console.log("Instruction", i, "encountered twice, exiting!\nAccumulator is:", iAcc);
            break;
        } else {
            oSet.add(i);
        }

        let aChunks = aInput[i].split(" ");
        switch (aChunks[0]) {
            case "nop":
                i++;
                break;
            case "acc":
                iAcc += Number(aChunks[1]);
                i++;
                break;
            case "jmp":
                i += Number(aChunks[1]);
                break;
            default:
                console.log("Unknown operation:", aInput[i]);
                return;
                break;
        }
    }
}