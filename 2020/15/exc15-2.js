// simulate([0, 3, 6], 30000000);
// simulate([1, 3, 2], 30000000);
// simulate([2, 1, 3], 30000000);
// simulate([1, 2, 3], 30000000);
// simulate([2, 3, 1], 30000000);
// simulate([3, 2, 1], 30000000);
// simulate([3, 1, 2], 30000000);
simulate([0, 6, 1, 7, 2, 19, 20], 30000000);

function simulate(aInput, iTurnCount) {
    let aInputLocal = Array.from(aInput);

    let oMap = new Map();

    let iLastNumberSpoken = 0;
    for (let iTurn = 1; iTurn <= iTurnCount; iTurn++) {
        // If there is something in the input remaining, say that
        if (aInputLocal.length > 0) {
            iLastNumberSpoken = aInputLocal.shift();
        } else {
            // If the last spoken number was said for the first time, say 0
            if (oMap.get(iLastNumberSpoken).length === 1) {
                iLastNumberSpoken = 0;
            } else {
                // Say the difference between the last two times the number has been spoken
                iLastNumberSpoken = oMap.get(iLastNumberSpoken)[0] - oMap.get(iLastNumberSpoken)[1];
            }
        }

        // console.log("Turn", iTurn, "says", iLastNumberSpoken);

        if (!oMap.has(iLastNumberSpoken)) { oMap.set(iLastNumberSpoken, [iTurn]); }
        else { oMap.set(iLastNumberSpoken, [iTurn, oMap.get(iLastNumberSpoken)[0]]); }
    }

    console.log("Given the starting numbers", aInput, "the", iTurnCount, "number spoken is", iLastNumberSpoken);
}

