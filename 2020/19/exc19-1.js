const sInputFile = process.argv[2];
const fs = require("fs");
const [aInput, aMessages] = fs.readFileSync(sInputFile, "utf8").split(/\r?\n\r?\n/).map(x => x.split(/\r?\n/));

const oRules = new Map();
const oRuleBuffer = new Map();
aInput.forEach(sInput => {
    const [sRuleId, sRule] = sInput.split(": ");
    oRules.set(sRuleId, sRule.trim().replace(/"/ig, ""));
});
console.log(oRules);

const sFinalRule = "^" + get_rule('0') + "$";
const oRegExp = new RegExp(sFinalRule);
console.log(sFinalRule);

if (aMessages) {
    let iCountValid = 0;
    aMessages.forEach(sMessage => {
        if (oRegExp.test(sMessage)) {
            iCountValid++;
        }
    });
    console.log("Valid messages:", iCountValid);
}

function get_rule(sRuleId) {
    if (!oRuleBuffer.has(sRuleId)) {
        const sRule = oRules.get(sRuleId);
        const aResponse = [];
        sRule.split(" | ").forEach(sRuleOr => {
            let sRuleOrComp = "";
            sRuleOr.trim().split(" ").forEach(sRuleBit => {
                if (isNaN(Number(sRuleBit))) {
                    sRuleOrComp += sRuleBit;
                } else {
                    sRuleOrComp += get_rule(sRuleBit);
                }
            });
            aResponse.push(sRuleOrComp);
        });

        const sResponse = (aResponse.length > 1) ? "(" + aResponse.join("|") + ")" : aResponse[0];
        oRuleBuffer.set(sRuleId, sResponse);
    }

    return oRuleBuffer.get(sRuleId);
}