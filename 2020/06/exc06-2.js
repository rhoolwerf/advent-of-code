const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n\n");

let iSum = 0;
aInput.forEach(aGroup => {
    let iPersonCount = 0;
    let oMap = new Map();
    aGroup.split("\n").forEach(sPerson => {
        iPersonCount++;
        sPerson.split("").forEach(sAnswer => {
            if (!oMap.has(sAnswer)) { oMap.set(sAnswer, 1); }
            else { oMap.set(sAnswer, oMap.get(sAnswer) + 1); }
        });
    });

    for (oEntry of oMap) {
        if (oEntry[1] === iPersonCount) {
            iSum++;
        }
    }
});
console.log(iSum);