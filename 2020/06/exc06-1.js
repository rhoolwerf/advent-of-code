const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n\n");

let aUniqueAnswers = [];
aInput.forEach(sInput => {
    // All answers in a single string
    let sAnswers = sInput.split("\n").join("");

    // Convert all answers to a Set (which makes answers unique)
    let oUniqueAnswers = new Set(sAnswers.split(""));
    aUniqueAnswers.push(oUniqueAnswers.size);
});
let iSum = aUniqueAnswers.reduce((a, b) => a + b, 0);
console.log(aUniqueAnswers.join(" + "), "=", iSum);
