const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n").map(x => { return { d: x.charAt(0), v: Number(x.substring(1)) } });

let iShipX = 0;
let iShipY = 0;
let iWaypointX = 10;
let iWaypointY = -1;

aInput.forEach(oInstruction => {
    if (oInstruction.d === "F") {
        // Move forward towards the waypoint X times
        for (let i = 0; i < oInstruction.v; i++) {
            iShipX += iWaypointX;
            iShipY += iWaypointY;
        }
        output_ship();
    } else if (oInstruction.d === "L" || oInstruction.d === "R") {
        // Based on https://calcworkshop.com/transformations/rotation-rules/#:~:text=90%20Degree%20Rotation,y%20and%20make%20y%20negative.
        let iDegrees = (oInstruction.d === "R" ? 360 - oInstruction.v : oInstruction.v) / 90;
        while (iDegrees-- > 0) {
            [iWaypointX, iWaypointY] = [iWaypointY, -1 * iWaypointX];
        }
        output_waypoint();
    } else {
        switch (oInstruction.d) {
            case "N":
                iWaypointY -= oInstruction.v;
                break;
            case "S":
                iWaypointY += oInstruction.v;
                break;
            case "W":
                iWaypointX -= oInstruction.v;
                break;
            case "E":
                iWaypointX += oInstruction.v;
                break;
        }
        output_waypoint();
    }
});
console.log(Math.abs(iShipY), "+", Math.abs(iShipX), "=", (Math.abs(iShipY) + Math.abs(iShipX)));

function output_ship() {
    console.log("Ship is at", Math.abs(iShipX), "units", iShipX > 0 ? "east" : "west", "and", Math.abs(iShipY), "units", iShipY > 0 ? "south" : "north");
}
function output_waypoint() {
    console.log("Waypoint is at", Math.abs(iWaypointX), "units", iWaypointX > 0 ? "east" : "west", "and", Math.abs(iWaypointY), "units", iWaypointY > 0 ? "south" : "north");
}