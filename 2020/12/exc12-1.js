const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n").map(x => { return { d: x.charAt(0), v: Number(x.substring(1)) } });
const aFacing = ["E", "S", "W", "N"];

let sFacing = "E";
let iX = 0;
let iY = 0;

aInput.forEach(oInstruction => {
    if (oInstruction.d === "L" || oInstruction.d === "R") {
        let iDegrees = (oInstruction.d === "L") ? 360 - oInstruction.v : oInstruction.v; // Determine how many degrees to turn right
        let iSteps = iDegrees / 90; // Determine how many steps that takes
        while (iSteps-- > 0) { // Adjust the facing-array for the amount of steps we need
            aFacing.push(aFacing.shift());
        }
        sFacing = aFacing[0]; // Update the facing
    } else {
        // If the instruction is to move forwards, to step into the Facing Direction
        let sDirection = oInstruction.d;
        if (sDirection === "F") {
            sDirection = sFacing;
        }

        // Adjust coordinates
        switch (sDirection) {
            case "N":
                iY -= oInstruction.v;
                break;
            case "S":
                iY += oInstruction.v;
                break;
            case "W":
                iX -= oInstruction.v;
                break;
            case "E":
                iX += oInstruction.v;
                break;
        }
    }
});
console.log(Math.abs(iY), Math.abs(iX), (Math.abs(iY) + Math.abs(iX)));