const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n");

let sMask = "";
let aMem = [];

aInput.forEach(sInstruction => {
    if (sInstruction.substring(0, 4) === "mask") {
        sMask = sInstruction.split(" = ")[1]
        console.log("Setting mask to:", sMask);
    } else {
        let iMemAddress = Number(sInstruction.split("=")[0].trim().split("[")[1].split("]")[0]);
        let iValue = Number(sInstruction.split("=")[1].trim());
        let sBinary = iValue.toString(2);
        while (sBinary.length < sMask.length) { sBinary = "0" + sBinary; }

        let sNewValue = "";
        for (let i = sBinary.length; i >= 0; i--) {
            if (sMask.charAt(i) === "X") { sNewValue = sBinary.charAt(i) + sNewValue; }
            else { sNewValue = sMask.charAt(i) + sNewValue; }
        }
        console.log(iMemAddress, iValue, sBinary, sNewValue, parseInt(sNewValue, 2));
        aMem[iMemAddress] = parseInt(sNewValue, 2);
    }
});
console.log("Puzzle answer:", aMem.reduce((acc, curr) => acc += curr, 0));