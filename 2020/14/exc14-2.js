const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n");

let sMask = "";
let aMem = [];

aInput.forEach(sInstruction => {
    if (sInstruction.substring(0, 4) === "mask") {
        sMask = sInstruction.split(" = ")[1]
        // console.log("Setting mask to:", sMask);
    } else {
        let iMemAddress = Number(sInstruction.split("=")[0].trim().split("[")[1].split("]")[0]);
        let iValue = Number(sInstruction.split("=")[1].trim());
        let sBinary = iMemAddress.toString(2);
        while (sBinary.length < sMask.length) { sBinary = "0" + sBinary; }

        let sNewValue = "";
        for (let i = sMask.length; i >= 0; i--) {
            if (sMask.charAt(i) === "1") { sNewValue = "1" + sNewValue; }
            else if (sMask.charAt(i) === "X") { sNewValue = "X" + sNewValue; }
            else { sNewValue = sBinary.charAt(i) + sNewValue; }
        }
        // console.log(iMemAddress, iValue, sBinary, sNewValue);

        get_memory_addresses(sNewValue).forEach(sMemoryBinary => {
            aMem[parseInt(sMemoryBinary, 2)] = iValue;
        });

    }
});
let iSum = 0;
for (iIndex in aMem) {
    iSum += aMem[iIndex];
}
console.log("Puzzle answer:", iSum);

function get_memory_addresses(sAddress) {
    // Convert Address to Array-of-Arrays
    let aAddress = [];
    for (let i = 0; i < sAddress.length; i++) {
        if (sAddress.charAt(i) === "X") { aAddress.push(['0', '1']); }
        else { aAddress.push([sAddress.charAt(i)]); }
    }

    return allPossibleCases(aAddress);
}

// Get all possible combinations from array of arrays (cartesian product)
// Borrowed from: https://stackoverflow.com/a/4331218
function allPossibleCases(arr) {
    if (arr.length == 1) {
        return arr[0];
    } else {
        var result = [];
        var allCasesOfRest = allPossibleCases(arr.slice(1)); // recur with the rest of array
        for (var i = 0; i < allCasesOfRest.length; i++) {
            for (var j = 0; j < arr[0].length; j++) {
                result.push(arr[0][j] + allCasesOfRest[i]);
            }
        }
        return result;
    }

}