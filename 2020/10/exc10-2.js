const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n").map(Number);

aInput.sort((a, b) => a - b);
aInput.push(3 + aInput[aInput.length - 1]);
aInput.unshift(0);

let oMap = new Map();

for (let i = 0; i < aInput.length; i++) {
    oMap.set(aInput[i], []);
    for (let j = i + 1; aInput[j] - aInput[i] <= 3; j++) {
        oMap.get(aInput[i]).push(aInput[j]);
    }
}

let oRoutes = new Map();
for (let i = aInput.length - 1; i >= 0; i--) {
    calc_routes(i);
}
console.log(oRoutes.get(0));

function calc_routes(iIndex) {
    let iNum = aInput[iIndex];
    if (oMap.get(iNum).length === 0) { oRoutes.set(iNum, 1); }
    else {
        let iSum = 0;
        oMap.get(iNum).forEach(iSubNum => {
            iSum += oRoutes.get(iSubNum);
        });
        oRoutes.set(iNum, iSum);
    }
}