const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n").map(Number);

aInput.sort((a, b) => a - b);
aInput.push(3 + aInput[aInput.length - 1]);
aInput.unshift(0);

let oMap = new Map();

for (let i = 0; i < aInput.length - 1; i++) {
    let iDiff = aInput[i + 1] - aInput[i];
    if (!oMap.has(iDiff)) { oMap.set(iDiff, 1); }
    else { oMap.set(iDiff, oMap.get(iDiff) + 1); }
}
console.log("1:", oMap.get(1), ", 3:", oMap.get(3), ", ", (oMap.get(1) * oMap.get(3)));

