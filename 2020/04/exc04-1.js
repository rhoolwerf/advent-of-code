const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n\n");

let iCountValid = 0;
aInput.forEach(sPassword => {
    aPassport = sPassword.split("\n").join(" ").split(" ");
    oPassport = {};
    aPassport.forEach(sBit => {
        oPassport[sBit.split(":")[0]] = sBit.split(":")[1];
    });
    if (is_valid(oPassport)) {
        iCountValid++;
    }
});
console.log(iCountValid);

function is_valid(oPassport) {
    if (oPassport.byr && oPassport.iyr && oPassport.eyr && oPassport.hgt && oPassport.hcl && oPassport.ecl && oPassport.pid) {
        return true;
    } else {
        return false;
    }
}