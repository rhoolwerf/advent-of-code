const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n\n");

let iCountValid = 0;
let re = new RegExp('#[a-f0-9]{6}');

aInput.forEach(sPassword => {
    aPassport = sPassword.split("\n").join(" ").split(" ");
    oPassport = {};
    aPassport.forEach(sBit => {
        if (sBit.split(":")[0] !== "cid") {
            oPassport[sBit.split(":")[0]] = sBit.split(":")[1];
        }
    });
    if (is_valid(oPassport)) {
        iCountValid++;
    }
});
console.log("Valid passports:", iCountValid);

function is_valid(oPassport) {
    if (oPassport.byr && oPassport.iyr && oPassport.eyr && oPassport.hgt && oPassport.hcl && oPassport.ecl && oPassport.pid) {
        if (oPassport.byr < 1920 || oPassport.byr > 2002) {
            return false;
        } else if (oPassport.iyr < 2010 || oPassport.iyr > 2020) {
            return false;
        } else if (oPassport.eyr < 2020 || oPassport.eyr > 2030) {
            return false;
        } else if (!is_valid_height(oPassport.hgt)) {
            return false;
        } else if (!re.test(oPassport.hcl)) {
            return false;
        } else if (!is_valid_eye_color(oPassport.ecl)) {
            return false;
        } else if (oPassport.pid.length !== 9 || isNaN(Number(oPassport.pid))) {
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}

function is_valid_height(sHeight) {
    if (sHeight.includes("cm")) {
        if (sHeight.split("cm")[0] >= 150 && sHeight.split("cm")[0] <= 193) {
            return true;
        }
    } else if (sHeight.includes("in")) {
        if (sHeight.split("in")[0] >= 59 && sHeight.split("in")[0] <= 76) {
            return true;
        }
    }

    return false;
}

function is_valid_eye_color(sEyeColor) {
    switch (sEyeColor) {
        case "amb":
        case "blu":
        case "brn":
        case "gry":
        case "grn":
        case "hzl":
        case "oth":
            return true;
        default:
            return false;
            break;
    }
}