const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n");

let oMap = new Map();

aInput.forEach(sRow => {
    let aChunks = sRow.split("contain");

    let sBag = aChunks[0].split(" bags ")[0].trim();
    let aContents = aChunks[1].split(",").map(x => x.trim().split("bag")[0].trim());
    oMap.set(sBag, []);
    aContents.forEach(sContains => {
        if (sContains !== "no other") {
            oMap.get(sBag).push({
                amount: Number(sContains.split(" ")[0]),
                bag: sContains.split(" ").slice(1, 3).join(" ")
            });
        }
    });
});
console.log(calc("shiny gold") - 1);

function calc(sBag) {
    iSum = 1;

    oMap.get(sBag).forEach(oContents => {
        iSum += oContents.amount * calc(oContents.bag);
    });

    return iSum;
}