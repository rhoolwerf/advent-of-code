const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n");

let oMap = new Map();

aInput.forEach(sRow => {
    let aChunks = sRow.split("contain");

    let sBag = aChunks[0].split(" bags ")[0].trim();
    let aContents = aChunks[1].split(",").map(x => x.trim().split("bag")[0].trim().split(" ").slice(1, 3).join(" "));
    aContents.forEach(sContains => {
        if (!oMap.has(sContains)) { oMap.set(sContains, []); }
        oMap.get(sContains).push(sBag);
    });
});

let oParents = new Set();
find("shiny gold");
console.log(oParents.size);

function find(sBag) {
    if (oMap.has(sBag)) {
        oMap.get(sBag).forEach(sParent => {
            oParents.add(sParent);
            find(sParent);
        });
    }
}