const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n").map(Number);

for (let i = 0; i < aInput.length; i++) {
    for (let j = i + 1; j < aInput.length; j++) {
        if (aInput[i] + aInput[j] === 2020) {
            console.log(aInput[i], "*", aInput[j], "=", (aInput[i] * aInput[j]))
        }
    }
}
