const sInputFile = process.argv[2];
const fs = require("fs");
const aTilesInput = fs.readFileSync(sInputFile, "utf8").split(/\r?\n\r?\n/);
const aTiles = [];

aTilesInput.forEach(sTileData => {
    // Determine TileId and original Tile orientation
    const sTileId = sTileData.split(/\r?\n/)[0].split(" ")[1].split(":")[0];
    aTileData = sTileData.split(/\r?\n/).slice(1).map(x => x.split(""));

    // Build all possible orientations (both flipping and rotating)
    const aAllOrienations = [];

    for (let i = 0; i < 2; i++) {
        for (let j = 0; j < 4; j++) {
            aTileData = rotate(aTileData);

            // Register orientation and also its edges
            aAllOrienations.push({
                tile: deep_clone(aTileData),
                adjacentTiles: [],
                top: aTileData[0].join(""),
                left: aTileData.map(x => x[0]).join(""),
                right: aTileData.map(x => x[x.length - 1]).join(""),
                bottom: aTileData[aTileData.length - 1].join("")
            });
        }
        aTileData.reverse();
    }

    aTiles.push({
        TileId: sTileId,
        Variants: aAllOrienations,
        ProperVariant: null
    });
});

// For each tile, determine for each orientation how many adjacent tiles there are
for (let i = 0; i < aTiles.length; i++) {
    const oTile = aTiles[i];
    const aVariants = oTile.ProperVariant ? [oTile.ProperVariant] : oTile.Variants;

    for (let j = i + 1; j < aTiles.length; j++) {
        const oCheckTile = aTiles[j];
        const aCheckVariants = oCheckTile.ProperVariant ? [oCheckTile.ProperVariant] : oCheckTile.Variants;

        for (let x = 0; x < oTile.Variants.length; x++) {
            for (let y = 0; y < oCheckTile.Variants.length; y++) {
                const oVariant = oTile.Variants[x];
                const oCheckVariant = oCheckTile.Variants[y];
                if (oVariant.top === oCheckVariant.bottom || oVariant.bottom === oCheckVariant.top || oVariant.left === oCheckVariant.right || oVariant.right === oCheckVariant.left) {
                    // console.log(aTiles[i].TileId, aTiles[j].TileId);
                    if (!aTiles[i].ProperVariant) { aTiles[i].ProperVariant = aTiles[i].Variants[x]; }
                    if (!aTiles[j].ProperVariant) { aTiles[j].ProperVariant = aTiles[j].Variants[y]; }
                    aTiles[i].ProperVariant.adjacentTiles.push(aTiles[j].TileId);
                    aTiles[j].ProperVariant.adjacentTiles.push(aTiles[i].TileId);
                    y = 50;
                    x = 50;
                }
            }
        }
    }
}

aTiles.sort((a, b) => { return a.ProperVariant.adjacentTiles.length - b.ProperVariant.adjacentTiles.length });
let iSum = 1;
aTiles.forEach(oTile => {
    if (oTile.ProperVariant.adjacentTiles.length === 2) {
        iSum *= Number(oTile.TileId);
    }
    // console.log(oTile.TileId, oTile.ProperVariant.adjacentTiles);
});
console.log(iSum);

// Borrowed from https://stackoverflow.com/a/58668351
function rotate(matrix) {
    return matrix[0].map((val, index) => matrix.map(row => row[index]).reverse());
}

// Borrowed from https://dev.to/samanthaming/how-to-deep-clone-an-array-in-javascript-3cig
function deep_clone(array_of_arrays) {
    return JSON.parse(JSON.stringify(array_of_arrays));
}