const sInputFile = process.argv[2];
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n");

let iSum = 0;
aInput.forEach(sFormula => {
    let iValue = wrapper(sFormula);
    console.log(iValue);
    iSum += iValue;
});
console.log("Puzzle answer:", iSum);

function wrapper(sFormula) {
    return process_formula(sFormula.split(" ").join("").split("").map(x => isNaN(Number(x)) ? x : Number(x)));
}

function process_formula(aFormula) {
    // console.log("Formula:", aFormula);

    // First recursively remove all parentheses
    let iDepth = 0;
    let iStart = 0;
    for (let i = 0; i < aFormula.length; i++) {
        if (aFormula[i] === "(") {
            if (iDepth === 0) {
                iStart = i;
            }
            iDepth++;
        } else if (aFormula[i] === ")") {
            iDepth--;
            if (iDepth === 0) {
                // We have reached the end of an entire "priority group", recalc the formula
                let aRemaining = aFormula.splice(i + 1);
                let aSubFormula = aFormula.splice(iStart);
                aFormula.push(process_formula(aSubFormula.slice(1, aSubFormula.length - 1)));
                aFormula = aFormula.concat(aRemaining);
                // console.log("New formula:", aFormula);
                i = 0;
            }
        }
    }

    // Now that we only have normal numbers / operations left, just calculate it. Do this per bit-of-3, as we can't use normal eval (it'll use wrong precedence)
    while (aFormula.length > 1) {
        let aSubFormula = [aFormula.shift(), aFormula.shift(), aFormula.shift()];
        aFormula.unshift(eval(aSubFormula.join(" ")));
    }
    return aFormula[0];
}