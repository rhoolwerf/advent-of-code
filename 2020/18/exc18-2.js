const sInputFile = process.argv[2];
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n");

let iSum = 0;
aInput.forEach(sFormula => {
    let iValue = wrapper(sFormula);
    console.log(iValue);
    iSum += iValue;
});
console.log("Puzzle answer:", iSum);

function wrapper(sFormula) {
    return process_formula(sFormula.split(" ").join("").split("").map(x => isNaN(Number(x)) ? x : Number(x)));
}

function process_formula(aFormula) {
    // console.log("Formula:", aFormula);

    // First recursively remove all parentheses
    let iDepth = 0;
    let iStart = 0;
    for (let i = 0; i < aFormula.length; i++) {
        if (aFormula[i] === "(") {
            if (iDepth === 0) {
                iStart = i;
            }
            iDepth++;
        } else if (aFormula[i] === ")") {
            iDepth--;
            if (iDepth === 0) {
                // We have reached the end of an entire "priority group", recalc the formula
                let aRemaining = aFormula.splice(i + 1);
                let aSubFormula = aFormula.splice(iStart);
                aFormula.push(process_formula(aSubFormula.slice(1, aSubFormula.length - 1)));
                aFormula = aFormula.concat(aRemaining);
                // console.log("New formula:", aFormula);
                i = 0;
            }
        }
    }

    // First handle all additions, remove them from array and replace with addition
    for (let i = 1; i < aFormula.length - 1; i++) {
        if (aFormula[i] === "+") {
            // let iSum = aFormula[i - 1] + aFormula[i + 1];
            aFormula.splice(i - 1, 3, aFormula[i - 1] + aFormula[i + 1]);
            i -= 2; // Reconsider the previous position as we've adjusted the array
        }
    }

    // Then just evaluate the remaining formula
    return eval(aFormula.join(" "));
}