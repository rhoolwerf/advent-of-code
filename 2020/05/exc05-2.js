const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n");

aSeatIDs = [];
aInput.forEach(sInput => {
    aSeatIDs.push(get_seat_id(sInput));
});
aSeatIDs.sort((a, b) => { return a - b; });
for (let i = 0; i < aSeatIDs.length; i++) {
    if (aSeatIDs[i + 1] - aSeatIDs[i] > 1) {
        console.log(aSeatIDs[i] + 1);
    }
}

function get_seat_id(sPass) {
    let sBinary = "";
    for (let i = 0; i < sPass.length; i++) {
        if (sPass.charAt(i) === "R" || sPass.charAt(i) === "B") { sBinary += "1"; }
        else { sBinary += "0"; }
    }
    return parseInt(sBinary, 2);
}