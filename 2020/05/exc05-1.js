const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n");

// test("BFFFBBFRRR");
// test("FFFBBBFRRR");
// test("BBFFBBFRLL");

let iHighestSeatId = 0;
aInput.forEach(sInput => {
    let iSeatId = get_seat_id(sInput);
    if (iSeatId > iHighestSeatId) {
        iHighestSeatId = iSeatId;
    }
});
console.log(iHighestSeatId);


function test(sPass) {
    console.log(sPass, get_seat_id(sPass));
}

function get_seat_id(sPass) {
    let sBinary = "";
    for (let i = 0; i < sPass.length; i++) {
        if (sPass.charAt(i) === "R" || sPass.charAt(i) === "B") { sBinary += "1"; }
        else { sBinary += "0"; }
    }
    return parseInt(sBinary, 2);
}