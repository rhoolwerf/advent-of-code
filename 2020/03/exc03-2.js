const sInputFile = process.argv[2];
if (!sInputFile) { console.log("No input file given"); return; }
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n");


let aSlopes = [
    [1, 1],
    [1, 3],
    [1, 5],
    [1, 7],
    [2, 1]
]

let iTreeMultiplier = 1;
aSlopes.forEach(aSlope => {
    let iTrees = traverse(aSlope[0], aSlope[1]);
    console.log(aSlope[0], aSlope[1], iTrees);
    iTreeMultiplier *= iTrees;
});
console.log(iTreeMultiplier);


function traverse(iDownInput, iRightInput) {
    let iRight = 0;
    let iCountTrees = 0;
    for (let iDown = 0; iDown < aInput.length; iDown += iDownInput) {
        if (aInput[iDown].charAt(iRight) === "#") {
            iCountTrees++;
        }
        // console.log(iDown, iRight, aInput[iDown].charAt(iRight));
        iRight += iRightInput;
        iRight = iRight % aInput[iDown].length;
    }

    return iCountTrees;
}