const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n").map(x => x.split(","));

let aBus = [];
aInput[1].forEach((sBusID, iIndex) => {
    if (!isNaN(Number(sBusID))) {
        aBus.push({ b: Number(sBusID), o: iIndex });
    }
});

// heavily inspired by:
// https://streamable.com/tojflp
// https://tranzystorek-io.github.io/paste/#XQAAAQC2BQAAAAAAAAA9iImGVD/UQZfk+oJTfwg2/VsJ0DpNkr2zGUvTQlsh3wTvXCCLwIRmGnWdI28YCQN/TadJlgmpa374ExNPygod+X0JAiaiivhXn/d1KXksQuzcxtBBVxiPs8iU+4l1Bt+e/W+HSuC7+ImYEZ7IO9sfhKXXB1NVsnCL2TjPS4dHSkBxLfZagPvq44wlARVNOd3jlG3LaJDa8qz2mGw6L9tX5DD2MS07G0WyMFAxQ8teusNbgggoQr6DcMHJRfYpJOVRYh1xaf5XzrwVG1lHNkHvSXbGEhAwXhPKUfeDR7+DU2yDiI8J3rXFZ0Nc5KYYjOfzAPYq54inmkzuOp7LdrppvmrUKdrlie4bAoX0Yv5FabBYMTN9dNJqz0JeB7MKRoE7oxAEBbnUgrhoLeBRNmJoSl3F8zXiOSBO7EzpwrN3GZTKI1XmI10LUodzQ1G23kNDSvwMWAZogN0U3XfoWbufQHKnLxrCp+oI03lzM6+klAvItiBM/mUqL7pvJDZ/C/BqbxncduYnz+wLE5jnrq+aCCskzguyJ0OcoowC73oMgPTiHA6Vi2tDaZCvhAFBgKHUgw69jd/VVW6Nx5HGumhX/xgZBITtKoqdrVFEHaRzJAMkk/nD1m3MOprOBGb9gdCkGV7f+ZCwpjnkbwcB+YKbOe6HOBbgxcbpe618GVaabmycHEkIFpK8shaX5MjlDkMRish9qhp1dvyOlUN2K7aHBw/2k93BVLnpKFNjgEf3IOS0VzsswkxSbvhAe+xPTDlyWkWhZuMB6afLK4GD7A8QVilPsQyh1IhitDwLhqRTvkhqwBaepcbaCclI67UGWzgWYi/FWCtnZBgo6E7/dzOIKsdRflwa8B9bB/TE0XdWBN16hA/A2byWe3blPJYC67lg7tsYpFjkrHKz4ijynFbNvhXNEq7s5qMa1CvlFZSu2rtIN1r1qdI7clDAoRv5mFHUfSTdYD2N4ckwYil7MPLKakAr9X+Qqf+2mjSy

let iOffset = 1; // Start with an increment of 1
let oBus = aBus.shift(); // Start considering the first buss
let iMinute = 0;
for (iMinute = iOffset; oBus !== undefined; iMinute += iOffset) {
    // If the bus+offset matches the time, adjust offset by bus (to keep in sync) and test the next bus
    if ((iMinute + oBus.o) % oBus.b === 0) {
        iOffset *= oBus.b;
        oBus = aBus.shift();
    }
}
console.log(iMinute - iOffset);