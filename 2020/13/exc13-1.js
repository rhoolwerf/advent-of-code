const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n").map(x => x.split(","));

let iMinute = Number(aInput[0][0]);
let aBusIDs = aInput[1].reduce((acc, curr) => {
    if (!isNaN(curr)) { acc.push(Number(curr)); }
    return acc;
}, []);

let iNextBusID = 0;
let iShortestWaitTime = 0;
aBusIDs.forEach(iBusID => {
    let iWaitTime = iBusID - iMinute % iBusID;
    if (!iShortestWaitTime || iWaitTime < iShortestWaitTime) {
        iNextBusID = iBusID;
        iShortestWaitTime = iWaitTime;
    }
    console.log(iMinute, iBusID, iWaitTime, iWaitTime * iBusID);
});
console.log(iNextBusID, iShortestWaitTime, (iNextBusID * iShortestWaitTime));