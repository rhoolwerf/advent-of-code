const sInputFile = process.argv[2];
const fs = require("fs");
const aInput = fs.readFileSync(sInputFile, "utf8").split("\n");

// Build the initial map
let oMap = new Map(); // key = [z,y,x], value is true/false based on active/inactive
aInput.forEach((aRow, iY) => {
    aRow.split("").forEach((sCell, iX) => {
        if (sCell === "#") {
            oMap.set([0, iY, iX].join(","), true);
        }
    });
});

for (let iCycle = 1; iCycle <= 6; iCycle++) {
    cycle();
}
console.log("After 6 cycles:", count_all_active());

function cycle() {
    let oNewMap = new Map();
    const [iMinZ, iMinY, iMinX, iMaxZ, iMaxY, iMaxX] = get_boundaries();
    for (let iZ = iMinZ - 1; iZ <= iMaxZ + 1; iZ++) {
        for (let iY = iMinY - 1; iY <= iMaxY + 1; iY++) {
            for (let iX = iMinX - 1; iX <= iMaxX + 1; iX++) {
                const iAdjacentActive = count_adjacent_active(iZ, iY, iX);
                const bIsActive = get_point(iZ, iY, iX);
                if (bIsActive && (iAdjacentActive === 2 || iAdjacentActive === 3)) {
                    oNewMap.set([iZ, iY, iX].join(","), true);
                } else if (!bIsActive && iAdjacentActive === 3) {
                    oNewMap.set([iZ, iY, iX].join(","), true);
                }
            }
        }
    }
    oMap = oNewMap;
}

function count_all_active() {
    return Array.from(oMap.values()).filter(x => x).length;
}

function count_adjacent_active(iZ, iY, iX) {
    let aAdjacentCells = [];
    for (let z = iZ - 1; z <= iZ + 1; z++) {
        for (let y = iY - 1; y <= iY + 1; y++) {
            for (let x = iX - 1; x <= iX + 1; x++) {
                if (z !== iZ || y !== iY || x !== iX) {
                    aAdjacentCells.push(get_point(z, y, x));
                }
            }
        }
    }
    return aAdjacentCells.filter(x => x).length;
}

function output_map() {
    const [iMinZ, iMinY, iMinX, iMaxZ, iMaxY, iMaxX] = get_boundaries();
    for (let iZ = iMinZ; iZ <= iMaxZ; iZ++) {
        console.log("z=", iZ);
        for (let iY = iMinY; iY <= iMaxY; iY++) {
            let sRow = "";
            for (let iX = iMinX; iX <= iMaxX; iX++) {
                sRow += get_point(iZ, iY, iX) ? "#" : ".";
            }
            console.log(sRow);
        }
        console.log();
    }
}

function get_point(iZ, iY, iX) {
    const sKey = [iZ, iY, iX].join(",");
    return oMap.has(sKey) ? oMap.get(sKey) : false;
}

function get_boundaries() {
    let iMinZ = 0;
    let iMinY = 0;
    let iMinX = 0;
    let iMaxZ = 0;
    let iMaxY = 0;
    let iMaxX = 0;
    Array.from(oMap.keys()).forEach(sKey => {
        const [iZ, iY, iX] = sKey.split(",").map(Number);
        if (iZ < iMinZ) { iMinZ = iZ; }
        if (iZ > iMaxZ) { iMaxZ = iZ; }
        if (iY < iMinY) { iMinY = iY; }
        if (iY > iMaxY) { iMaxY = iY; }
        if (iX < iMinX) { iMinX = iX; }
        if (iX > iMaxX) { iMaxX = iX; }
    });

    return [iMinZ, iMinY, iMinX, iMaxZ, iMaxY, iMaxX];
}