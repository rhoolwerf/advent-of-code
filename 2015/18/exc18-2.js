const sInputFile = process.argv[2];
const iMaxStepCount = Number(process.argv[3]);
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");

let aState = sInput.split("\n");
// console.log(aState.join("\n"));

for (let iIteration = 1; iIteration <= iMaxStepCount; iIteration++) {
    let aNewState = [];
    for (let iY = 0; iY < aState.length; iY++) {
        let sNewStateLine = "";
        for (let iX = 0; iX < aState[iY].length; iX++) {

            // Four corners are always on
            if ((iX === 0 && iY === 0) || (iX === 0 && iY === aState.length - 1) || (iX === aState[0].length - 1 && iY === 0) || (iX === aState[0].length - 1 && iY === aState.length - 1)) {
                sNewStateLine += "#";
            } else {
                let iCountSurroundingLights = count_surrounding_lights_on(iY, iX);
                if (aState[iY].charAt(iX) === "#") {
                    if (iCountSurroundingLights === 2 || iCountSurroundingLights === 3) {
                        sNewStateLine += "#";
                    } else {
                        sNewStateLine += ".";
                    }
                } else {
                    if (iCountSurroundingLights === 3) {
                        sNewStateLine += "#";
                    } else {
                        sNewStateLine += ".";
                    }
                }
            }
        }
        aNewState.push(sNewStateLine);
    }
    aState = aNewState;
    // console.log("\nAfter", iIteration, "steps:");
    // console.log(aNewState.join("\n"));
}

let iLightCount = 0;
for (let iY = 0; iY < aState.length; iY++) {
    for (let iX = 0; iX < aState[iY].length; iX++) {
        iLightCount += get_light_state(iY, iX);
    }
}
console.log(iLightCount);

function count_surrounding_lights_on(iY, iX) {
    let iCount = 0;
    iCount += get_light_state(iY - 1, iX - 1);
    iCount += get_light_state(iY - 1, iX);
    iCount += get_light_state(iY - 1, iX + 1);
    iCount += get_light_state(iY, iX - 1);
    iCount += get_light_state(iY, iX + 1);
    iCount += get_light_state(iY + 1, iX - 1);
    iCount += get_light_state(iY + 1, iX);
    iCount += get_light_state(iY + 1, iX + 1);
    return iCount;
}

function get_light_state(iY, iX) {
    // console.log(iY,iX);
    if (iY < 0 || iY >= aState.length) {
        return 0;
    } else if (iX < 0 || iX >= aState[0].length) {
        return 0;
    } else {
        return (aState[iY].charAt(iX) === "#" ? 1 : 0);
    }
}