const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInstructions = sInput.split("\n");

let oRegisters = {
    a: 0,
    b: 0
}

for (let iPointer = 0; iPointer < aInstructions.length; iPointer++) {
    let aChunks = aInstructions[iPointer].split(" ");
    switch (aChunks[0]) {
        case "hlf":
            oRegisters[aChunks[1]] /= 2;
            break;
        case "tpl":
            oRegisters[aChunks[1]] *= 3;
            break;
        case "inc":
            oRegisters[aChunks[1]]++;
            break;
        case "jie":
            if (oRegisters[aChunks[1].split(",")[0]] % 2 === 0) {
                iPointer += Number(aChunks[2]) - 1;
            }
            break;
        case "jio":
            if (oRegisters[aChunks[1].split(",")[0]] === 1) {
                iPointer += Number(aChunks[2]) - 1;
            }
            break;
        case "jmp":
            iPointer += Number(aChunks[1]) - 1;
            break;
        default:
            console.error("Unknown instruction:", aChunks[0]);
            return;
    }
}
console.log(oRegisters);