const sInputFile = process.argv[2];
const iSecondsInput = Number(process.argv[3]);
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aReindeer = sInput.split("\n").map(x => {
    return {
        name: x.split(" ")[0],
        speed: parseInt(x.split(" ")[3]),
        duration: parseInt(x.split(" ")[6]),
        rest: parseInt(x.split(" ")[13]),
        current_duration: parseInt(x.split(" ")[6]),
        current_rest: 0,
        distance: 0
    }
});

for (let iSeconds = 1; iSeconds <= iSecondsInput; iSeconds++) {
    aReindeer.forEach(oReindeer => {
        if (oReindeer.current_duration > 0) {
            oReindeer.distance += oReindeer.speed;
            oReindeer.current_duration--;
            if (oReindeer.current_duration === 0) {
                oReindeer.current_rest = oReindeer.rest;
            }
        } else {
            oReindeer.current_rest--;
            if (oReindeer.current_rest === 0) {
                oReindeer.current_duration = oReindeer.duration;
            }
        }
    });
}

aReindeer.sort((a, b) => {
    if (a.distance < b.distance) { return 1; }
    else if (a.distance > b.distance) { return -1; }
    else { return 0; }
});
console.log("After", iSecondsInput, " seconds, the score is:");
aReindeer.forEach(oReindeer => {
    console.log(oReindeer.name, oReindeer.distance);
});