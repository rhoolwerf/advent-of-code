// let oInput = [1, 2, 3];
// oInput = [1, { "c": "red", "b": 2 }, 3];
// oInput = { "d": "red", "e": [1, 2, 3, 4], "f": 5 };
// oInput = [1, "red", 5];

const sInputFile = process.argv[2];
const fs = require("fs");
const oInput = JSON.parse(fs.readFileSync(sInputFile, "utf8"));

let iSum = 0;
parse_unknown(oInput);
console.log(iSum);

function parse_unknown(oInput) {
    if (Array.isArray(oInput)) {
        parse_array(oInput);
    } else if (typeof oInput === "object") {
        parse_object(oInput);
    } else if (typeof oInput === "number") {
        iSum += oInput;
    }
}

function parse_array(aInput) {
    aInput.forEach(oElement => {
        parse_unknown(oElement);
    });
}

function parse_object(oInput) {
    aObjectValues = Object.values(oInput);
    if (aObjectValues.includes("red")) {
        return;
    } else {
        aObjectValues.forEach(oValue => {
            parse_unknown(oValue);
        });
    }

}