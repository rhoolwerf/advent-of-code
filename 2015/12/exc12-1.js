// console.log(get_sum_of_numbers(JSON.stringify([1, 2, 3])));
// console.log(get_sum_of_numbers(JSON.stringify({ "a": 2, "b": 4 })));
// console.log(get_sum_of_numbers(JSON.stringify([[[3]]])));
// console.log(get_sum_of_numbers(JSON.stringify({ "a": { "b": 4 }, "c": -1 })));
// console.log(get_sum_of_numbers(JSON.stringify({ "a": [-1, 1] })));
// console.log(get_sum_of_numbers(JSON.stringify([-1, { "a": 1 }])));
// console.log(get_sum_of_numbers(JSON.stringify([])));
// console.log(get_sum_of_numbers(JSON.stringify({})));

const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
console.log(get_sum_of_numbers(sInput));

function get_sum_of_numbers(sInput) {
    let oRegex = /-?\d+/g;
    let aNumbers = sInput.match(oRegex);
    if (aNumbers === null) { return 0; }
    else {
        let iSum = 0;
        aNumbers.forEach(sNumber => {
            iSum += parseInt(sNumber);
        })
        return iSum;
    }
}