const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");

let iLevel = 0;
let iFirstInstructionAtBasement = 0;
sInput.split("").forEach((sInstruction, iIndex) => {
    if (sInstruction === "(") { iLevel++; }
    else { iLevel--; }
    if (!iFirstInstructionAtBasement && iLevel < 0) {
        iFirstInstructionAtBasement = iIndex + 1;
    }
});
console.log(iFirstInstructionAtBasement);