const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");

let iLevel = 0;
sInput.split("").forEach(sInstruction => {
    if (sInstruction === "(") { iLevel++; }
    else { iLevel--; }
})
console.log(iLevel);