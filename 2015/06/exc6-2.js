const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n").map(x => map_instruction(x));

// init map
let oMap = new Map();
for (let y = 0; y <= 999; y++) {
    oMap.set(y, new Map());
    for (let x = 0; x <= 999; x++) {
        oMap.get(y).set(x, 0);
    }
}

// process each instruction
aInput.forEach(oInstruction => {
    for (let y = oInstruction.from.y; y <= oInstruction.to.y; y++) {
        for (let x = oInstruction.from.x; x <= oInstruction.to.x; x++) {
            switch (oInstruction.cmd) {
                case 'turn on':
                    oMap.get(y).set(x, oMap.get(y).get(x) + 1);
                    break;
                case 'turn off':
                    oMap.get(y).set(x, Math.max(oMap.get(y).get(x) - 1, 0));
                    break;
                case 'toggle':
                    oMap.get(y).set(x, oMap.get(y).get(x) + 2);
                    break;
            }
        }
    }
});

// Count the lights that are on
let iLightCount = 0;
for (let y = 0; y <= 999; y++) {
    for (let x = 0; x <= 999; x++) {
        iLightCount += oMap.get(y).get(x);
    }
}
console.log(iLightCount);

// map input line to instruction-set
function map_instruction(sInstruction) {
    let [sFirst, sMiddle, sLast] = sInstruction.split(",").map(x => x.split(" "));

    return {
        cmd: sFirst.slice(0, sFirst.length - 1).join(" "),
        from: {
            x: Number(sFirst.pop()),
            y: Number(sMiddle.shift())
        },
        to: {
            x: Number(sMiddle.pop()),
            y: Number(sLast.shift())
        }
    };
}