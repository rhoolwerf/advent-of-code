const sInputFile = process.argv[2];
const fs = require("fs");

const oVowels = new Set("aeiou".split(""));
const oDisallowed = new Set(["ab", "cd", "pq", "xy"]);

if (sInputFile) {
    const sInput = fs.readFileSync(sInputFile, "utf8");
    const aInput = sInput.split("\n");
    let iValid = 0;
    aInput.forEach(sLine => {
        if (is_nice(sLine)) {
            iValid++;
        }
    });
    console.log(iValid);
} else {
    console.log(is_nice("ugknbfddgicrmopn"));
    console.log(is_nice("aaa"));
    console.log(is_nice("jchzalrnumimnmhp"));
    console.log(is_nice("haegwjzuvuyypxyu"));
    console.log(is_nice("dvszwmarrgswjxmb"));
}

function is_nice(sInput) {
    let aInput = sInput.split("");

    let iVowelCount = 0;
    let bHasDoubles = false;
    let bHasDisallowed = false;
    aInput.forEach((sChar, iIndex) => {
        // Count vowels
        if (oVowels.has(sChar)) {
            iVowelCount++
        };

        // Check for doubles
        if (aInput[iIndex + 1] === sChar) {
            bHasDoubles = true;
        }

        // Check for follow-ups
        if (oDisallowed.has(aInput[iIndex] + aInput[iIndex + 1])) {
            bHasDisallowed = true;
        }
    });

    // return [sInput, iVowelCount, bHasDoubles, bHasDisallowed, (iVowelCount >= 3 && bHasDoubles && !bHasDisallowed)];
    return (iVowelCount >= 3 && bHasDoubles && !bHasDisallowed);
}