const sInputFile = process.argv[2];
const fs = require("fs");
const { isUndefined } = require("util");

if (sInputFile) {
    const sInput = fs.readFileSync(sInputFile, "utf8");
    const aInput = sInput.split("\n");
    let iValid = 0;
    aInput.forEach(sLine => {
        if (is_nice(sLine)) {
            iValid++;
        }
    });
    console.log(iValid);
} else {
    console.log(is_nice("qjhvhtzxzqqjkmpb"));
    console.log(is_nice("xxyxx"));
    console.log(is_nice("uurcxstgmygtbstg"));
    console.log(is_nice("ieodomkazucvgmuy"));
}

function is_nice(sInput) {
    let aInput = sInput.split("");

    let bPairAgain = false;
    let bPairBetween = false;

    aInput.forEach((sChar, iIndex) => {
        // Try to find the same character two positions further
        if (aInput[iIndex + 2] === sChar) {
            bPairBetween = true;
        }

        // Try to find the combination between current and +1 again somewhere later in the input
        const sPair = aInput[iIndex] + aInput[iIndex + 1];
        for (let iFind = iIndex + 2; iFind < aInput.length; iFind++) {
            if (sPair === (aInput[iFind] + aInput[iFind + 1])) {
                bPairAgain = true;
            }
        }
    });

    // return [sInput, bPairAgain, bPairBetween];
    return (bPairAgain && bPairBetween);
}