// Unfortunately, heavily inspired on https://www.reddit.com/r/adventofcode/comments/3y5jco/day_25_solutions/cyaqseg/?utm_source=reddit&utm_medium=web2x&context=3

const iTargetRow = 3010;
const iTargetColumn = 3019;

let iRow = 1;
let iColumn = 1;
let iCode = 20151125;
let aWrongCodes = [];
while (iRow !== iTargetRow || iColumn !== iTargetColumn) {
    iCode = (iCode * 252533) % 33554393;

    if (aWrongCodes.length < 10) { aWrongCodes.push(iCode); }

    if (iRow === 1) {
        iRow = iColumn + 1;
        iColumn = 1;
    } else {
        iRow--;
        iColumn++;
    }
}
console.log(iCode);