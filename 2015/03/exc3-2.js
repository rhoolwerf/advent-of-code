const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");

let oMap = new Map();
let iX = 0;
let iY = 0;
let iSantaX = 0;
let iSantaY = 0;
let iRobotX = 0;
let iRobotY = 0;
let iCounter = 0;
set_point();

sInput.split("").forEach(sInstruction => {
    // Who is moving?
    if (iCounter % 2 === 0) { iX = iSantaX; iY = iSantaY; }
    else { iX = iRobotX; iY = iRobotY; }

    // Process instruction
    switch (sInstruction) {
        case ">":
            iX++;
            break;
        case "<":
            iX--;
            break;
        case "^":
            iY--;
            break;
        case "v":
            iY++;
            break;
    }
    set_point();

    // Update memory
    if (iCounter % 2 === 0) { iSantaX = iX; iSantaY = iY; }
    else { iRobotX = iX; iRobotY = iY; }

    // Update the who-is-moving counter
    iCounter += 1;
});

console.log(count_houses_visited());

function count_houses_visited() {
    let iCount = 0;
    for (oRow of oMap) {
        for (oHouse of oRow[1]) {
            iCount++;
        }
    }
    return iCount;
}

function set_point() {
    if (!oMap.has(iY)) { oMap.set(iY, new Map()); }
    if (!oMap.get(iY).has(iX)) { oMap.get(iY).set(iX, 1); }
    else { oMap.get(iY).set(iX, oMap.get(iY).get(iX)); }
}