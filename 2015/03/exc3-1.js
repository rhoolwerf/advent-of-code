const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");

let oMap = new Map();
let iX = 0;
let iY = 0;
set_point(iY, iX);

sInput.split("").forEach(sInstruction => {
    switch (sInstruction) {
        case ">":
            iX++;
            break;
        case "<":
            iX--;
            break;
        case "^":
            iY--;
            break;
        case "v":
            iY++;
            break;
    }
    set_point();
});

console.log(count_houses_visited());

function count_houses_visited() {
    let iCount = 0;
    for (oRow of oMap) {
        for (oHouse of oRow[1]) {
            iCount++;
        }
    }
    return iCount;
}

function set_point() {
    if (!oMap.has(iY)) { oMap.set(iY, new Map()); }
    if (!oMap.get(iY).has(iX)) { oMap.get(iY).set(iX, 1); }
    else { oMap.get(iY).set(iX, oMap.get(iY).get(iX)); }
}