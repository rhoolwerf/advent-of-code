const aWeapons = [
    { name: "Dagger", cost: 8, damage: 4, armor: 0 },
    { name: "Shortsword", cost: 10, damage: 5, armor: 0 },
    { name: "Warhammer", cost: 25, damage: 6, armor: 0 },
    { name: "Longsword", cost: 40, damage: 7, armor: 0 },
    { name: "Greataxe", cost: 74, damage: 8, armor: 0 }
];
const aArmor = [
    { name: "", cost: 0, damage: 0, armor: 0 }, // 'No armor'
    { name: "Leather", cost: 13, damage: 0, armor: 1 },
    { name: "Chainmail", cost: 31, damage: 0, armor: 2 },
    { name: "Splintmail", cost: 53, damage: 0, armor: 3 },
    { name: "Bandedmail", cost: 75, damage: 0, armor: 4 },
    { name: "Platemail", cost: 102, damage: 0, armor: 5 }
];
const aRings = [
    { name: "Damage +1", cost: 25, damage: 1, armor: 0 },
    { name: "Damage +2", cost: 50, damage: 2, armor: 0 },
    { name: "Damage +3", cost: 100, damage: 3, armor: 0 },
    { name: "Defense +1", cost: 20, damage: 0, armor: 1 },
    { name: "Defense +2", cost: 40, damage: 0, armor: 2 },
    { name: "Defense +3", cost: 80, damage: 0, armor: 3 }
];

// Example fight
let oBoss = {
    hp: 12,
    damage: 7,
    armor: 2
}
let oPlayer = {
    hp: 8,
    damage: 5,
    armor: 5
}
// console.log(simulate_game(oBoss, oPlayer));


// Actual game
oBoss = { hp: 100, damage: 8, armor: 2 }
oPlayer = { hp: 100, damage: 0, armor: 0, cost: 0 }

let aPlayers = [];
for (let iWeapon = 0; iWeapon < aWeapons.length; iWeapon++) {
    for (let iArmor = 0; iArmor < aArmor.length; iArmor++) {
        for (let iRing1 = 0; iRing1 < aRings.length; iRing1++) {
            for (let iRing2 = iRing1 + 1; iRing2 < aRings.length; iRing2++) {
                aPlayers.push(build_player(oPlayer, iWeapon, iArmor, iRing1, iRing2));
            }

            // Also handle with only 1 ring
            aPlayers.push(build_player(oPlayer, iWeapon, iArmor, iRing1));
        }

        // Also handle without rings
        aPlayers.push(build_player(oPlayer, iWeapon, iArmor));
    }
}

function build_player(oPlayer, iWeapon, iArmor, iRing1, iRing2) {
    let oPlayerClone = JSON.parse(JSON.stringify(oPlayer));
    oPlayerClone.damage = aWeapons[iWeapon].damage + aArmor[iArmor].damage + (iRing1 !== undefined ? aRings[iRing1].damage : 0) + (iRing2 !== undefined ? aRings[iRing2].damage : 0);
    oPlayerClone.armor = aWeapons[iWeapon].armor + aArmor[iArmor].armor + (iRing1 !== undefined ? aRings[iRing1].armor : 0) + (iRing2 !== undefined ? aRings[iRing2].armor : 0);
    oPlayerClone.cost = aWeapons[iWeapon].cost + aArmor[iArmor].cost + (iRing1 !== undefined ? aRings[iRing1].cost : 0) + (iRing2 !== undefined ? aRings[iRing2].cost : 0);
    return oPlayerClone;
}

// Simulate each game and register
let aWinningGames = [];
aPlayers.forEach(oPlayerSelected => {
    // Do we lose?
    if (!simulate_game(oBoss, oPlayerSelected)) {
        aWinningGames.push(oPlayerSelected);
    }
});
// Sort by cost, descending
aWinningGames.sort((a, b) => {
    if (a.cost < b.cost) { return 1; }
    else if (a.cost > b.cost) { return -1; }
    else { return 0; }
})
console.log(aWinningGames[0]);

function simulate_game(oBossInput, oPlayerInput) {
    let oBoss = JSON.parse(JSON.stringify(oBossInput));
    let oPlayer = JSON.parse(JSON.stringify(oPlayerInput));

    while (oBoss.hp > 0 && oPlayer.hp > 0) {
        // console.log("The player deals", oPlayer.damage, "-", oBoss.armor, "=", Math.max(1, oPlayer.damage - oBoss.armor), "; the boss goes down to", (oBoss.hp - Math.max(1, oPlayer.damage - oBoss.armor)), "hit points.");
        oBoss.hp -= Math.max(1, oPlayer.damage - oBoss.armor);
        if (oBoss.hp <= 0) {
            // console.log("Player wins!");
            return true;
        }

        // console.log("The boss deals", oBoss.damage, "-", oPlayer.armor, "=", Math.max(1, oBoss.damage - oPlayer.armor), "; the player goes down to", (oPlayer.hp - Math.max(1, oBoss.damage - oPlayer.armor)), "hit points.");
        oPlayer.hp -= Math.max(1, oBoss.damage - oPlayer.armor);
        if (oPlayer.hp <= 0) {
            // console.log("Boss win...");
            return false;
        }
    }
}