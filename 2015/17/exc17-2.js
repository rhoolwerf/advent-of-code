const sInputFile = process.argv[2];
const iRequiredAmount = Number(process.argv[3]);
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n").map(Number);

let iCount = 0;
let iMinimumContainerCount = 0;
let iAmountHasMinContainerCount = 0;
for (let i = 1; i.toString(2).length <= aInput.length; i++) {
    let sBinary = i.toString(2);
    while (sBinary.length < aInput.length) {
        sBinary = "0" + sBinary;
    }

    let aBinaryChunks = sBinary.split("");
    let iTotal = 0;
    let iCountContainers = 0;
    for (let i = 0; i < aBinaryChunks.length; i++) {
        iTotal += aBinaryChunks[i] * aInput[i];
        if (Number(aBinaryChunks[i]) === 1) {
            iCountContainers++;
        }
    }

    if (iTotal === iRequiredAmount) {
        if (iCountContainers === iMinimumContainerCount) {
            iAmountHasMinContainerCount++;
        } else if (iCountContainers < iMinimumContainerCount || iMinimumContainerCount === 0) {
            iMinimumContainerCount = iCountContainers;
            iAmountHasMinContainerCount = 1;
        }
    }
}
console.log(iAmountHasMinContainerCount);