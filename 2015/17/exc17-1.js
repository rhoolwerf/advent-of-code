const sInputFile = process.argv[2];
const iRequiredAmount = Number(process.argv[3]);
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n").map(Number);

let iCount = 0;
for (let i = 1; i.toString(2).length <= aInput.length; i++) {
    let sBinary = i.toString(2);
    while (sBinary.length < aInput.length) {
        sBinary = "0" + sBinary;
    }

    let aBinaryChunks = sBinary.split("");
    let iTotal = 0;
    for (let i = 0; i < aBinaryChunks.length; i++) {
        iTotal += aBinaryChunks[i] * aInput[i];
    }
    // console.log(sBinary, iTotal, iRequiredAmount);
    if (iTotal === iRequiredAmount) {
        iCount++;
        // console.log(sBinary, iTotal);
    }
}
console.log(iCount);