const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n");

let aIngredients = [];
aInput.forEach(sInputLine => {
    let aChunks = sInputLine.split(" ");
    aIngredients.push({
        name: aChunks[0].split(":")[0],
        capacity: parseInt(aChunks[2]),
        durability: parseInt(aChunks[4]),
        flavor: parseInt(aChunks[6]),
        texture: parseInt(aChunks[8]),
        calories: parseInt(aChunks[10])
    });
});

// Calculate all possible permutations
let aPermutations = [];
if (aIngredients.length === 2) {
    for (let a = 0; a <= 100; a++) {
        aPermutations.push([a, 100 - a]);
    }
} else {
    for (let a = 0; a <= 100; a++) {
        for (let b = 0; b <= 100; b++) {
            for (let c = 0; c <= 100; c++) {
                let d = 100 - a - b - c;
                if (d > 0) {
                    aPermutations.push([a, b, c, d]);
                }
            }
        }
    }
}

// For each permutation, calculate the score
let aScores = [];
aPermutations.forEach((aPermutation, iPermutation) => {
    let iCapacity = 0;
    let iDurability = 0;
    let iFlavor = 0;
    let iTexture = 0;
    let iCalories = 0;
    for (let i = 0; i < aPermutation.length; i++) {
        iCapacity += aIngredients[i].capacity * aPermutation[i];
        iDurability += aIngredients[i].durability * aPermutation[i];
        iFlavor += aIngredients[i].flavor * aPermutation[i];
        iTexture += aIngredients[i].texture * aPermutation[i];
        iCalories += aIngredients[i].calories * aPermutation[i];
    }
    if (iCalories === 500) {
        let iScore = Math.max(0, iCapacity) * Math.max(0, iDurability) * Math.max(0, iFlavor) * Math.max(0, iTexture);
        if (iScore > 0) {
            aScores.push({
                p: iPermutation,
                pe: aPermutation,
                s: iScore
            });
        }
    }
});
aScores.sort((a, b) => {
    if (a.s < b.s) { return 1; }
    else if (a.s > b.s) { return -1; }
    else { return 0; }
})
console.log(aScores[0]);