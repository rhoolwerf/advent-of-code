const sInputFile = "exc16.in";//process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n");

const oCheckObject = {
    children: 3,
    cats: 7,
    samoyeds: 2,
    pomeranians: 3,
    akitas: 0,
    vizslas: 0,
    goldfish: 5,
    trees: 3,
    cars: 2,
    perfumes: 1
}

// console.log(oCheckObject);

aInput.forEach(sLine => {
    let aChunks = sLine.split(" ");
    let bCorrectAunt = true;
    for (let i = 2; i < aChunks.length; i += 2) {
        let sProperty = aChunks[i].split(":")[0];
        let iDesiredValue = parseInt(aChunks[i + 1].split(",")[0])
        switch (sProperty) {
            case 'cats':
            case 'trees':
                if (oCheckObject[sProperty] >= iDesiredValue) {
                    bCorrectAunt = false;
                }

                break;

            case 'pomeranians':
            case 'goldfish':
                if (oCheckObject[sProperty] <= iDesiredValue) {
                    bCorrectAunt = false;
                }
                break;
            default:
                if (oCheckObject[sProperty] !== iDesiredValue) {
                    bCorrectAunt = false;
                }
                break;
        }
    }
    if (bCorrectAunt) {
        console.log(parseInt(aChunks[1].split(":")[0]));
    }
});