const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");

let iRibbonLength = 0;
sInput.split("\n").forEach(sPresent => {
    let aDimensions = sPresent.split("x");
    aDimensions.sort((a, b) => { return a - b; });
    iRibbonLength += 2 * aDimensions[0] + 2 * aDimensions[1]; // wrapper
    iRibbonLength += aDimensions[0] * aDimensions[1] * aDimensions[2]; // bow
});
console.log(iRibbonLength);