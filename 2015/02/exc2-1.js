const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");

let iTotalArea = 0;
sInput.split("\n").forEach(sPresent => {
    let [l, w, h] = sPresent.split("x");

    let l1 = l * w;
    let w1 = w * h;
    let h1 = h * l;

    let iSurplus = Math.min(l1, Math.min(w1, h1));
    iTotalArea += 2 * l1 + 2 * w1 + 2 * h1 + iSurplus;
});
console.log(iTotalArea);