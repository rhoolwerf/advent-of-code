const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n");

let oPaths = new Map();
let oPoints = new Set();
aInput.forEach(sPath => {
    let aChunks = sPath.split(" ");
    register_path(aChunks[0], aChunks[2], parseInt(aChunks[4]));
    register_path(aChunks[2], aChunks[0], parseInt(aChunks[4]));
    oPoints.add(aChunks[0]);
    oPoints.add(aChunks[2]);
});


let oRoutes = new Set();
oPoints.forEach(sPoint => {
    find_route(new Set(), sPoint);
});

let aRoutes = [];
oRoutes.forEach(oRoute => {
    let aRoute = Array.from(oRoute);
    let iLength = 0;
    for (let i = 0; i < aRoute.length - 1; i++) {
        iLength += oPaths.get(aRoute[i]).get(aRoute[i + 1]);
    }
    aRoutes.push({ r: aRoute.join(" -> "), d: iLength });
});
aRoutes.sort((a, b) => {
    if (a.d < b.d) { return 1; }
    else if (a.d > b.d) { return -1; }
    else { return 0; }
})
console.log(aRoutes[0]);

function find_route(oPath, sPoint) {
    if ((oPath.size + 1) === oPoints.size) {
        oRoutes.add(new Set(oPath).add(sPoint));
    }
    oPaths.get(sPoint).forEach((iDistance, sNextPoint) => {
        if (!oPath.has(sNextPoint)) {
            find_route(new Set(oPath).add(sPoint), sNextPoint);
        }
    });
}

function register_path(sSource, sDestination, iLength) {
    if (!oPaths.has(sSource)) {
        oPaths.set(sSource, new Map());
    }

    oPaths.get(sSource).set(sDestination, iLength);
}