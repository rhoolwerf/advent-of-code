const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n").map(x => x.trim());

let iSumCharacterCount = 0;
let iSumMemoryCount = 0.

aInput.forEach(sOriginalLine => {
    iSumCharacterCount += sOriginalLine.length;
    iSumMemoryCount += eval(sOriginalLine).length; // Evaluate the string in JS, which does everything we want to happen
});
console.log(iSumCharacterCount, "-", iSumMemoryCount, "=", (iSumCharacterCount - iSumMemoryCount));