const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n").map(x => x.trim());

let iSumCharacterCount = 0;
let iSumMemoryCount = 0.

aInput.forEach(sOriginalLine => {
    iSumCharacterCount += JSON.stringify(sOriginalLine).length;
    iSumMemoryCount += sOriginalLine.length; // JSON.stringify is a sort of uneval
});
console.log(iSumCharacterCount, "-", iSumMemoryCount, "=", (iSumCharacterCount - iSumMemoryCount));