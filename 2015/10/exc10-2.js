let sInput = "3113322113";

for (let iIteration = 0; iIteration < 50; iIteration++) {
    let sNewInput = "";
    for (let i = 0; i < sInput.length; i++) {
        let iCount = 1;
        for (let j = i + 1; j < sInput.length; j++) {
            if (sInput.charAt(i) === sInput.charAt(j)) {
                iCount++;
            } else {
                break;
            }
        }
        sNewInput += String(iCount) + sInput.charAt(i);
        i += iCount - 1;
    }
    sInput = sNewInput;
}
console.log(sInput.length);