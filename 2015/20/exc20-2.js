// Grab all divisors of a given number
// Initially I looped from 0..numb but that was very slow, then I read online that "We know that a number's square root multiplied by itself is the largest possible divisor besides itself"
// That helped with the performance by a *lot*
// 
// Inspired by: https://codereview.stackexchange.com/questions/120642/getting-all-divisors-from-an-integer
function get_divisors(iNum) {
    let oDivisors = new Set();
    for (let iDivisorAttempt = Math.floor(Math.sqrt(iNum)); iDivisorAttempt > 0; iDivisorAttempt--) {
        if (iNum % iDivisorAttempt === 0) {
            oDivisors.add(iDivisorAttempt);
            oDivisors.add(iNum / iDivisorAttempt); // iNum / iDivisors = also a divisor (as in: 24 / 4 = 6, which makes 6 automatically a divisor)
        }
    }
    return oDivisors;
}

const iTarget = Number(process.argv[2]);
let aHouseVisits = [];
if (iTarget > 0) {
    for (let iHouse = 1; true; iHouse++) {
        let iPresents = 0;
        get_divisors(iHouse).forEach(iElf => {
            // How often has an Elf visited a house?
            aHouseVisits[iElf] = aHouseVisits[iElf] || 0;
            if (aHouseVisits[iElf] < 50) {
                // Only if an Elf hasn't visited 50 houses, he visits another one
                iPresents += iElf * 11;
                aHouseVisits[iElf]++;
            }
        });
        // if (iHouse % 100000 === 0) {
        //     console.log("House", iHouse, "got", iPresents, "presents");
        // }
        if (iPresents >= iTarget) {
            console.log("House", iHouse, "got the target amount of ", iPresents, "presents, surpassing requested target", iTarget);
            break;
        }
    }
} else {
    console.error("Please give a target");
}


