const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n").map(x => x.split(".")[0].split(" "));

// Parse the input into a Map of Couples and their Happiness Gain, and a Set of unique Persons
let oCouples = new Map();
let oPersons = new Set();
aInput.forEach(aLine => {
    register_couple(aLine[0], aLine[10], parseInt((aLine[2] === "gain" ? aLine[3] : aLine[3] * -1)));
    register_couple(aLine[0], aLine[10], parseInt((aLine[2] === "gain" ? aLine[3] : aLine[3] * -1)));
    oPersons.add(aLine[0]);
    oPersons.add(aLine[10]);
});


// Add myself to the seating-list as well
oPersons.forEach(sPerson => {
    register_couple("Roald", sPerson, 0);
    register_couple(sPerson, "Roald", 0);
});
oPersons.add("Roald");

// Determine all possible sitting arrangements
let oSittings = new Set();
oPersons.forEach(sPerson => {
    find_sitting(new Set(), sPerson);
});
// Calculate happiness per seating arrangement
let aSittingHappines = [];
oSittings.forEach(aSitting => {
    let iHappinessGain = 0;
    for (let i = 0; i < aSitting.length; i++) {
        let p1 = aSitting[i];
        let p2 = aSitting[i === aSitting.length - 1 ? 0 : i + 1];
        iHappinessGain += oCouples.get(p1).get(p2);
        iHappinessGain += oCouples.get(p2).get(p1);
    }
    aSittingHappines.push({ s: aSitting, h: iHappinessGain });
});
// Sort by happness
aSittingHappines.sort((a, b) => {
    if (a.h > b.h) { return -1; }
    else if (a.h < b.h) { return 1; }
    else { return 0; }
});
// Output the first/best
console.log(aSittingHappines[0].h);

function find_sitting(oAlreadySeated, sPerson) {
    if (oAlreadySeated.size + 1 === oPersons.size) {
        oSittings.add(Array.from(new Set(oAlreadySeated).add(sPerson)));
    } else {
        oCouples.get(sPerson).forEach((iHappinessGain, sNext) => {
            if (!oAlreadySeated.has(sNext)) {
                find_sitting(new Set(oAlreadySeated).add(sPerson), sNext);
            }
        });
    }
}

function register_couple(sPerson1, sPerson2, iHappinessGain) {
    if (!oCouples.has(sPerson1)) {
        oCouples.set(sPerson1, new Map());
    }
    oCouples.get(sPerson1).set(sPerson2, iHappinessGain);
}