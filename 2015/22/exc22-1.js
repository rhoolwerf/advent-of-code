const NOTHING = -1;
const MAGIC_MISSILE = 0;
const DRAIN = 1;
const SHIELD = 2;
const POISON = 3;
const RECHARGE = 4;

const aSpellCosts = [
    53,
    73,
    113,
    173,
    229
];

// console.log(simulate_game(true));

iMinCost = 0;
for (let i = 0; i < 1000000; i++) {
    let [bResult, iCost] = simulate_game();
    if (bResult) {
        // console.log("Win", i, "and it cost", iCost);
        iMinCost = (iMinCost === 0 ? iCost : Math.min(iCost, iMinCost));
    }
}
console.log(iMinCost);

function simulate_game(bDebug = false) {
    // Start out with some setup
    bPlayerTurn = true; // Player always starts
    iPlayerHealth = 50;
    iPlayerMana = 500;
    iPlayerArmor = 0;
    iBossHealth = 58;
    iBossDamage = 9;

    // Keep track of how much mana is spend
    iTotalManaCost = 0;

    // Some effect timers
    iShield = 0;
    iPoison = 0;
    iRecharge = 0;

    while (true) {
        if (bDebug) {
            console.log((bPlayerTurn ? "Player" : "Boss"), "turn");
            console.log("- Player has", iPlayerHealth, "hit points,", iPlayerArmor, "armor,", iPlayerMana, "mana");
            console.log("- Boss has", iBossHealth, "hit points");
        }

        // At the start of each turn, handle all effects
        if (iShield > 0) {
            iShield--;
            if (bDebug) { console.log("Shield's timer is now", iShield); }
            if (iShield === 0) {
                iPlayerArmor = 0;
            }
        }
        if (iPoison > 0) {
            iBossHealth -= 3;//Poison hits the boss for 3 damage
            iPoison--;
            if (bDebug) { console.log("Poisons deals", 3, "damage; its timer is now", iPoison); }
        }
        if (iRecharge > 0) {
            iPlayerMana += 101;
            iRecharge--;
            if (bDebug) { console.log("Recharge provides", 101, "mana; its timer is now", iRecharge); }
        }

        // Is anyone dead yet?
        if (iPlayerHealth <= 0) {
            if (bDebug) { console.log("Player health reaches", iPlayerHealth, "and dies, losing the game"); }
            return [false, 0];
        }
        if (iBossHealth <= 0) {
            if (bDebug) { console.log("Boss health reaches", iBossHealth, "and dies, winning the game!"); }
            return [true, iTotalManaCost];
        }

        // Now that we've taken care of all Effects, handle the turn
        if (bPlayerTurn) {
            // Choose which spell to cast, randomly (no need for intelligence here)
            let iSpell = choose_spell(iPlayerMana, iPoison, iShield, iRecharge);
            switch (iSpell) {
                case NOTHING:
                    // Can't cast any spell? Player loses!
                    if (bDebug) { console.log("Player has run out of mana, losing the game"); }
                    return [false, 0];
                    break;
                case MAGIC_MISSILE:
                    if (bDebug) { console.log("Player casts MAGIC MISSILE"); }
                    iTotalManaCost += aSpellCosts[MAGIC_MISSILE];
                    iPlayerMana -= aSpellCosts[MAGIC_MISSILE];
                    iBossHealth -= 4;
                    break;
                case DRAIN:
                    if (bDebug) { console.log("Player casts DRAIN"); }
                    iTotalManaCost += aSpellCosts[DRAIN];
                    iPlayerMana -= aSpellCosts[DRAIN];
                    iBossHealth -= 2;
                    iPlayerHealth += 2;
                    break;
                case SHIELD:
                    if (bDebug) { console.log("Player casts SHIELD"); }
                    iTotalManaCost += aSpellCosts[SHIELD];
                    iPlayerMana -= aSpellCosts[SHIELD];
                    iShield = 6;
                    iPlayerArmor = 7;
                    break;
                case POISON:
                    if (bDebug) { console.log("Player casts POISON"); }
                    iTotalManaCost += aSpellCosts[POISON];
                    iPlayerMana -= aSpellCosts[POISON];
                    iPoison = 6;
                    break;
                case RECHARGE:
                    if (bDebug) { console.log("Player casts RECHARGE"); }
                    iTotalManaCost += aSpellCosts[RECHARGE];
                    iPlayerMana -= aSpellCosts[RECHARGE];
                    iRecharge = 5;
                    break;
            }
        } else {
            // Boss always does fixed amount of damage
            iPlayerHealth -= Math.max(1, iBossDamage - iPlayerArmor);
            if (bDebug) { console.log("Boss deals", Math.max(1, iBossDamage - iPlayerArmor), "damage"); }
        }

        if (bDebug) { console.log(""); }
        bPlayerTurn = !bPlayerTurn;
    }
}

function choose_spell(iPlayerMana, iPoison, iShield, iRecharge) {
    if (iPlayerMana <= aSpellCosts[0]) {
        return NOTHING; // Not enough mana to cast even the cheapest of spells
    } else {
        while (true) {
            let iSpell = getRandomInt(0, 4);
            if (aSpellCosts[iSpell] <= iPlayerMana) {
                switch (iSpell) {
                    case MAGIC_MISSILE:
                    case DRAIN:
                        return iSpell;
                        break;
                    case SHIELD:
                        if (iShield <= 0) {
                            return iSpell;
                        }
                        break;
                    case POISON:
                        if (iPoison <= 0) {
                            return iSpell;
                        }
                        break;
                    case RECHARGE:
                        if (iRecharge <= 0) {
                            return iSpell;
                        }
                        break;
                }
            }
        }
    }
}

// Borrowed from https://stackoverflow.com/questions/1527803/generating-random-whole-numbers-in-javascript-in-a-specific-range
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}