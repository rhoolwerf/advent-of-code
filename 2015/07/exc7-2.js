const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n");//.map(x => map_instruction(x));

let oFormulas = new Map();
aInput.forEach(sLine => {
    let [sOutput, oInstruction] = map_instruction(sLine);
    oFormulas.set(sOutput, oInstruction);
});

let oBuffer = new Map();

let iAnswerA = get_value("a");
console.log("Getting a: ", iAnswerA);

// Reset the buffer, but pre-load B with the old A answer instead 
oBuffer = new Map();
oBuffer.set("b", iAnswerA);
console.log(get_value("a"));

function get_value(sOutput) {
    if (oBuffer.has(sOutput)) { return oBuffer.get(sOutput); }

    let oInstruction = oFormulas.get(sOutput);
    // console.log(sOutput, oInstruction);

    if (!oInstruction) { return sOutput; }

    if (oInstruction.input) { return oInstruction.input; }
    else {
        let iAnswer = 0;
        switch (oInstruction.cmd) {
            case 'NOT':
                iAnswer = ~get_value(oInstruction.p1) & 0xffff;
                break;
            case 'AND':
                iAnswer = get_value(oInstruction.p1) & get_value(oInstruction.p2);
                break;
            case 'OR':
                iAnswer = get_value(oInstruction.p1) | get_value(oInstruction.p2);
                break;
            case 'LSHIFT':
                iAnswer = get_value(oInstruction.p1) << oInstruction.p2;
                break;
            case 'RSHIFT':
                iAnswer = get_value(oInstruction.p1) >> oInstruction.p2;
                break;
            case 'CP':
                iAnswer = get_value(oInstruction.p1);
                break;
            default:
                return sOutput;
                break;
        }
        oBuffer.set(sOutput, iAnswer);
        return iAnswer;
    }
}

function map_instruction(sLine) {
    let [aInput, aOutput] = sLine.split(" -> ").map(x => x.split(" "));

    let oInstruction = {};

    switch (aInput.length) {
        case 1:
            if (!isNaN(aInput[0])) {
                oInstruction.input = Number(aInput[0]);
            } else {
                oInstruction.cmd = "CP"; // Just copy value directly
                oInstruction.p1 = aInput[0];
            }
            break;

        case 2:
            oInstruction.cmd = aInput[0];
            oInstruction.p1 = (isNaN(aInput[1]) ? aInput[1] : Number(aInput[1]));
            break;
        case 3:
            oInstruction.cmd = aInput[1];
            oInstruction.p1 = (isNaN(aInput[0]) ? aInput[0] : Number(aInput[0]));
            oInstruction.p2 = (isNaN(aInput[2]) ? aInput[2] : Number(aInput[2]));
            break;
    }

    return [aOutput[0], oInstruction];
}