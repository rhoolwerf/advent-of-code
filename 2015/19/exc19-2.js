const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aMapping = sInput.split("\n\n")[0].split("\n").map(x => x.split(" => "));
let sMolecule = sInput.split("\n\n")[1];

let iStepsTaken = 0;

// Just try to match every reolacaement-condition we can find, no need to be more complex than that (based on https://www.reddit.com/r/adventofcode/comments/3xflz8/day_19_solutions/cy4nalb/?utm_source=reddit&utm_medium=web2x&context=3)
while (sMolecule !== 'e') {
    aMapping.forEach(([sReplace, sSearch], iIndex) => {
        if (sMolecule.includes(sSearch)) {
            sMolecule = sMolecule.replace(sSearch, sReplace);
            iStepsTaken++;
            console.log(iStepsTaken, sMolecule);
        }
    });
}
console.log(iStepsTaken);