const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aInput = sInput.split("\n");

const sMolecule = aInput.pop();
aInput.pop();// Remove empty instruction

let oNewMolecules = new Set();
aInput.forEach(sLine => {
    let [sSearch, sReplace] = sLine.split(" => ");
    let oRegEx = new RegExp(sSearch, "g"); // g = global, (no i because case-insenstive)

    while ((oResult = oRegEx.exec(sMolecule))) {
        let sNewMolecule = sMolecule.substring(0, oResult.index) + sReplace + sMolecule.substring(oResult.index + sSearch.length)
    }
});
console.log(oNewMolecules.size);