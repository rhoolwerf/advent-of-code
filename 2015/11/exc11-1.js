
// console.log("hijklmmn", is_valid("hijklmmn"));
// console.log("abbceffg", is_valid("abbceffg"));
// console.log("abbcegjk", is_valid("abbcegjk"));

function is_valid(sAttempt) {

    // Check 1: must contain at least one 'sequence of three'
    let b1 = false;
    for (let i = 0; i < sAttempt.length; i++) {
        if (sAttempt.charCodeAt(i) === sAttempt.charCodeAt(i + 1) - 1 && sAttempt.charCodeAt(i) === sAttempt.charCodeAt(i + 2) - 2) {
            b1 = true;
            break;
        }
    }

    // Check 2: we do not accept letters i,o,l
    let b2 = !sAttempt.includes('i') && !sAttempt.includes('o') && !sAttempt.includes('l');

    // Check 3: find two seperate doubles
    let b3 = false;
    for (let i = 0; i < sAttempt.length; i++) {
        if (sAttempt.charCodeAt(i) === sAttempt.charCodeAt(i + 1)) {
            // First double found, find another one after this one of a different character
            for (let j = i + 2; j < sAttempt.length; j++) {
                if (sAttempt.charCodeAt(i) !== sAttempt.charCodeAt(j)) {
                    // Characters cannot match
                    if (sAttempt.charCodeAt(j) === sAttempt.charCodeAt(j + 1)) {
                        // Second pair found
                        b3 = true;
                    }
                }
            }
        }
    }

    return b1 && b2 && b3;
}

// Part 1: find the next
console.log(find_next("hxbxwxba"));

function find_next(sInput) {
    // Convert input to integer to start with
    let iInput = 0;
    let iStart = 0;
    sInput.split("").map(x => x.charCodeAt(0) - 97).forEach((iDigit, iPosition) => {
        iStart += iDigit * Math.pow(26, sInput.length - iPosition - 1);
    });

    // From start, convert each integer to password-string and validate
    for (let i = iStart + 1; true; i++) {
        let sConverted = i.toString(26);
        let sAttempt = sConverted.split("").map(x => String.fromCharCode(parseInt(x, 26) + 97)).join("");
        if (is_valid(sAttempt)) {
            return sAttempt;
        }
    }
}