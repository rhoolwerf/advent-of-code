const sInputFile = process.argv[2];
const fs = require("fs");
const sInput = fs.readFileSync(sInputFile, "utf8");
const aPresents = sInput.split("\n").map(Number).sort((a, b) => b - a);

// Calculate total weight and therefor weight-per-group
let iTotalWeight = 0;
aPresents.forEach(x => iTotalWeight += x);
let iWeightPerGroup = iTotalWeight / 4;

// Determine smallest amount of presents needed to fill the bag
let iLowestQE = 0;
let iSmallestGroup = aPresents.length + 1;
for (let i = 1; true; i++) {
    let sBinary = i.toString(2);
    if (sBinary.length > aPresents.length) { return; }
    while (sBinary.length < aPresents.length) {
        sBinary = "0" + sBinary;
    };

    let aPackages = [];
    let iSum = 0;
    for (let i = sBinary.length - 1; i >= 0 && iSum < iWeightPerGroup && aPackages.length < iSmallestGroup; i--) {
        if (sBinary.charAt(i) == 1) {
            aPackages.push(aPresents[i]);
            iSum += aPresents[i];
        }
    }
    if (iSum === iWeightPerGroup) {
        if (aPackages.length < iSmallestGroup) {
            console.log("Setting smallest group to:", aPackages.length);
            iSmallestGroup = aPackages.length;
        }

        let iQE = 1;
        aPackages.forEach(x => iQE *= x);
        if (!iLowestQE || iQE < iLowestQE) {
            console.log(aPackages);
            console.log("Lowest QE set to:", iQE);
            iLowestQE = iQE;
        }
    }
}
console.log(iLowestQE);