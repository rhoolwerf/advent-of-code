import { readFileSync } from 'fs';
import { EOL } from 'os';
import { mul } from '../util/util';

function count_trees(file: string, offsetY: number, offsetX: number) {
    const grid: Array<Array<string>> = readFileSync(file, 'utf8')
        .split(EOL)
        .map((r) => r.split(''));

    let trees = 0;

    for (let y = 0, x = 0; y < grid.length; y += offsetY, x += offsetX) {
        if (grid[y][x % grid[0].length] === '#') {
            trees++;
        }
    }

    // console.log(file, { offsetY, offsetX, trees });
    return trees;
}

function multi_slopes(file: string) {
    const slopes = [
        [1, 1],
        [3, 1],
        [5, 1],
        [7, 1],
        [1, 2],
    ].map(([offsetX, offsetY]) => count_trees(file, offsetY, offsetX));
    console.log(file, slopes, mul(slopes));
}

multi_slopes('./t1');
multi_slopes('./input');
