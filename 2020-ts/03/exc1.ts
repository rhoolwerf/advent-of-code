import { readFileSync } from 'fs';
import { EOL } from 'os';

function count_trees(file: string) {
    const grid: Array<Array<string>> = readFileSync(file, 'utf8')
        .split(EOL)
        .map((r) => r.split(''));

    let trees = 0;

    for (let y = 0, x = 0; y < grid.length; y++, x += 3) {
        if (grid[y][x % grid[0].length] === '#') {
            trees++;
        }
    }

    console.log(file, trees);
}

count_trees('./t1');
count_trees('./input');
