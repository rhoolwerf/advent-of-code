import { readFileSync } from 'fs';
import { EOL } from 'os';

const required = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']; // no 'cid'
function is_valid(pp: { [key: string]: string }): boolean {
    const valid = {
        all_required: true,
        byr: true,
        iyr: true,
        eyr: true,
        hgt: true,
        hcl: true,
        ecl: true,
        pid: true,
    };

    // Check for all required fields:
    valid.all_required = !required.some((req) => ![...Object.keys(pp)].includes(req));

    // Valid birth year?
    valid.byr = Number(pp.byr) >= 1920 && Number(pp.byr) <= 2002;

    // Valid issue year?
    valid.iyr = Number(pp.iyr) >= 2010 && Number(pp.iyr) <= 2020;

    // Valid expiration year?
    valid.eyr = Number(pp.eyr) >= 2020 && Number(pp.eyr) <= 2030;

    // Height must either be in 'cm' or 'in'
    if (pp.hgt?.endsWith('cm')) {
        const val = Number(pp.hgt.substring(0, pp.hgt.length - 2));
        if (val < 150 || val > 193) valid.hgt = false;
    } else if (pp.hgt?.endsWith('in')) {
        const val = Number(pp.hgt.substring(0, pp.hgt.length - 2));
        if (val < 59 || val > 76) valid.hgt = false;
    } else {
        // No valid unit of measure
        valid.hgt = false;
    }

    // Valid hair color?
    valid.hcl = /^#[0-9a-f]{6}$/.test(pp.hcl);

    // Valid eye color
    valid.ecl = ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'].includes(pp.ecl);

    // Valid Passport ID
    valid.pid = /^[0-9]{9}$/.test(pp.pid);

    return (
        valid.all_required && valid.byr && valid.iyr && valid.eyr && valid.hgt && valid.hcl && valid.ecl && valid.pid
    );
}

function count_valid(file: string) {
    const input = readFileSync(file, 'utf8')
        .split(`${EOL}${EOL}`)
        .map((r) =>
            Object.assign(
                {},
                ...r
                    .split(EOL)
                    .join(' ')
                    .split(' ')
                    .map((c) => c.match(/([a-z]+):([^\s\\]+)/))
                    .map(([_, key, value]) => ({ [key]: value }))
            )
        );

    return input.filter(is_valid).length;
}

function test(file: string, expected: number) {
    const actual = count_valid(file);
    console.assert(expected === actual, '', { file, expected, actual });
}

test('./t2-valid', 4);
test('./t2-invalid', 0);
console.log(count_valid('./input'));
