import { readFileSync } from 'fs';
import { EOL } from 'os';

function count_valid(file: string) {
    const required = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']; // no 'cid'

    const input = readFileSync(file, 'utf8')
        .split(`${EOL}${EOL}`)
        .map((r) =>
            Object.assign(
                {},
                ...r
                    .split(EOL)
                    .join(' ')
                    .split(' ')
                    .map((c) => c.match(/([a-z]+):([^\s\\]+)/))
                    .map(([_, key, value]) => ({ [key]: value }))
            )
        );

    const valid = input.filter((pp) => !required.some((req) => ![...Object.keys(pp)].includes(req)));

    console.log(file, valid.length);
}
count_valid('./t1');
count_valid('./input');
