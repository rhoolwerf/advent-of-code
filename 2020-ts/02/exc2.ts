import { readFileSync } from 'fs';
import { EOL } from 'os';

function count_valid(file: string) {
    const checks = readFileSync(file, 'utf8')
        .split(EOL)
        .map((l) => [...l.match(/(\d+)-(\d+) ([a-z]): ([a-z]+)/)])
        .map(([_, p1, p2, find, string]) => ({ p1: Number(p1), p2: Number(p2), find, string }))
        .map(({ p1, p2, string, ...rest }) => ({
            ...rest,
            p1,
            p2,
            string,
            v1: string.charAt(p1 - 1),
            v2: string.charAt(p2 - 1),
        }))
        .map(({ v1, v2, find, ...rest }) => ({
            v1,
            v2,
            find,
            ...rest,
            valid: (v1 == find || v2 === find) && v1 !== v2,
        }));
    console.log(file, checks.filter(({ valid }) => valid).length);
}

count_valid('./t1');
count_valid('./input');
