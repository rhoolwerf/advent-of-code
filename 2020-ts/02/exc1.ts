import { readFileSync } from 'fs';
import { EOL } from 'os';

function count_valid(file: string) {
    const checks = readFileSync(file, 'utf8')
        .split(EOL)
        .map((l) => [...l.match(/(\d+)-(\d+) ([a-z]): ([a-z]+)/)])
        .map(([_, lower, upper, find, string]) => ({ lower: Number(lower), upper: Number(upper), find, string }))
        .map((obj) => ({
            ...obj,
            count: obj.string.split('').reduce(
                (count, char) => {
                    count[char] = (count[char] || 0) + 1;
                    return count;
                },
                {} as { [key: string]: number }
            ),
        }))
        .map(({ lower, upper, find, count, ...rest }) => ({
            ...rest,
            lower,
            upper,
            find,
            count,
            valid: count[find] >= lower && count[find] <= upper,
        }));
    console.log(file, checks.filter(({ valid }) => valid).length);
}

count_valid('./t1');
count_valid('./input');
