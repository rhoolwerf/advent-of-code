import { readFileSync } from 'fs';
import { EOL } from 'os';

function proc(file: string, preamble: number) {
    const numbers = readFileSync(file, 'utf8').split(EOL).map(Number);
    for (let i = preamble; i < numbers.length; i++) {
        const check = numbers[i];
        let found = false;

        for (let x = i - preamble; x < i - 1; x++) {
            for (let y = x + 1; y < i; y++) {
                const v1 = numbers[x];
                const v2 = numbers[y];

                if (v1 + v2 === check) {
                    found = true;
                }
            }
        }

        if (!found) {
            console.log(file, check, 'is not found');
        }
    }
}

proc('./t1', 5);
proc('./input', 25);
