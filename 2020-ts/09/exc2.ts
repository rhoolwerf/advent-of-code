import { readFileSync } from 'fs';
import { EOL } from 'os';
import { sum } from '../util/util';

function find_missing_check(numbers: Array<number>, preamble: number): number {
    for (let i = preamble; i < numbers.length; i++) {
        const check = numbers[i];
        let found = false;

        for (let x = i - preamble; x < i - 1; x++) {
            for (let y = x + 1; y < i; y++) {
                const v1 = numbers[x];
                const v2 = numbers[y];

                if (v1 + v2 === check) {
                    found = true;
                }
            }
        }

        if (!found) {
            return check;
        }
    }
}

function proc(file: string, preamble: number) {
    const numbers = readFileSync(file, 'utf8').split(EOL).map(Number);
    const check = find_missing_check(numbers, preamble);

    for (let i = 0; i < numbers.length; i++) {
        const sub = [];
        for (let j = i + 1; sum(sub) < check; j++) {
            sub.push(numbers[j]);
        }

        if (sum(sub) === check) {
            const min = Math.min(...sub);
            const max = Math.max(...sub);
            console.log(file, { min, max, prod: min + max });
            return;
        }
    }
}

proc('./t1', 5);
proc('./input', 25);
