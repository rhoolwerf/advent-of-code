import { readFileSync } from 'fs';
import { EOL } from 'os';

type Instruction = { cmd: 'nop' | 'acc' | 'jmp'; val: number };
type Instructions = Array<Instruction>;

function run_file(file: string) {
    const input: Instructions = readFileSync(file, 'utf8')
        .split(EOL)
        .map((r) => {
            const cmd = r.split(' ')[0] as Instruction['cmd'];
            const val = Number(r.split(' ')[1]);
            return { cmd, val };
        });

    const choices = input
        .map((r, i) => ({ r, i }))
        .filter(({ r }) => r.cmd !== 'acc')
        .map(({ i }) => i);

    const runs = Array.from({ length: choices.length }, (_, i) => {
        const base = JSON.parse(JSON.stringify(input));
        base[choices[i]].cmd = base[choices[i]].cmd === 'nop' ? 'jmp' : 'nop';
        return base;
    });

    const results = runs.map((r) => proc(r));
    const [result] = results.filter((r) => r !== Infinity);
    console.log({ result });
}

function proc(instr: Instructions): number {
    const pointers: Array<number> = [];
    let accumulator = 0;
    for (let pointer = 0; pointer < instr.length; ) {
        if (pointers.includes(pointer)) {
            return Infinity;
        }

        pointers.push(pointer);

        const { cmd, val } = instr[pointer];

        switch (cmd) {
            case 'nop':
                pointer++;
                break;

            case 'acc':
                accumulator += val;
                pointer++;
                break;

            case 'jmp':
                pointer += val;
                break;

            default:
                console.error('Unknown instruction:', { cmd, val });
                process.exit(-1);
        }
    }

    return accumulator;
}

// run_file('./t1');
run_file('./input');
