import { readFileSync } from 'fs';
import { EOL } from 'os';

function proc(file: string) {
    const instr = readFileSync(file, 'utf8')
        .split(EOL)
        .map((r) => ({ cmd: r.split(' ')[0], val: Number(r.split(' ')[1]) }));

    const pointers: Array<number> = [];
    let accumulator = 0;
    for (let pointer = 0; pointer < instr.length; ) {
        if (pointers.includes(pointer)) {
            console.log('Repeat of', { pointer, accumulator });
            return;
        }

        pointers.push(pointer);

        const { cmd, val } = instr[pointer];

        switch (cmd) {
            case 'nop':
                pointer++;
                break;

            case 'acc':
                accumulator += val;
                pointer++;
                break;

            case 'jmp':
                pointer += val;
                break;

            default:
                console.error('Unknown instruction:', { cmd, val });
                process.exit(-1);
        }
    }
}

proc('./t1');
proc('./input');
