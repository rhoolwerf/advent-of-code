import { readFileSync } from 'fs';
import { EOL } from 'os';

function process_array(array: Array<number>, steps: Array<boolean>): number {
    for (const step of steps) {
        if (step) {
            array.splice(array.length / 2);
        } else {
            array.splice(0, array.length / 2);
        }
    }

    return array[0];
}

function get_seat(instr: string): { row: number; col: number } {
    const rows = Array.from({ length: 128 }, (_, i) => i);
    const cols = Array.from({ length: 8 }, (_, i) => i);

    const row_steps = instr
        .substring(0, 6)
        .split('')
        .map((s) => s === 'F');

    const col_steps = instr
        .substring(7)
        .split('')
        .map((s) => s === 'L');

    const row = process_array(rows, row_steps);
    const col = process_array(cols, col_steps);

    return { row, col };
}

function get_seat_id(instr: string): number {
    const { row, col } = get_seat(instr);
    return row * 8 + col;
}

function test(instr: string, expected: number) {
    const actual = get_seat_id(instr);
    console.assert(expected === actual, '', { instr, expected, actual });
}

test('FBFBBFFRLR', 357);
test('FFFBBBFRRR', 119);
test('BBFFBBFRLL', 820);

const input = readFileSync('./input', 'utf8').split(EOL);
const seat_ids = input.map(get_seat_id);
const highest = Math.max(...seat_ids);
console.log({ highest });
