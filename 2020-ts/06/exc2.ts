import { readFileSync } from 'fs';
import { EOL } from 'os';
import { sum } from '../util/util';

function calc_answers(file: string) {
    const groups = readFileSync(file, 'utf8')
        .split(`${EOL}${EOL}`)
        .map((g) =>
            g
                .split(EOL)
                .map((p) =>
                    p.split('').reduce(
                        (count, answer) => {
                            count[answer] = (count[answer] || 0) + 1;
                            return count;
                        },
                        {} as { [key: string]: number }
                    )
                )
                .reduce(
                    (count, person) => {
                        [...Object.keys(person)].forEach((a) => {
                            count[a] = (count[a] || 0) + 1;
                        });
                        count.persons++;

                        return count;
                    },
                    { persons: 0 } as { [key: string]: number }
                )
        );

    const mapped = groups.map(
        ({ persons, ...count }) => [...Object.keys(count)].filter((a) => count[a] === persons).length
    );
    console.log(file, sum(mapped));
}

calc_answers('./t1');
calc_answers('./input');
