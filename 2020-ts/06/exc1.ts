import { readFileSync } from 'fs';
import { EOL } from 'os';
import { sum } from '../util/util';

function calc_answers(file: string) {
    const groups = readFileSync(file, 'utf8').split(`${EOL}${EOL}`);
    const answers = groups.map((g) => new Set(g.split(EOL).join('').split('')));
    const counts = answers.map((s) => s.size);

    console.log(file, sum(counts));
}

calc_answers('./t1');
calc_answers('./input');
