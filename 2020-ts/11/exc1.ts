import { readFileSync } from 'fs';
import { EOL } from 'os';
import { sum } from '../util/util';

function proc(file: string) {
    let grid = readFileSync(file, 'utf8')
        .split(EOL)
        .map((r) => r.split(''));

    for (let i = 0; true; i++) {
        const future: Array<Array<string>> = JSON.parse(JSON.stringify(grid));

        for (let y = 0; y < grid.length; y++) {
            for (let x = 0; x < grid[y].length; x++) {
                // Count surrounding arrangment
                const surrounding = [
                    // Above
                    [-1, -1],
                    [-1, 0],
                    [-1, 1],

                    // Same row
                    [0, -1],
                    [0, 1],

                    // Below
                    [1, -1],
                    [1, 0],
                    [1, 1],
                ]
                    .map(([offsetY, offsetX]) => (grid[y + offsetY] ? grid[y + offsetY][x + offsetX] || '.' : '.'))
                    .reduce(
                        (count, pos) => {
                            if (pos === 'L') count.empty++;
                            else if (pos === '#') count.occupied++;
                            return count;
                        },
                        { empty: 0, occupied: 0 }
                    );

                // Adjust future grid based on current surrounding
                if (grid[y][x] === 'L' && surrounding.occupied === 0) future[y][x] = '#';
                if (grid[y][x] === '#' && surrounding.occupied >= 4) future[y][x] = 'L';
            }
        }

        // If future is the same as base, we're done
        if (grid.map((r) => r.join('')).join('') === future.map((r) => r.join('')).join('')) {
            console.log(file, 'After', i, 'rounds', sum(grid.map((r) => r.filter((c) => c === '#').length)));
            return;
        }

        grid = future;
    }
}

proc('./t1');
proc('./input');
