import { readFileSync } from 'fs';
import { EOL } from 'os';

function find_2020(file: string) {
    const numbers = readFileSync(file, 'utf8').split(EOL).map(Number);
    for (let i = 0; i < numbers.length; i++) {
        const v1 = numbers[i];
        for (let j = i + 1; j < numbers.length; j++) {
            const v2 = numbers[j];
            if (v1 + v2 === 2020) {
                console.log({ file, v1, v2, mul: v1 * v2 });
                return;
            }
        }
    }
}

find_2020('./t1');
find_2020('./input');
