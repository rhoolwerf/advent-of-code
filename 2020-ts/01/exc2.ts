import { readFileSync } from 'fs';
import { EOL } from 'os';

function find_2020(file: string) {
    const numbers = readFileSync(file, 'utf8').split(EOL).map(Number);
    for (let i = 0; i < numbers.length; i++) {
        const v1 = numbers[i];
        for (let j = i + 1; j < numbers.length; j++) {
            const v2 = numbers[j];

            for (let k = j + 1; k < numbers.length; k++) {
                const v3 = numbers[k];

                if (v1 + v2 + v3 === 2020) {
                    console.log({ file, v1, v2, v3, mul: v1 * v2 * v3 });
                    return;
                }
            }
        }
    }
}

find_2020('./t1');
find_2020('./input');
