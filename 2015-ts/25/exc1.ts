// // Testing, testing...
// let code = 20151125;
// let targetRow = 6;
// let targetColumn = 6;

// Had to borrow inspiration from the previous attempt, which also borrowed inspiration
// Basically, we just need a number, don't actually need to keep track of previous values

// Actual input
let code = 20151125;
let targetRow = 3010;
let targetColumn = 3019;

// Lets go!
let row = 1;
let column = 1;

while (row !== targetRow || column !== targetColumn) {
    code = (code * 252533) % 33554393;

    if (row === 1) {
        // Als we op de eerste rij zijn, beginnen we op de volgende regel in de eerste kolom
        row = column + 1;
        column = 1;
    } else {
        // Anders altijd regel omhoog, kolom rechts
        row--;
        column++;
    }
}
console.log({ code });
