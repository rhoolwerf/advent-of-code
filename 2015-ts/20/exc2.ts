const INPUT = 29000000;

const elfVisits: { [key: number]: number } = {};

function getElfsVisiting(house: number): Array<number> {
    const elfs = new Set<number>();
    for (let i = Math.floor(Math.sqrt(house)); i > 0; i--) {
        if (house % i === 0) {
            elfs.add(i);
            elfs.add(house / i);
        }
    }

    return Array.from(elfs.values());
}

for (let house = 1; true; house++) {
    const elfs = getElfsVisiting(house).sort((a, b) => a - b);
    let presents = 0;
    for (const elf of elfs) {
        if (!(elf in elfVisits)) elfVisits[elf] = 0;

        if (elfVisits[elf] <= 50) {
            elfVisits[elf]++;
            presents += 11 * elf;
        }
    }

    if (presents >= INPUT) {
        console.log({ house, presents });
        break;
    }
}
