const INPUT = 29000000;

function getElfsVisiting(house: number): Array<number> {
    const elfs = new Set<number>();
    for (let i = Math.floor(Math.sqrt(house)); i > 0; i--) {
        if (house % i === 0) {
            elfs.add(i);
            elfs.add(house / i);
        }
    }

    return Array.from(elfs.values());
}

for (let house = 1; true; house++) {
    const elfs = getElfsVisiting(house).sort((a, b) => a - b);
    const presents = elfs.reduce((sum, val) => sum + val * 10, 0);
    if (presents >= INPUT) {
        console.log({ house, presents });
        break;
    }
}
