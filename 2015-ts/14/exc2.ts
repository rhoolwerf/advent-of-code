import { readFileSync } from 'fs';
import { EOL } from 'os';

const SECONDS = Number(process.argv[3]);

const reindeers = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) => [
        /([a-zA-Z]+) can fly (\d+) km\/s for (\d+) seconds, but then must rest for (\d+) seconds./.exec(line),
    ])
    .map((match) => [...match].flat())
    .map(([_1, name, speed, duration, pause]) => ({
        name,
        speed: Number(speed),
        duration: Number(duration),
        pause: Number(pause),
    }))
    .map(({ name, speed, duration, pause }) => {
        const steps: Array<number> = [];
        for (let i = 0; i < duration; i++) steps.push(speed);
        for (let i = 0; i < pause; i++) steps.push(0);
        return { name, speed, duration, pause, steps, score: 0, distance: 0 };
    });

for (let i = 0; i < SECONDS; i++) {
    // Process each reindeer in their steps / distance
    reindeers.forEach((r) => {
        r.distance += r.steps[i % r.steps.length];
    });

    // Get the furthest reindeer, give a point to all reindeers at that distance
    const top = Math.max(...reindeers.map(({ distance }) => distance));
    reindeers.filter(({ distance }) => distance === top).forEach((r) => r.score++);
}

const scores = reindeers.map(({ name, distance, score }) => ({ name, distance, score }));
console.log(scores);
console.log('Winner after', SECONDS, 'seconds:', scores.sort((a, b) => b.score - a.score)[0]);
