import { readFileSync } from 'fs';
import { EOL } from 'os';

const SECONDS = Number(process.argv[3]);

const reindeers = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) => [
        /([a-zA-Z]+) can fly (\d+) km\/s for (\d+) seconds, but then must rest for (\d+) seconds./.exec(line),
    ])
    .map((match) => [...match].flat())
    .map(([_1, name, speed, duration, pause]) => ({
        name,
        speed: Number(speed),
        duration: Number(duration),
        pause: Number(pause),
    }))
    .map(({ name, speed, duration, pause }) => {
        const steps: Array<number> = [];
        for (let i = 0; i < duration; i++) steps.push(speed);
        for (let i = 0; i < pause; i++) steps.push(0);
        return { name, speed, duration, pause, steps };
    });

const sum = (values: Array<number>): number => values.reduce((sum, val) => sum + val, 0);

const distances = reindeers.map(({ name, steps }) => {
    const blocks = Math.floor(SECONDS / steps.length);
    const remainder = SECONDS % steps.length;

    const distance = blocks * sum(steps) + sum(steps.slice(0, remainder));

    return { name, distance };
});

console.log({ distances });
console.log('Winner after', SECONDS, 'seconds:', distances.sort((a, b) => b.distance - a.distance)[0]);
