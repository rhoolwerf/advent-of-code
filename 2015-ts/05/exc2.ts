import { readFileSync } from 'fs';
import { EOL } from 'os';

function nice(input: string): boolean {
    let pairTwice = false;
    let repeatBetween = false;

    for (let i = 0; i < input.length; i++) {
        if (input[i] === input[i + 2]) repeatBetween = true;

        for (let j = i + 2; j < input.length; j++) {
            if (input[i] === input[j] && input[i + 1] === input[j + 1]) pairTwice = true;
        }
    }

    return repeatBetween && pairTwice;
}

function test(input: string, expected: boolean): void {
    const actual = nice(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

test('qjhvhtzxzqqjkmpb', true);
test('xxyxx', true);
test('uurcxstgmygtbstg', false);
test('ieodomkazucvgmuy', false);

console.log(readFileSync('./input', 'utf8').split(EOL).map(nice).filter(Boolean).length);
