import { readFileSync } from 'fs';
import { EOL } from 'os';

const vowels = ['a', 'e', 'i', 'o', 'u'];
const forbidden = ['ab', 'cd', 'pq', 'xy'];

function nice(input: string): boolean {
    let hasDouble = false;
    let vowelCount = 0;
    let hasForbidden = false;

    // Are there any doubles and/or vowels
    for (let i = 0; i < input.length; i++) {
        if (vowels.includes(input[i])) vowelCount++;
        if (input[i] === input[i + 1]) hasDouble = true;
    }

    // Are any of the forbidden strings found?
    forbidden.forEach((f) => (hasForbidden = input.indexOf(f) >= 0 ? true : hasForbidden));

    return hasDouble && vowelCount >= 3 && !hasForbidden;
}

function test(input: string, expected: boolean): void {
    const actual = nice(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

test('ugknbfddgicrmopn', true);
test('aaa', true);
test('jchzalrnumimnmhp', false);
test('haegwjzuvuyypxyu', false);
test('dvszwmarrgswjxmb', false);

console.log(readFileSync('./input', 'utf8').split(EOL).map(nice).filter(Boolean).length);
