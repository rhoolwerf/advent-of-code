import { createHash } from 'crypto';
function exec(input: string): number {
    let i = 1;
    while (!createHash('md5').update(`${input}${i}`).digest('hex').startsWith('00000')) {
        i++;
    }
    return i;
}

function test(input: string, expected: number): void {
    const actual = exec(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

test('abcdef', 609043);
test('pqrstuv', 1048970);

console.log(exec('ckczppom'));
