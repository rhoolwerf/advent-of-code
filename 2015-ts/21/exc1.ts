import { readFileSync } from 'fs';
import { sum } from '../util/util';
import { EOL } from 'os';

type Player = { name: 'Player' | 'Boss'; hp: number; damage: number; armor: number };

type Item = { name: String; cost: number; damage: number; armor: number; type: 'W' | 'A' | 'R' };
const store: Array<Item> = [
    // Weapons
    { name: 'Dagger', cost: 8, damage: 4, armor: 0, type: 'W' },
    { name: 'Shortsword', cost: 10, damage: 5, armor: 0, type: 'W' },
    { name: 'Warhammer', cost: 25, damage: 6, armor: 0, type: 'W' },
    { name: 'Longsword', cost: 40, damage: 7, armor: 0, type: 'W' },
    { name: 'Greataxe', cost: 74, damage: 8, armor: 0, type: 'W' },

    // Armor
    { name: 'Leather', cost: 13, damage: 0, armor: 1, type: 'A' },
    { name: 'Chainmail', cost: 31, damage: 0, armor: 2, type: 'A' },
    { name: 'Splintmail', cost: 53, damage: 0, armor: 3, type: 'A' },
    { name: 'Bandedmail', cost: 75, damage: 0, armor: 4, type: 'A' },
    { name: 'Platemail', cost: 102, damage: 0, armor: 5, type: 'A' },

    // Rings
    { name: 'Damage +1', cost: 25, damage: 1, armor: 0, type: 'R' },
    { name: 'Damage +2', cost: 50, damage: 2, armor: 0, type: 'R' },
    { name: 'Damage +3', cost: 100, damage: 3, armor: 0, type: 'R' },
    { name: 'Defense +1', cost: 20, damage: 0, armor: 1, type: 'R' },
    { name: 'Defense +2', cost: 40, damage: 0, armor: 2, type: 'R' },
    { name: 'Defense +3', cost: 80, damage: 0, armor: 3, type: 'R' },
];

// Generate all possible player outfits
const outfits: Array<{ equipment: Array<Item>; cost: number; damage: number; armor: number }> = [];
for (let i = 0; i.toString(2).length <= store.length; i++) {
    const binary = i.toString(2).padStart(store.length, '0');
    const flags = binary.split('').map(Number).map(Boolean);
    const equipment = flags.map((toggle, index) => (toggle ? store[index] : null)).filter(Boolean) as Array<Item>;

    // Validate outfit
    const weapons = equipment.filter(({ type }) => type === 'W').length;
    const armor = equipment.filter(({ type }) => type === 'A').length;
    const rings = equipment.filter(({ type }) => type === 'R').length;

    if (weapons === 1 && armor <= 1 && rings <= 2) {
        outfits.push({
            equipment,
            cost: sum(equipment.map(({ cost }) => cost)),
            damage: sum(equipment.map(({ damage }) => damage)),
            armor: sum(equipment.map(({ armor }) => armor)),
        });
    }
}

function simulate_fight(player: Player, boss: Player): Player {
    const isDead = (player: Player): boolean => player.hp <= 0;
    const attack = (source: Player, target: Player) => {
        target.hp -= source.damage - target.armor;
    };

    while (!isDead(player) && !isDead(boss)) {
        attack(player, boss);
        if (!isDead(boss)) {
            attack(boss, player);
        }
    }

    return player.hp > 0 ? player : boss;
}

const input = readFileSync('./input', 'utf8')
    .split(EOL)
    .map((row) => row.split(': '));

const matches = outfits.map((outfit) => {
    const { damage, armor } = outfit;
    const player: Player = { name: 'Player', hp: 100, damage, armor };
    const boss: Player = {
        name: 'Boss',
        hp: Number(input[0][1]),
        damage: Number(input[1][1]),
        armor: Number(input[2][1]),
    };
    return { outfit, winner: simulate_fight(player, boss) };
});

console.dir(
    matches.filter(({ winner: { name } }) => name === 'Player').sort((a, b) => a.outfit.cost - b.outfit.cost)[0],
    { depth: null }
);
