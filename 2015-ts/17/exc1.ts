import { readFileSync } from 'fs';
import { EOL } from 'os';
import { sum } from '../util/util';

const input = readFileSync(process.argv[2], 'utf8').split(`${EOL}${EOL}`);
const liters = Number(input[0]);
const buckets = input[1].split(EOL).map(Number);

// Make a binary representation of each bucket enabled or no
// So for example, i = 5 means 101 so for our example (existing of 5 buckets)
// this becomes 00101 which means buckets 10 and 5 (second one)
// which becomes 15 liters which isn't enough to satisfy the required 25

let valid = 0;
for (let i = 1; i.toString(2).length <= buckets.length; i++) {
    const flags = i.toString(2).padStart(buckets.length, '0').split('').map(Number).map(Boolean);
    const available = buckets.map((b, j) => (flags[j] ? b : 0)).filter(Boolean);
    if (sum(available) === liters) {
        // console.log(i, i.toString(2), flags, available, sum(available));
        valid++;
    }
}

console.log({ valid });
