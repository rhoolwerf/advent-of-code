import { readFileSync } from 'fs';

function find_digits(input: string): number {
    const regex_digits = new RegExp(/-?\d+/, 'g');
    const matches = input.matchAll(regex_digits);
    const mapped = [...matches].map(([match]) => Number(match));
    return mapped.reduce((sum, val) => sum + val, 0);
}

function test_find_digits(input: string, expected: number): void {
    const actual = find_digits(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

test_find_digits('[1,2,3]', 6);
test_find_digits('{"a":2,"b":4}', 6);
test_find_digits('[[[3]]]', 3);
test_find_digits('{"a":{"b":4},"c":-1}', 3);
test_find_digits('{"a":[-1,1]}', 0);
test_find_digits('[-1,{"a":1}]', 0);
test_find_digits('[]', 0);
test_find_digits('{}', 0);

console.log(find_digits(readFileSync('./input', 'utf8')));
