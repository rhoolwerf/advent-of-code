import { readFileSync } from 'fs';

function find_digits(input: string): number {
    const data = parse_unknown(JSON.parse(input));
    let numbers = data as Array<number>;
    let flat = 0;
    while (isNaN(numbers.map(Number).reduce((sum, val) => sum + val, 0))) {
        numbers = numbers.flat();
        flat++;
    }
    console.log('Needed flattening:', flat);
    return numbers.reduce((sum, val) => sum + val, 0);
}

function parse_unknown(input: unknown): unknown {
    if (Array.isArray(input)) {
        return parse_array(input);
    } else if (typeof input === 'object') {
        return parse_object(input as object);
    } else if (typeof input === 'number') {
        return Number(input);
    }

    return 0;
}

function parse_array(input: Array<unknown>): unknown {
    return input.map(parse_unknown);
}

function parse_object(input: object): unknown {
    const values = Object.values(input);
    if (values.includes('red')) {
        return 0;
    }
    return values.map(parse_unknown);
}

function test_find_digits(input: string, expected: number): void {
    const actual = find_digits(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

// test_find_digits('[1,2,3]', 6);
// test_find_digits('{"a":2,"b":4}', 6);
// test_find_digits('[1,{"c":"red","b":2},3]', 4);
// test_find_digits('{"d":"red","e":[1,2,3,4],"f":5}', 0);
// test_find_digits('[1,"red",5]', 6);

console.log(find_digits(readFileSync('./input', 'utf8')));
