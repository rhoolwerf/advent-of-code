import { readFileSync } from 'fs';
import { EOL } from 'os';
import { shuffle } from '../util/util';

const input = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((line) => line.split(' '))
    .map(([person, _1, plus_or_minus, value, _2, _3, _4, _5, _6, _7, neighbour]) => ({
        person,
        value: plus_or_minus === 'gain' ? Number(value) : -1 * Number(value),
        neighbour: neighbour.slice(0, neighbour.length - 1),
    }));

const persons = Array.from(
    new Set([input.map(({ person }) => person), input.map(({ neighbour }) => neighbour)].flat())
);

function find_happiness(arrangment: Array<string>): number {
    return arrangment
        .map((p1, i) => {
            const left = persons[i === 0 ? persons.length - 1 : i - 1];
            const right = persons[(i + 1) % persons.length];

            const left_val =
                input.find(({ person, neighbour }) => person === p1 && left === neighbour)?.value || Infinity;
            const right_val =
                input.find(({ person, neighbour }) => person === p1 && right === neighbour)?.value || Infinity;
            return left_val + right_val;
        })
        .reduce((sum, val) => sum + val, 0);
}

let maxHappiness = 0;
for (let i = 0; i < 1000000; i++) {
    shuffle(persons);
    const happiness = find_happiness(persons);
    maxHappiness = Math.max(maxHappiness, happiness);
    // console.log({ persons: persons.join(', '), happiness, maxHappiness });
}
console.log('Max happiness:', maxHappiness);
