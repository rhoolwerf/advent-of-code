import { readFileSync } from 'fs';

function run(input: string): number {
    const mapped = input.split('').map((c) => (c === '(' ? 1 : -1));
    let sum = 0;
    for (const index in mapped) {
        sum += mapped[index];
        if (sum < 0) {
            return Number(index) + 1;
        }
    }

    return -1;
}

function test(input: string, expected: number): void {
    const actual = run(input);
    console.assert(actual === expected, `Test failed:`, { input, expected, actual });
}

test(')', 1);
test('()())', 5);

console.log(run(readFileSync(process.argv[2], 'utf8')));
