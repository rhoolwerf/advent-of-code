import { readFileSync } from 'fs';

function run(input: string): number {
    return input
        .split('')
        .map((c) => (c === '(' ? 1 : -1))
        .reduce((sum, val) => sum + val, 0);
}

function test(input: string, expected: number): void {
    const actual = run(input);
    console.assert(actual === expected, `Test failed:`, { input, expected, actual });
}

test('(())', 0);
test('))(((((', 3);

console.log(run(readFileSync(process.argv[2], 'utf8')));
