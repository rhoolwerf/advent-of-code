import { readFileSync } from 'fs';
import { EOL } from 'os';
import { shuffle } from '../util/util';

const paths: Map<string, Array<{ city: string; distance: number }>> = new Map();
const nodes: Set<string> = new Set();

function register_paths(from: string, to: string, distance: number): void {
    nodes.add(from);
    nodes.add(to);
    register_path(from, to, distance);
    register_path(to, from, distance);
}

function register_path(from: string, to: string, distance: number): void {
    if (!paths.has(from)) {
        paths.set(from, [{ city: to, distance }]);
    } else {
        paths.get(from)?.push({ city: to, distance });
    }
}

readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((r) => r.split(' '))
    .forEach(([from, _1, to, _2, distance]) => register_paths(from, to, Number(distance)));

const order = Array.from(nodes);
const journeys = new Map<string, number>();

// Just bruteforce our way into an answer
for (let i = 0; i < 100000; i++) {
    shuffle(order);
    const distance = order.reduce((sum, from, index) => {
        const to = order[index + 1];
        if (!to) return sum;

        const f = paths.get(from)?.find(({ city }) => city === to);
        if (f) return sum + f.distance;
        return Infinity;
    }, 0);

    if (distance !== Infinity) journeys.set(order.join(', '), distance);
}

console.log('Shortest:', [...journeys.values()].sort((a, b) => a - b)[0]);
console.log('Longest:', [...journeys.values()].sort((a, b) => b - a)[0]);
