import { readFileSync } from 'fs';
import { EOL } from 'os';

const input = readFileSync('./input', 'utf8').split(EOL);

console.log(
    input.map((r) => JSON.stringify(r).length).reduce((sum, val) => sum + val, 0) -
        input.map((r) => r.length).reduce((sum, val) => sum + val, 0)
);
