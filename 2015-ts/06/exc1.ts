import { readFileSync } from 'fs';
import { EOL } from 'os';

// Init grid
const grid: Array<Array<boolean>> = [];
for (let y = 0; y <= 999; y++) {
    grid[y] = [];
    for (let x = 0; x <= 999; x++) {
        grid[y][x] = false;
    }
}

const on = (_inp: boolean): boolean => true;
const off = (_inp: boolean): boolean => false;
const toggle = (inp: boolean): boolean => !inp;

// Process input
readFileSync('./input', 'utf8')
    .split(EOL)
    .forEach((inst) => {
        const parts = inst.split(' ');
        let func: (i: boolean) => boolean;
        let from: Array<number>;
        let to: Array<number>;

        if (parts[0] === 'turn') {
            from = parts[2].split(',').map(Number);
            to = parts[4].split(',').map(Number);

            func = parts[1] === 'on' ? on : off;
        } else if (parts[0] === 'toggle') {
            func = toggle;
            from = parts[1].split(',').map(Number);
            to = parts[3].split(',').map(Number);
        } else {
            console.error('Instruction unclear:', inst);
            process.exit(-1);
        }

        for (let y = from[1]; y <= to[1]; y++) {
            for (let x = from[0]; x <= to[0]; x++) {
                grid[y][x] = func(grid[y][x]);
            }
        }
    });

// Calculate lights on
console.log(grid.map((row) => row.filter(Boolean).length).reduce((sum, val) => sum + val, 0));
