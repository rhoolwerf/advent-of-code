import { readFileSync } from 'fs';
import { EOL } from 'os';

const instructions = readFileSync(process.argv[2], 'utf8').split(EOL);
const registers: { [key: string]: () => number } = {};

const buffer = new Map<string, number>();

function get(key: string): number {
    if (!isNaN(Number(key))) return Number(key);

    const val = buffer.get(key) || registers[key]();
    buffer.set(key, val);

    return val;
}

for (const instr of instructions) {
    const [input, target] = instr.split(' -> ');
    const parts = input.split(' ');

    if (parts.length === 1) {
        registers[target] = () => get(input);
    } else if (input.startsWith('NOT')) {
        registers[target] = () => ~get(parts[1]) & 0xffff;
    } else if (input.indexOf('AND') >= 0) {
        registers[target] = () => get(parts[0]) & get(parts[2]);
    } else if (input.indexOf('OR') >= 0) {
        registers[target] = () => get(parts[0]) | get(parts[2]);
    } else if (input.indexOf('LSHIFT') >= 0) {
        registers[target] = () => get(parts[0]) << Number(parts[2]);
    } else if (input.indexOf('RSHIFT') >= 0) {
        registers[target] = () => get(parts[0]) >> Number(parts[2]);
    } else {
        console.error('Instruction unclear:', instr);
        process.exit(-1);
    }
}

buffer.set('b', 956);

console.log(get('a'));
