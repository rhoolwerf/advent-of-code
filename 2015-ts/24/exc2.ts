import { readFileSync } from 'fs';
import { EOL } from 'os';
import { sum } from '../util/util';

const packages = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map(Number)
    .sort((a, b) => a - b);

const fourthGroupSize = sum(packages) / 4;

let smallestGroupSize = Infinity;
let QE = Infinity;

for (let i = 1; i.toString(2).length <= packages.length; i++) {
    const binary = i.toString(2).padStart(packages.length, '0');
    const flags = binary.split('').map(Number).map(Boolean);
    const selected = packages.map((p, i) => (flags[i] ? p : 0)).filter(Boolean);

    let firstGroupSum = 0;
    let firstGroup: Array<number> = [];
    for (let j = selected.length - 1; j >= 0 && firstGroupSum <= fourthGroupSize; j--) {
        firstGroup.push(selected[j]);
        firstGroupSum += selected[j];
    }

    if (firstGroupSum !== fourthGroupSize) continue;

    if (firstGroup.length <= smallestGroupSize) {
        // console.log('Smallest group:', { firstGroup, smallestGroupSize });
        smallestGroupSize = firstGroup.length;
        const newQE = firstGroup.reduce((mul, val) => mul * val, 1);
        if (newQE < QE) {
            QE = newQE;
            console.log('Lower QE:', { QE });
        }
    }
}
