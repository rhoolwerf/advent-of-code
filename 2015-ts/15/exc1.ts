import { readFileSync } from 'fs';
import { EOL } from 'os';

const ingredients = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map(
        (line) =>
            /([a-zA-Z]+): capacity (-?\d+), durability (-?\d+), flavor (-?\d+), texture (-?\d+), calories (-?\d+)/.exec(
                line
            ) as RegExpExecArray
    )
    .map((match) => {
        const name = match[1];
        const [capacity, durability, flavor, texture, calories] = match.slice(2, 7).map(Number);
        return { name, capacity, durability, flavor, texture, calories };
    });

const permutations: Array<Array<number>> = [];
if (ingredients.length === 2) {
    for (let i = 0; i <= 100; i++) {
        permutations.push([i, 100 - i]);
    }
} else if (ingredients.length === 4) {
    for (let a = 0; a <= 100; a++) {
        for (let b = 0; b <= 100 - a; b++) {
            for (let c = 0; c <= 100 - a - b; c++) {
                permutations.push([a, b, c, 100 - a - b - c]);
            }
        }
    }
}

const scores = permutations.map((p) => {
    let capacity = 0;
    let durability = 0;
    let flavor = 0;
    let texture = 0;

    for (let i = 0; i < ingredients.length; i++) {
        capacity += ingredients[i].capacity * p[i];
        durability += ingredients[i].durability * p[i];
        flavor += ingredients[i].flavor * p[i];
        texture += ingredients[i].texture * p[i];
    }

    return Math.max(0, capacity) * Math.max(0, durability) * Math.max(0, flavor) * Math.max(0, texture);
});

console.log(scores.sort((a, b) => b - a)[0]);
