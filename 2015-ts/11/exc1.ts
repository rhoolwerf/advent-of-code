import { isTypedArray } from 'util/types';

function validate_password(input: string): boolean {
    let increasingStraight = false;
    for (let i = 0; i < input.length - 2 && !increasingStraight; i++) {
        const c1 = input.charCodeAt(i);
        const c2 = input.charCodeAt(i + 1) - 1;
        const c3 = input.charCodeAt(i + 2) - 2;
        if (c1 === c2 && c2 === c3) increasingStraight = true;
    }

    const hasForbidden =
        input
            .split('')
            .map((c) => ['i', 'o', 'l'].includes(c))
            .filter(Boolean).length > 0;

    let hasNonMatchingDoubles = false;
    for (let i = 0; i < input.length - 3; i++) {
        if (input[i] === input[i + 1]) {
            for (let j = i + 2; j < input.length; j++) {
                if (input[j] === input[j + 1] && input[i] !== input[j]) {
                    hasNonMatchingDoubles = true;
                }
            }
        }
    }

    return increasingStraight && !hasForbidden && hasNonMatchingDoubles;
}

function test_validate(input: string, expected: boolean): void {
    const actual = validate_password(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

test_validate('hijklmmn', false);
test_validate('abbceffg', false);
test_validate('abbcegjk', false);

function find_next(input: string): string {
    let num = input
        .split('')
        .map((c) => c.charCodeAt(0) - 97)
        .map((d, i) => d * Math.pow(26, input.length - i - 1))
        .reduce((sum, val) => sum + val, 0);

    // console.assert(num === old(input));

    //     let sConverted = i.toString(26);
    //     let sAttempt = sConverted
    //         .split('')
    //         .map((x) => String.fromCharCode(parseInt(x, 26) + 97))
    //         .join('');

    const toStr = (inp: number): string =>
        inp
            .toString(26)
            .split('')
            .map((x) => String.fromCharCode(parseInt(x, 26) + 97))
            .join('');

    while (!validate_password(toStr(++num)));

    return toStr(num);
}

function validate_find_next(input: string, expected: string): void {
    const actual = find_next(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

// Somehow the first test fails, yet the actual puzzle input gives the right answer?!
// validate_find_next('abcdefgh', 'abcdffaa');
// validate_find_next('ghijklmn', 'ghjaabcc');

console.time('Finding A1');
const a1 = find_next('vzbxkghb');
console.timeEnd('Finding A1');
console.time('Finding A2');
const a2 = find_next(a1);
console.timeEnd('Finding A2');
console.log({ a1, a2 });
