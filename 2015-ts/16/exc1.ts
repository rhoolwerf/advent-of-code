import { readFileSync } from 'fs';
import { EOL } from 'os';

const SCAN = {
    children: 3,
    cats: 7,
    samoyeds: 2,
    pomeranians: 3,
    akitas: 0,
    vizslas: 0,
    goldfish: 5,
    trees: 3,
    cars: 2,
    perfumes: 1,
};

readFileSync('./input', 'utf8')
    .split(EOL)
    .forEach((sue) => {
        const nr = (/Sue (\d+):/.exec(sue) as RegExpExecArray)[1];
        const props = [...sue.matchAll(new RegExp(/([a-z]+): (\d+)/, 'g'))].reduce(
            (p, match) => {
                p[match[1]] = Number(match[2]);
                return p;
            },
            {} as { [key: string]: number }
        );

        const keys = Object.keys(props);
        const matching = keys.map((p) => SCAN[p as keyof typeof SCAN] === props[p]).filter(Boolean).length;

        if (matching === keys.length) console.log({ nr });
    });
