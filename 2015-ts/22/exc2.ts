type Player = {
    hp: number;
    armor: number;
    mana: number;
    timers: {
        shield: number;
        poison: number;
        recharge: number;
    };
};
type Boss = {
    hp: number;
    damage: number;
};
type Spell = {
    name: string;
    cost: number;
    check: (p: Player, cost: number) => boolean;
    exec: (p: Player, cost: number, b: Boss) => void;
};

const SPELLS: Array<Spell> = [
    {
        name: 'Magic Missile',
        cost: 53,
        check: (p, cost) => p.mana >= cost,
        exec: (p, cost, b) => {
            p.mana -= cost;
            b.hp -= 4;
        },
    },
    {
        name: 'Drain',
        cost: 73,
        check: (p, cost) => p.mana >= cost,
        exec: (p, cost, b) => {
            p.mana -= cost;
            p.hp += 2;
            b.hp -= 2;
        },
    },
    {
        name: 'Shield',
        cost: 113,
        check: (p, cost) => p.mana >= cost && p.timers.shield === 0,
        exec: (p, cost) => {
            p.mana -= cost;
            p.timers.shield = 6;
            p.armor = 7;
        },
    },
    {
        name: 'Poison',
        cost: 173,
        check: (p, cost) => p.mana >= cost && p.timers.poison === 0,
        exec: (p, cost) => {
            p.mana -= cost;
            p.timers.poison = 6;
        },
    },
    {
        name: 'Recharge',
        cost: 229,
        check: (p, cost) => p.mana >= cost && p.timers.recharge === 0,
        exec: (p, cost) => {
            p.mana -= cost;
            p.timers.recharge = 5;
        },
    },
];

// // Testing
// const { playerWins, totalManaCost } = simulate_game(
//     {
//         hp: 10,
//         mana: 250,
//         armor: 0,
//         timers: {
//             shield: 0,
//             poison: 0,
//             recharge: 0,
//         },
//     },
//     { hp: 13, damage: 8 },
//     true
// );
// console.log({ playerWins, totalManaCost });

function simulate_game(
    p: Player,
    b: Boss,
    shoudlDebug: boolean = false
): { playerWins: boolean; totalManaCost: number; log: Array<string> } {
    // Keep track of what is happening
    const log: Array<string> = [];
    const debug = (...args: Array<unknown>) => {
        if (shoudlDebug) console.log(...args);
        log.push(args.join(' '));
    };

    // Player always starts
    let playerTurn = true;
    let totalManaCost = 0;

    while (true) {
        debug('-- Start of turn', playerTurn ? 'Player' : 'Boss');
        debug('- Player has', p.hp, 'hp,', p.armor, 'armor and', p.mana, 'mana');
        debug('- Boss has', b.hp, 'hp');

        // First of all, handle timers
        if (p.timers.poison > 0) {
            b.hp -= 3;
            debug('Timer - Poison tick', p.timers.poison, 'and deals damage to boss, reducing to', b.hp);
            p.timers.poison--;
            if (p.timers.poison === 0) {
                debug('- and it wears off');
            }
        }

        if (p.timers.shield > 0) {
            debug('Timer - Shield still active');
            p.timers.shield--;
            if (p.timers.shield === 0) {
                p.armor = 0;
                debug('- and it wears off');
            }
        }

        if (p.timers.recharge > 0) {
            debug('Timer - Recharge still active');
            p.mana += 101;
            p.timers.recharge--;
            if (p.timers.recharge === 0) {
                debug('- and it wears off');
            }
        }

        // Due to hardmode, player loses 1 hp per his turn
        if (playerTurn) {
            p.hp--;
            debug('- HARDMODE - Player loses 1 hp, bringing to', p.hp, 'hp');
        }

        // After the timers are done, are we all still alive?
        if (p.hp <= 0) return { playerWins: false, totalManaCost, log };
        if (b.hp <= 0) return { playerWins: true, totalManaCost, log };

        // Who should do something?
        if (playerTurn) {
            // Which spells can the player cast?
            const available = SPELLS.filter(({ cost, check }) => check(p, cost));
            debug('Player can cast:', available.map(({ name }) => name).join(', '));
            if (available.length === 0) {
                debug('Player cannot cast any spell, so loses');
                return { playerWins: false, totalManaCost, log };
            }
            const toCast = Math.floor(Math.random() * available.length);
            const spell = available[toCast];
            debug('Player casts', spell.name);
            spell.exec(p, spell.cost, b);
            totalManaCost += spell.cost;
            debug('Boss now has', b.hp, 'hp');
        } else {
            // Boss just does damage
            const damage = Math.max(1, b.damage - p.armor);
            p.hp -= damage;
            debug('Boss deals', damage, 'damage to player, reducing player to', p.hp);
        }

        debug();

        // Other one should do something
        playerTurn = !playerTurn;
    }
}

let lowestCost = Infinity;
let lowestLog: Array<string> = [];
for (let i = 0; i < 1e6; i++) {
    const { playerWins, totalManaCost, log } = simulate_game(
        { hp: 50, armor: 0, mana: 500, timers: { poison: 0, shield: 0, recharge: 0 } },
        { hp: 58, damage: 9 }
    );

    if (playerWins) {
        // console.log('Player wins, having spend', totalManaCost);
        if (totalManaCost < lowestCost) {
            lowestCost = totalManaCost;
            console.log('New lowest cost:', lowestCost);
            lowestLog = log;
        }
    }
}

console.log({ lowestCost });
