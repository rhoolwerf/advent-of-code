import { readFileSync } from 'fs';
import { EOL } from 'os';

function calc_line(input: string): number {
    const [l, w, h] = input.split('x').map(Number);

    const [smallest, nearly_smallest] = [l, w, h].sort((a, b) => a - b);

    return (smallest + nearly_smallest) * 2 + l * w * h;
}

function test(input: string, expected: number): void {
    const actual = calc_line(input);
    console.assert(expected === actual, 'Test failed', { input, expected, actual });
}

test('2x3x4', 34);
test('1x1x10', 14);

console.log(
    readFileSync('./input', 'utf8')
        .split(EOL)
        .map(calc_line)
        .reduce((sum, val) => sum + val, 0)
);
