import { readFileSync } from 'fs';
import { EOL } from 'os';

const input = readFileSync(process.argv[2], 'utf8').split(`${EOL}${EOL}`);
const molecule = input[1];
const transformations = input[0]
    .split(EOL)
    .map((line) => line.split(' => '))
    .map(([from, to]) => ({ from, to }));

const distinct = new Set(
    transformations
        .map(({ from, to }) => {
            const indexes = [...molecule.matchAll(new RegExp(`${from}`, 'g'))].map(({ index }) => index);
            return indexes.map((i) => [molecule.substring(0, i), to, molecule.substring(i + from.length)].join(''));
        })
        .flat()
);

console.log(distinct.size);
