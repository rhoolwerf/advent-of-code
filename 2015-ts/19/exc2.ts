import { readFileSync } from 'fs';
import { EOL } from 'os';

const input = readFileSync(process.argv[2], 'utf8').split(`${EOL}${EOL}`);
let molecule = input[1];
const transformations = input[0]
    .split(EOL)
    .map((line) => line.split(' => '))
    .map(([from, to]) => ({ from, to }));

let steps = 0;

while (molecule !== 'e') {
    transformations.forEach(({ from, to }) => {
        // For each replacement, add a step
        while (molecule.indexOf(to) >= 0) {
            molecule = molecule.replace(to, from);
            steps += 1;
        }
    });
}

console.log({ steps });
