import { readFileSync } from 'fs';

function exec(input: string): number {
    const map = new Map<string, number>();
    let y = 0;
    let x = 0;

    const enter = () => {
        const key = [y, x].join(',');
        map.set(key, (map.get(key) || 0) + 1);
    };

    enter();

    for (const inp of input.split('')) {
        switch (inp) {
            case '>':
                x++;
                break;
            case '<':
                x--;
                break;
            case '^':
                y--;
                break;
            case 'v':
                y++;
                break;
        }

        enter();
    }

    return map.size;
}

function test(input: string, expected: number): void {
    const actual = exec(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

test('>', 2);
test('^>v<', 4);
test('^v^v^v^v^v', 2);

console.log(exec(readFileSync('./input', 'utf8')));
