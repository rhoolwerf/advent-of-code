import { readFileSync } from 'fs';

function exec(input: string): number {
    const map = new Map<string, number>();
    const who = [
        [0, 0], // Santa
        [0, 0], // Robot
    ];

    const enter = (w: number) => {
        const key = who[w % 2].join(',');
        map.set(key, (map.get(key) || 0) + 1);
    };

    let w = 0;
    enter(w++);

    for (const inp of input.split('')) {
        switch (inp) {
            case '>':
                who[w % 2][1]++;
                break;
            case '<':
                who[w % 2][1]--;
                break;
            case '^':
                who[w % 2][0]--;
                break;
            case 'v':
                who[w % 2][0]++;
                break;
        }

        enter(w++);
    }

    return map.size;
}

function test(input: string, expected: number): void {
    const actual = exec(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

test('^v', 3);
test('^>v<', 3);
test('^v^v^v^v^v', 11);

console.log(exec(readFileSync('./input', 'utf8')));
