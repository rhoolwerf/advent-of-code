import { readFileSync } from 'fs';
import { EOL } from 'os';

type Registers = { a: number; b: number };

function handle(registers: Registers): Registers {
    const instructions = readFileSync(process.argv[2], 'utf8').split(EOL);
    for (let i = 0; i < instructions.length; ) {
        const [_, cmd, ...rest] = Array.from(
            /([a-z]{3}) ([a-z])?(?:(?:, )?([+-]\d+))?/.exec(instructions[i]) as RegExpExecArray
        ).filter(Boolean);

        const register = rest[0] as keyof typeof registers;

        switch (cmd) {
            case 'inc':
                registers[register]++;
                i++;
                break;
            case 'tpl':
                registers[register] *= 3;
                i++;
                break;
            case 'hlf':
                registers[register] /= 2;
                i++;
                break;
            case 'jmp':
                i += Number(register);
                break;
            case 'jio':
                if (registers[register] === 1) {
                    i += Number(rest[1]);
                } else {
                    i++;
                }
                break;
            case 'jie':
                if (registers[register] % 2 === 0) {
                    i += Number(rest[1]);
                } else {
                    i++;
                }
                break;

            default:
                console.error('Command not found', cmd);
                process.exit(-1);
        }
    }

    return registers;
}

console.log('P1:', handle({ a: 0, b: 0 }));
console.log('P2:', handle({ a: 1, b: 0 }));
