function look_and_say(input: string): string {
    const output: Array<number | string> = [];

    for (let i = 0; i < input.length; ) {
        let count = 1;
        for (let j = i + 1; input[i] === input[j]; j++) count++;
        output.push(count);
        output.push(input[i]);
        i += count;
    }

    return output.join('');
}

function test(input: string, expected: string): void {
    const actual = look_and_say(input);
    console.assert(expected === actual, '', { input, expected, actual });
}

test('1', '11');
test('11', '21');
test('21', '1211');
test('1211', '111221');
test('111221', '312211');

// Part 1
let input = '1321131112';
for (let i = 0; i < 40; i++) input = look_and_say(input);
console.log(input.length);

// Part 2
input = '1321131112';
for (let i = 0; i < 50; i++) input = look_and_say(input);
console.log(input.length);
