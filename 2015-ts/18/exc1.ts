import { readFileSync } from 'fs';
import { EOL } from 'os';
import { sum } from '../util/util';

const ON = '#';
const OFF = '.';
const VOID = ' ';

const SURROUNDING = [
    // Above
    [-1, -1],
    [-1, 0],
    [-1, 1],

    // Sides
    [0, -1],
    [0, 1],

    // Below
    [1, -1],
    [1, 0],
    [1, 1],
];

function display(reset = false) {
    if (reset) console.clear();
    console.log(grid.map((row) => row.join('')).join(EOL));
}

function getSurrounding(y: number, x: number): Array<string> {
    return SURROUNDING.map(([offsetY, offsetX]) =>
        grid[y + offsetY] && grid[y + offsetY][x + offsetX] ? grid[y + offsetY] && grid[y + offsetY][x + offsetX] : VOID
    );
}

function update_grid() {
    const newGrid = JSON.parse(JSON.stringify(grid));
    for (let y = 0; y < grid.length; y++) {
        for (let x = 0; x < grid[y].length; x++) {
            const surr = getSurrounding(y, x).filter((x) => x !== VOID);
            const countON = surr.filter((b) => b === ON).length;

            // If on and 2 or 3 neighbors are on, stay on, else turn off
            if (grid[y][x] === ON && ![2, 3].includes(countON)) {
                newGrid[y][x] = OFF;
                // If off and exactly 3 neighbors are on, turn on, else stay off
            } else if (grid[y][x] === OFF && countON === 3) {
                newGrid[y][x] = ON;
            }
        }
    }

    grid = newGrid;
}

const steps = Number(process.argv[3]);

let grid = readFileSync(process.argv[2], 'utf8')
    .split(EOL)
    .map((row) => row.split(''));

for (let i = 0; i < steps; i++) {
    update_grid();
}

const countON = sum(grid.map((row) => row.filter((s) => s === ON).length));
console.log({ countON });
