import { readFileSync } from "fs";

const input = readFileSync(process.argv[2], "utf8").split("\n");

let horizontal = 0;
let depth = 0;
let aim = 0;
input.forEach((instruction) => {
  const [direction, offset] = instruction.split(" ");
  switch (direction) {
    case "up":
      aim -= Number(offset);
      break;
    case "down":
      aim += Number(offset);
      break;
    case "forward":
      horizontal += Number(offset);
      depth += Number(offset) * aim;
      break;
  }
  console.log({ direction, offset }, { horizontal, depth, aim });
});

console.log(horizontal, depth, horizontal * depth);
