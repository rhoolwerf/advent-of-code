import { readFileSync } from "fs";

const input = readFileSync(process.argv[2], "utf8").split("\n");

let horizontal = 0;
let depth = 0;
input.forEach((instruction) => {
  const [direction, offset] = instruction.split(" ");
  switch (direction) {
    case "up":
      depth -= Number(offset);
      break;
    case "down":
      depth += Number(offset);
      break;
    case "forward":
      horizontal += Number(offset);
      break;
  }
});

console.log(horizontal, depth, horizontal * depth);
