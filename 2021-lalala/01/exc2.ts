import { readFileSync } from "fs";
console.log(
  readFileSync(process.argv[2], "utf8")
    .split("\n")
    .map(Number)
    .map((v, i, a) => a[i] + a[i + 1] + a[i + 2])
    .map((v, i, a) => v < a[i + 1])
    .filter((v) => v).length
);
