var aStepCount = new Map();
function determineSteps(iStart) {
	if (iStart === 1) {
		return 1;
	} else if (aStepCount.has(iStart)) {
		return aStepCount.get(iStart);
	} else {
		var iNextNumberToCheck = 0;
		if (iStart % 2 === 0) {
			iNextNumberToCheck = Number(iStart / 2);
		} else {
			iNextNumberToCheck = Number(iStart * 3 + 1);
		}
		aStepCount.set(iStart, 1 + determineSteps(iNextNumberToCheck));
		return aStepCount.get(iStart);
	}
}

console.time('determineSteps');
var iLargestStepCount = 0;
var iNumber = 0;
for (var i = 1; i <= 1000000; i++) {
	var iNewStepCount = determineSteps(i);
	if (iNewStepCount > iLargestStepCount) {
		iLargestStepCount = iNewStepCount;
		iNumber = i;
	}
}
console.log(iNumber);
console.timeEnd('determineSteps');