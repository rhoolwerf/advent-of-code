let aInput = ['AR', 06, 16, 40, 06, 50, 'AR', 07, 17, 40, 07, 50, 'AR', 08, 18, 40, 08, 50, 'AR', 16, 06, 50, 16, 40, 'AR', 17, 07, 50, 17, 40, 'AR', 18, 08, 50, 18, 40, 'AP', 26, 36, 40, 26, 50, 'AP', 34, 24, 50, 34, 40, 'AP', 36, 26, 50, 36, 40, 'AP', 37, 27, 50, 37, 40, 'AP', 38, 28, 50, 38, 40];

for (let i = 0; i < aInput.length; i += 6) {
    console.log('    when \'' + format(aInput[i + 1]) + '\'.');
    console.log('      ev_af      = \'' + format(aInput[i + 2]) + '\'.');
    console.log('      ev_af_park = \'' + format(aInput[i + 3]) + '\'.');
    console.log('      ev_op      = \'' + format(aInput[i + 4]) + '\'.');
    console.log('      ev_op_park = \'' + format(aInput[i + 5]) + '\'.');
}

function format(input) {
    return ("0" + input).slice(-2);
}

// ev_af = '31'.
//     ev_af_park = '40'.
//         ev_op = '21'.
//             ev_op_park = '50'.
