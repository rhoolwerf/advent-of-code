types:
  ty_number type p length 16,
  begin of ty_step,
    step  type ty_number,
    count type ty_number,
  end of ty_step.

data:
  gt_buffer                            type hashed table of ty_step with unique key step.

start-of-selection.
  perform execute_roald.
  perform execute_mark.

*&---------------------------------------------------------------------*
*&      Form  execute
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*

form execute_mark.

  data:
    lv_step      type ty_number,
    lv_n         type ty_number,
    lv_count     type ty_number,
    lv_max_step  type ty_number,
    lv_max_count type ty_number,
    ls_buffer    type ty_step,
    lv_start     type i,
    lv_end       type i,
    lv_diff      type i.

  field-symbols:
    <buffer> type ty_step.

  get run time field lv_start.

  clear gt_buffer.

  ls_buffer-step  = 1.
  ls_buffer-count = 1.
  insert ls_buffer into table gt_buffer.

  lv_step = 1.
  while lv_step < 1000000.
    add 1 to lv_step.

    lv_n     = lv_step.
    lv_count = 0.

    while 1 = 1.
      read table gt_buffer assigning <buffer> with key step = lv_n.
      if sy-subrc <> 0.
        if lv_n mod 2 = 0.
          lv_n = lv_n / 2.
        else.
          lv_n = lv_n * 3 + 1.
        endif.
        add 1 to lv_count.
      else.
        exit.
      endif.
    endwhile.

    ls_buffer-step  = lv_step.
    ls_buffer-count = lv_count + <buffer>-count.
    insert ls_buffer into table gt_buffer.

    if ls_buffer-count > lv_max_count.
      lv_max_count = ls_buffer-count.
      lv_max_step  = ls_buffer-step.
    endif.

  endwhile.

  get run time field lv_end.
  lv_diff = lv_end - lv_start.

  write: / 'Mark logica :', lv_max_step, lv_diff.

endform.

form execute_roald.

  data:
    lv_step      type ty_number,
    lv_max_count type ty_number,
    lv_max_step  type ty_number,
    lv_count     type ty_number,
    lv_start     type i,
    lv_end       type i,
    lv_diff      type i.

  get run time field lv_start.

  lv_step = 1.
  while lv_step < 1000000.
    add 1 to lv_step.
    perform determine_steps using lv_step changing lv_count.

    if lv_count > lv_max_count.
      lv_max_step  = lv_step.
      lv_max_count = lv_count.
    endif.
  endwhile.

  get run time field lv_end.
  lv_diff = lv_end - lv_start.
  write: / 'Roald logica:', lv_max_step, lv_diff.

endform.                    "execute

*&---------------------------------------------------------------------*
*&      Form  determine_steps
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->IV_STEP    text
*      <--RV_COUNT   text
*----------------------------------------------------------------------*
form determine_steps using iv_step type ty_number changing rv_count type ty_number.

  data:
    ls_buffer    type ty_step,
    lv_next_step type ty_number.

  field-symbols:
    <buffer>                           type ty_step.

  if iv_step = 1.
    rv_count = 1.
  else.
    read table gt_buffer assigning <buffer> with key step = iv_step.
    if sy-subrc <> 0.
      ls_buffer-step = iv_step.
      if iv_step mod 2 = 0.
        lv_next_step = iv_step / 2.
      else.
        lv_next_step = iv_step * 3 + 1.
      endif.
      perform determine_steps using lv_next_step changing ls_buffer-count.
      add 1 to ls_buffer-count.
      insert ls_buffer into table gt_buffer assigning <buffer>.
    endif.
    rv_count = <buffer>-count.
  endif.

endform.                    "determine_steps