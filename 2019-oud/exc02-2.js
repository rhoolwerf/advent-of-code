for (var noun = 0; noun <= 100; noun++) {
	for (var verb = 0; verb <= 100; verb++) {
		// Input
		var tokens = [1, 0, 0, 3, 1, 1, 2, 3, 1, 3, 4, 3, 1, 5, 0, 3, 2, 6, 1, 19, 1, 19, 5, 23, 2, 10, 23, 27, 2, 27, 13, 31, 1, 10, 31, 35, 1, 35, 9, 39, 2, 39, 13, 43, 1, 43, 5, 47, 1, 47, 6, 51, 2, 6, 51, 55, 1, 5, 55, 59, 2, 9, 59, 63, 2, 6, 63, 67, 1, 13, 67, 71, 1, 9, 71, 75, 2, 13, 75, 79, 1, 79, 10, 83, 2, 83, 9, 87, 1, 5, 87, 91, 2, 91, 6, 95, 2, 13, 95, 99, 1, 99, 5, 103, 1, 103, 2, 107, 1, 107, 10, 0, 99, 2, 0, 14, 0];

		// Hardcoded replace
		// - replace position 1 with the value 12 and replace position 2 with the value 2. 
		tokens[1] = noun;
		tokens[2] = verb;

		for (var i = 0; i <= tokens.length; i += 4) {
			if (tokens[i] == 99) {
				break;
			} else if (tokens[i] == 1) {
				tokens[tokens[i + 3]] = tokens[tokens[i + 1]] + tokens[tokens[i + 2]];
			} else if (tokens[i] == 2) {
				tokens[tokens[i + 3]] = tokens[tokens[i + 1]] * tokens[tokens[i + 2]];
			}
		}

		if (tokens[0] == 19690720) {
			console.log(100 * noun + verb);
			return;
		}
	}
}