// Some much need anti magic numbers
const MODE_POSITION = 0;
const MODE_IMMEDIATE = 1;

const OPCODE_EXIT = 99;
const OPCODE_ADD = 1;
const OPCODE_MULTI = 2;
const OPCODE_READ_INPUT = 3;
const OPCODE_WRITE_OUTPUT = 4;

let aInstructions = [];
let sInput = 1;

// The program 3,0,4,0,99 outputs whatever it gets as input, then halts.
aInstructions = [3, 0, 4, 0, 99]; // Should result in reading input 1, writing output 1

// This instruction multiplies its first two parameters. The first parameter, 4 in position mode, works like 
// it did before - its value is the value stored at address 4 (33). The second parameter, 3 in immediate mode, 
// simply has value 3. The result of this operation, 33 * 3 = 99, is written according to the third parameter, 
// 4 in position mode, which also works like it did before - 99 is written to address 4.
aInstructions = [1002, 4, 3, 4, 33];

// Negative integer test
aInstructions = [1101, 100, -1, 4, 0];

// Actual puzzle input
aInstructions = [3, 225, 1, 225, 6, 6, 1100, 1, 238, 225, 104, 0, 1102, 72, 20, 224, 1001, 224, -1440, 224, 4, 224, 102, 8, 223, 223, 1001, 224, 5, 224, 1, 224, 223, 223, 1002, 147, 33, 224, 101, -3036, 224, 224, 4, 224, 102, 8, 223, 223, 1001, 224, 5, 224, 1, 224, 223, 223, 1102, 32, 90, 225, 101, 65, 87, 224, 101, -85, 224, 224, 4, 224, 1002, 223, 8, 223, 101, 4, 224, 224, 1, 223, 224, 223, 1102, 33, 92, 225, 1102, 20, 52, 225, 1101, 76, 89, 225, 1, 117, 122, 224, 101, -78, 224, 224, 4, 224, 102, 8, 223, 223, 101, 1, 224, 224, 1, 223, 224, 223, 1102, 54, 22, 225, 1102, 5, 24, 225, 102, 50, 84, 224, 101, -4600, 224, 224, 4, 224, 1002, 223, 8, 223, 101, 3, 224, 224, 1, 223, 224, 223, 1102, 92, 64, 225, 1101, 42, 83, 224, 101, -125, 224, 224, 4, 224, 102, 8, 223, 223, 101, 5, 224, 224, 1, 224, 223, 223, 2, 58, 195, 224, 1001, 224, -6840, 224, 4, 224, 102, 8, 223, 223, 101, 1, 224, 224, 1, 223, 224, 223, 1101, 76, 48, 225, 1001, 92, 65, 224, 1001, 224, -154, 224, 4, 224, 1002, 223, 8, 223, 101, 5, 224, 224, 1, 223, 224, 223, 4, 223, 99, 0, 0, 0, 677, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1105, 0, 99999, 1105, 227, 247, 1105, 1, 99999, 1005, 227, 99999, 1005, 0, 256, 1105, 1, 99999, 1106, 227, 99999, 1106, 0, 265, 1105, 1, 99999, 1006, 0, 99999, 1006, 227, 274, 1105, 1, 99999, 1105, 1, 280, 1105, 1, 99999, 1, 225, 225, 225, 1101, 294, 0, 0, 105, 1, 0, 1105, 1, 99999, 1106, 0, 300, 1105, 1, 99999, 1, 225, 225, 225, 1101, 314, 0, 0, 106, 0, 0, 1105, 1, 99999, 1107, 677, 226, 224, 1002, 223, 2, 223, 1005, 224, 329, 101, 1, 223, 223, 7, 677, 226, 224, 102, 2, 223, 223, 1005, 224, 344, 1001, 223, 1, 223, 1107, 226, 226, 224, 1002, 223, 2, 223, 1006, 224, 359, 1001, 223, 1, 223, 8, 226, 226, 224, 1002, 223, 2, 223, 1006, 224, 374, 101, 1, 223, 223, 108, 226, 226, 224, 102, 2, 223, 223, 1005, 224, 389, 1001, 223, 1, 223, 1008, 226, 226, 224, 1002, 223, 2, 223, 1005, 224, 404, 101, 1, 223, 223, 1107, 226, 677, 224, 1002, 223, 2, 223, 1006, 224, 419, 101, 1, 223, 223, 1008, 226, 677, 224, 1002, 223, 2, 223, 1006, 224, 434, 101, 1, 223, 223, 108, 677, 677, 224, 1002, 223, 2, 223, 1006, 224, 449, 101, 1, 223, 223, 1108, 677, 226, 224, 102, 2, 223, 223, 1006, 224, 464, 1001, 223, 1, 223, 107, 677, 677, 224, 102, 2, 223, 223, 1005, 224, 479, 101, 1, 223, 223, 7, 226, 677, 224, 1002, 223, 2, 223, 1006, 224, 494, 1001, 223, 1, 223, 7, 677, 677, 224, 102, 2, 223, 223, 1006, 224, 509, 101, 1, 223, 223, 107, 226, 677, 224, 1002, 223, 2, 223, 1006, 224, 524, 1001, 223, 1, 223, 1007, 226, 226, 224, 102, 2, 223, 223, 1006, 224, 539, 1001, 223, 1, 223, 108, 677, 226, 224, 102, 2, 223, 223, 1005, 224, 554, 101, 1, 223, 223, 1007, 677, 677, 224, 102, 2, 223, 223, 1006, 224, 569, 101, 1, 223, 223, 8, 677, 226, 224, 102, 2, 223, 223, 1006, 224, 584, 1001, 223, 1, 223, 1008, 677, 677, 224, 1002, 223, 2, 223, 1006, 224, 599, 1001, 223, 1, 223, 1007, 677, 226, 224, 1002, 223, 2, 223, 1005, 224, 614, 101, 1, 223, 223, 1108, 226, 677, 224, 1002, 223, 2, 223, 1005, 224, 629, 101, 1, 223, 223, 1108, 677, 677, 224, 1002, 223, 2, 223, 1005, 224, 644, 1001, 223, 1, 223, 8, 226, 677, 224, 1002, 223, 2, 223, 1006, 224, 659, 101, 1, 223, 223, 107, 226, 226, 224, 102, 2, 223, 223, 1005, 224, 674, 101, 1, 223, 223, 4, 223, 99, 226];

// Puzzle input from Mark
// aInstructions = [3, 225, 1, 225, 6, 6, 1100, 1, 238, 225, 104, 0, 1102, 68, 5, 225, 1101, 71, 12, 225, 1, 117, 166, 224, 1001, 224, -100, 224, 4, 224, 102, 8, 223, 223, 101, 2, 224, 224, 1, 223, 224, 223, 1001, 66, 36, 224, 101, -87, 224, 224, 4, 224, 102, 8, 223, 223, 101, 2, 224, 224, 1, 223, 224, 223, 1101, 26, 51, 225, 1102, 11, 61, 224, 1001, 224, -671, 224, 4, 224, 1002, 223, 8, 223, 1001, 224, 5, 224, 1, 223, 224, 223, 1101, 59, 77, 224, 101, -136, 224, 224, 4, 224, 1002, 223, 8, 223, 1001, 224, 1, 224, 1, 223, 224, 223, 1101, 11, 36, 225, 1102, 31, 16, 225, 102, 24, 217, 224, 1001, 224, -1656, 224, 4, 224, 102, 8, 223, 223, 1001, 224, 1, 224, 1, 224, 223, 223, 101, 60, 169, 224, 1001, 224, -147, 224, 4, 224, 102, 8, 223, 223, 101, 2, 224, 224, 1, 223, 224, 223, 1102, 38, 69, 225, 1101, 87, 42, 225, 2, 17, 14, 224, 101, -355, 224, 224, 4, 224, 102, 8, 223, 223, 1001, 224, 2, 224, 1, 224, 223, 223, 1002, 113, 89, 224, 101, -979, 224, 224, 4, 224, 1002, 223, 8, 223, 1001, 224, 7, 224, 1, 224, 223, 223, 1102, 69, 59, 225, 4, 223, 99, 0, 0, 0, 677, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1105, 0, 99999, 1105, 227, 247, 1105, 1, 99999, 1005, 227, 99999, 1005, 0, 256, 1105, 1, 99999, 1106, 227, 99999, 1106, 0, 265, 1105, 1, 99999, 1006, 0, 99999, 1006, 227, 274, 1105, 1, 99999, 1105, 1, 280, 1105, 1, 99999, 1, 225, 225, 225, 1101, 294, 0, 0, 105, 1, 0, 1105, 1, 99999, 1106, 0, 300, 1105, 1, 99999, 1, 225, 225, 225, 1101, 314, 0, 0, 106, 0, 0, 1105, 1, 99999, 7, 677, 677, 224, 1002, 223, 2, 223, 1006, 224, 329, 1001, 223, 1, 223, 1007, 226, 226, 224, 1002, 223, 2, 223, 1006, 224, 344, 1001, 223, 1, 223, 1108, 226, 677, 224, 102, 2, 223, 223, 1005, 224, 359, 1001, 223, 1, 223, 1107, 226, 677, 224, 1002, 223, 2, 223, 1006, 224, 374, 101, 1, 223, 223, 1107, 677, 226, 224, 1002, 223, 2, 223, 1006, 224, 389, 101, 1, 223, 223, 7, 226, 677, 224, 1002, 223, 2, 223, 1005, 224, 404, 101, 1, 223, 223, 1008, 677, 226, 224, 102, 2, 223, 223, 1005, 224, 419, 101, 1, 223, 223, 1008, 226, 226, 224, 102, 2, 223, 223, 1006, 224, 434, 101, 1, 223, 223, 107, 226, 226, 224, 1002, 223, 2, 223, 1005, 224, 449, 1001, 223, 1, 223, 108, 226, 677, 224, 102, 2, 223, 223, 1005, 224, 464, 101, 1, 223, 223, 1108, 677, 226, 224, 102, 2, 223, 223, 1005, 224, 479, 101, 1, 223, 223, 1007, 226, 677, 224, 102, 2, 223, 223, 1006, 224, 494, 101, 1, 223, 223, 107, 677, 677, 224, 102, 2, 223, 223, 1005, 224, 509, 101, 1, 223, 223, 108, 677, 677, 224, 102, 2, 223, 223, 1006, 224, 524, 1001, 223, 1, 223, 8, 226, 677, 224, 102, 2, 223, 223, 1005, 224, 539, 101, 1, 223, 223, 107, 677, 226, 224, 102, 2, 223, 223, 1005, 224, 554, 1001, 223, 1, 223, 8, 226, 226, 224, 102, 2, 223, 223, 1006, 224, 569, 1001, 223, 1, 223, 7, 677, 226, 224, 1002, 223, 2, 223, 1005, 224, 584, 1001, 223, 1, 223, 1108, 226, 226, 224, 102, 2, 223, 223, 1005, 224, 599, 1001, 223, 1, 223, 1107, 677, 677, 224, 1002, 223, 2, 223, 1006, 224, 614, 1001, 223, 1, 223, 1007, 677, 677, 224, 1002, 223, 2, 223, 1006, 224, 629, 1001, 223, 1, 223, 108, 226, 226, 224, 102, 2, 223, 223, 1005, 224, 644, 1001, 223, 1, 223, 8, 677, 226, 224, 1002, 223, 2, 223, 1005, 224, 659, 1001, 223, 1, 223, 1008, 677, 677, 224, 1002, 223, 2, 223, 1006, 224, 674, 1001, 223, 1, 223, 4, 223, 99, 226];

// As the for-loop doesn't have a default increment, we need to be able to exit cleanly
let bKeepLoopRunning = true;

// Do we want debug-info on the screen?
let bDebug = false;
function write_debug(message) {
    if (bDebug) { console.log(message); }
}

// Process arguments (to determine if we want to debug)
process.argv.forEach(function (val, index, array) {
    if (val.toLowerCase() == '-d') { bDebug = true; }
});

// The actual processing of opcodes
for (let iInstructionPointer = 0; iInstructionPointer <= aInstructions.length && bKeepLoopRunning;) {
    // Pad the instruction with leading zero's where needed
    let sInstruction = String(aInstructions[iInstructionPointer]);
    for (let x = sInstruction.length; x < 5; x++) {
        sInstruction = '0' + sInstruction;
    }

    // Split instruction into modes
    let sOpcode = sInstruction.substring(3, 5);
    let iMode1 = sInstruction.substring(2, 3);
    let iMode2 = sInstruction.substring(1, 2);
    let iMode3 = sInstruction.substring(0, 1);

    if (iInstructionPointer > 0) { write_debug(''); }
    write_debug('Processing opcode: ' + sOpcode);
    switch (Number(sOpcode)) {
        case OPCODE_EXIT:
            // Exit-instruction
            write_debug('Exiting!');
            bKeepLoopRunning = false;
            break;
        case OPCODE_ADD:
            // Add values
            let iAddValue1 = read_position(iInstructionPointer + 1, iMode1);
            let iAddValue2 = read_position(iInstructionPointer + 2, iMode2);
            let iAddResult = iAddValue1 + iAddValue2;
            write_position(iInstructionPointer + 3, iMode3, iAddResult);
            iInstructionPointer += 4;
            continue;
        case OPCODE_MULTI:
            // Multiplate values
            let iMultiplyValue1 = read_position(iInstructionPointer + 1, iMode1);
            let iMultiplyValue2 = read_position(iInstructionPointer + 2, iMode2);
            let iMultiplyResult = iMultiplyValue1 * iMultiplyValue2;
            write_position(iInstructionPointer + 3, iMode3, iMultiplyResult);
            iInstructionPointer += 4;
            continue;
        case OPCODE_READ_INPUT:
            // Read from input and store at given address
            write_debug('Reading input');
            write_position(iInstructionPointer + 1, iMode1, sInput);
            iInstructionPointer += 2;
            continue;
        case OPCODE_WRITE_OUTPUT:
            // Read given address and write to output.
            write_debug('Writing to output');
            console.log('Output: ' + read_position(iInstructionPointer + 1, iMode1));
            iInstructionPointer += 2;
            continue;
    }

    if (bKeepLoopRunning) { console.error('Ran to the end of the loop??'); }
    break; // Exit from loop as we have no idea how to continue
}
// console.log('\nResulting aInstructions:');
// console.log(aInstructions);

function read_position(position, mode) {
    write_debug('Reading position ' + position + ' in mode ' + mode + ' resulting in address ' + (mode == 0 ? aInstructions[position] : position) + ' and in value ' + (mode == 0 ? aInstructions[aInstructions[position]] : aInstructions[position]));
    switch (Number(mode)) {
        case MODE_POSITION:
            // Position mode
            return aInstructions[aInstructions[position]];
            break;
        case MODE_IMMEDIATE:
            // Immediate mode
            return aInstructions[position];
            break;
        default:
            write_debug('ERROR: wrong mode given to read_position: ' + mode);
    }
}

function write_position(position, mode, value) {
    write_debug('Writing value ' + value + ' to position ' + position + ' in mode ' + mode + ' so address ' + (mode == 0 ? aInstructions[position] : position));
    switch (Number(mode)) {
        case MODE_POSITION:
            // Position mode
            aInstructions[aInstructions[position]] = value;
            break;
        case MODE_IMMEDIATE:
            // Immediate mode
            aInstructions[position] = value;
            break;
        default:
            write_debug('ERROR: wrong mode given to write_position: ' + mode);
    }
}