let input = '134792-675810';

let start = input.split('-')[0];
let end = input.split('-')[1];

function validate_number(number) {
    let has_at_least_one_double = false;

    let checked_digits = new Set();
    for (let i = 0; i < number.length - 1; i++) {
        // Check if the digit is a group of 2, we need only one group
        if (!checked_digits.has(number.charAt(i))) {
            let count_occurrences = 0;
            for (let x = i; x < number.length; x++) {
                if (number.charAt(i) == number.charAt(x)) {
                    count_occurrences++;
                }
            }
            if (count_occurrences == 2) { has_at_least_one_double = true; }
            checked_digits.add(number.charAt(i));
        }

        if (number.charAt(i) > number.charAt(i + 1)) {
            return false; // If we're dissatisfied, no need to continue
        }
    }

    return has_at_least_one_double;
}

// console.log(validate_number(String(111122)));

let possible_codes = new Set();
for (let i = start; i <= end; i++) {
    if (validate_number(String(i))) {
        possible_codes.add(i);
    }
}
console.log(possible_codes.size);