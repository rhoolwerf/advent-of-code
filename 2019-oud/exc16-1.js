const aInitialPhaseSequence = [0, 1, 0, -1];

// Example 1: After 4 phases: 01029498
let sInput = '12345678';
let iMaxPhaseCount = 4;

// Example 2: After 100 phases: 24176176
sInput = '80871224585914546619083218645595';
iMaxPhaseCount = 100;
let sExpectedOutput = '24176176';

// // Example 3: After 100 phases: 73745418
// sInput = '19617804207202209144916044189917';
// iMaxPhaseCount = 100;

// // Example 4: After 100 phases: 52432133
// sInput = '69317163492948606335995924319873';
// iMaxPhaseCount = 100;

// Actual input
sInput = '59719811742386712072322509550573967421647565332667367184388997335292349852954113343804787102604664096288440135472284308373326245877593956199225516071210882728614292871131765110416999817460140955856338830118060988497097324334962543389288979535054141495171461720836525090700092901849537843081841755954360811618153200442803197286399570023355821961989595705705045742262477597293974158696594795118783767300148414702347570064139665680516053143032825288231685962359393267461932384683218413483205671636464298057303588424278653449749781937014234119757220011471950196190313903906218080178644004164122665292870495547666700781057929319060171363468213087408071790';
iMaxPhaseCount = 100;
sExpectedOutput = '30550349';

// Pre-buffer all sequences
let aAllSequences = [];
for (iSequence in sInput.split("")) {
    aAllSequences.push(calc_sequence(Number(iSequence), Number(sInput.length)).reverse());
}

let aInput = sInput.split("");
let iPhaseCount = 1;
while (iPhaseCount++ <= iMaxPhaseCount) {
    let aNewInput = [];
    for (aSequenceSource of aAllSequences) {
        let iNewResult = 0;
        let aSequence = aSequenceSource.slice(0);
        for (iCharacter of aInput) {
            iNewResult += Number(aSequence.pop()) * Number(iCharacter);
        }
        iNewResult = String(Math.abs(iNewResult));
        iNewResult = iNewResult.charAt(iNewResult.length - 1);
        aNewInput.push(iNewResult);
    }
    aInput = aNewInput;
}

let sResult = aInput.slice(0, 8).join("");
console.log(sResult);
if (sResult == sExpectedOutput) { console.log('done!'); }
else { console.log('error'); }

// return;

// // Calculate sequences
// let aSequences = [];
// for (let iSequence = 0; iSequence < sInput.length; iSequence++) {
//     aSequences.push(calc_sequence(iSequence, sInput.length));
// }

// while (iPhaseCount <= iMaxPhaseCount) {
//     // console.log('Phase:\t' + iPhaseCount + ': ' + sInput);

//     // For each digit, generate a sequence
//     let sNewInput = '';
//     for (let iSequence = 0; iSequence < sInput.length; iSequence++) {
//         let aSequence = aSequences[iSequence];
//         let iSubResult = 0;
//         let sResultCalc = '';
//         for (let iCharacter = 0; iCharacter < sInput.length; iCharacter++) {
//             if (sResultCalc != '') { sResultCalc += '\t+ '; }
//             sResultCalc += String(sInput.charAt(iCharacter)) + '*' + String(aSequence[iCharacter]);
//             iSubResult += sInput.charAt(iCharacter) * aSequence[iCharacter];
//         }
//         iSubResult = String(Math.abs(iSubResult));
//         // console.log(sResultCalc + ' = ' + iSubResult.charAt(iSubResult.length - 1));
//         sNewInput += String(iSubResult.charAt(iSubResult.length - 1));
//     }

//     sInput = sNewInput;
//     iPhaseCount++;
// }

// console.log('After ' + iMaxPhaseCount + ' phases: ' + sInput.substring(0, 8));// + ' (' + sInput + ')');

function calc_sequence(iSequenceCount, iMaxLength) {
    let aOutput = [];
    let iSequenceCounter = 0;
    let bSkipAdding = true;
    while (aOutput.length < iMaxLength) {
        for (let i = 0; i < iSequenceCount + 1; i++) {
            if (bSkipAdding) { bSkipAdding = false; }
            else {
                aOutput.push(aInitialPhaseSequence[iSequenceCounter % 4]);
                if (aOutput.length >= iMaxLength) { break; }
            }
        }
        iSequenceCounter++;
    }
    return aOutput;
}