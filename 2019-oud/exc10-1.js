let aInput = [];

function name_asteroid(counter) { return String.fromCharCode(Number(counter) + 65); }

// Example 1, expected 3,4 with 8
input = ['.#..#', '.....', '#####', '....#', '...##'];

// Example 2, expected 5,8 with 33
input = ['......#.#.', '#..#.#....', '..#######.', '.#.#.###..', '.#..#.....', '..#....#.#', '#..#....#.', '.##.#..###', '##...#..#.', '.#....####'];

// Example 3, expected 1,2 with 35
input = ['#.#...#.#.', '.###....#.', '.#....#...', '##.#.#.#.#', '....#.#.#.', '.##..###.#', '..#...##..', '..##....##', '......#...', '.####.###.'];

// Example 4, expected 6,3 with 41
input = ['.#..#..###', '####.###.#', '....###.#.', '..###.##.#', '##.##.#.#.', '....###..#', '..#.#..#.#', '#..#.#.###', '.##...##.#', '.....#.#..'];

// Example 5, expected 11,13 with 210
input = ['.#..##.###...#######', '##.############..##.', '.#.######.########.#', '.###.#######.####.#.', '#####.##.#.##.###.##', '..#####..#.#########', '####################', '#.####....###.#.#.##', '##.#################', '#####.##.###..####..', '..######..##.#######', '####.##.####...##..#', '.#####..#.######.###', '##...#.##########...', '#.##########.#######', '.####.#.###.###.#.##', '....##.##.###..#####', '.#.#.###########.###', '#.#.#.#####.####.###', '###.##.####.##.#..##'];

// Actual input
input = ['#....#.....#...#.#.....#.#..#....#', '#..#..##...#......#.....#..###.#.#', '#......#.#.#.....##....#.#.....#..', '..#.#...#.......#.##..#...........', '.##..#...##......##.#.#...........', '.....#.#..##...#..##.....#...#.##.', '....#.##.##.#....###.#........####', '..#....#..####........##.........#', '..#...#......#.#..#..#.#.##......#', '.............#.#....##.......#...#', '.#.#..##.#.#.#.#.......#.....#....', '.....##.###..#.....#.#..###.....##', '.....#...#.#.#......#.#....##.....', '##.#.....#...#....#...#..#....#.#.', '..#.............###.#.##....#.#...', '..##.#.........#.##.####.........#', '##.#...###....#..#...###..##..#..#', '.........#.#.....#........#.......', '#.......#..#.#.#..##.....#.#.....#', '..#....#....#.#.##......#..#.###..', '......##.##.##...#...##.#...###...', '.#.....#...#........#....#.###....', '.#.#.#..#............#..........#.', '..##.....#....#....##..#.#.......#', '..##.....#.#......................', '.#..#...#....#.#.....#.........#..', '........#.............#.#.........', '#...#.#......#.##....#...#.#.#...#', '.#.....#.#.....#.....#.#.##......#', '..##....#.....#.....#....#.##..#..', '#..###.#.#....#......#...#........', '..#......#..#....##...#.#.#...#..#', '.#.##.#.#.....#..#..#........##...', '....#...##.##.##......#..#..##....'];

// Determine each Asteroid
let aAsteroids = [];
let iAsteroidCount = 0;
for (let y = 0; y < input.length; y++) {
    for (let x = 0; x < input[y].length; x++) {
        if (input[y][x] == '#') {
            aAsteroids[name_asteroid(iAsteroidCount++)] = x + ',' + y;
        }
    }
}
// console.log(aAsteroids);

// For each asteroid, calculate and count unique angles to all other asteroids
let aAsteroidHitCount = [];
let iAsteroidMaxHit = 0;
let sAsteroidMaxHit = '';
for (sAsteroid in aAsteroids) {
    aAsteroidHitCount[sAsteroid] = new Set();
    for (sAsteroid2 in aAsteroids) {
        if (sAsteroid == sAsteroid2) { continue; }

        // Calculate the angle
        let iX1 = aAsteroids[sAsteroid].split(',')[0];
        let iY1 = aAsteroids[sAsteroid].split(',')[1];
        let iX2 = aAsteroids[sAsteroid2].split(',')[0];
        let iY2 = aAsteroids[sAsteroid2].split(',')[1];
        let iDiffX = iX2 - iX1;
        let iDiffY = iY2 - iY1;

        // Calculate angle, with offset for directiob
        let angle = iDiffX / iDiffY;
        if (isNaN(angle)) { angle = 0; }
        angle += ',' + (iDiffX < 0) + ',' + (iDiffY < 0);
        aAsteroidHitCount[sAsteroid].add(angle);
    }

    // console.log(sAsteroid + ': ' + aAsteroidHitCount[sAsteroid].size);

    if (aAsteroidHitCount[sAsteroid].size > iAsteroidMaxHit) {
        iAsteroidMaxHit = aAsteroidHitCount[sAsteroid].size;
        sAsteroidMaxHit = sAsteroid;
    }
}
// console.log(aAsteroidHitCount['I']);
console.log('Max: Asteroid ' + sAsteroidMaxHit + ' at ' + aAsteroids[sAsteroidMaxHit] + ' with ' + iAsteroidMaxHit);