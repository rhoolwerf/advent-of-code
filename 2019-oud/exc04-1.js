let input = '134792-675810';

let start = input.split('-')[0];
let end = input.split('-')[1];

start *= 1000;
end *= 1000;

function validate_number(number) {
    let at_least_one_double = false;
    let only_increasing_digits = true;

    for (let i = 0; i < number.length - 1; i++) {
        if (number.charAt(i) > number.charAt(i + 1)) {
            only_increasing_digits = false;
            return false; // If we're dissatisfied, no need to continue
        }
        if (number.charAt(i) == number.charAt(i + 1)) {
            at_least_one_double = true;
        }
    }

    return (at_least_one_double && only_increasing_digits);
}

// let possible_codes = new Set();
let count = 0;
for (let i = start; i <= end; i++) {
    if (validate_number(String(i))) {
        // possible_codes.add(i);
        count++;
    }
}
// console.log(possible_codes.size);
console.log(count);